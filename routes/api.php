<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['namespace' => 'Modules'], function () {
    Route::get('/series', 'APIWebsiteController@seriesAPI')->name('api.series');
    Route::get('/channel-videos', 'APIWebsiteController@channelVideos')->name('api.channel');
    Route::get('/channel-latest-video/{param}', 'APIWebsiteController@getDataVideoChannel')->name('api.getDataVideoChannel');
    Route::get('/channel-latest-series/{param}', 'APIWebsiteController@getDataSeriesChannel')->name('api.getDataSeriesChannel');
});
