@if(auth()->user()->roles->first()->name == 'admin')
<div class='btn-block'>
    <a href="{{ route('youtube.analytics.detail', [$yta->user_id, $yta->year.'-'.$yta->month]) }}" data-url="{{ route('youtube.analytics.detail', [$yta->user_id, $yta->year.'-'.$yta->month]) }}" class='btn btn-primary btn-block' title="Edit Master Singer">
        <i class="fa fa-eye"></i> Detail Analytics
    </a>
    {{-- <a href="{{ route('youtube.analytics.detail', [$yta->user_id, $yta->year.'-'.$yta->month]) }}" data-url="{{ route('youtube.analytics.detail', [$yta->user_id, $yta->year.'-'.$yta->month]) }}" class='btn btn-primary btn-xs' title="Edit Master Singer">
        <i class="fa fa-pencil"></i> Edit
    </a> --}}
    {{-- <a href="{{ route('mastersinger.destroy', $yta->year.'-'.$yta->month) }}" class='btn btn-danger btn-xs' title="Delete Master Singer" onclick="return confirm('Are you sure to delete?')" style="margin-left:5px;">
        <i class="fa fa-trash"></i> Delete
    </a> --}}
</div>
@elseif(auth()->user()->roles->first()->name == 'legal')
<div class='btn-block'>
    <a href="{{ route('legal.youtube.analytics.detail', [$yta->user_id, $yta->year.'-'.$yta->month]) }}" data-url="{{ route('youtube.analytics.detail', [$yta->user_id, $yta->year.'-'.$yta->month]) }}" class='btn btn-primary btn-block' title="Edit Master Singer">
        <i class="fa fa-eye"></i> Detail Analytics
    </a>
    {{-- <a href="{{ route('legal.youtube.analytics.detail', [$yta->user_id, $yta->year.'-'.$yta->month]) }}" data-url="{{ route('legal.youtube.analytics.detail', [$yta->user_id, $yta->year.'-'.$yta->month]) }}" class='btn btn-primary btn-xs' title="Edit Master Singer">
        <i class="fa fa-pencil"></i> Edit
    </a> --}}
    {{-- <a href="{{ route('legal.mastersinger.destroy', $id) }}" class='btn btn-danger btn-xs' title="Delete Master Singer" onclick="return confirm('Are you sure to delete?')" style="margin-left:5px;">
        <i class="fa fa-trash"></i> Delete
    </a> --}}
</div>
@elseif(auth()->user()->roles->first()->name == 'anr')
<div class='btn-block'>
    <a href="{{ route('anr.youtube.analytics.detail', [$yta->user_id, $yta->year.'-'.$yta->month]) }}" data-url="{{ route('youtube.analytics.detail', [$yta->user_id, $yta->year.'-'.$yta->month]) }}" class='btn btn-primary btn-block' title="Edit Master Singer">
        <i class="fa fa-eye"></i> Detail Analytics
    </a>
    {{-- <a href="{{ route('anr.youtube.analytics.detail', [$yta->user_id, $yta->year.'-'.$yta->month]) }}" data-url="{{ route('anr.youtube.analytics.detail', [$yta->user_id, $yta->year.'-'.$yta->month]) }}" class='btn btn-primary btn-xs' title="Edit Master Singer">
        <i class="fa fa-pencil"></i> Edit
    </a> --}}
    {{-- <a href="{{ route('legal.mastersinger.destroy', $id) }}" class='btn btn-danger btn-xs' title="Delete Master Singer" onclick="return confirm('Are you sure to delete?')" style="margin-left:5px;">
        <i class="fa fa-trash"></i> Delete
    </a> --}}
</div>
@endif
