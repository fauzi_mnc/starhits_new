<html>
<head>
<style>
body{
    margin: 10px;
}
table{
    margin: 20px 0px;
}
table, th, td{
    border-collapse: collapse;
}
.table-bordered table, .table-bordered th, .table-bordered td {
    border: 2px solid black;
}
th, td {
    padding: 5px;
    /*text-align: left;
    text-align: center;*/
}
.signature{
    padding: 40px 50px;
}
.silver{
    background-color: silver;
}
</style>
</head>
<body onload="print();">
    <?php
    $dates = $dsp->year."-".$dsp->month."-01";
    ?>
<table class="table-bordered" style="width:100%" border="1">
    <thead style="font-size: 16px;">
        <tr class="silver">
            <th>No</th>
            <th>Reporting Month</th>
            <th>Sales Month</th>
            <th>Platform</th>
            <th>Country</th>
            <th>Label Name</th>
            <th>Artist Name</th>
            <th>Release Title</th>
            <th>Track Title</th>
            <!-- <th>UPC</th>
            <th>ISRC</th>
            <th>Release Catalog NB</th>
            <th>Release Type</th>
            <th>Sales Type</th>
            <th>Quantity</th>
            <th>Client Pay</th>
            <th>Unit Price</th>
            <th>Mechanic</th>
            <th>Gross Revenue</th>
            <th>Client Share</th>
            <th>Net Revenue EUR</th> -->
            <th>Net Revenue IDR</th>
            <th>Pendapatan Pencipta</th>
            <th>Pendapatan Penyanyi</th>    
        </tr>
    </thead>
    <tbody style="font-size: 14px;">
        <?php
        $total_net_revenue_idr=0;
        $i=1;
        ?>
        @foreach($dsp->detail as $row)
            <?php
                $total_net_revenue_idr += $row->pivot->net_revenue_idr;
            ?>
            <tr>
                <td align="center"><?=$i?></td>
                <td align="center">{{ date('d-m-Y', strtotime($dsp->reporting_month)) }}</td>
                <td align="center">{{ date('d-m-Y', strtotime($row->pivot->sales_month)) }}</td>
                <td align="center">{{$row->pivot->platform}}</td>
                <td align="center">{{$row->pivot->country}}</td>
                <td align="center">{{$row->pivot->label_name}}</td>
                <td align="center">{{$row->pivot->artist_name}}</td>
                <td align="center">{{$row->pivot->release_title}}</td>
                <td align="center">{{$row->pivot->track_title}}</td>
                <!-- <td align="center">{{$row->pivot->upc}}</td>
                <td align="center">{{$row->pivot->isrc}}</td>
                <td align="center">{{$row->pivot->release_catalog_nb}}</td>
                <td align="center">{{$row->pivot->release_type}}</td>
                <td align="center">{{$row->pivot->sales_type}}</td>
                <td align="center">{{$row->pivot->quantity}}</td>
                <td align="right">{{$row->pivot->client_pay}}</td>
                <td align="right">{{$row->pivot->unit_price}}</td>
                <td align="right">{{$row->pivot->mechanic}}</td>
                <td align="right">{{$row->pivot->gross_revenue}}</td>
                <td align="right">{{$row->pivot->client_share}}</td>
                <td align="right">{{$row->pivot->net_revenue_eur}}</td> -->
                <td align="right">{{$row->pivot->net_revenue_idr}}</td>
                <td align="right">{{(((int)$row->pivot->net_revenue_idr/100)*9)}}</td>
                <td align="right">{{(((int)$row->pivot->net_revenue_idr/100)*6)}}</td>
            </tr>
        <?php $i++; ?>
        @endforeach
    </tbody>
    <tfoot>
        <tr >
            <td class="silver" colspan="11" align="center">
                <b>Total Net Revenue IDR :</b> 
            </td>
            <td align="right">
                <b>Rp {{number_format((float)$total_net_revenue_idr, 0,",",".")}}</b> 
            </td>
        </tr>
    </tfoot>
    
</table>
</body>
</html> 