<div class='btn-group'>
    <a href="{{ route('dsp.view', $id) }}" class='btn btn-primary btn-md' title="Show Revenue">
        <i class="fa fa-eye"></i> View
    </a>
    <!-- <a href="{{ route('dsp.download.report_pdf', $id) }}" class='btn btn-default btn-xs' target="_blank" title="Print">
        <i class="fa fa-print"></i>
    </a>
    <a href="{{ route('dsp.download', $id) }}" class='btn btn-default btn-xs' target="_blank" title="Download Revenue PDF">
        <i class="fa fa-file-pdf-o"></i>
    </a>
    <a href="{{ route('dsp.excel', $id) }}" class='btn btn-default btn-xs' title="Download Revenue Excel">
        <i class="fa fa-file-excel-o"></i>
    </a> -->
</div>
