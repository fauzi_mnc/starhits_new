<html>
<head>
<style>
body{
    margin: 10px;
}
table{
    width: 100%;;
    margin: 20px 0px;
}
table, th, td{
    border-collapse: collapse;
}
thead tr th{
    height: 50px;
}
.table-bordered table, .table-bordered th, .table-bordered td {
    border: 2px solid black;
}
th, td {
    padding: 5px;
    /*text-align: left;
    text-align: center;*/
}
.signature{
    padding: 70px 50px;
}
.silver{
    background-color: silver;
}
.number{
    text-align:right;
}
</style>
</head>
<body onload="print();">
    <!-- <div style="float: right;margin-bottom: 20px;">
         {{ date('d F Y', strtotime($dsp->reporting_month)) }}                   
    </div> -->
    <div style="width:100%; text-align:center;">
        <img style="width:350px;" src="{{ asset('img/hitsrecords.png') }}">
        <h2 style="margin:5px;">PT. Suara Mas Abadi</h2>
        <p>Data DSP Report</p>
        <p>Periode {{ date('F Y',strtotime($dsp->reporting_month)) }} </p>
    </div>
<div class="row">
    <div class="col-lg-12">
        <section>
            <div class="col-sm-12 ">
                <table class="table table-bordered" id="">
                    <thead>
                        <tr class="silver">                                
                            <th>Track Title</th>
                            <th>Release Title</th>
                            <th>Singer</th>
                            <th>Revenue Penyanyi (Rp)</th>
                            <th>Revenue Pencipta (Rp)</th>
                            <th>Net Revenue (Rp)</th>
                        </tr>
                    </thead>
                    <tbody>
                            @php 
                                $i = 1;
                                $no =0;
                                $totSing =0;
                                $totSong =0;
                                $totRev = 0;
                                $totRev2 = 0;
                            @endphp
                            @foreach($all_dsp as $rows)
                                @php
                                    $net_revenue = App\Model\DspDetails::where(['isrc'=>$rows->isrc,'dsp_id'=>$dsp->id])
                                    ->sum('net_revenue_idr');

                                    $totRev2 = $totRev2+$net_revenue;
                                @endphp
                                <tr>
                                    <td>{{$rows->track_title}}</td>
                                    <td>{{$rows->release_title}}</td>
                                    <td>-</td>
                                    <td class="number">0</td>
                                    <td class="number">0</td>
                                    <td class="number">{{number_format((float)round($net_revenue,2),0)}}</td>
                                </tr>
                            @endforeach

                            @foreach($masterlagu as $key => $row)
                                @php
                                    $net_revenue_idr = App\Model\DspDetails::where(['isrc'=>$row->isrc,'dsp_id'=>$dsp->id])
                                    ->sum('net_revenue_idr');

                                    $sing = ($net_revenue_idr*$row->percentage_penyanyi);
                                    $song = ($net_revenue_idr*$row->percentage_pencipta);

                                    $totSing = $totSing+$sing;
                                    $totSong = $totSong+$song;
                                    $totRev = $totRev+$net_revenue_idr;
                                @endphp
                                <tr>
                                    <td>{{$row->track_title}}</td>
                                    <td>{{$row->release_title}}</td>
                                    <td>{{$masterlagu[$key]->rolelagu[0]->penyanyi->name_master}} </td>
                                    <td class="number">{{number_format((float)round($sing,2),0)}}</td>
                                    <td class="number">{{number_format((float)round($song,2),0)}}</td>
                                    <td class="number">{{number_format((float)round($net_revenue_idr,2),0)}}</td>
                                </tr>
                                @php $no ++; @endphp
                            @endforeach

                            @php
                               $totRev = $totRev+ $totRev2;
                            @endphp
                            <tr style="font-weight: bold; background-color:#dadfe1;">
                                <td colspan="3" >Total Revenue</td>
                                <td class="number">{{number_format((float)round($totSing,2),0)}}</td>
                                <td class="number">{{number_format((float)round($totSong,2),0)}}</td>
                                <td class="number">{{number_format((float)round($totRev,2),0)}}</td>
                            </tr>   
                    </tbody>
                </table>
                * <i>Print Date : {{ date('d F Y',strtotime(date("Y-m-d"))) }} </i>

                <!-- <table style="width: 100%; font-size: 18px; text-align: center;" border="2">
                    @php
                        $count = count($checker);
                    @endphp
                    <tr><?php for($x=1; $x<=$count; $x++){ ?>
                        <td></td>
                        <?php } ?>
                        <td>Approved By</td>
                    </tr>
                    <tr>
                        <?php for($x=1; $x<=$count; $x++){ ?>
                        <td class="signature"></td>
                        <?php } ?>
                        <td class="signature"></td>    
                    </tr>
                    <tr>
                        @foreach($checker as $user)
                            <td>{{ $user->name }}</td>
                        @endforeach
                        <td>{{ $approver->name }}</td>
                    </tr>                    
                </table> -->

                <table style="width: 100%; font-size: 18px; text-align: center;" border="2">
                    <tr>
                        <td >Prepared By</td>
                        <td colspan="4">Reviewed By</td>
                        <td colspan="2">Approved By</td>
                    </tr>
                    <tr>
                        <td class="signature"></td>
                        <td class="signature"></td>
                        <td class="signature"></td>
                        <td class="signature"></td>
                        <td class="signature"></td>
                        <td class="signature"></td>
                        <td class="signature"></td>
                    </tr>
                    <tr style="font-weight: bold;font-size: 14px;">
                        <td>Admin</td>
                        <td>Head Of Content</td>
                        <td>A&R</td>
                        <td>Legal</td>
                        <td>Finance</td>
                        <td>CFO</td>
                        <td>Managing Director</td>
                    </tr>
                </table>
                <br><br>
            </div>
        
        </section>
    </div>
</div>
</body>
</html>