<?php
header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=template_revenue.xls");  
header("Pragma: no-cache"); 
header("Expires: 0");
?>
<html>
<head>
<style>
.table-bordered table, .table-bordered th, .table-bordered td {
    border: 1px solid #000000;
}
th, td {
    padding: 5px;
    text-align: left;
    
}
.signature{
    padding: 40px 50px;
}
th{
    background-color: #f0f0f0;
    font-size: 14px;
}
</style>
</head>
<body>
<table class="table-bordered" style="width:50%" border="1">
    <thead>
        <tr>
            <th>User ID</th>
            <th>Channel</th>
            <th>Estimated Gross Revenue </th>
        </tr>
    </thead>
    <tbody>
        <?php $i = 0; ?>
        @foreach($creators as $creator)
        <tr>
            <td>{{$creator->user_id}}</td>
            <td>{{$creator->name}}</td>
            <td></td>
        </tr>
        @endforeach
    </tbody>
</table>
</body>
</html>