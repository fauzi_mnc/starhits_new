@extends('layouts.crud')

@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>DSP Report</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">DSP Report</a>
            </li>
            <li class="active">
                <strong>View</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2"></div>
</div>
@endsection

@section('css')
@include('layouts.datatables_css')
@endsection 

@section('contentCrud')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@include('flash::message')
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                DSP Report
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <div class="col-sm-12">
                        @if(auth()->user()->hasRole('admin'))
                            <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{{route('dsp.create')}}">Add New</a>
                        @endif
                        <table class="table table-bordered" id="table-revenue-creator">
                            <thead>
                                <tr>
                                    <th>Report Month</th>
                                    @if(auth()->user()->hasRole('admin') || auth()->user()->hasRole('finance'))
                                        <th>Revenue</th>
                                    @endif
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 1; ?>
                                @foreach($datas as $data)

                                    @if(auth()->user()->roles->first()->name == 'singer' || auth()->user()->roles->first()->name == 'songwriter' )

                                        @php
                                        $isrc =  App\Model\MasterLagu::with('rolelagu','rolelagu.penyanyi','rolelagu.pencipta')
                                                    ->whereIn('isrc', function($query) use($data) {
                                                        $query->select('isrc')
                                                        ->from(with(new App\Model\DspDetails)->getTable())
                                                        ->where(['dsp_id'=> $data->id]);
                                                    })->whereHas('rolelagu', function($q){
                                                        $q->where('role_lagu.id_user', auth()->user()->id);
                                                    })->get()->pluck('isrc')->toArray();

                                        $revenue_idr = App\Model\DspDetails::where('dsp_id',$data->id)
                                                    ->whereIn('isrc', $isrc)
                                                    ->sum('net_revenue_idr');
                                        @endphp

                                    @else
                                        @php
                                            $revenue_idr = App\Model\DspDetails::where(['dsp_id'=>$data->id])
                                            ->sum('net_revenue_idr');
                                        @endphp
                                    @endif
                                    <tr>
                                        <td data-sort='{{strtotime($data->reporting_month)}}'>{{ date('d F Y', strtotime($data->reporting_month)) }}</td>
                                        
                                        @if(auth()->user()->hasRole('admin') || auth()->user()->hasRole('finance'))
                                            <td>Rp. {{number_format((float)round($revenue_idr,0),0)}},-</td>
                                        @endif

                                        <td>
                                            @if($data->status==0)
                                                <b>Draft</b>
                                            @elseif($data->status==1)
                                                <b>Checked</b>
                                            @elseif($data->status==2)
                                                <b>Revisi</b>
                                            @elseif($data->status==3)
                                                <b>Finish</b>
                                            @else 
                                                <b>Draft</b>
                                            @endif
                                        </td>
                                        <td>
                                            <a href="{{url('')}}/{{ auth()->user()->roles()->where('name', request()->segment(1))->first()->name }}/dsp/{{$data->id}}/view/" class='btn btn-success btn-md' title="Show Revenue">
                                                <i class="fa fa-eye"></i> View
                                            </a>

                                            @if(auth()->user()->hasRole('admin') || auth()->user()->hasRole('finance') || auth()->user()->hasRole('anr'))
                                                @if($data->status==0 )
                                                    <a href="
                                                    {{url('')}}/{{ auth()->user()->roles()->where('name', request()->segment(1))->first()->name }}/dsp/checked/{{$data->id}}" class='btn btn-info btn-md' title="Status Checked">
                                                        <i class="fa fa-check"></i> Checked
                                                    </a>
                                                    <!-- <a href="{{ route('dsp.revisi', $data->id) }}" class='btn btn-danger btn-md' title="Status Revisi">
                                                        <i class="fa fa-pencil"></i> Revisi
                                                    </a> -->
                                                @endif
                                            @endif

                                            @if(auth()->user()->hasRole('admin') )
                                                @if($data->status==1)
                                                    <a href="{{url('')}}/{{ auth()->user()->roles()->where('name', request()->segment(1))->first()->name }}/dsp/finish/{{$data->id}}" class='btn btn-primary btn-md' title="Status Finish">
                                                        <i class="fa fa-flag"></i> Finish
                                                    </a>
                                                    <!-- <a href="{{ route('dsp.revisi', $data->id) }}" class='btn btn-danger btn-md' title="Status Revisi">
                                                        <i class="fa fa-pencil"></i> Un Finish
                                                    </a> -->
                                                @endif
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>        
    </div>
</div>

    @section('scripts')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/js/plugins/piexif.min.js" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/js/plugins/sortable.min.js" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/js/plugins/purify.min.js" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/js/fileinput.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/themes/fa/theme.js"></script>

        <script src="{{ asset('js/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
        <script src="{{ asset('js/plugins/bootstrap-select.min.js') }}"></script>
        @include('layouts.datatables_js')
        <script>    
            /*$(document).ready(function(){
                $('#table-revenue-creator').DataTable();
                $("div").removeClass("ui-toolbar");
            });*/
        </script>

        <script>    
            $(document).ready(function(){
                $('#table-revenue-creator').DataTable({
                    dom             : 'rltip',
                    order           : [[ 0, "desc"]],
                    processing      : true,
                    serverMethod    : 'post',
                    responsive      : true,
                    autoWidth       : false,
                    aLengthMenu     : [[10, 50, 100, 250, -1], [10, 50, 100, 250, 'All']],
                    columnDefs      : [{ "width": "30%", "targets": 1 }],
                });
                $("div").removeClass("ui-toolbar");
            });
        </script>
    @endsection


@endsection