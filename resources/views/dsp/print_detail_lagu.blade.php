<html>
<head>

<style>
body{
    margin: 10px;
}
table{
    width: 100%;;
    margin: 20px 0px;
}
table, th, td{
    border-collapse: collapse;
}
thead tr th{
    height: 50px;
}
.table-bordered table, .table-bordered th, .table-bordered td {
    border: 2px solid black;
}
.noborder table, .noborder th, .noborder td {
    border: none;
}
th, td {
    padding: 5px;
    /*text-align: left;
    text-align: center;*/
}
.signature{
    padding: 40px 50px;
}
.silver{
    background-color: silver;
}
 .number{
    text-align:right;
}

.font-12{
    font-size: 12px;
}
.font-14{
    font-size: 12px;
}
</style>
</head>
<body onload="print();">
    <div style="width:100%; text-align:center;">
        <img style="width:300px;" src="{{ asset('img/hitsrecords.png') }}">
        <h2 >PT. Suara Mas Abadi</h2>
        @php 
            $reporting_month = App\Model\Dsp::where(['id' => $getdetails->dsp_id])->first();
        @endphp
        <p>Periode {{ date('F Y',strtotime($reporting_month->reporting_month)) }} </p>
    </div>

    <table class="table borderless">
        <tr>
            <td width="10%">Penyanyi</td>
            <td> :
                @php $countSing = 0;
                    $retSing = "";
                @endphp             
                @foreach($masterlagu->rolelagu as $key => $singer)
                    @if(!empty($singer->penyanyi))
                        <?php 
                            $retSing .= $singer->penyanyi->name_master." & ";
                        ?>
                    @endif
                @endforeach
                <?php 
                    echo rtrim($retSing," & ");
                ?>
            
            </td>
        </tr>
        <tr>
            <td>Pencipta</td>
            <td>: 
                @php $countSong = 0;
                    $ret="";
                @endphp     
                @foreach($masterlagu->rolelagu as $keys => $song)
                    @if(!empty($song->pencipta))
                    <?php 
                        $ret .= $song->pencipta->name_master." & ";
                    ?>
                    @endif              
                
                @endforeach
                    <?php 
                        echo rtrim($ret," & ");
                    ?>
                
            </td>
        </tr>
        <tr>
            <td>Judul Lagu</td>
            <td>: {{$masterlagu->track_title}}</td>                    
        </tr>
        <tr>
            <td>Album</td>
            <td>: {{$masterlagu->release_title}}</td>
        </tr>
        <tr>
            <td>Label</td>
            <td>: {{$masterlagu->label_name}}</td>
        </tr>
    </table>

    <table class="table table-bordered" id="table-dsp-detail">
        <thead>
            <tr >                                
                <th>Platform</th>     
                @if(auth()->user()->hasRole('admin') || auth()->user()->hasRole('finance')|| auth()->user()->hasRole('anr')|| auth()->user()->hasRole('legal') || auth()->user()->hasRole('singer'))                           
                    <th>Revenue Penyanyi (Rp)</th>
                @endif

                @if(auth()->user()->hasRole('admin') || auth()->user()->hasRole('finance')||auth()->user()->hasRole('anr')|| auth()->user()->hasRole('legal')|| auth()->user()->hasRole('songwriter'))
                    <th>Revenue Pencipta (Rp)</th>
                @endif

                @if(auth()->user()->hasRole('admin')|| auth()->user()->hasRole('anr')|| auth()->user()->hasRole('legal') || auth()->user()->hasRole('finance'))
                    <th>Revenue (Rp)</th>
                @endif
            </tr>
        </thead>
        <tbody>
            @php 
                $no =0;
                $totSing =0;
                $totSong =0;
                $totRev = 0;
            @endphp
            @foreach($dsp_detail as $key => $row)
                   @php
                    $net_revenue_idr = App\Model\DspDetails::where([
                                    'isrc'=>$row->isrc,'dsp_id'=>$row->dsp_id,
                                    'platform'=>$row->platform])
                                    ->sum('net_revenue_idr');

                    $revenue = round($net_revenue_idr,0);
                    $sing = ($revenue*$masterlagu->percentage_penyanyi);
                    $song = ($revenue*$masterlagu->percentage_pencipta);

                    $totRev = $totRev+$revenue;
                    $totSing = $totSing+$sing;
                    $totSong = $totSong+$song;

                @endphp
                <tr>
                    <td>{{$row->platform}}</td>   
                     @if(auth()->user()->hasRole('admin') || auth()->user()->hasRole('finance') || auth()->user()->hasRole('anr')|| auth()->user()->hasRole('legal') || auth()->user()->hasRole('singer'))                                 
                    <td class="number">{{number_format((float)$sing,0)}}</td>
                    @endif

                     @if(auth()->user()->hasRole('admin') || auth()->user()->hasRole('finance')|| auth()->user()->hasRole('anr')|| auth()->user()->hasRole('legal') || auth()->user()->hasRole('songwriter'))
                    <td class="number">{{number_format((float)$song,0)}}</td>
                    @endif

                     @if(auth()->user()->hasRole('admin') || auth()->user()->hasRole('anr')|| auth()->user()->hasRole('legal') || auth()->user()->hasRole('finance'))
                    <td class="number">{{number_format((float)$revenue,0)}}</td>
                    @endif

                </tr>
                @php $no ++; @endphp
            @endforeach
        </tbody>


        <tfoot >
            <tr style="font-weight: bold;">
                <td >Total Revenue</td>
                
                 @if(auth()->user()->hasRole('admin') || auth()->user()->hasRole('finance')|| auth()->user()->hasRole('anr')|| auth()->user()->hasRole('legal') || auth()->user()->hasRole('singer'))
                <td class="number">{{number_format((float)round($totSing,0),0)}}</td>
                @endif

                 @if(auth()->user()->hasRole('admin') || auth()->user()->hasRole('finance')|| auth()->user()->hasRole('anr')|| auth()->user()->hasRole('legal') || auth()->user()->hasRole('songwriter'))
                <td class="number">{{number_format((float)round($totSong,0),0)}}</td>
                @endif

                 @if(auth()->user()->hasRole('admin') || auth()->user()->hasRole('anr')|| auth()->user()->hasRole('legal') || auth()->user()->hasRole('finance'))
                <td class="number">{{number_format((float)round($totRev,0),0)}}</td>
                @endif
            </tr>
        </tfoot>
    </table>

    <section >
        <div class="form-group">
            <div class="col-sm-12 table-responsive">
                
                
                <table style="width: 100%; font-size: 15px; text-align: center;" border="1">
                    <tr>
                        <td>Prepared By</td>
                        <td>Reviewed By</td>
                        @if($net_revenue <= 5000000)
                            <td colspan="2">Approved By</td>
                        @else
                            <td colspan="3">Approved By</td>
                        @endif
                    </tr>
                    <tr>
                        <td class="signature" width="150"><i style="font-size: 11px;">Print Date : {{ date('d F Y',strtotime(date("Y-m-d"))) }} </i></td>
                        <td class="signature"></td>
                        <td class="signature"></td>
                        @if($net_revenue > 5000000)
                            <td class="signature"></td>
                        @endif
                    </tr>
                    <tr style="font-weight: bold;font-size: 14px;">
                        <td width="25%"><i style="font-size: 11px;">Print by System Starhits</i></td>
                        <td>Head Of Finance</td>
                        <td>CFO</td>
                        @if($net_revenue > 5000000)
                            <td>Director</td>
                        @endif
                    </tr>
                </table>

            </div>
        </div>
    </section>
</body>
</html>