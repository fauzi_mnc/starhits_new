@section('css')
<link href="{{ asset('css/jquery-step.css') }}" rel="stylesheet">
<link href="{{ asset('js/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css') }}" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
<link href="{{ asset('css/plugins/bootstrap-select.min.css') }}" rel="stylesheet">
<style type="text/css">
    #btn-save{
        margin-top: 100px;
    }
</style>
@include('layouts.datatables_css')
@endsection
<div>
    @if($formType == 'create')
    <h3>Type Selection</h3>
    <section>
        <div class="form-group" style="margin-right: 20px;">
            <a href="{{route('dsp.template')}}" class="btn btn-primary pull-right" id="btn-template">Download Template</a>
        </div>
        <div class="form-group">
            {!! Form::label('reporting_month', 'Date:', ['class' => 'reporting_month col-sm-2 control-label']) !!}
            <div class="col-sm-4">
                {!! Form::text('reporting_month', null, ['class' => 'reporting_month form-control datepicker ']) !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('type', 'Input Dsp:', ['class' => 'col-sm-2 control-label']) !!}
            <div class="col-sm-4">
                {!! Form::select('type', ['' => 'Select', 1 => 'Upload Excel or CSV'], null, ['class' => 'form-control ']) !!}
            </div>
        </div>
        <div class="form-group " id="type-content" style="display:none">
            {!! Form::label('file', '', ['class' => 'col-sm-2 control-label']) !!}
            <div class="col-md-8 col-sm-8" >
                 <input id="input-id" type="file" name="file" data-preview-file-type="text" >
            </div>
        </div>
    </section>
    @endif
    <div class="form-group" style="margin-right: 20px;">
        <button type="submit" class="btn btn-primary pull-right" id="btn-save">
            Save And Submit
        </button> 
    </div>

</div>
@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/js/plugins/piexif.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/js/plugins/sortable.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/js/plugins/purify.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/js/fileinput.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/themes/fa/theme.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-steps/1.1.0/jquery.steps.js"></script>
<script src="{{ asset('js/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-select.min.js') }}"></script>
@include('layouts.datatables_js')
<script>
    function readCsv(){
    
    }

    $(document).ready(function(){
        $('.datepicker').datepicker({
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true,
            //viewMode: 'months',
            // minViewMode: 'months',
            format: 'dd-mm-yyyy'
        });

        $('.monthpicker').datepicker({
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true,
            viewMode: 'months',
            minViewMode: 'months',
            format: 'mm'
        });
    
        $('.yearpicker').datepicker({
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true,
            viewMode: 'years',
            minViewMode: 'years',
            format: 'yyyy'
        });
        
        $('#type').change(function(){
            if($(this).val() == 1){
                $('#type-content').show();
            } else {
                $('#type-content').hide();
            }
        });
    
        $("#input-id").fileinput({
            uploadUrl: "#",
            showUpload: false,
            allowedFileExtensions: ["csv", 'xlsx'],
            fileActionSettings: {
                showUpload: false,
            }
        });
    
        $('#input-id').change(function(){
            if($(this).get(0).files.length === 0){
                $('#btn-process-csv').attr('disabled', true); 
            }else{
                $('#btn-process-csv').attr('disabled', false);
            }
        });
    
        $('#btn-process-csv').click(function(){
            readCsv();
        });
    
     
    });
    
    $(".actions").on("click","#btn-save",function(){
        $('#status').val(3);
        form.submit();
    });
    
    $(".actions").on("click","#btn-update",function(){
        $('#status').val(3);
        form.submit();
    });
    
    $('#btn-dsp').click(function(){
    form.validate().settings.ignore = ":disabled";
    
    if(form.valid()){
        table.row.add( [ index, $('#detail_id option:selected').html(), $('#estimate_gross_dsp').val(),'<button type="button" data-id="'+$('#detail_id').val()+'" class="btn btn-default edit">Edit</button>'] )
            .draw()
            .node();

        $('#content-dsp').append('<input type="hidden" name="dsp_detail[]" value="'+[$('#detail_id').val(), $('#estimate_gross_dsp').val()]+'">');
        $('.rm-detail').find('[value='+$('#detail_id').val()+']').remove();
        $('.rm-detail').selectpicker('refresh');
        $('#estimate_gross_dsp').val('');
        index++;
    }
    
    });
    

</script>
@endsection