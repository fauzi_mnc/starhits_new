<form action="{{ route('dsp.read') }}" method="post" enctype="multipart/form-data">
    Select image to upload:
    <input type="file" name="file" id="file">
    {{ csrf_field() }}
    <input type="submit" value="Upload Image" name="submit">
</form>