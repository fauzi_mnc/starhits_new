@extends('layouts.crud')

@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><i class="fa fa-file-audio-o"></i> DSP Reports</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">DSP Reports</a>
            </li>
            <li class="active">
                <strong>Table</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
@endsection
@section('contentCrud')
@include('flash::message')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>DSP Report</h5>
                    {{-- <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{{route('dsp.create')}}">Add New</a> --}}
                </div>
                <div class="ibox-content">
                    @include('dsp.table')
                </div>
            </div>
        </div>
    </div>
@endsection


