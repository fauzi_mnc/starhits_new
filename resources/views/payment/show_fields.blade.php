
@section('css')
<link href="{{ asset('js/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css') }}" rel="stylesheet">
<link href="{{ asset('css/fileinput.min.css') }}" rel="stylesheet">

<style>
.kv-photo .krajee-default.file-preview-frame,.kv-photo .krajee-default.file-preview-frame:hover {
    margin: 0;
    padding: 0;
    border: none;
    box-shadow: none;
    text-align: center;
}
.kv-photo .file-input {
    display: table-cell;
    max-width: 220px;
}
.kv-reqd {
    color: red;
    font-family: monospace;
    font-weight: normal;
}
</style>
@include('layouts.datatables_css')
@endsection

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('bank-name', 'Bank Name:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::text('bank_name', null, ['class' => 'form-control', 'disabled' => true]) !!}
    </div>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('bank-acc', 'Bank Account Number:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::text('bank_acc_number', null, ['class' => 'form-control', 'disabled' => true]) !!}
    </div>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('bank-holder', 'Bank Holder Name:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::text('holder_name', null, ['class' => 'form-control', 'disabled' => true]) !!}
    </div>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('bank-location', 'Bank Location:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::text('bank_location', null, ['class' => 'form-control', 'disabled' => true]) !!}
    </div>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('npwp', 'NPWP:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::text('npwp', null, ['class' => 'form-control', 'disabled' => true]) !!}
    </div>
</div>

<!-- Submit Field -->
<div class="form-group">
    <div class="col-sm-4 col-sm-offset-2">
    <a href="{!! route('payment.edit', $user->id) !!}" class="btn btn-default">Update</a>
    </div>
</div>

@section('scripts')
<script src="{{ asset('js/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-fileinput/piexif.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-fileinput/sortable.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-fileinput/purify.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-fileinput/fileinput.min.js') }}"></script>
<script src="{{ asset('js/plugins/tinymce/jquery.tinymce.min.js') }}"></script>
<script src="{{ asset('js/plugins/tinymce/tinymce.min.js') }}"></script>
<script>

    var formType = '{{ $formType }}';
    $('.dateinput').datepicker({
        format: 'dd-mm-yyyy'
    });
    
</script>
@if($formType == 'edit')
@include('layouts.datatables_js')
<script>
    $.fn.dataTable.ext.buttons.create = {
        action: function (e, dt, button, config) {
            $('#myModal').modal('show');
        }
    };

    var hash = document.location.hash;
    if (hash) {
        console.log(hash);
        $('.nav-tabs a[href="'+hash+'"]').tab('show')
    }

    // Change hash for page-reload
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
    window.location.hash = e.target.hash;
    });
</script>
@endif
@endsection