@section('css')
<link href="{{ asset('css/jquery-step.css') }}" rel="stylesheet">
<link href="{{ asset('js/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css') }}" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
<link href="{{ asset('css/plugins/bootstrap-select.min.css') }}" rel="stylesheet">
<style type="text/css">
    #btn-save{
    margin-top: 100px;
    }
    .uppercase{
        text-transform: uppercase !important;
    }
</style>
@include('layouts.datatables_css')
@endsection
<div>
    @if($formType == 'create')
    @php 
    $numI = 1;
    $numX = 1;
    $numC = 1;
    @endphp
    <section>
        {{-- <div class="form-group">
            {!! Form::label('type', 'Input Master Lagu:', ['class' => 'col-sm-2 control-label']) !!}
            <div class="col-sm-4">
                {!! Form::select('type', ['' => 'Select', 1 => 'Upload Excel or CSV', 2 => 'Manual'], null, ['class' => 'form-control ']) !!}
            </div>
        </div>
        <div class="form-group " id="type-content" style="display:none">
            <div class="col-md-11 col-sm-12">
                @if(auth()->user()->roles->first()->name === 'admin')
                <a href="{{route('masterlagu.download.template')}}" class="btn btn-primary pull-right" id="btn-template">Download Template</a>
                @elseif(auth()->user()->roles->first()->name === 'legal')
                <a href="{{route('legal.masterlagu.download.template')}}" class="btn btn-primary pull-right" id="btn-template">Download Template</a>
                @endif
            </div>
            {!! Form::label('file', '', ['class' => 'col-sm-2 control-label']) !!}
            <div class="col-md-6 col-sm-6" >
                <input id="input-id" type="file" name="file" data-preview-file-type="text" >
            </div>
        </div> --}}
        <div class="form-group " id="type-manual" style="display:block">
            <div class="col-md-12 hide" style="padding:10px;">
                {!! Form::label('Type User', '', ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-md-4 col-sm-4" >
                    {!! Form::select('typeuser', ['' => 'Select', 1 => 'Penyanyi', 2 => 'Pencipta'], null, ['class' => 'form-control ', 'id'=>'typeuser']) !!}
                </div>
            </div>
            {!! Form::hidden('role_id', '', ['id' => 'role_id'] ) !!}
            <div class="col-md-12" id="" style="padding:10px;">
                {!! Form::label('Penyanyi', '', ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-md-4 col-sm-4" >
                    <select id="singer{{$numI}}" onchange="ceksinger(<?=$numI?>);" name="singer[]" class="form-control" >
                        <option value="">Pilih Penyanyi</option>
                        @foreach($singerx as $singer)
                            <option value="{{$singer->id}}" >{{$singer->name}}</option>
                        @endforeach
                    </select>
                </div>
                <!-- <div class="col-md-2 col-sm-2" >
                    <span id="" class="btn add-singer btn-success"><i class="fa fa-plus"></i> Add</span>
                </div> -->
                {{-- <div class="col-md-2 col-sm-2">
                    {!! Form::checkbox('transing[]', '1', null, array('class' => 'mt10', 'id' => 'transing'))!!}
                    {!! Form::label('transing', 'Transfer',array('class' => 'control-label')) !!}
                </div> --}}
            </div>
            <div class="col-md-12" id="ContainerSinger"></div>
            <div class="row">
                <div class="col-md-6 col-sm-6"></div>
                <div class="col-md-6 col-sm-6" id="addsinger" style="border:1px #f0f0f solid;" >
                    <span id="" class="btn add-singer btn-success"><i class="fa fa-plus"></i> Add</span> <span><em>*Max 10 Penyanyi</em></span>
                </div>
            </div>
            <hr>

            <div class="col-md-12" id="" style="padding:10px;">
                <label class="col-sm-2 control-label">Pencipta / Publishing</label>
                <div class="col-md-4 col-sm-4" >
                    <select id="songwriter{{$numX}}" onchange="ceksongwriter(<?=$numX?>);" name="songwriter[]" class="form-control" >
                        <option value="">Pilih Pencipta / Publishing</option>
                        @foreach($songwriterx as $songwriter)
                            <option value="{{$songwriter->id}}">{{$songwriter->name}}</option>
                        @endforeach
                    </select>
                </div>
                <!-- <div class="col-md-2 col-sm-2" >
                    <span id="" class="btn add-songwriter btn-success"><i class="fa fa-plus"></i> Add</span>
                </div> -->
                {{-- <div class="col-md-2 col-sm-2">
                    {!! Form::checkbox('transong[]', '1', null, array('class' => 'mt10', 'id' => 'transong'))!!}
                    {!! Form::label('transong', 'Transfer',array('class' => 'control-label')) !!}
                </div> --}}
            </div>
            <div class="col-md-12" id="ContainerSongwriter"></div>
            <div class="row">
                <div class="col-md-6 col-sm-6"></div>
                <div class="col-md-6 col-sm-6" id="addsongwriter" style="border:1px #f0f0f solid;" >
                    <span id="" class="btn add-songwriter btn-success"><i class="fa fa-plus"></i> Add</span> <span><em>*Max 10 Pencipta / Publishing</em></span>
                </div>
            </div>

            <div class="col-md-12" style="padding:10px;">
                {!! Form::label('No. Contract', '', ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-md-4 col-sm-4" >
                    {!! Form::text('no_contract', '', ['class' => 'form-control', 'autocomplete'=>'off']) !!}
                </div>
            </div>
            <div class="col-md-12" style="padding:10px;">
                {!! Form::label('Judul', '', ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-md-4 col-sm-4" >
                    {!! Form::text('track_title', '', ['class' => 'form-control', 'autocomplete'=>'off']) !!}
                </div>
            </div>
            <div class="col-md-12" style="padding:10px;">
                {!! Form::label('Album', '', ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-md-4 col-sm-4" >
                    {!! Form::text('release_title', '', ['class' => 'form-control', 'autocomplete'=>'off']) !!}
                </div>
            </div>
            <div class="col-md-12" style="padding:10px;">
                {!! Form::label('Tanggal Release', '', ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-md-4 col-sm-4" >
                    {!! Form::text('release_date', '', ['class' => 'form-control datepicker', 'autocomplete'=>'off', 'id' => 'datepicker']) !!}
                </div>
            </div>
            <div class="col-md-12" style="padding:10px;">
                {!! Form::label('Label Name', '', ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-md-4 col-sm-4" >
                    {!! Form::text('label_name', 'Hits Records', ['class' => 'form-control', 'autocomplete'=>'off']) !!}
                </div>
            </div>
            <div class="col-md-12" style="padding:10px;">
                {!! Form::label('ISRC', '', ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-md-4 col-sm-4" >
                    {!! Form::text('isrc', '', ['class' => 'form-control uppercase', 'autocomplete'=>'off', '']) !!}
                </div>
            </div>
            <div class="col-md-12" style="padding:10px;">
                {!! Form::label('UPC', '', ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-md-4 col-sm-4" >
                    {!! Form::text('upc', '', ['class' => 'form-control', 'autocomplete'=>'off']) !!}
                </div>
            </div>
            <div class="col-md-12" style="padding:10px;">
                {!! Form::label('% Pencipta / Publishing', '', ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-md-4 col-sm-4" >
                    {!! Form::text('percentpencipta', '', ['class' => 'form-control', 'autocomplete'=>'off']) !!}
                </div>
            </div>
            {{-- <div class="col-md-12" style="padding:10px;">
                {!! Form::label('% Publishing', '', ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-md-4 col-sm-4" >
                    {!! Form::text('percentpublishing', '', ['class' => 'form-control', 'autocomplete'=>'off']) !!}
                </div>
            </div> --}}
            <div class="col-md-12" style="padding:10px;">
                {!! Form::label('% Penyanyi', '', ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-md-4 col-sm-4" >
                    {!! Form::text('percentpenyanyi', '', ['class' => 'form-control', 'autocomplete'=>'off']) !!}
                </div>
            </div>
            <div class="col-md-12" style="padding:10px;">
                {!! Form::label('% Rights Recording', '', ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-md-4 col-sm-4" >
                    {!! Form::text('percentrights', '', ['class' => 'form-control', 'autocomplete'=>'off']) !!}
                </div>
            </div>
            <hr>
            <div class="col-md-12" id="" style="padding:10px;">
                {!! Form::label('Negara', '', ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-md-4 col-sm-4" >
                    <select id="country{{$numC}}" onchange="cekcountry(<?=$numC?>);" name="country[]" class="form-control">
                        <option value="">Pilih Negara</option>
                        @foreach($countryx as $country)
                            
                            <option value="{{$country->id}}" >{{$country->country_name}}</option>
                        @endforeach
                    </select>
                </div>
                <!-- <div class="col-md-2 col-sm-2" >
                    <span id="" class="btn add-singer btn-success"><i class="fa fa-plus"></i> Add</span>
                </div> -->
            </div>
            <div class="col-md-12" id="ContainerCountry"></div>
            <div class="row">
                <div class="col-md-6 col-sm-6"></div>
                <div class="col-md-6 col-sm-6" id="addcountry" style="border:1px #f0f0f solid;" >
                    <span id="" class="btn add-country btn-success"><i class="fa fa-plus"></i> Add</span> <span><em>*Max 10 Country</em></span>
                </div>
            </div>
            
        </div>
    </section>


    @elseif($formType == 'edit')
    <section>
        <div class="form-group " id="">
            {!! Form::hidden('master_id', $masterlagu->id, ['id' => 'master_id'] ) !!}
            {!! Form::hidden('singer_id', $masterlagu->id_penyanyi, ['id' => 'singer_id'] ) !!}
            {!! Form::hidden('songwriter_id', $masterlagu->id_pencipta, ['id' => 'songwriter_id'] ) !!}

            @php $numX = 0; @endphp
            @foreach($penyanyi as $penyanyi)
            <div class="row-sing{{$numX}}">
                <div class="col-md-12" id="" style="padding:10px;">
                    {!! Form::label('Penyanyi', '', ['class' => 'col-sm-2 control-label']) !!}
                    <div class="col-md-4 col-sm-4" >
                        <select id="singer{{$numX}}" onchange="ceksinger(<?=$numX?>);" name="singer[]" class="form-control" >    
                            <option value="">Pilih Penyanyi</option>
                            @foreach($singerx as $ssinger)
                            <option value="{{$ssinger->user_id}}" <?php if($ssinger->user_id==$penyanyi->id){echo'selected';} ?> >{{$ssinger->name_master}}</option>
                            @endforeach
                        </select>
                    </div>
                    {{-- <div class="col-md-2 col-sm-2">
                        {!! Form::checkbox('transing'.$numX, '1', null, array('class' => 'mt10', 'id' => 'transing'.$numX))!!}
                        {!! Form::label('transing'.$numX, 'Transfer', array('class' => 'control-label')) !!}
                    </div> --}}
                    @if($numX>0)
                    <div class="col-md-2 col-sm-2" >
                        <i id="{{$numX}}" class="btn btn-danger remove-sing" ><i class="fa fa-remove"></i></i>
                    </div>
                    @endif
                </div>
            </div>
            @php $numX++; @endphp
            @endforeach
            <div class="col-md-12" id="ContainerSinger"></div>
            <div class="row">
                <div class="col-md-6 col-sm-6"></div>
                <div class="col-md-6 col-sm-6" style="border:1px #f0f0f solid;" >
                    <span id="" class="btn add-singer btn-success"><i class="fa fa-plus"></i> Add</span> <span><em>*Max 10 Penyanyi</em></span>
                </div>
            </div>

            <hr>
            @php $numI = 0; @endphp
            @foreach($pencipta as $pencipta)
            <div class="row-song{{$numI}}">
                <div class="col-md-12" id="" style="padding:10px;">
                    <label class="col-sm-2 control-label">Pencipta / Publishing</label>
                    <div class="col-md-4 col-sm-4" >
                        <select id="songwriter{{$numI}}" onchange="ceksongwriter(<?=$numI?>);" name="songwriter[]" class="form-control" >
                            <option value="">Pilih Pencipta / Publishing</option>
                            @foreach($songwriterx as $songwriter)
                                <option value="{{$songwriter->user_id}}" <?php if($songwriter->user_id==$pencipta->id){echo'selected';} ?>>{{$songwriter->name_master}}</option>
                            @endforeach
                        </select>
                    </div>
                    {{-- <div class="col-md-2 col-sm-2">
                        {!! Form::checkbox('transong'.$numI, '1', null, array('class' => 'mt10', 'id' => 'transong'.$numI))!!}
                        {!! Form::label('transong'.$numI, 'Transfer',array('class' => 'control-label')) !!}
                    </div> --}}
                    @if($numI>0)
                    <div class="col-md-2 col-sm-2" >
                        <i id="{{$numI}}" class="btn btn-danger remove-song" ><i class="fa fa-remove"></i></i>
                    </div>
                    @endif
                </div>
            </div>
            @php $numI++; @endphp
            @endforeach
            <div class="col-md-12" id="ContainerSongwriter"></div>

            <div class="row">
                <div class="col-md-6 col-sm-6"></div>
                <div class="col-md-6 col-sm-6" style="border:1px #f0f0f solid;" >
                    <span id="" class="btn add-songwriter btn-success"><i class="fa fa-plus"></i> Add</span> <span><em>*Max 10 Pencipta</em></span>
                </div>
            </div>
            <hr>
            
            <div class="col-md-12" style="padding:10px;">
                {!! Form::label('No. Contract', '', ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-md-4 col-sm-4" >
                    {!! Form::text('no_contract', $masterlagu->no_contract, ['class' => 'form-control', 'autocomplete'=>'off',]) !!}
                </div>
            </div>
            <div class="col-md-12" style="padding:10px;">
                {!! Form::label('Judul', '', ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-md-4 col-sm-4" >
                    {!! Form::text('track_title', $masterlagu->track_title, ['class' => 'form-control', 'autocomplete'=>'off',]) !!}
                </div>
            </div>
            <div class="col-md-12" style="padding:10px;">
                {!! Form::label('Album', '', ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-md-4 col-sm-4" >
                    {!! Form::text('release_title', $masterlagu->release_title, ['class' => 'form-control', 'autocomplete'=>'off',]) !!}
                </div>
            </div>
            <div class="col-md-12" style="padding:10px;">
                {!! Form::label('Tanggal Release', '', ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-md-4 col-sm-4" >
                    {!! Form::text('release_date', ($masterlagu->release_date != '0000-00-00') ? date('d-m-Y', strtotime($masterlagu->release_date)) : '', ['class' => 'form-control datepicker', 'autocomplete'=>'off', 'id' => 'datepicker',]) !!}
                </div>
            </div>
            <div class="col-md-12" style="padding:10px;">
                {!! Form::label('Label Name', '', ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-md-4 col-sm-4" >
                    {!! Form::text('label_name', ($masterlagu->label_name != '') ? $masterlagu->label_name : 'Hits Records', ['class' => 'form-control', 'autocomplete'=>'off',]) !!}
                </div>
            </div>
            <div class="col-md-12" style="padding:10px;">
                {!! Form::label('ISRC', '', ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-md-4 col-sm-4" >
                    {!! Form::text('isrc', $masterlagu->isrc, ['class' => 'form-control uppercase', 'autocomplete'=>'off', ]) !!}
                </div>
            </div>
            <div class="col-md-12" style="padding:10px;">
                {!! Form::label('UPC', '', ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-md-4 col-sm-4" >
                    {!! Form::text('upc', $masterlagu->upc, ['class' => 'form-control', 'autocomplete'=>'off']) !!}
                </div>
            </div>
            <div class="col-md-12" style="padding:10px;">
                {!! Form::label('% Pencipta / Publishing', '', ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-md-4 col-sm-4" >
                    {!! Form::text('percentpencipta', sprintf("%.0f", $masterlagu->percentage_pencipta * 100), ['class' => 'form-control', 'autocomplete'=>'off']) !!}
                </div>
            </div>
            <!-- <div class="col-md-12" style="padding:10px;">
                {!! Form::label('% Publishing', '', ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-md-4 col-sm-4" >
                    {!! Form::text('percentpublishing', sprintf("%.0f", $masterlagu->percentage_publishing * 100), ['class' => 'form-control', 'autocomplete'=>'off']) !!}
                </div>
            </div> -->
            <div class="col-md-12" style="padding:10px;">
                {!! Form::label('% Penyanyi', '', ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-md-4 col-sm-4" >
                    {!! Form::text('percentpenyanyi', sprintf("%.0f", $masterlagu->percentage_penyanyi * 100), ['class' => 'form-control', 'autocomplete'=>'off']) !!}
                </div>
            </div>
            <div class="col-md-12" style="padding:10px;">
                {!! Form::label('% Right Recording', '', ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-md-4 col-sm-4" >
                    {!! Form::text('percentrights', sprintf("%.0f", $masterlagu->percentage_rights * 100), ['class' => 'form-control', 'autocomplete'=>'off']) !!}
                </div>
            </div>

            <hr>
            @php $numC = 0; @endphp
            @foreach($negara as $negara)

            <div class="row-country{{$numC}}">
                <div class="col-md-12" id="" style="padding:10px;">
                    <label class="col-sm-2 control-label">Negara</label>
                    <div class="col-md-4 col-sm-4" >
                        <select id="country{{$numC}}" onchange="cekcountry(<?=$numC?>);" name="country[]" class="form-control" >
                            <option value="">Pilih Negara</option>
                            @foreach($countryx as $country)
                                <option value="{{$country->id}}" <?php if($country->id==$negara->id){echo'selected';} ?>>{{$country->country_name}}</option>
                            @endforeach
                        </select>
                    </div>
                    @if($numC>0)
                    <div class="col-md-2 col-sm-2" >
                        <i id="{{$numC}}" class="btn btn-danger remove-country" ><i class="fa fa-remove"></i></i>
                    </div>
                    @endif
                </div>
            </div>
            @php $numC++; @endphp
            @endforeach
            <div class="col-md-12" id="ContainerCountry"></div>

            <div class="row">
                <div class="col-md-6 col-sm-6"></div>
                <div class="col-md-6 col-sm-6" id="addcountry" style="border:1px #f0f0f solid;" >
                    <span id="" class="btn add-country btn-success"><i class="fa fa-plus"></i> Add</span> <span><em>*Max 10 Country</em></span>
                </div>
            </div>
            <hr>
        </div>
    </section>
    @endif
    <!-- Submit Field -->
    <div class="form-group">
        <div class="col-sm-4 col-sm-offset-2">
            {!! Form::submit( 'Save', ['class' => 'btn btn-success', 'name' => 'submitbutton', 'value' => 'save'])!!}
            {!! Form::submit( 'Save As Draft', ['class' => 'btn btn-primary', 'name' => 'submitbutton', 'value' => 'save-draft']) !!}            
        @if(auth()->user()->roles->first()->name === 'admin')
            <a href="{!! route('masterlagu.index') !!}" class="btn btn-default">Back</a>
        @elseif(auth()->user()->roles->first()->name === 'legal')
            <a href="{!! route('legal.masterlagu.index') !!}" class="btn btn-default">Back</a>
        @elseif(auth()->user()->roles->first()->name === 'anr')
            <a href="{!! route('anr.masterlagu.index') !!}" class="btn btn-default">Back</a>
        @endif
        </div>
    </div>
</div>


@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/js/plugins/piexif.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/js/plugins/sortable.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/js/plugins/purify.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/js/fileinput.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/themes/fa/theme.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-steps/1.1.0/jquery.steps.js"></script>
<script src="{{ asset('js/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-select.min.js') }}"></script>
@include('layouts.datatables_js')

<script type="text/javascript">

    @if(!empty($numX)) //Variabel Global SINGER
        var x = <?=$numX?>;
    @else 
         var x = 0;
    @endif

    @if(!empty($numI)) //Variabel Global SONGWRITER
        var i = {{$numI}};
    @else 
         var i = 0;
    @endif

    @if(!empty($numI)) //Variabel Global COUNTRY
        var c = {{$numI}};
    @else 
         var c = 0;
    @endif




$(document).ready(function () {

    var MaxInputs = 9;
    var SingWrapper = $("#ContainerSinger");
    var SongWrapper = $("#ContainerSongwriter");
    var singLength = SingWrapper.length;
    var songLength = SongWrapper.length;
    var counLength = SongWrapper.length;

    var singCount = 1;
    var songCount = 1;
    var counCount = 1;
    
    $(".alert-danger").fadeTo(3000, 1000).slideUp(1000, function(){
        $(".alert-danger").slideUp(1000);
    });
    $(".alert-success").fadeTo(3000, 1000).slideUp(1000, function(){
        $(".alert-success").slideUp(1000);
    });
    
    /*+++++++++++Tambah element Field singer*/
    $(".add-singer").click(function (e) {
        //e.preventDefault();

        if(x <= MaxInputs){
            var list_sing = '<label class="col-sm-2 control-label">Penyanyi</label>';
                list_sing += '<div class="col-md-4 col-sm-4" ><select id="singer'+x+'" onchange="ceksinger('+x+')"  name="singer[]" class="form-control"><option value="">Pilih Penyanyi</option>@foreach($singerx as $singer)';
                list_sing += '<option value="{{$singer->user_id}}">{{$singer->name}}</option>@endforeach</select></div>';
                // list_sing += '<div class="col-sm-2">';
                // list_sing += '<input class="mt10" name="transing'+x+'" type="checkbox" value="1" id="transing'+x+'">';
                // list_sing += '<label for="transing'+x+'" class="control-label" style="margin-left:2px;">Transfer</label>';
                // list_sing += '</div>';
                list_sing += '<div class="col-sm-2" >';
                list_sing += '<button id="'+ (x) +'" class="btn btn-danger remove-sing" ><i class="fa fa-remove"></i></button>';
                list_sing += '</div>';

            var btn = '<div class="row"><div class="col-xs-12 col-sm-7 col-md-7">&nbsp;</div><div class="col-xs-12 col-sm-1 col-md-1"  ></div></div>';

            var newIn = '<div class="row-sing'+ x +'">' +
                        '' + list_sing +
                        btn + '</div>';
            
            $('#ContainerSinger').append(newIn);

            x++;
        }else{
            alert('Max 10 Penyanyi');
        }
        return false;
    });

    $("body").on("click",".remove-sing", function(e){ //user click on remove text
        console.log(x);
            if( x > 1 ) {
                var choice = confirm("Are you sure to delete this item?");
                if(choice == true){
                    console.log(x);
                    var fieldNumx = this.id.valueOf();
                    var fieldIDx = ".row-sing" + fieldNumx;
                    $(this).remove();
                    $(fieldIDx).remove();
                    //$(this).parent('div').remove(); //remove text box
                    x--; //decrement textbox
                }
            }
        return false;
    });

    /*+++++++++++Tambah element Field songwriter*/
    $(".add-songwriter").click(function (e) {
        e.preventDefault();        
        if(i <= MaxInputs){
            var list_song = '<label class="col-sm-2 control-label">Pencipta / Publishing</label>';
            list_song += '<div class="col-md-4 col-sm-4" ><select id="songwriter'+i+'" onchange="ceksongwriter('+i+')"  name="songwriter[]" class="form-control"><option value="">Pilih Pencipta / Publishing</option>@foreach($songwriterx as $songwriter)';
            list_song += '<option value="{{$songwriter->user_id}}">{{$songwriter->name}}</option>@endforeach</select></div>';
            // list_song += '<div class="col-sm-2">';
            // list_song += '<input class="mt10" name="transong'+i+'" type="checkbox" value="1" id="transong'+i+'">';
            // list_song += '<label for="transong'+i+'" class="control-label" style="margin-left:2px;">Transfer</label>';
            // list_song += '</div>';
            list_song += '<div class="col-md-2 col-sm-2" >';
            list_song += '<button id="'+(i)+'" class="btn btn-danger remove-song" ><i class="fa fa-remove"></i></button>';
            list_song += '</div>';

            var btn = '<div class="row"><div class="col-xs-12 col-sm-7 col-md-7">&nbsp;</div><div class="col-xs-12 col-sm-1 col-md-1"  ></div></div>';

            var newIn = '<div class="row-song'+ i +'">' +
                        '' + list_song +
                        btn + '</div>';
            $('#ContainerSongwriter').append(newIn);
            i++;
        }else{
            alert('Max 10 Pencipta');
        }
        return false;

    });

    $("body").on("click",".remove-song", function(e){ //user click on remove text
        console.log(i);
            if( i > 1 ) {
                var choice = confirm("Are you sure to delete this item?");
                console.log(choice);
                if(choice == true){
                    var fieldNumi = this.id.valueOf();
                    var fieldIDi = ".row-song" + fieldNumi;
                    $(this).remove();
                    $(fieldIDi).remove();
                    //$(this).parent('div').remove(); //remove text box
                    i--; //decrement textbox
                }
                
            }
        return false;
    });

    /*+++++++++++Tambah element Field singer*/
    $(".add-country").click(function (e) {
        //e.preventDefault();

        if(c <= MaxInputs){
            var list_country = '<label class="col-sm-2 control-label">Negara</label>';
                list_country += '<div class="col-md-4 col-sm-4" ><select id="country'+c+'" onchange="cekcountry('+c+')"  name="country[]" class="form-control"><option value="">Pilih Country</option>@foreach($countryx as $country)';
                list_country += '@if($country->id == "247") @else <option value="{{$country->id}}">{{$country->country_name}}</option> @endif';
                list_country += '@endforeach</select></div>';
                list_country += '<div class="col-md-2 col-sm-2" >';
                list_country += '<button id="'+ (c) +'" class="btn btn-danger remove-country" ><i class="fa fa-remove"></i></button>';
                list_country += '</div>';

            var btn = '<div class="row"><div class="col-xs-12 col-sm-7 col-md-7">&nbsp;</div><div class="col-xs-12 col-sm-1 col-md-1"  ></div></div>';

            var newIn = '<div class="row-country'+ c +'">' +
                        '' + list_country +
                        btn + '</div>';
            
            $('#ContainerCountry').append(newIn);

            c++;
        }else{
            alert('Max 10 Country');
        }
        return false;
    });

    $("body").on("click",".remove-country", function(e){ //user click on remove text
        console.log(c);
            if( c > 1 ) {
                var choice = confirm("Are you sure to delete this item?");
                if(choice == true){
                    console.log(c);
                    var fieldNumc = this.id.valueOf();
                    var fieldIDc = ".row-country" + fieldNumc;
                    $(this).remove();
                    $(fieldIDc).remove();
                    console.log(fieldIDc);
                    //$(this).parent('div').remove(); //remove text box
                    c--; //decrement textbox
                }
            }
        return false;
    });


});

/*h*/

function ceksinger(idx){
    for ( cekx = x-1; cekx >= 0; --cekx) {
        if( cekx != idx){ //sorting element yg mau dicari
            var data = $('#singer'+cekx).val();
            //console.log(cekx+','+x+','+data);
            if( $('#singer'+idx).val() == data){ ///Jika salah
                document.getElementById("singer"+idx).style.borderColor = "red";
                $('#singer'+idx).val('');
                alert('Data Singer is Duplicate');
                return false;
            }else{ ///Jika benar
                document.getElementById("singer"+idx).style.borderColor = "green";
            }
        }
    }
}

function ceksongwriter(idi){
    for ( cekx = i-1; cekX >= 0; --cekx) {
        if( cekx != idx){ //sorting element yg mau dicari
            var dataX = $('#songwriter'+cekx).val();
            //console.log(cekX+','+x+','+dataX);
            if( $('#songwriter'+idx).val() == dataX){  ///Jika salah
                document.getElementById("songwriter"+idx).style.borderColor = "red";
                $('#songwriter'+idx).val('');
                alert('Data Songwriter or Publishing is Duplicate');
                return false;
            }else{
                document.getElementById("songwriter"+idx).style.borderColor = "green";
            }
        }
        console.log('cek x :'+ cekx);
        //var sumNumb = $('#varX'+x).val();
        //console.log('cek X : '+ x);
    }
}

function cekcountry(idc){
    $('#addcountry').show();
    if( $('#country'+idc).val() == '247'){
        $('#addcountry').hide();
        var fieldNumc = this.id.valueOf();
        var fieldIDc = ".row-country" + fieldNumc;
        $(this).remove();
        $(fieldIDc).remove();
    }else{
        $('#addcountry').show();
    }

    for ( cekc = i-1; cekc >= 0; --cekc) {
        if( cekc != idc){ //sorting element yg mau dicari
            var data = $('#singer'+cekc).val();
            //console.log(cekI+','+i+','+data);
            if( $('#country'+idc).val() == data){ ///Jika salah
                document.getElementById("country"+idc).style.borderColor = "red";
                $('#country'+idc).val('');
                alert('Data is Duplicate');
                return false;
            }else{ ///Jika benar
                document.getElementById("country"+idc).style.borderColor = "green";
            }
        }
    }
}


</script>



<script>
    $(document).ready(function(){
        $('.datepicker').datepicker({
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true,
            //viewMode: 'months',
            // minViewMode: 'months',
            format: 'dd-mm-yyyy'
        });
        
        $('#type').change(function(){
            if($(this).val() == 1){
                $('#type-content').show();
                $('#type-manual').hide();
            }else if($(this).val() == 2) {
                $('#type-manual').show();
                $('#type-content').hide();
            }else{
                $('#type-content').hide();
                $('#type-manual').hide();
            }
        });
        

        $('#typeuser').change(function(){
            if($(this).val() == 1){
                $('#role_singer').show();
                $('#role_songwriter').hide();
                $('#role_id').val(9);
            }else if($(this).val() == 2) {
                $('#role_singer').hide();
                $('#role_songwriter').show();
                $('#role_id').val(10);
            }else{
                $('#role_singer').hide();
                $('#role_songwriter').hide();
            }
        });

        $("#input-id").fileinput({
            uploadUrl: "#",
            showUpload: false,
            allowedFileExtensions: ["csv", 'xlsx'],
            fileActionSettings: {
                showUpload: false,
            }
        });

    });
    
</script>
@endsection