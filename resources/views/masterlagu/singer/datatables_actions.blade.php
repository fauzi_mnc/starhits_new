@if(auth()->user()->roles->first()->name == 'admin')
<div class='btn-group'>
    <a href="{{ route('mastersinger.edit', $s->id) }}" data-url="{{ route('mastersinger.edit', $s->id) }}" class='btn btn-primary btn-xs' title="Edit Master Singer">
        <i class="fa fa-pencil"></i> Edit
    </a>
    <a href="{{ route('mastersinger.destroy', $s->id) }}" class='btn btn-danger btn-xs' title="Delete Master Singer" onclick="return confirm('Are you sure to delete?')" style="margin-left:5px;">
        <i class="fa fa-trash"></i> Delete
    </a>
</div>
@elseif(auth()->user()->roles->first()->name == 'legal')
<div class='btn-group'>
    <a href="{{ route('legal.mastersinger.edit', $s->id) }}" data-url="{{ route('legal.mastersinger.edit', $s->id) }}" class='btn btn-primary btn-xs' title="Edit Master Singer">
        <i class="fa fa-pencil"></i> Edit
    </a>
    {{-- <a href="{{ route('legal.mastersinger.destroy', $id) }}" class='btn btn-danger btn-xs' title="Delete Master Singer" onclick="return confirm('Are you sure to delete?')" style="margin-left:5px;">
        <i class="fa fa-trash"></i> Delete
    </a> --}}
</div>
@elseif(auth()->user()->roles->first()->name == 'anr')
<div class='btn-group'>
    <a href="{{ route('anr.mastersinger.edit', $s->id) }}" data-url="{{ route('anr.mastersinger.edit', $s->id) }}" class='btn btn-primary btn-xs' title="Edit Master Singer">
        <i class="fa fa-pencil"></i> Edit
    </a>
    {{-- <a href="{{ route('legal.mastersinger.destroy', $id) }}" class='btn btn-danger btn-xs' title="Delete Master Singer" onclick="return confirm('Are you sure to delete?')" style="margin-left:5px;">
        <i class="fa fa-trash"></i> Delete
    </a> --}}
</div>
@endif
