@extends('layouts.crud')

@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Master Lagu</h2>
        <ol class="breadcrumb">
            <li>
                @if(auth()->user()->roles->first()->name === 'admin')
                <a href="{{ route('masterlagu.index') }}">Master Lagu</a>
                @elseif(auth()->user()->roles->first()->name === 'legal')
                <a href="{{ route('legal.masterlagu.index') }}">Master Lagu</a>
                @endif
            </li>
            <li class="active">
                <strong>Edit</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
@endsection

@section('contentCrud')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@include('flash::message')
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                Edit Master Lagu 
            </div>
            <div class="panel-body">
                @if(auth()->user()->roles->first()->name == 'admin')
                {!! Form::model($masterlagu, ['route' => ['masterlagu.update', $masterlagu->id], 'method' => 'put', 'class' => 'form-horizontal']) !!}
                @elseif(auth()->user()->roles->first()->name == 'legal')
                {!! Form::model($masterlagu, ['route' => ['legal.masterlagu.update', $masterlagu->id], 'method' => 'put', 'class' => 'form-horizontal']) !!}
                @elseif(auth()->user()->roles->first()->name == 'anr')
                {!! Form::model($masterlagu, ['route' => ['anr.masterlagu.update', $masterlagu->id], 'method' => 'put', 'class' => 'form-horizontal']) !!}
                @endif
                @include('masterlagu.fields', ['formType' => 'edit'])
                {!! Form::close() !!}
                </form>
            </div>

        </div>
        
    </div>
</div>
@endsection