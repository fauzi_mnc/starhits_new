<html>
<head>
<style>
body{
    margin: 10px;
}
table{
    margin: 20px 0px;
}
table, th, td{
    border-collapse: collapse;
}
.table-bordered table, .table-bordered th, .table-bordered td {
    border: 2px solid #000;
}
th, td {
    padding: 5px;
}
.signature{
    padding: 40px 50px;
}
.silver{
    background-color: #f0f0f0;
}
</style>
</head>
<body onload="print();">
    <div style="width:100%; text-align:center;">
        <img style="width:350px;" src="{{ asset('img/hitsrecords.png') }}">
        <h2 style="margin:5px;">PT. Suara Mas Abadi</h2>
        @if(auth()->user()->roles->first()->name === 'admin')
        <p>Data Master Lagu</p>
        @else
        <p>Daftar Lagu {{ auth()->user()->name }}</p>
        @endif
    </div>
<table class="table-bordered" style="width:100%" border="1">
    <thead style="font-size: 16px;">
        <tr class="silver">
            <th>No</th>
            <th>Judul</th>
            <th>Album</th>
            <th>Tanggal Release</th>
            <th>Penyanyi</th>
            <th>Pencipta / Publishing</th>
            @if(auth()->user()->roles->first()->name === 'admin')
            <th>ISRC</th>
            <th>UPC</th>
            <th>% Penyanyi</th>
            <th>% Pencipta / Publishing</th>
            <th>% Right Recording</th>
            <th>Country</th>
            @endif
        </tr>
    </thead>
    <tbody style="font-size: 14px;">
        <?php
        //$total_net_revenue_idr=0;
        $i=1;
        ?>
        @foreach($data as $m)
            <?php
               // $total_net_revenue_idr += $m->pivot->net_revenue_idr;
            ?>
            <tr>
                <td align="center"><?=$i?></td>
                <td align="center">{{$m->track_title}}</td>
                <td align="center">{{$m->release_title}}</td>
                <td align="center">
                    @if($m->release_date=="0000-00-00")
                    @else
                    {{date("d-m-Y", strtotime($m->release_date))}}
                    @endif
                </td>
                <td align="center">
                    @php
                        $sing = "";
                    @endphp             
                    @foreach($m->rolelagu as $key => $singer)
                        @if(!empty($singer->penyanyi))
                            @php 
                                $sing .= $singer->penyanyi->name_master." & ";
                            @endphp
                        @endif
                    @endforeach
                    @php 
                        echo rtrim($sing," & ");
                    @endphp
                </td>
                <td align="center">
                    @php
                        $song = "";
                    @endphp             
                    @foreach($m->rolelagu as $key => $songwriter)
                        @if(!empty($songwriter->pencipta))
                            @php 
                                $song .= $songwriter->pencipta->name_master." & ";
                            @endphp
                        @endif
                    @endforeach
                    @php 
                        echo rtrim($song," & ");
                    @endphp
                </td>
                @if(auth()->user()->roles->first()->name === 'admin')
                <td align="center">{{$m->isrc}}</td>
                <td align="center">{{$m->upc}}</td>
                <td align="center">{{ sprintf("%.0f%%", $m->percentage_penyanyi * 100) }}</td>
                <td align="center">{{ sprintf("%.0f%%", $m->percentage_pencipta * 100) }}</td>
                <td align="center">{{ sprintf("%.0f%%", $m->percentage_rights * 100) }}</td>
                <td align="center">
                    @php
                        $coun = "";
                    @endphp             
                    @foreach($m->rolelagucountry as $key => $country)
                        @if(!empty($country->country))
                            @php 
                                $coun .= $country->country->country_name." & ";
                            @endphp
                        @endif
                    @endforeach
                    @php 
                        echo rtrim($coun," & ");
                    @endphp
                </td>

                @endif
            </tr>
        <?php $i++; ?>
        @endforeach
    </tbody>
    
</table>
</body>
</html> 