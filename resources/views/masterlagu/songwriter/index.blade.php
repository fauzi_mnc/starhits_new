@extends('layouts.crud')

@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><i class="fa fa-file-audio-o"></i> Master Songwriter / Publisher</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Master Songwriter / Publisher</a>
            </li>
            <li class="active">
                <strong>Table</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
@endsection
@section('contentCrud')
@include('flash::message')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Master Songwriter / Publisher</h5>
                    {{-- @if(auth()->user()->roles->first()->name == 'admin')
                    <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{{route('mastersongwriter.create')}}">Add New</a>
                    @elseif(auth()->user()->roles->first()->name == 'legal')
                    <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{{route('legal.mastersongwriter.create')}}">Add New</a>
                    @endif --}}
                </div>
                <div class="ibox-content">
                    @include('masterlagu.songwriter.table')
                </div>
            </div>
        </div>
    </div>
@endsection


