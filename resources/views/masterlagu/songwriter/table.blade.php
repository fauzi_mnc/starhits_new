@section('css')
    @include('layouts.datatables_css')
@endsection

<table class="table" id="table">
    <thead>
        <tr>
            <th class="text-center">Updated At</th>
            <th class="text-center">Name</th>
            <th class="text-center">Email</th>
            @if(auth()->user()->roles->first()->name == 'admin' || auth()->user()->roles->first()->name == 'legal' || auth()->user()->roles->first()->name == 'anr')
            <th class="text-center">Action</th>
            @endif
        </tr>
    </thead>
    <tbody>
        @foreach($songwriter AS $s)
        <tr>
            <td class="text-center" data-sort="{{ strtotime($s->updated_at) }}">
                @php
                if($s->updated_at == '0000-00-00 00:00:00'){
                    $dateupdated = '';
                }else{
                    $dateupdated = date('d-m-Y'.'\<\b\r\>'.'h:m:s', strtotime($s->updated_at));
                }
                echo $dateupdated;   
                @endphp
            </td>
            <td>
                <b>
                {{ $s->name_master }}
                </b>
            </td>
            <td>{{ $s->email }}</td>
            @if(auth()->user()->roles->first()->name == 'admin' || auth()->user()->roles->first()->name == 'legal' || auth()->user()->roles->first()->name == 'anr')
            <td class="text-center">@include('masterlagu.songwriter.datatables_actions')</td>
            @endif
        </tr>
        @endforeach
    </tbody>
</table>

@section('scripts')
@include('layouts.datatables_js')
<script>
    $(document).ready(function () {
        var dataTable = $('#table').DataTable({
            @if(auth()->user()->roles->first()->name == 'admin' || auth()->user()->roles->first()->name == 'legal' || auth()->user()->roles->first()->name == 'anr')
            dom             : 'Brtlip',
            @endif
            order           : [[ 0, "desc"]],
            processing      : true,
            serverMethod    : 'post',
            responsive      : true,
            autoWidth       : false,
            aLengthMenu     : [[10, 50, 100, 250, -1], [10, 50, 100, 250, 'All']],
            columnDefs      : [{ "width": "15%", "targets": 0 }, { "width": "15%", "targets": 3 }],
            buttons         : [
                                @if(auth()->user()->roles->first()->name == 'admin' || auth()->user()->roles->first()->name == 'legal' || auth()->user()->roles->first()->name == 'anr')
                                { 
                                    "extend" : 'create', 
                                    "text" : '<i class="fa fa-plus"></i> Add New',
                                    "className" : 'btn-primary',
                                    "action" : function( e, dt, button, config){ 
                                        window.location = "mastersongwriter/create";
                                    }
                                }
                                @endif
                            ],
        });
        $(".alert-danger").fadeTo(3000, 1000).slideUp(1000, function(){
            $(".alert-danger").slideUp(1000);
        });
        $(".alert-success").fadeTo(3000, 1000).slideUp(1000, function(){
            $(".alert-success").slideUp(1000);
        });
        //lengthmenu -> add a margin to the right and reset clear 
        $(".dataTables_length").css('clear', 'none');
        $(".dataTables_length").css('margin-right', '20px');

        //info -> reset clear and padding
        $(".dataTables_info").css('clear', 'none');
        $(".dataTables_info").css('padding', '0');
        $("div").removeClass("ui-toolbar");

        $('#songwriter').on('change', function(){
            $('#album').val('');
            dataTable.search(this.value).draw(); 
        });
        $('#album').on('change', function(){
            $('#songwriter').val('');
            dataTable.search(this.value).draw();   
        });
        $('#status').on('change', function(){
            $('#songwriter').val('');
            $('#album').val('');
            dataTable.search(this.value).draw();   
        });
        $('.form-inline').find('.clear-filtering').on('click', function(e) {
            $('#songwriter').val('');
            $('#album').val('');
            $('#status').val('');
            dataTable.search(this.value).draw();
            e.preventDefault();
        });
    });
</script>
@endsection