@if(auth()->user()->roles->first()->name == 'admin')
<div class='btn-group'>
    <a href="{{ route('masterlagu.edit', $m->id) }}" data-url="{{ route('masterlagu.edit', $m->id) }}" class='btn btn-primary btn-xs' title="Edit Master Lagu">
        <i class="fa fa-pencil"></i> Edit
    </a>
    <a href="{{ route('masterlagu.destroy', $m->id) }}" class='btn btn-danger btn-xs' title="Delete Master Lagu" onclick="return confirm('Are you sure to delete?')" style="margin-left:5px;">
        <i class="fa fa-trash"></i> Delete
    </a>
</div>
@elseif(auth()->user()->roles->first()->name == 'legal')
<div class='btn-group'>
    <a href="{{ route('legal.masterlagu.edit', $m->id) }}" data-url="{{ route('legal.masterlagu.edit', $m->id) }}" class='btn btn-primary btn-xs' title="Edit Master Lagu">
        <i class="fa fa-pencil"></i> Edit
    </a>
    {{-- <a href="{{ route('legal.mastersongwriter.destroy', $m->id) }}" class='btn btn-danger btn-xs' title="Delete Master Songwriter" onclick="return confirm('Are you sure to delete?')" style="margin-left:5px;">
        <i class="fa fa-trash"></i> Delete
    </a> --}}
</div>
@elseif(auth()->user()->roles->first()->name == 'anr')
<div class='btn-group'>
    <a href="{{ route('anr.masterlagu.edit', $m->id) }}" data-url="{{ route('anr.masterlagu.edit', $m->id) }}" class='btn btn-primary btn-xs' title="Edit Master Lagu">
        <i class="fa fa-pencil"></i> Edit
    </a>
    {{-- <a href="{{ route('legal.mastersongwriter.destroy', $m->id) }}" class='btn btn-danger btn-xs' title="Delete Master Songwriter" onclick="return confirm('Are you sure to delete?')" style="margin-left:5px;">
        <i class="fa fa-trash"></i> Delete
    </a> --}}
</div>
@endif
