@section('css')
@include('layouts.datatables_css')
@endsection
<div class="responsive">
    <table class="table" id="table" width="100%">
        <thead>
            <tr>
                <td>Judul Lagu</td>
                <td>Album</td>
                {{-- <td>Tanggal Release</td>
                <td>Label</td> --}}
                <td>Penyanyi</td>
            </tr>
        </thead>
        <tbody>
            @foreach($songwriter->rolelagu AS $row)
            <tr>
                <td>
                    @php
                        if(auth()->user()->roles->first()->name == 'admin'){
                            $routedetail = "masterlagu.detail";
                        }elseif(auth()->user()->roles->first()->name == 'legal'){
                            $routedetail = "legal.masterlagu.detail";
                        }elseif(auth()->user()->roles->first()->name == 'anr'){
                            $routedetail = "anr.masterlagu.detail";
                        }elseif(auth()->user()->roles->first()->name == 'creator'){
                            $routedetail = "creator.masterlagu.detail";
                        }elseif(auth()->user()->roles->first()->name == 'singer'){
                            $routedetail = "singer.masterlagu.detail";
                        }elseif(auth()->user()->roles->first()->name == 'legal'){
                            $routedetail = "songwriter.masterlagu.detail";
                        }
                    @endphp
                    <a href="{{ route($routedetail.".lagu", $row->masterlagu->id) }}">{{ $row->masterlagu->track_title }}</a>
                </td>
                <td>{{ $row->masterlagu->release_title }}</td>
                {{-- <td>{{ date('d F Y', strtotime($row->masterlagu->release_date)) }}</td>
                <td>{{ $row->masterlagu->label_name }}</td> --}}
                <td>
                    @php
                        $sing = "";
                    @endphp             
                    @foreach($row->masterlagu->rolelagu as $key => $singer)
                        @if(!empty($singer->penyanyi))
                            @php 
                                $sing .= '<a href="'.route($routedetail.".singer", $singer->penyanyi->id).'">'.$singer->penyanyi->name_master."</a> & ";
                            @endphp
                        @endif
                    @endforeach
                    @php 
                        echo rtrim($sing," & ");
                    @endphp
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@section('scripts')
    @include('layouts.datatables_js')
    <script>
        $(document).ready(function () {
            $('#table').DataTable( {
                "dom": 'rtlip',
                "lengthMenu": [ [10, 25, 50, 100, -1], [10, 25, 50, 100, "All"] ]
            });
            $(".alert-danger").fadeTo(3000, 1000).slideUp(1000, function(){
                $(".alert-danger").slideUp(1000);
            });
            $(".alert-success").fadeTo(3000, 1000).slideUp(1000, function(){
                $(".alert-success").slideUp(1000);
            });
            //lengthmenu -> add a margin to the right and reset clear 
            $(".dataTables_length").css('clear', 'none');
            $(".dataTables_length").css('margin-right', '20px');

            //info -> reset clear and padding
            $(".dataTables_info").css('clear', 'none');
            $(".dataTables_info").css('padding', '0');
            $("div").removeClass("ui-toolbar");

            $('.form-horizontal').find('.btn-search').on('click', function(e) {
                LaravelDataTables["dataTable"].draw();
                e.preventDefault();
            });
            $('.form-horizontal').find('.clear-filtering').on('click', function(e) {
                $('#album').val('');
                LaravelDataTables["dataTable"].draw();
                e.preventDefault();
            });
        });
    </script>
@endsection