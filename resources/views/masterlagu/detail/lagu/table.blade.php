@section('css')
<style>
    table, tr, td{border:0 !important;}
</style>
@endsection
@php
    if(auth()->user()->roles->first()->name == 'admin'){
        $routedetail = "masterlagu.detail";
    }elseif(auth()->user()->roles->first()->name == 'legal'){
        $routedetail = "legal.masterlagu.detail";
    }elseif(auth()->user()->roles->first()->name == 'anr'){
        $routedetail = "anr.masterlagu.detail";
    }elseif(auth()->user()->roles->first()->name == 'creator'){
        $routedetail = "creator.masterlagu.detail";
    }elseif(auth()->user()->roles->first()->name == 'singer'){
        $routedetail = "singer.masterlagu.detail";
    }elseif(auth()->user()->roles->first()->name == 'legal'){
        $routedetail = "songwriter.masterlagu.detail";
    }
@endphp
<div class="table-responsive">
    <table class="table" id="table">
        <tbody>
            <tr>
                <td width="180px">Judul Lagu</td>
                <td width="10px">:</td>
                <td>{{$masterlagu->track_title}}</td>
            </tr>
            <tr>
                <td width="180px">Album</td>
                <td width="10px">:</td>
                <td>{{$masterlagu->release_title}}</td>
            </tr>
            <tr>
                <td width="180px">Tanggal Release</td>
                <td width="10px">:</td>
                <td>{{ date('d F Y', strtotime($masterlagu->release_date)) }}</td>
            </tr>
            <tr>
                <td width="180px">Label</td>
                <td width="10px">:</td>
                <td>{{$masterlagu->label_name}}</td>
            </tr>
            <tr>
                <td width="180px">Penyanyi</td>
                <td width="10px">:</td>
                <td>
                    @php
                        $sing = "";
                    @endphp             
                    @foreach($masterlagu->rolelagu as $key => $singer)
                        @if(!empty($singer->penyanyi))
                            @php
                                if(auth()->user()->roles->first()->name == 'admin' || auth()->user()->roles->first()->name == 'legal'){
                                    $sing .= '<a href="'.route($routedetail.".singer", $singer->penyanyi->id).'">'.$singer->penyanyi->name_master."</a> & ";
                                }else{
                                    $sing .= $singer->penyanyi->name_master." & ";
                                }
                            @endphp
                        @endif
                    @endforeach
                    @php 
                        echo rtrim($sing," & ");
                    @endphp
                </td>
            </tr>
            <tr>
                <td width="180px">Pencipta / Publishing</td>
                <td width="10px">:</td>
                <td>
                    @php
                        $song = "";
                    @endphp             
                    @foreach($masterlagu->rolelagu as $key => $songwriter)
                        @if(!empty($songwriter->pencipta))
                            @php
                                if(auth()->user()->roles->first()->name == 'admin' || auth()->user()->roles->first()->name == 'legal'){ 
                                    $song .= '<a href="'.route($routedetail.".songwriter", $songwriter->pencipta->id).'">'.$songwriter->pencipta->name_master."</a> & ";
                                }else{
                                    $song .= $songwriter->pencipta->name_master." & ";
                                }
                            @endphp
                        @endif
                    @endforeach
                    @php 
                        echo rtrim($song," & ");
                    @endphp
                </td>
            </tr>
            @if(auth()->user()->roles->first()->name === 'admin' || auth()->user()->roles->first()->name === 'legal')
            <tr>
                    <td width="180px">No. Contract</td>
                    <td width="10px">:</td>
                    <td>{{ strtoupper($masterlagu->no_contract) }}</td>
                </tr>
            <tr>
                <td width="180px">ISRC</td>
                <td width="10px">:</td>
                <td>{{ strtoupper($masterlagu->isrc) }}</td>
            </tr>
            <tr>
                <td width="180px">UPC</td>
                <td width="10px">:</td>
                <td>{{ $masterlagu->upc }}</td>
            </tr>
            <tr>
                <td width="180px">% Penyanyi</td>
                <td width="10px">:</td>
                <td>{{ sprintf("%.0f%%", $masterlagu->percentage_penyanyi * 100) }}</td>
            </tr>
            <tr>
                <td width="180px">% Pencipta / Publishing</td>
                <td width="10px">:</td>
                <td>{{ sprintf("%.0f%%", $masterlagu->percentage_pencipta * 100) }}</td>
            </tr>
            <tr>
                <td width="180px">% Rights Recording</td>
                <td width="10px">:</td>
                <td>{{ sprintf("%.0f%%", $masterlagu->percentage_rights * 100) }}</td>
            </tr>
            <tr>
                <td width="180px">Country</td>
                <td width="10px">:</td>
                <td>
                    @php
                        $coun = "- & ";
                    @endphp             
                    @foreach($masterlagu->rolelagucountry as $key => $c)
                        @if(!empty($c->country))
                            @php
                                $coun  = "";
                                $coun .= $c->country->country_name." & ";
                            @endphp
                        @endif
                    @endforeach
                    @php 
                        echo rtrim($coun," & ");
                    @endphp
                </td>
            </tr>
            @endif
        </tbody>
    </table>
    <a href="javascript:history.back()" class="btn btn-primary"><i class="fa fa-long-arrow-left"></i> Back</a>
</div>


@section('scripts')

@endsection