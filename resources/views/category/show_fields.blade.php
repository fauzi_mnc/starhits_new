
@section('css')
<link href="{{ asset('css/fileinput.min.css') }}" rel="stylesheet">
<link href="{{ asset('js/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css') }}" rel="stylesheet">

<style>
.kv-photo .krajee-default.file-preview-frame,.kv-photo .krajee-default.file-preview-frame:hover {
    margin: 0;
    padding: 0;
    border: none;
    box-shadow: none;
    text-align: center;
}
.kv-photo .file-input {
    display: table-cell;
    max-width: 220px;
}
.kv-reqd {
    color: red;
    font-family: monospace;
    font-weight: normal;
}
</style>
@include('layouts.datatables_css')
@endsection
<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Title:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::text('title', null, ['class' => 'form-control', 'disabled' => true, 'maxlength' => 125]) !!}
    </div>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('type', 'Type:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::select('type', ['0' => 'Image', '1' => 'Video'], null, ['placeholder' => 'Select Type', 'class' => 'form-control',  'disabled' => true]) !!}
    </div>
</div>

<!-- Picture Field -->
<div class="form-group image" style="display: none;">
    {!! Form::label('image', 'Picture:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        <div class="kv-photo center-block text-center">
            <input id="photo" name="image" type="file" class="file-loading">
        </div>

        <div id="kv-photo-errors-1" class="center-block alert alert-block alert-danger" style="display:none"></div>
    </div>
</div>

<!-- Picture Field -->
<div class="form-group video" style="display: none;">
    {!! Form::label('video', 'Video:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        <div class="kv-photo center-block text-center">
            <input id="video" name="video" type="file" class="file-loading">
        </div>

        <div id="kv-photo-errors-2" class="center-block alert alert-block alert-danger" style="display:none"></div>
    </div>
</div>

<div class="form-group">
    {!! Form::label('youtube_id', 'Youtube ID:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::text('youtube_id', null, ['class' => 'form-control', 'disabled' => true, 'maxlength' => 125]) !!}
        <span class="help-block m-b-none">Navigating URL when you click on Ads.</span>
    </div>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('target', 'URL Target:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::select('target', ['0' => 'Open link in new window', '1' => 'Open link in current window'], null, ['placeholder' => 'Select URL Target', 'class' => 'form-control', 'disabled' => true]) !!}
    </div>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('position', 'Position:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::select('position', ['0' => 'Full', '1' => 'Top', '2' => 'Bottom', '3' => 'Center', '4' => 'Right'], null, ['placeholder' => 'Select Position', 'class' => 'form-control', 'disabled' => true]) !!}
    </div>
</div>

<!-- Title Field -->
<div class="form-group" id="data_5">
    {!! Form::label('dr', 'Date range:', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        <div class="input-group input-daterange">
            {!! Form::text('start_date', isset($ads->start_date) ? $ads->start_date->format('Y-m-d') : '', ['class' => 'input-sm form-control', 'disabled' => true]) !!}
            <span class="input-group-addon">to</span>
            {!! Form::text('end_date', isset($ads->end_date) ? $ads->end_date->format('Y-m-d') : '', ['class' => 'input-sm form-control', 'disabled' => true]) !!}
        </div>
    </div>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::select('status', ['0' => 'Inactive', '1' => 'Active'], null, ['placeholder' => 'Select Status', 'class' => 'form-control', 'disabled' => true]) !!}
    </div>
</div>

<!-- Submit Field -->
<div class="form-group">
    <div class="col-sm-4 col-sm-offset-2">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('ads.index') !!}" class="btn btn-default">Cancel</a>
    </div>
</div>

@section('scripts')
<script src="{{ asset('js/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-fileinput/piexif.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-fileinput/sortable.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-fileinput/purify.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-fileinput/fileinput.min.js') }}"></script>
<script src="{{ asset('js/plugins/tinymce/jquery.tinymce.min.js') }}"></script>
<script src="{{ asset('js/plugins/tinymce/tinymce.min.js') }}"></script>
<script>


    var formType = '{{ $formType }}';
    $('#photo').val('');
    $('#data_5 .input-daterange').datepicker({
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true,
        format: 'yyyy-mm-dd'
    });

    $("#photo").fileinput({
        overwriteInitial: true,
        maxFileSize: 1024,
        showClose: false,
        showCaption: false,
        browseLabel: '',
        removeLabel: '',
        browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
        removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-photo-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        defaultPreviewContent: '<img src="{{ ($formType == 'edit' && $ads->image) ? asset($ads->image) : asset('img/upload.png') }}" alt="Your icon" style="width:160px">',
        layoutTemplates: {main2: '{preview} {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png", "jpeg", "gif"]
    });

    $("#video").fileinput({
        overwriteInitial: true,
        maxFileSize: 8192,
        showClose: false,
        showCaption: false,
        browseLabel: '',
        removeLabel: '',
        browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
        removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-photo-errors-2',
        msgErrorClass: 'alert alert-block alert-danger',
        defaultPreviewContent: '<img src="{{ ($formType == 'edit' && $ads->video) ? asset($ads->video) : asset('img/upload.png') }}" alt="Your icon" style="width:160px">',
        layoutTemplates: {main2: '{preview} {remove} {browse}'},
        allowedFileExtensions: ["mp4", "avi", "asf"]
    });

    tinymce.init({
        forced_root_block : "",
        selector: "textarea",
        setup: function (editor) {
            editor.on('change', function () {
                tinymce.triggerSave();
            });
        },
        file_picker_types: 'image',
        images_upload_credentials: true,
        automatic_uploads: false,
        theme: "modern",
        paste_data_images: true,
        plugins: [
        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars code fullscreen",
        "insertdatetime nonbreaking save table contextmenu directionality",
        "template paste textcolor colorpicker textpattern"
        ],
        toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | forecolor",
        image_advtab: true,
        file_picker_callback: function(cb, value, meta) {
            var input = document.createElement('input');
            input.setAttribute('type', 'file');
            input.setAttribute('accept', 'image/*');
            
            // Note: In modern browsers input[type="file"] is functional without 
            // even adding it to the DOM, but that might not be the case in some older
            // or quirky browsers like IE, so you might want to add it to the DOM
            // just in case, and visually hide it. And do not forget do remove it
            // once you do not need it anymore.
        
            input.onchange = function() {
              var file = this.files[0];
              
              var reader = new FileReader();
              reader.readAsDataURL(file);
              reader.onload = function () {
                // Note: Now we need to register the blob in TinyMCEs image blob
                // registry. In the next release this part hopefully won't be
                // necessary, as we are looking to handle it internally.
                var id = 'blobid' + (new Date()).getTime();
                var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
                var base64 = reader.result.split(',')[1];
                var blobInfo = blobCache.create(id, file, base64);
                blobCache.add(blobInfo);
        
                // call the callback and populate the Title field with the file name
                cb(blobInfo.blobUri(), { title: file.name });
              };
            };
            
            input.click();
        }
    });

    $('select[name="type"]').change(function(){
        if ($(this).val() == "1") {
            $('.image').hide();
            $('.video').show();
        }else{
            $('.image').show();
            $('.video').hide();
        }
    });

    var type = $('#type').val();
    if (type == "1") {
            $('.image').hide();
            $('.video').show();
        }else{
            $('.image').show();
            $('.video').hide();
        }
</script>
@if($formType == 'edit')
@include('layouts.datatables_js')
<script>
    $.fn.dataTable.ext.buttons.create = {
        action: function (e, dt, button, config) {
            $('#myModal').modal('show');
        }
    };

    var hash = document.location.hash;
    if (hash) {
        console.log(hash);
        $('.nav-tabs a[href="'+hash+'"]').tab('show')
    }

    // Change hash for page-reload
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
    window.location.hash = e.target.hash;
    });
</script>
@endif
@endsection