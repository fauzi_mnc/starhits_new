<div class='btn-group'>
    <a href="{{ route('category.edit', $id) }}" class='btn btn-default btn-xs' title="Edit Category">
        <i class="glyphicon glyphicon-edit"></i>
    </a>
</div>