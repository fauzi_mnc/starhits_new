@extends('layouts.crud')

@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Creators</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Creator</a>
            </li>
            <li class="active">
                <strong>Table</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
@endsection
@section('contentCrud')
@include('flash::message')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Creator</h5>
                    {{-- @if(auth()->user()->roles->first()->name === 'finance')
                        <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{{route('finance.creator.create')}}">Add New</a>
                    @else
                        <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{{route('creator.create')}}">Add New</a>
                    @endif --}}
                </div>
                <div class="ibox-content">
                    @include('creator.table')
                </div>
            </div>
        </div>
    </div>
@endsection
