@extends('layouts.crud')
@section('css')
<style>
.yellow-icon {
  color: #F9B100;
}

.red-icon {
  color: #e74c3c;
}

.mb-5 {
  margin-bottom: 5px;
}

.info-numbers {
  margin-top: 25px;
  font-size: 18px;
  font-weight: bold;
}

.info-text {
  margin-top: 7px;
  color: #e74c3c;
  font-weight: bold;
}

.info-percent-green {
  margin-top: 15px;
  font-size: 16px;
  color: #2ecc71;
}

.info-percent-red {
  margin-top: 15px;
  font-size: 16px;
  color: #e74c3c;
}

.male-wrapper {
  padding-left: 4px;
  margin-top: 25px;
}

.text-percent-females {
  font-size: 16px;
  margin-left: 13px;
}

.text-percent-males {
  font-size: 16px;
  margin-left: 13px;
}

.text-gender {
  font-size: 16px;
  margin-left: 18px;
}

.tabs-container .nav-tabs > li {
  float: none;
  display: inline-block;
}

#date {
  margin-bottom: 20px;
}

.panel-body .title {
  font-size: 14px;
}

.panel-body .number {
  font-size: 20px;
  font-weight: bold;
  color: #F9B100;
}


</style>
@endsection
@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Analytic</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Analytic</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
@endsection
@section('contentCrud')
@include('flash::message')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>DISPLAY ANALYTICS</h5>
                </div>
                <div class="ibox-content">
                  <div class="row" style="margin-top: 20px">
                    <div class="col-sm-3">
                        <select id="date" class="form-control" required="required" name="date">
                          <option value="week">Last 7 Days</option>
                          <option value="twoweeks">Last 14 Days</option>
                          <option value="thirtydays">Last 30 Days</option>
                        </select>
                      </div>
                    <div class="col-lg-12">
                      <div class="tabs-container">
                          <ul class="nav nav-tabs text-center">
                              <li class="active"><a data-toggle="tab" href="#tab-1">OVERVIEW</a></li>
                              <li class=""><a data-toggle="tab" href="#tab-2">TRAFFIC</a></li>
                              <li class=""><a data-toggle="tab" href="#tab-3">ENGANGEMENT</a></li>
                              <li class="hide"><a data-toggle="tab" href="#tab-4">DEMOGRAPHICS</a></li>
                          </ul>
                          <div class="tab-content">
                              <div id="tab-1" class="tab-pane active">
                                  <div class="panel-body">
                                    <div class="row">
                                      <div class="col-md-3">
                                          <div class="panel panel-success">
                                              <div class="panel-heading">
                                                  TRAFFIC
                                              </div>
                                              <div class="panel-body">
                                                <div class="icon-wrapper">
                                                @for ($i = 0; $i < 4; $i++)
                                                    <i class="fa fa-male fa-2x mb-5 yellow-icon"></i>
                                                    <i class="fa fa-male fa-2x mb-5 yellow-icon"></i>
                                                    <i class="fa fa-female fa-2x mb-5 yellow-icon"></i>
                                                    <i class="fa fa-female fa-2x mb-5 yellow-icon"></i>
                                                    <i class="fa fa-female fa-2x mb-5 yellow-icon"></i>
                                                    <i class="fa fa-female fa-2x mb-5 yellow-icon"></i>
                                                    <i class="fa fa-male fa-2x mb-5 yellow-icon"></i>
                                                    <i class="fa fa-male fa-2x mb-5 yellow-icon"></i>
                                                @endfor
                                                </div>
                                                <div class="info-text-wrapper text-center">
                                                  <div id="traffic-numbers" class="info-numbers">
                                                    {{$data->total_views_info}}
                                                  </div>
                                                  <div class="info-text">
                                                    VIEWS
                                                  </div>
                                                  @if($data->percent_views >= 0)
                                                    <div class="info-percent-green">
                                                      <i class="fa fa-arrow-up"></i> {{$data->percent_views}}%
                                                    </div>
                                                  @else
                                                    <div class="info-percent-red">
                                                      <i class="fa fa-arrow-down"></i> {{$data->percent_views}}%
                                                    </div>
                                                  @endif
                                                  <div class="info-date">
                                                    {{$data->info}}
                                                  </div>
                                                </div>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="col-md-3">
                                          <div class="panel panel-success">
                                              <div class="panel-heading">
                                                  ENGAGEMENT
                                              </div>
                                              <div class="panel-body">
                                                  <div id="chart-overview-engagement" style="height:200px;">
                                                  </div>
                                                  <div class="info-text-wrapper text-center">
                                                  <div id="interactions-numbers" class="info-numbers">
                                                    {{$data->total_interactions_info}}
                                                  </div>
                                                  <div class="info-text">
                                                    INTERACTIONS
                                                  </div>
                                                  @if($data->percent_interactions >= 0)
                                                    <div class="info-percent-green">
                                                      <i class="fa fa-arrow-up"></i> {{$data->percent_interactions}}%
                                                    </div>
                                                  @else
                                                    <div class="info-percent-red">
                                                      <i class="fa fa-arrow-down"></i> {{$data->percent_interactions}}%
                                                    </div>
                                                  @endif
                                                  <div class="info-date">
                                                  {{$data->info}}
                                                  </div>
                                                </div>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="col-md-3">
                                          <div class="panel panel-success">
                                              <div class="panel-heading">
                                                  FOLLOWERS
                                              </div>
                                              <div class="panel-body">
                                                  <div class="icon-wrapper">
                                                @for ($i = 0; $i < 4; $i++)
                                                    <i class="fa fa-male fa-2x mb-5 yellow-icon"></i>
                                                    <i class="fa fa-male fa-2x mb-5 yellow-icon"></i>
                                                    <i class="fa fa-female fa-2x mb-5 yellow-icon"></i>
                                                    <i class="fa fa-female fa-2x mb-5 yellow-icon"></i>
                                                    <i class="fa fa-female fa-2x mb-5 yellow-icon"></i>
                                                    <i class="fa fa-female fa-2x mb-5 yellow-icon"></i>
                                                    <i class="fa fa-male fa-2x mb-5 yellow-icon"></i>
                                                    <i class="fa fa-male fa-2x mb-5 yellow-icon"></i>
                                                @endfor
                                                </div>
                                                <div class="info-text-wrapper text-center">
                                                  <div id="followers-numbers" class="info-numbers">
                                                    {{$data->total_followers_info}}
                                                  </div>
                                                  <div class="info-text">
                                                    <span class="yellow-icon">FOLLOWERS</span>
                                                  </div>
                                                  <div class="info-percent">
                                                    @if($data->percent_followers >= 0)
                                                    <div class="info-percent-green">
                                                      <i class="fa fa-arrow-up"></i> {{$data->percent_followers}}%
                                                    </div>
                                                  @else
                                                    <div class="info-percent-red">
                                                      <i class="fa fa-arrow-down"></i> {{$data->percent_followers}}%
                                                    </div>
                                                  @endif
                                                  </div>
                                                  <div class="info-date">
                                                    {{$data->info}}
                                                  </div>
                                                </div>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="col-md-3">
                                          <div class="panel panel-success">
                                              <div class="panel-heading">
                                                  AUDIENCE
                                              </div>
                                              <div class="panel-body">
                                                  <div class="male-female-wrapper">
                                                    <div class="female-wrapper">
                                                      <i class="fa fa-female fa-4x red-icon"></i> <span class="text-percent-females">{{$data->percent_females}}%</span> <span class="text-gender">Females</span>
                                                    </div>
                                                    <div class="male-wrapper">
                                                      <i class="fa fa-male fa-4x yellow-icon"></i>  <span class="text-percent-males" style="margin-left:22px;">{{$data->percent_males}}%</span> <span class="text-gender">Males</span>
                                                    </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <div class="col-md-6">
                                          <div class="panel panel-default">
                                              <div class="panel-body">
                                                  <div id="chart-overview-subscribers">
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="col-md-6">
                                          <div class="panel panel-default">
                                              <div class="panel-body">
                                                  <div class="col-md-6">
                                                    <div class="text-center">
                                                      <b>VIEWS BY PLATFORM</b>
                                                    </div>
                                                    <div id="chart-overview-v-platforms">
                                                    </div>
                                                  </div>
                                                  <div class="col-md-6">
                                                    <div class="text-center">
                                                      <b>SUBSCRIBERS BY PLATFORM</b>
                                                    </div>
                                                    <div id="chart-overview-s-platforms">
                                                    </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <div class="col-md-2">
                                          <div class="panel panel-default">
                                              <div class="panel-body">
                                                  <span class="title">Shares / video</span>
                                                  <br>
                                                  <span id="shares-videos" class="number">
                                                    {{$data->shares_videos}}
                                                  </span>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="col-md-2">
                                          <div class="panel panel-default">
                                              <div class="panel-body">
                                                  <span class="title">Likes / video</span>
                                                  <br>
                                                  <span id="likes-videos" class="number">{{$data->likes_videos}}</span>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="col-md-2">
                                          <div class="panel panel-default">
                                              <div class="panel-body">
                                                  <span class="title">Comments / video</span>
                                                  <br>
                                                  <span id="comments-videos" class="number">{{$data->comments_videos}}</span>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="col-md-2">
                                          <div class="panel panel-default">
                                              <div class="panel-body">
                                                  <span class="title">Shares / subscriber</span>
                                                  <br>
                                                  <span id="shares-subs" class="number">{{$data->shares_subs}}</span>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="col-md-2">
                                          <div class="panel panel-default">
                                              <div class="panel-body">
                                                  <span class="title">Likes / subscriber</span>
                                                  <br>
                                                  <span id="likes-subs" class="number">{{$data->likes_subs}}</span>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="col-md-2">
                                          <div class="panel panel-default">
                                              <div class="panel-body">
                                                  <span class="title">Comments / subscriber</span>
                                                  <br>
                                                  <span id="comments-subs" class="number">{{$data->comments_subs}}</span>
                                              </div>
                                          </div>
                                      </div>
                                    </div>
                                    <!-- <div class="row">
                                      <div class="col-md-6">
                                          <div class="panel panel-default">
                                              <div class="panel-heading">
                                                MOST VIEWED VIDEOS
                                              </div>
                                              <div class="panel-body">
                                                  <div class="col-md-12 mb-5">
                                                    <div class="row">
                                                      <div class="col-md-12">
                                                      facebook
                                                      </div>
                                                      <div class="col-md-1">
                                                      1
                                                      </div>
                                                      <div class="col-md-3">
                                                      foto
                                                      </div>
                                                      <div class="col-md-5">
                                                      title etc.
                                                      </div>
                                                      <div class="col-md-3">
                                                      views 1k
                                                      </div>
                                                    </div>
                                                    <hr>
                                                    <div class="row">
                                                      <div class="col-md-4 text-center">
                                                        Views 1k
                                                      </div>
                                                      <div class="col-md-4 text-center">
                                                        Engagement 442
                                                      </div>
                                                      <div class="col-md-4 text-center">
                                                        Eng/View: 38.7%
                                                      </div>
                                                    </div>
                                                  </div>
                                                  <div class="col-md-12">
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt est vitae ultrices accumsan. Aliquam ornare lacus adipiscing, posuere lectus et, fringilla augue.</p>
                                                  </div>
                                                  <div class="col-md-12">
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt est vitae ultrices accumsan. Aliquam ornare lacus adipiscing, posuere lectus et, fringilla augue.</p>
                                                  </div>
                                                  <div class="col-md-12">
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt est vitae ultrices accumsan. Aliquam ornare lacus adipiscing, posuere lectus et, fringilla augue.</p>
                                                  </div>
                                                  <div class="col-md-12">
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt est vitae ultrices accumsan. Aliquam ornare lacus adipiscing, posuere lectus et, fringilla augue.</p>
                                                  </div>
                                                  <div class="col-md-12">
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt est vitae ultrices accumsan. Aliquam ornare lacus adipiscing, posuere lectus et, fringilla augue.</p>
                                                  </div>
                                              </div>

                                          </div>
                                      </div>
                                      <div class="col-md-6">
                                          <div class="panel panel-default">
                                            <div class="panel-heading">
                                              MOST ENGAGEMENT VIDEOS
                                            </div>
                                            <div class="panel-body">
                                                <div class="col-md-12">
                                                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt est vitae ultrices accumsan. Aliquam ornare lacus adipiscing, posuere lectus et, fringilla augue.</p>
                                                </div>
                                                <div class="col-md-12">
                                                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt est vitae ultrices accumsan. Aliquam ornare lacus adipiscing, posuere lectus et, fringilla augue.</p>
                                                </div>
                                                <div class="col-md-12">
                                                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt est vitae ultrices accumsan. Aliquam ornare lacus adipiscing, posuere lectus et, fringilla augue.</p>
                                                </div>
                                                <div class="col-md-12">
                                                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt est vitae ultrices accumsan. Aliquam ornare lacus adipiscing, posuere lectus et, fringilla augue.</p>
                                                </div>
                                                <div class="col-md-12">
                                                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt est vitae ultrices accumsan. Aliquam ornare lacus adipiscing, posuere lectus et, fringilla augue.</p>
                                                </div>
                                                <div class="col-md-12">
                                                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt est vitae ultrices accumsan. Aliquam ornare lacus adipiscing, posuere lectus et, fringilla augue.</p>
                                                </div>
                                            </div>
                                          </div>
                                      </div>
                                    </div>  -->       
                                  </div>
                              </div>
                              <div id="tab-2" class="tab-pane">
                                  <div class="panel-body">
                                      <div class="row">
                                        <div class="col-md-6">
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <div id="chart-traffic-subscribers">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <div class="col-md-6">
                                                      <div class="text-center">
                                                        <b>VIEWS BY PLATFORM</b>
                                                      </div>
                                                      <div id="chart-traffic-v-platforms">
                                                      </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                      <div class="text-center">
                                                        <b>SUBSCRIBERS BY PLATFORM</b>
                                                      </div>
                                                      <div id="chart-traffic-s-platforms">
                                                      </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                          <div class="panel panel-default">
                                              <div class="panel-body">
                                                <div id="chart-traffic-subscribers-evo">
                                                </div>
                                              </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                          <div class="panel panel-default">
                                              <div class="panel-body">
                                                  <div id="chart-traffic-viewers-evo">
                                                  </div>
                                              </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                          <div class="panel panel-default">
                                              <div class="panel-body">
                                                <div class="col-md-6">
                                                  <div id="chart-traffic-estimated-minutes">
                                                  </div>
                                                </div>
                                                <div class="col-md-6">
                                                  <div id="chart-traffic-average-view">
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                        </div>
                                    </div>
                                  </div>
                              </div>
                              <div id="tab-3" class="tab-pane">
                                <div class="panel-body">
                                  <div class="row">
                                    <div class="col-md-12">
                                      <div class="panel panel-default">
                                        <div class="panel-body">
                                          <div id="chart-engagement-all-interactions">
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="panel panel-default">
                                        <div class="panel-body">
                                          <div id="chart-engagement-shares">
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="panel panel-default">
                                        <div class="panel-body">
                                          <div id="chart-engagement-likes">
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="panel panel-default">
                                        <div class="panel-body">
                                          <div id="chart-engagement-subscribers">
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="panel panel-default">
                                        <div class="panel-body">
                                          <div id="chart-engagement-comments">
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-12">
                                      <div class="panel panel-default">
                                        <div class="panel-body">
                                          <div id="chart-engagement-annotation-impressions">
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-12">
                                      <div class="panel panel-default">
                                        <div class="panel-body">
                                          <div id="chart-engagement-annotation-click-close-impressions">
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-12">
                                      <div class="panel panel-default">
                                        <div class="panel-body">
                                          <div id="chart-engagement-annotation-click-close">
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-12">
                                      <div class="panel panel-default">
                                        <div class="panel-body">
                                          <div id="chart-engagement-annotation-through-close-rate">
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div id="tab-4" class="tab-pane">
                                  <div class="panel-body">
                                      <strong>Donec quam felis</strong>

                                      <p>Thousand unknown plants are noticed by me: when I hear the buzz of the little world among the stalks, and grow familiar with the countless indescribable forms of the insects
                                          and flies, then I feel the presence of the Almighty, who formed us in his own image, and the breath </p>

                                      <p>I am alone, and feel the charm of existence in this spot, which was created for the bliss of souls like mine. I am so happy, my dear friend, so absorbed in the exquisite
                                          sense of mere tranquil existence, that I neglect my talents. I should be incapable of drawing a single stroke at the present moment; and yet.</p>
                                  </div>
                              </div>
                          </div>
                     </div>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
<script src="https://apis.google.com/js/api.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="{{ asset('js/plugins/moment.min.js') }}"></script>
<script>
    
    $(document).ready(function(){
        var start_date, end_date, dates, total_shares, total_comments, total_likes, total_views, total_interactions, total_followers, estimate_minutes_watched, average_view_duration, all_interactions, shares, likes, comments, annotation_impressions, click_impressions, close_impressions, annotation_click, annotation_close,annotation_click_through_rate, annotation_close_rate, total_likes_yt, total_likes_dm, total_views_yt, total_views_dm, total_followers_yt,total_followers_dm, views_yt, views_dm, subscribers_yt, subscribers_dm, all_interactions_yt, all_interactions_dm, likes_yt, likes_dm;

        prepareDataChart();
        makeStartEndDate();
        callChart();
        initBindings();
        
    });
  
    function prepareDataChart(){
      total_likes_yt = {{$data->total_likes_yt}};
      total_views_yt = {{$data->total_views_yt}};
      total_followers_yt = {{$data->total_followers_yt}};
      total_likes_dm = {{$data->total_likes_dm}};
      total_views_dm = {{$data->total_views_dm}};
      total_followers_dm = {{$data->total_followers_dm}};
      total_shares = {{$data->total_shares_yt+$data->total_shares_dm}};
      total_comments = {{$data->total_comments_yt+$data->total_comments_dm}};
      total_likes = {{$data->total_likes_yt+$data->total_likes_dm}};
      total_views = {{$data->total_views_yt+$data->total_views_dm}};
      total_followers = {{$data->total_followers_yt+$data->total_followers_dm}};
      dates = {!!json_encode($data->dates)!!};
      views_yt = {!!json_encode($data->views_yt)!!};
      subscribers_yt = {!!json_encode($data->subscribers_yt)!!};
      views_dm = {!!json_encode($data->views_dm)!!};
      subscribers_dm = {!!json_encode($data->subscribers_dm)!!};
      estimate_minutes_watched = {!!json_encode($data->estimate_minutes_watched_yt)!!};
      average_view_duration = {!!json_encode($data->average_view_duration_yt)!!};
      all_interactions = {!!json_encode($data->all_interactions_yt)!!};
      all_interactions_yt = {!!json_encode($data->all_interactions_yt)!!};
      all_interactions_dm = {!!json_encode($data->all_interactions_dm)!!};
      shares = {!!json_encode($data->shares_yt)!!};
      likes = {!!json_encode($data->likes_yt)!!};
      likes_yt = {!!json_encode($data->likes_yt)!!};
      likes_dm = {!!json_encode($data->likes_dm)!!};
      comments = {!!json_encode($data->comments_yt)!!};
      annotation_impressions = {!!json_encode($data->annotation_impressions_yt)!!};
      click_impressions = {!!json_encode($data->click_impressions_yt)!!};
      close_impressions = {!!json_encode($data->close_impressions_yt)!!};
      annotation_click = {!!json_encode($data->annotation_click_yt)!!};
      annotation_close = {!!json_encode($data->annotation_close_yt)!!};
      annotation_click_through_rate = {!!json_encode($data->annotation_click_through_rate_yt)!!};
      annotation_close_rate = {!!json_encode($data->annotation_close_rate_yt)!!};
    }

    function makeStartEndDate(){
      start_date = moment().subtract(2, 'months').format("YYYY-MM-DD");
      end_date = moment().format("YYYY-MM-DD");
    }

    function callChart(){
      chartOverviewEngagement();
      chartOverviewSubscribers();
      chartOverviewVPlatforms();
      chartOverviewSPlatforms();
      chartTrafficSubscribers();
      chartTrafficVPlatforms();
      chartTrafficSPlatforms();
      chartTrafficSubscribersEvo();
      chartTrafficViewersEvo();
      chartTrafficEstimated();
      chartTrafficAverage();
      chartEngagementAllInteractions();
      chartEngagementShares();
      chartEngagementLikes();
      chartEngagementSubscribers();
      chartEngagementComments();
      chartEngagementAnnotationImpressions();
      chartEngagementAnnotationClickCloseImpressions();
      chartEngagementAnnotationClickClose();
      chartEngagementAnnotationThroughCloseRate();
    }

    //chart
    function chartOverviewEngagement(){
      
      // Build the chart
      Highcharts.chart('chart-overview-engagement', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        colors: ['#bdc3c7', '#f1c40f', '#c0392b'],
        exporting: {
          buttons: {
            contextButton: {
              enabled: false
            }    
          }
        },
        title: {
            text: null
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.y}</b>'
        },
        plotOptions: {
          pie: {
            dataLabels: {
                enabled: false
            },
            showInLegend: true
          },
        },
        series: [{
            name: 'Interactions',
            colorByPoint: true,
            data: [{
                name: 'Shares',
                y: total_shares
            }, {
                name: 'Comments',
                y: total_comments
            }, {
                name: 'Likes',
                y: total_likes
            }]
        }],
        credits: {
          enabled: false
        },
      });
    }

    function chartOverviewSubscribers(){


      Highcharts.chart('chart-overview-subscribers', {
          chart: {
              type: 'spline',
              scrollablePlotArea: {
                  scrollPositionX: 1
              }
          },
          colors: ['#f1c40f', '#c0392b'],
          exporting: {
            buttons: {
              contextButton: {
                enabled: false
              }    
            }
          },
          credits: {
            enabled: false
          },
          title: {
              text: 'VIEWS AND SUBSCRIBERS EVOLUTION'
          },
          xAxis: {
              categories: dates
          },
          yAxis: {
              title: {
                  text: null
              },
              minorGridLineWidth: 0,
              gridLineWidth: 0,
              alternateGridColor: null,
          },
          series: [{
              name: 'View per video',
              data: views_yt

          }, {
              name: 'View per subscriber',
              data: subscribers_yt
          }]
      });
    }

    function chartOverviewVPlatforms(){

      Highcharts.setOptions({
          colors: ['#ee5253', '#48dbfb', '#54a0ff', '#2e86de']
      });

      Highcharts.chart('chart-overview-v-platforms', {
          chart: {
              plotBackgroundColor: null,
              plotBorderWidth: 0,
              plotShadow: false
          },
          exporting: {
            buttons: {
              contextButton: {
                enabled: false
              }    
            }
          },
          credits: {
            enabled: false
          },
          title: {
              text: total_views,
              align: 'center',
              verticalAlign: 'middle',
              y: -20
          },
          tooltip: {
              pointFormat: '{point.y}'
          },
          plotOptions: {
            pie: {
              allowPointSelect: false,
              cursor: 'pointer',
              dataLabels: {
                  enabled: false
              },
              showInLegend: true
            }
          },
          series: [{
              type: 'pie',
              innerSize: '70%',
              data: [{
              name: 'Youtube',
              y: total_views_yt,
              dataLabels: {
                  enabled: false
                  }
              },
              {
              name: 'Dailymotion',
              y: total_views_dm,
              dataLabels: {
                  enabled: false
                  }
              }]
          }]
      });
    }

    function chartOverviewSPlatforms(){

      Highcharts.chart('chart-overview-s-platforms', {
          chart: {
              plotBackgroundColor: null,
              plotBorderWidth: 0,
              plotShadow: false
          },
          exporting: {
            buttons: {
              contextButton: {
                enabled: false
              }    
            }
          },
          credits: {
            enabled: false
          },
          title: {
              text: total_followers,
              align: 'center',
              verticalAlign: 'middle',
              y: -20
          },
          tooltip: {
              pointFormat: '{point.y}'
          },
            plotOptions: {
              pie: {
                  allowPointSelect: true,
                  cursor: 'pointer',
                  dataLabels: {
                      enabled: false
                  },
                  showInLegend: true
              }
          },
          series: [{
            type: 'pie',
            innerSize: '70%',
            data: [{
              name: 'Youtube',
              y: total_followers_yt,
              dataLabels: {
                  enabled: false
                  }
              },
              {
              name: 'Dailymotion',
              y: total_followers_dm,
              dataLabels: {
                  enabled: false
                  }
              }]
          }]
      });
    }

    function chartTrafficSubscribers(){

      Highcharts.chart('chart-traffic-subscribers', {
          chart: {
              type: 'spline',
              scrollablePlotArea: {
                  scrollPositionX: 1
              }
          },
          colors: ['#f1c40f', '#c0392b'],
          exporting: {
            buttons: {
              contextButton: {
                enabled: false
              }    
            }
          },
          credits: {
            enabled: false
          },
          title: {
              text: 'VIEWS AND SUBSCRIBERS EVOLUTION'
          },
          xAxis: {
              categories: dates
          },
          yAxis: {
              title: {
                  text: null
              },
              minorGridLineWidth: 0,
              gridLineWidth: 0,
              alternateGridColor: null,
          },
          series: [{
              name: 'View per video',
              data: views_yt

          }, {
              name: 'View per subscriber',
              data: subscribers_yt
          }]
      });
    }

    function chartTrafficVPlatforms(){

      Highcharts.setOptions({
          colors: ['#ee5253', '#48dbfb', '#54a0ff', '#2e86de']
      });

      Highcharts.chart('chart-traffic-v-platforms', {
          chart: {
              plotBackgroundColor: null,
              plotBorderWidth: 0,
              plotShadow: false
          },
          exporting: {
            buttons: {
              contextButton: {
                enabled: false
              }    
            }
          },
          credits: {
            enabled: false
          },
          title: {
              text: total_views,
              align: 'center',
              verticalAlign: 'middle',
              y: -20
          },
          tooltip: {
              pointFormat: '{point.y}'
          },
          plotOptions: {
            pie: {
              allowPointSelect: true,
              cursor: 'pointer',
              dataLabels: {
                  enabled: false
              },
              showInLegend: true
            }
          },
          series: [{
              type: 'pie',
              innerSize: '70%',
              data: [{
              name: 'Youtube',
              y: total_views_yt,
              dataLabels: {
                  enabled: false
                  }
              },
              {
              name: 'Dailymotion',
              y: total_views_dm,
              dataLabels: {
                  enabled: false
                  }
              }]
          }]
      });
    }

    function chartTrafficSPlatforms(){
      Highcharts.setOptions({
          colors: ['#ee5253', '#48dbfb', '#54a0ff', '#2e86de']
      });

      Highcharts.chart('chart-traffic-s-platforms', {
          chart: {
              plotBackgroundColor: null,
              plotBorderWidth: 0,
              plotShadow: false
          },
          exporting: {
            buttons: {
              contextButton: {
                enabled: false
              }    
            }
          },
          credits: {
            enabled: false
          },
          title: {
              text: total_followers,
              align: 'center',
              verticalAlign: 'middle',
              y: -20
          },
          tooltip: {
              pointFormat: '{point.y}'
          },
            plotOptions: {
              pie: {
                  allowPointSelect: true,
                  cursor: 'pointer',
                  dataLabels: {
                      enabled: false
                  },
                  showInLegend: true
              }
          },
          series: [{
            type: 'pie',
            innerSize: '70%',
            data: [{
              name: 'Youtube',
              y: total_followers_yt,
              dataLabels: {
                  enabled: false
                  }
              },
              {
              name: 'Dailymotion',
              y: total_followers_dm,
              dataLabels: {
                  enabled: false
                  }
              }]
          }]
      });
    }

    function chartTrafficSubscribersEvo(){

      Highcharts.chart('chart-traffic-subscribers-evo', {
          chart: {
              type: 'column'
          },
          credits: {
            enabled: false
          },
          exporting: {
            buttons: {
              contextButton: {
                enabled: false
              }    
            }
          },
          title: {
              text: 'SUBSCRIBERS EVOLUTION'
          },
          xAxis: {
              categories: dates
          },
          yAxis: {
              min: 0,
              title: {
                  text: null
              }
          },
          tooltip: {
              pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b><br/>',
              shared: true
          },
          series: [{
              name: 'Youtube',
              data: subscribers_yt
          },
          {
              name: 'Dailymotion',
              data: subscribers_dm
          }]
      });
    }

    function chartTrafficViewersEvo(){

      Highcharts.chart('chart-traffic-viewers-evo', {
          chart: {
              type: 'column'
          },
          credits: {
            enabled: false
          },
          exporting: {
            buttons: {
              contextButton: {
                enabled: false
              }    
            }
          },
          title: {
              text: 'VIEWERS EVOLUTION'
          },
          xAxis: {
              categories: dates
          },
          yAxis: {
              min: 0,
              title: {
                  text: null
              }
          },
          tooltip: {
              pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b><br/>',
              shared: true
          },
          series: [{
              name: 'Youtube',
              data: views_yt
          },
          {
              name: 'Dailymotion',
              data: views_dm
          }]
      });
    }

    function chartTrafficEstimated(){
      Highcharts.chart('chart-traffic-estimated-minutes', {
          chart: {
              type: 'spline',
              scrollablePlotArea: {
                  scrollPositionX: 1
              }
          },
          exporting: {
            buttons: {
              contextButton: {
                enabled: false
              }    
            }
          },
          credits: {
            enabled: false
          },
          title: {
              text: 'ESTIMATED MINUTES WATCHED'
          },
          subtitle: {
            text: 'The numbers of minutes that users watched videos' 
          },
          xAxis: {
              categories: dates
          },
          yAxis: {
              title: {
                  text: null
              },
              minorGridLineWidth: 0,
              gridLineWidth: 0,
              alternateGridColor: null,
          },
          series: [{
              name: 'Minutes',
              data: estimate_minutes_watched

          }]
      });
    }

    function chartTrafficAverage(){
      Highcharts.chart('chart-traffic-average-view', {
          chart: {
              type: 'spline',
              scrollablePlotArea: {
                  scrollPositionX: 1
              }
          },
          exporting: {
            buttons: {
              contextButton: {
                enabled: false
              }    
            }
          },
          credits: {
            enabled: false
          },
          title: {
              text: 'AVERAGE VIEW DURATION'
          },
          subtitle: {
              text: 'The average length in seconds of video playbacks'
          },
          xAxis: {
              categories: dates
          },
          yAxis: {
              title: {
                  text: null
              },
              minorGridLineWidth: 0,
              gridLineWidth: 0,
              alternateGridColor: null,
          },
          series: [{
              name: 'Seconds',
              data: average_view_duration
          }]
      });
    }

    function chartEngagementAllInteractions(){

      Highcharts.chart('chart-engagement-all-interactions', {
          chart: {
              type: 'column'
          },
          credits: {
            enabled: false
          },
          exporting: {
            buttons: {
              contextButton: {
                enabled: false
              }    
            }
          },
          title: {
              text: 'ALL INTERACTIONS'
          },
          xAxis: {
              categories: dates
          },
          yAxis: {
              min: 0,
              title: {
                  text: null
              }
          },
          tooltip: {
              pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b><br>',
              shared: true
          },
          series: [{
              name: 'Youtube',
              data: all_interactions_yt
          },
          {
              name: 'Dailymotion',
              data: all_interactions_dm
          }]
      });
    }

    function chartEngagementShares(){

      Highcharts.chart('chart-engagement-shares', {
          chart: {
              type: 'column'
          },
          credits: {
            enabled: false
          },
          exporting: {
            buttons: {
              contextButton: {
                enabled: false
              }    
            }
          },
          title: {
              text: 'SHARES'
          },
          xAxis: {
              categories: dates
          },
          yAxis: {
              min: 0,
              title: {
                  text: null
              }
          },
          tooltip: {
              pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b>',
              shared: true
          },
          series: [{
              name: 'Youtube',
              data: shares
          }]
      });
    }

    function chartEngagementLikes(){

      Highcharts.chart('chart-engagement-likes', {
          chart: {
              type: 'column'
          },
          credits: {
            enabled: false
          },
          exporting: {
            buttons: {
              contextButton: {
                enabled: false
              }    
            }
          },
          title: {
              text: 'LIKES'
          },
          xAxis: {
              categories: dates
          },
          yAxis: {
              min: 0,
              title: {
                  text: null
              }
          },
          tooltip: {
              pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b><br>',
              shared: true
          },
          series: [{
              name: 'Youtube',
              data: likes_yt
          },
          {
              name: 'Dailymotion',
              data: likes_dm
          }]
      });
    }

    function chartEngagementSubscribers(){

      Highcharts.chart('chart-engagement-subscribers', {
          chart: {
              type: 'column'
          },
          credits: {
            enabled: false
          },
          exporting: {
            buttons: {
              contextButton: {
                enabled: false
              }    
            }
          },
          title: {
              text: 'SUBSCRIBERS'
          },
          xAxis: {
              categories: dates
          },
          yAxis: {
              min: 0,
              title: {
                  text: null
              }
          },
          tooltip: {
              pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b><br>',
              shared: true
          },
          series: [{
              name: 'Youtube',
              data: subscribers_yt
          },
          {
              name: 'Dailymotion',
              data: subscribers_dm
          }]
      });
    }

    function chartEngagementComments(){

      Highcharts.chart('chart-engagement-comments', {
          chart: {
              type: 'column'
          },
          credits: {
            enabled: false
          },
          exporting: {
            buttons: {
              contextButton: {
                enabled: false
              }    
            }
          },
          title: {
              text: 'COMMENTS'
          },
          xAxis: {
              categories: dates
          },
          yAxis: {
              min: 0,
              title: {
                  text: null
              }
          },
          tooltip: {
              pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b>',
              shared: true
          },
          series: [{
              name: 'Youtube',
              data: comments
          }]
      });
    }

    function chartEngagementAnnotationImpressions(){
      Highcharts.chart('chart-engagement-annotation-impressions', {
          chart: {
              type: 'spline',
              scrollablePlotArea: {
                  scrollPositionX: 1
              }
          },
          exporting: {
            buttons: {
              contextButton: {
                enabled: false
              }    
            }
          },
          credits: {
            enabled: false
          },
          title: {
            text: 'ANNOTATION IMPRESSIONS'
          },
          subtitle: {
            text: 'The total numbers of annotation impressions' 
          },
          xAxis: {
              categories: dates
          },
          yAxis: {
              title: {
                  text: null
              },
              minorGridLineWidth: 0,
              gridLineWidth: 0,
              alternateGridColor: null,
          },
          series: [{
              name: 'Annotation Impressions',
              data: annotation_impressions
          }]
      });
    }

    function chartEngagementAnnotationClickCloseImpressions(){
      Highcharts.chart('chart-engagement-annotation-click-close-impressions', {
          chart: {
              type: 'spline',
              scrollablePlotArea: {
                  scrollPositionX: 1
              }
          },
          exporting: {
            buttons: {
              contextButton: {
                enabled: false
              }    
            }
          },
          colors: ['#54a0ff', '#ff9f43'],
          credits: {
            enabled: false
          },
          title: {
              text: 'ANNOTATION CLICK AND CLOSE IMPRESSIONS'
          },
          subtitle: {
            text: 'The number of annotations that appeared and could be clicked or closed' 
          },
          xAxis: {
              categories: dates
          },
          yAxis: [{
            min: 0,
            title: {
                text: 'Annotation Clickable Impressions',
                style: {
                  color: '#54a0ff'
                }
              }
            }, {
            min: 0,
            title: {
              text: 'Annotation Closeable Impressions',
              style: {
                color: '#ff9f43'
              }
            },
            opposite: true
          }],
          series: [{
              name: 'Annotation Clickable Impressions',
              data: click_impressions
          }, {
              name: 'Annotation Closeable Impressions',
              data: close_impressions,
              yAxis: 1
          }]
      });
    }

    function chartEngagementAnnotationClickClose(){
      Highcharts.chart('chart-engagement-annotation-click-close', {
          chart: {
              type: 'spline',
              scrollablePlotArea: {
                  scrollPositionX: 1
              }
          },
          colors: ['#1dd1a1', '#ee5253'],
          exporting: {
            buttons: {
              contextButton: {
                enabled: false
              }    
            }
          },
          credits: {
            enabled: false
          },
          title: {
              text: 'ANNOTATION CLICKS AND CLOSES'
          },
          subtitle: {
            text: 'The number of clicked and closed annotations' 
          },
          xAxis: {
              categories: dates
          },
          yAxis: [{
            min: 0,
            title: {
                text: 'Annotation Clicks',
                style: {
                  color: '#1dd1a1'
                }
              }
            }, {
            min: 0,
            title: {
              text: 'Annotation Closes',
              style: {
                color: '#ee5253'
              }
            },
            opposite: true
          }],
          series: [{
              name: 'Annotation Clicks',
              data: annotation_click
          }, {
              name: 'Annotation Closes',
              data: annotation_close,
              yAxis: 1
          }]
      });
    }

    function chartEngagementAnnotationThroughCloseRate(){
      Highcharts.chart('chart-engagement-annotation-through-close-rate', {
          chart: {
              type: 'spline',
              scrollablePlotArea: {
                  scrollPositionX: 1
              }
          },
          colors: ['#10ac84', '#48dbfb'],
          exporting: {
            buttons: {
              contextButton: {
                enabled: false
              }    
            }
          },
          credits: {
            enabled: false
          },
          title: {
              text: 'ANNOTATION CLICK THROUGH AND CLOSE RATE'
          },
          subtitle: {
            text: 'The ratio of annotations that viewers clicked or closed to the total number of clickable annotation impressions' 
          },
          xAxis: {
              categories: dates
          },
          yAxis: [{
            min: 0,
            title: {
                text: 'Annotation Click Thorugh Rate',
                style: {
                  color: '#10ac84'
                }
              }
            }, {
            min: 0,
            title: {
              text: 'Annotation Close Rate',
              style: {
                color: '#48dbfb'
              }
            },
            opposite: true
          }],
          series: [{
              name: 'Annotation Click Through Rate',
              data: annotation_click_through_rate
          }, {
              name: 'Annotation Close Rate',
              data: annotation_close_rate,
              yAxis: 1
          }]
      });
    }
    
    function getDataAnalytics(_type){
      $.ajax({
          url: "{{ route('creator.analytic.get') }}",
          method: "GET",
          data: {
            type : _type
          },
          headers: {
              'X-CSRF-TOKEN': '{{ csrf_token() }}'
          },
          success: function(response){
            toastr.success("Mengambil data berhasil.");
            var data = response.result;
            console.log(data);
            $('#traffic-numbers').text(data.total_views_info);
            $('#followers-numbers').text(data.total_followers_info);
            $('#interactions-numbers').text(data.total_interactions_info);
            $('.info-date').text(data.info);
            $('.text-percent-females').text(data.percent_females+'%');
            $('.text-percent-males').text(data.percent_males+'%');
            $('#shares-videos').text(data.shares_videos);
            $('#likes-videos').text(data.likes_videos);
            $('#comments-videos').text(data.comments_videos);
            $('#shares-subs').text(data.shares_subs);
            $('#likes-subs').text(data.likes_subs);
            $('#comments-subs').text(data.comments_subs);
            total_shares = data.total_shares_yt + data.total_shares_dm;
            total_comments = data.total_comments_yt + data.total_comments_dm;
            total_likes = data.total_likes_yt + data.total_likes_dm;
            total_views_yt = data.total_views_yt;
            total_followers_yt = data.total_followers_yt;
            total_likes_dm = data.total_likes_dm;
            total_views_dm = data.total_views_dm;
            total_followers_dm = data.total_followers_dm;
            total_likes_yt = data.total_likes_yt;
            total_followers = data.total_followers;
            dates = data.dates;
            views_yt = data.views_yt;
            subscribers_yt = data.subscribers_yt;
            views_dm = data.views_dm;
            subscribers_dm = data.subscribers_dm;
            estimate_minutes_watched = data.estimate_minutes_watched;
            average_view_duration = data.average_view_duration;
            all_interactions = data.all_interactions_yt;
            all_interactions_yt = data.all_interactions_yt;
            all_interactions_dm = data.all_interactions_dm;
            shares = data.shares_yt;
            shares_yt = data.shares_yt;
            shares_dm = data.shares_dm;
            likes = data.likes;
            likes_yt = data.likes_yt;
            likes_dm = data.likes_dm;
            comments = data.comments_yt;
            annotation_impressions = data.annotation_impressions_yt;
            click_impressions = data.click_impressions_yt;
            close_impressions = data.close_impressions_yt;
            annotation_click = data.annotation_click_yt;
            annotation_close = data.annotation_close_yt;
            annotation_click_through_rate = data.annotation_click_through_rate_yt;
            annotation_close_rate = data.annotation_close_rate_yt;
            callChart();
          },
          error: function(response){
            toastr.error("Mengambil data gagal.");
          }
      });
    }

    // init event bindings
    function initBindings(){
        
        // change date
        $("#date").change(function(e){
          e.preventDefault();
          getDataAnalytics(this.value);
        });
    }
    // end init event bindings
    
</script>
@endsection
