
@section('css')
<link href="{{ asset('css/fileinput.min.css') }}" rel="stylesheet">
<link href="{{ asset('js/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css') }}" rel="stylesheet">
<link href="{{ asset('css/plugins/bootstrap-tagsinput.css') }}" rel="stylesheet">
<link href="{{ asset('css/chosen.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/plugins/iCheck/custom.css') }}" rel="stylesheet">

<style>
.kv-photo .krajee-default.file-preview-frame,.kv-photo .krajee-default.file-preview-frame:hover {
    margin: 0;
    padding: 0;
    border: none;
    box-shadow: none;
    text-align: center;
}
.kv-photo .file-input {
    display: table-cell;
    max-width: 220px;
}
.kv-reqd {
    color: red;
    font-family: monospace;
    font-weight: normal;
}
</style>
@include('layouts.datatables_css')
@endsection

<!-- Brand Field -->
<div class="form-group">
    {!! Form::label('brand_id', 'Brand:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::select('brand_id', $user, null, ['placeholder' => 'Select Brand', 'class' => 'form-control', 'disabled' => true]) !!}
    </div>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Title:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::text('title', null, ['class' => 'form-control', 'disabled' => true, 'maxlength' => 100]) !!}
    </div>
</div>

<div class="form-group" id="data_5">
    {!! Form::label('dr', 'Date range:', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        <div class="input-group input-daterange">
            {!! Form::text('start_date', isset($camp->start_date) ? $camp->start_date : '', ['class' => 'input-sm form-control', 'disabled' => true]) !!}
            <span class="input-group-addon">to</span>
            {!! Form::text('end_date', isset($camp->end_date) ? $camp->end_date : '', ['class' => 'input-sm form-control', 'disabled' => true]) !!}
        </div>
    </div>
</div>

<!-- Brief Field -->
<div class="form-group">
    {!! Form::label('brief', 'Brief Campaign:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::textarea('brief_campaign', null, ['class' => 'form-control', 'id' => 'tiny', 'disabled' => true]) !!}
    </div>
</div>
@if(!is_null($infl->approval))
<!-- Status Field -->
<div class="form-group">
    {!! Form::label('target', 'Approval:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::select('approval', ['0' => 'Rejected', '1' => 'Approved'], $infl->approval, ['placeholder' => 'Waiting for approval', 'class' => 'form-control', 'disabled' => true]) !!}
    </div>
</div>
@if($infl->approval == 0)
<div class="form-group">
    {!! Form::label('reason', 'Reason:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::textarea('reason', $infl->reason, ['class' => 'form-control', 'id' => 'tin', 'disabled' => true]) !!}
    </div>
</div>
@endif
@endif
<!-- Submit Field -->
<div class="form-group">
    <div class="col-sm-4 col-sm-offset-2">
    @if(is_null($infl->approval) && $camp->status != 3)
    <button type="button" class="btn btn-primary" id="btn_app">
        Approve
    </button>
    <button type="button" class="btn btn-danger" id="btn_rej">
        Reject
    </button>
    @endif
    <a href="{!! route('influencer.campaign.index') !!}" class="btn btn-default">Close</a>
    </div>
</div>
<div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-commenting-o modal-icon"></i>
                <h4 class="modal-title">Modal title</h4>
            </div>
            <form class="appForm" action="{{ route('influencer.campaign.approval') }}" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <p>Are you sure want to <span id="app">approve</span> this campaign?</p>
                    <input type="hidden" name="id" value="{{$camp->camp_id}}">
                    <input type="hidden" name="approval" id="approval" value="">
                    <div class="form-group" id="reason"><label>Reason</label><textarea placeholder="Enter your reason" name="reason" id="other" class="form-control"></textarea></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    <input class="btn btn-primary" type="submit" value="Submit">
                </div>
            </form>
        </div>
    </div>
</div>
@section('scripts')
<script src="{{ asset('js/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-fileinput/piexif.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-fileinput/sortable.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-fileinput/purify.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-fileinput/fileinput.min.js') }}"></script>
<script src="{{ asset('js/plugins/tinymce/jquery.tinymce.min.js') }}"></script>
<script src="{{ asset('js/plugins/tinymce/tinymce.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-tagsinput.min.js') }}"></script>
<script src="{{ asset('js/plugins/chosen.jquery.min.js') }}"></script>
<script src="{{ asset('js/plugins/typeahed.min.js') }}"></script>
<script src="{{ asset('js/plugins/iCheck/icheck.min.js') }}"></script>
<script>


    var formType = '{{ $formType }}';
    $(document).ready(function(){
        $('#btn_app').click(function(){
            $('.modal-title').text('Approve Campaign');
            $('#approval').val(1);
            $('#reason').hide();
            $('#app').text('approve');
            $('#myModal').modal('show');
        });

        $('#btn_rej').click(function(){
            $('.modal-title').text('Reject Campaign');
            $('#approval').val(0);
            $('#reason').show();
            $('#app').text('reject');
            $('#myModal').modal('show');
        });
    });

    $(function() {
        $('#tag-form-submit').on('click', function(e) {
            e.preventDefault();
            $.ajax({
                type: "POST",
                url: "{{ route('influencer.campaign.approval') }}",
                data: $('form.appForm').serialize(),
                success: function(response) {
                    alert(response['response']);
                },
                error: function() {
                    alert('Error');
                }
            });
            return false;
        });
    });
    
    $('#data_5 .input-daterange').datepicker({
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true,
        format: 'yyyy-mm-dd'
    });

    tinymce.init({
        forced_root_block : "",
        mode : "exact",
        elements : ["tiny", "tin"],
        readonly: 1,
        setup: function (editor) {
            editor.on('change', function () {
                tinymce.triggerSave();
            });
        },
        file_picker_types: 'image',
        images_upload_credentials: true,
        automatic_uploads: false,
        theme: "modern",
        paste_data_images: true,
        plugins: [
        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars code fullscreen",
        "insertdatetime nonbreaking save table contextmenu directionality",
        "template paste textcolor colorpicker textpattern"
        ],
        toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | forecolor",
        image_advtab: true,
        file_picker_callback: function(cb, value, meta) {
            var input = document.createElement('input');
            input.setAttribute('type', 'file');
            input.setAttribute('accept', 'image/*');
            
            // Note: In modern browsers input[type="file"] is functional without 
            // even adding it to the DOM, but that might not be the case in some older
            // or quirky browsers like IE, so you might want to add it to the DOM
            // just in case, and visually hide it. And do not forget do remove it
            // once you do not need it anymore.
        
            input.onchange = function() {
              var file = this.files[0];
              
              var reader = new FileReader();
              reader.readAsDataURL(file);
              reader.onload = function () {
                // Note: Now we need to register the blob in TinyMCEs image blob
                // registry. In the next release this part hopefully won't be
                // necessary, as we are looking to handle it internally.
                var id = 'blobid' + (new Date()).getTime();
                var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
                var base64 = reader.result.split(',')[1];
                var blobInfo = blobCache.create(id, file, base64);
                blobCache.add(blobInfo);
        
                // call the callback and populate the Title field with the file name
                cb(blobInfo.blobUri(), { title: file.name });
              };
            };
            
            input.click();
        }
    });

    tinymce.init({
        forced_root_block : "",
        mode : "exact",
        elements : "other",
        setup: function (editor) {
            editor.on('change', function () {
                tinymce.triggerSave();
            });
        },
        file_picker_types: 'image',
        images_upload_credentials: true,
        automatic_uploads: false,
        theme: "modern",
        paste_data_images: true,
        plugins: [
        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars code fullscreen",
        "insertdatetime nonbreaking save table contextmenu directionality",
        "template paste textcolor colorpicker textpattern"
        ],
        toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | forecolor",
        image_advtab: true,
        file_picker_callback: function(cb, value, meta) {
            var input = document.createElement('input');
            input.setAttribute('type', 'file');
            input.setAttribute('accept', 'image/*');
            
            // Note: In modern browsers input[type="file"] is functional without 
            // even adding it to the DOM, but that might not be the case in some older
            // or quirky browsers like IE, so you might want to add it to the DOM
            // just in case, and visually hide it. And do not forget do remove it
            // once you do not need it anymore.
        
            input.onchange = function() {
              var file = this.files[0];
              
              var reader = new FileReader();
              reader.readAsDataURL(file);
              reader.onload = function () {
                // Note: Now we need to register the blob in TinyMCEs image blob
                // registry. In the next release this part hopefully won't be
                // necessary, as we are looking to handle it internally.
                var id = 'blobid' + (new Date()).getTime();
                var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
                var base64 = reader.result.split(',')[1];
                var blobInfo = blobCache.create(id, file, base64);
                blobCache.add(blobInfo);
        
                // call the callback and populate the Title field with the file name
                cb(blobInfo.blobUri(), { title: file.name });
              };
            };
            
            input.click();
        }
    });
</script>
@if($formType == 'edit')
@include('layouts.datatables_js')
<script>
    $.fn.dataTable.ext.buttons.create = {
        action: function (e, dt, button, config) {
            $('#myModal').modal('show');
        }
    };

    var hash = document.location.hash;
    if (hash) {
        console.log(hash);
        $('.nav-tabs a[href="'+hash+'"]').tab('show')
    }

    // Change hash for page-reload
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
    window.location.hash = e.target.hash;
    });
</script>
@endif
@endsection