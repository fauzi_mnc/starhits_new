@extends('layouts.crud')

@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Campaign</h2>
        <ol class="breadcrumb">
            <li>
                @if(auth()->user()->roles->first()->name === 'admin')
                <a href="{{route('admin.campaign.index')}}">Campaign</a>
                @else
                <a href="{{route('brand.campaign.index')}}">Campaign</a>
                @endif
            </li>
            <li class="active">
                <strong>Create</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
@endsection
@section('contentCrud')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@include('flash::message')

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Campaign</h5>
                </div>
                <div class="ibox-content">
                @if(auth()->user()->roles->first()->name === 'admin')
                {!! Form::open(['route' => 'admin.campaign.store', 'class' => 'form-horizontal', 'files' => true]) !!}
                @else
                {!! Form::open(['route' => 'brand.campaign.store', 'class' => 'form-horizontal', 'files' => true]) !!}
                @endif
                
                @include('campaign.fields', ['formType' => 'create'])

                {!! Form::close() !!}
                </div>
            </div>
@endsection
