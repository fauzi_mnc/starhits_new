@extends('layouts.crud')

@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Campaign Table</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Campaign</a>
            </li>
            <li class="active">
                <strong>Table</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
@endsection
@section('contentCrud')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@include('flash::message')
            @if(auth()->user()->roles->first()->name === 'admin')
            {!! Form::model($camp, ['route' => ['admin.campaign.close'], 'method' => 'post', 'class' => 'form-horizontal', 'files' => true]) !!}
            @else
            {!! Form::model($camp, ['route' => ['brand.campaign.close'], 'method' => 'post', 'class' => 'form-horizontal', 'files' => true]) !!}
            @endif
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Campaign</h5>
                </div>
                <div class="ibox-content">
                        @include('campaign.influencer', ['formType' => 'edit'])
                </div>
            </div>
            <!-- <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Search Influencer</h5>
                        </div>

                        <div class="ibox-content">
                            {!! $dataTable->table(['width' => '100%']) !!}
                        </div>
                    </div>
                </div>
            </div> -->
            @include('flash::message')
            <div class="row">
                <div class="col-lg-12">
                    <div class="table-responsive">
                        <div class="ibox float-e-margins">
                            <div class="ibox-content">
                                <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>Influencer </th>
                                            <th>Email </th>
                                            <th>Phone </th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($infl as $val)
                                        <tr>
                                            <td><input type="hidden" name="check[]" value="{{$val->id}}">{{$val->name}}</td>
                                            <td>{{$val->email}}</td>
                                            <td>{{$val->phone}}</td>
                                            @foreach($val->influencer as $infl)
                                                @if($infl->campaign_id == $id AND $infl->user_id == $val->id)
                                                    @if($infl->approval == 4 || is_null($infl->approval))
                                                    <td>Process</td>
                                                    <td><input type="checkbox" name="mail[]" class="send" value="{{$val->id}}"></td>
                                                    @else
                                                    <td>{{!empty($infl->approval) && ($infl->approval === 1) ? 'Approved' : 'Rejected'}}</td>
                                                    <td></td>
                                                    @endif
                                                @endif
                                            @endforeach
                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                        @if(auth()->user()->roles->first()->name === 'admin')
                                        <a href="{!! route('admin.campaign.index') !!}" class="btn btn-default">Cancel</a>
                                        @if($camp->status == 2)
                                        {!! Form::button('Running Campaign', [
                                            'type' => 'submit',
                                            'class' => 'btn btn-default pull-right sub',
                                            'name' => 'action',
                                            'value' => 'finish'
                                        ]) !!}
                                        {!! Form::button('Sendmail', [
                                            'type' => 'submit',
                                            'class' => 'btn btn-info pull-right mail',
                                            'name' => 'action',
                                            'value' => 'email',
                                            'disabled' => true
                                        ]) !!}
                                        @endif
                                        @else
                                        <a href="{!! route('brand.campaign.index') !!}" class="btn btn-default">Back</a>
                                        @endif
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            {!! Form::close() !!}
@endsection