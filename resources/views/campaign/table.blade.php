@section('css')
    @include('layouts.datatables_css')
@endsection

{!! $dataTable->table(['width' => '100%']) !!}

@section('scripts')
    @include('layouts.datatables_js')
    {!! $dataTable->scripts() !!}
    @include('layouts.datatables_limit')
 <script>
 	$(".dataTables_filter").show(); 
     
    $('#query').bind("change keyup", function() {
        console.log('oke');
        var val = $(this).val();
        var regex = /[%*]/g;
        if (val.match(regex)) {
            val = val.replace(regex, "");
            $(this).val(val);
        }
        $("p").html(val);
    });

	$('#gas').click(function(){
            console.log($('#query').val());
            console.log($('#status').val());
            LaravelDataTables["dataTableBuilder"].draw();
        });
 </script>
@endsection