
@section('css')
<link href="{{ asset('css/fileinput.min.css') }}" rel="stylesheet">
<link href="{{ asset('js/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css') }}" rel="stylesheet">
<link href="{{ asset('css/plugins/bootstrap-tagsinput.css') }}" rel="stylesheet">
<link href="{{ asset('css/chosen.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/plugins/iCheck/custom.css') }}" rel="stylesheet">

<style>
.kv-photo .krajee-default.file-preview-frame,.kv-photo .krajee-default.file-preview-frame:hover {
    margin: 0;
    padding: 0;
    border: none;
    box-shadow: none;
    text-align: center;
}
.kv-photo .file-input {
    display: table-cell;
    max-width: 220px;
}
.kv-reqd {
    color: red;
    font-family: monospace;
    font-weight: normal;
}
</style>
@include('layouts.datatables_css')
@endsection

@if(auth()->user()->roles->first()->name === 'admin')
<!-- Brand Field -->
<div class="form-group">
    {!! Form::label('brand_id', 'Brand:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::select('brand_id', $user, null, ['placeholder' => 'Select Brand', 'class' => 'form-control', 'required' => 'required']) !!}
    </div>
</div>
@endif

<div class="form-group">
    {!! Form::label('type', 'Type:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        <div>
            <label><input type="radio" name="type" value="1" {{isset($camp->type) && ($camp->type == 1) ? 'checked' : ''}}> Exclusive </label>
        </div>
        <div>
            <label><input type="radio" name="type" value="0" {{isset($camp->type) && ($camp->type == 0) ? 'checked' : ''}}> Non Exclusive </label>
        </div>
    </div>
</div>

{{-- <div class="form-group">
    {!! Form::label('product', 'Product Category:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::select('product', $parent, isset($selected_category) ? $selected_category : null, ['placeholder' => 'Select Category', 'class' => 'form-control', 'required' => 'required']) !!}
    </div>
</div> --}}

<div class="form-group">
    {!! Form::label('category_id', 'Category:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::select('category_id', $cat, null, ['placeholder' => 'Select Category', 'class' => 'form-control', 'required' => 'required']) !!}
    </div>
</div>
<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Title:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::text('title', null, ['class' => 'form-control', 'required' => 'required', 'maxlength' => 100]) !!}
    </div>
</div>

<div class="form-group" id="data_5">
    {!! Form::label('dr', 'Date range:', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        <div class="input-group input-daterange">
            {!! Form::text('start_date', isset($camp->start_date) ? $camp->start_date : '', ['class' => 'input-sm form-control']) !!}
            <span class="input-group-addon">to</span>
            {!! Form::text('end_date', isset($camp->end_date) ? $camp->end_date : '', ['class' => 'input-sm form-control']) !!}
        </div>
    </div>
</div>
<!-- Status Field -->
<div class="form-group">
    {!! Form::label('socio', 'Socio-Economic Status:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::select('socio_economic_status', ['0' => 'A', '1' => 'B', '2' => 'C', '3' => 'D', '4' => 'E'], null, ['placeholder' => 'Select SES', 'class' => 'form-control', 'required' => 'required']) !!}
    </div>
</div>

<!-- Brief Field -->
<div class="form-group">
    {!! Form::label('brief', 'Brief Campaign:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::textarea('brief_campaign', null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('budget', 'Budget:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        <div class="input-group m-b">
            <span class="input-group-addon">Rp.</span>
            {!! Form::text('budget', null, ['class' => 'form-control', 'required' => 'required', 'maxlength' => 15]) !!}
        </div>
    </div>
</div>

<div class="form-group">
    {!! Form::label('post_to', 'Post to:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        @foreach($post as $val)
        <div class="i-checks col-sm-4">
            <label> <input name="post_to[]" type="checkbox" value="{{$val->id}}"
                @if($val->id == 6)
                id="package"
                @else
                class="post"
                @endif 
                @isset($post_selected)
                @foreach($post_selected as $pos)
                @if($val->id == $pos->post_id)
                {{'checked'}}
                @endif
                @endforeach
                @endisset
                > <i></i> {{$val->title}} </label>
        </div>
        @endforeach
    </div>
</div>

<div class="form-group notes" style="display: none;">
    {!! Form::label('notes', 'Notes:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::textarea('notes', null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('category', 'Category:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        @foreach($category as $val)
        <div class="i-checks col-sm-4">
            <label> <input name="category[]" type="checkbox" class="cat" value="{{$val->id}}" 
                @isset($category_selected)
                @foreach($category_selected as $cat)
                @if($val->id == $cat->category_id)
                {{'checked'}}
                @endif
                @endforeach
                @endisset
                > <i></i> {{$val->title}} </label>
        </div>
        @endforeach
        <div class="i-checks col-sm-4">
            <label> <input type="checkbox" id="cat_all"> <i></i> All </label>
        </div>
    </div>
</div>

<div class="form-group">
    {!! Form::label('tag', 'Hastag (maks 5)', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
    {!! Form::text('tags', isset($camp->tags) ? $camp->tags : null, ['id' => 'tags-input', 'class' => 'form-control']) !!}
    </div>
</div>

<!-- Submit Field -->
<div class="form-group">
    <div class="col-sm-4 col-sm-offset-2">
    @if((isset($camp->status) && $camp->status != 3) || empty($camp->status))
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    @endif
    @if(auth()->user()->roles->first()->name === 'admin')
    <a href="{!! route('admin.campaign.index') !!}" class="btn btn-default">Back</a>
    @else
    <a href="{!! route('brand.campaign.index') !!}" class="btn btn-default">Back</a>
    @endif
    </div>
</div>

@section('scripts')
<script src="{{ asset('js/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-fileinput/piexif.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-fileinput/sortable.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-fileinput/purify.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-fileinput/fileinput.min.js') }}"></script>
<script src="{{ asset('js/plugins/tinymce/jquery.tinymce.min.js') }}"></script>
<script src="{{ asset('js/plugins/tinymce/tinymce.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-tagsinput.min.js') }}"></script>
<script src="{{ asset('js/plugins/chosen.jquery.min.js') }}"></script>
<script src="{{ asset('js/plugins/typeahed.min.js') }}"></script>
<script src="{{ asset('js/plugins/iCheck/icheck.min.js') }}"></script>
<script>


    var formType = '{{ $formType }}';
    $(document).ready(function () {
        initSourceTags();
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });
    });

    $('#post_all').on('ifChecked', function () { 
        $('.post').iCheck('check');
    });

    $('#package').on('ifChecked', function () { 
        $('.post').iCheck('uncheck');
        $('.notes').show();
    });

    $('#package').on('ifUnchecked', function () { 
        $('.cek').prop('disabled', false);
    });

    $('.post').each(function(){
        $(this).on('ifChecked', function () { 
            $('#package').iCheck('uncheck');
            $('.notes').hide();
        });
    });

    $('.cat').each(function(){
        $(this).on('ifUnchecked', function () { 
            $('#cat_all').iCheck('uncheck');
        });
    });

    $('#cat_all').on('ifChecked', function () { 
        $('.cat').iCheck('check');
    });

    $('#photo').val('');
    $('#data_5 .input-daterange').datepicker({
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true,
        format: 'yyyy-mm-dd'
    });

    tinymce.init({
        forced_root_block : "",
        selector: "textarea",
        setup: function (editor) {
            editor.on('change', function () {
                tinymce.triggerSave();
            });
        },
        file_picker_types: 'image',
        images_upload_credentials: true,
        automatic_uploads: false,
        theme: "modern",
        paste_data_images: true,
        plugins: [
        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars code fullscreen",
        "insertdatetime nonbreaking save table contextmenu directionality",
        "template paste textcolor colorpicker textpattern"
        ],
        toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | forecolor",
        image_advtab: true,
        file_picker_callback: function(cb, value, meta) {
            var input = document.createElement('input');
            input.setAttribute('type', 'file');
            input.setAttribute('accept', 'image/*');
            
            // Note: In modern browsers input[type="file"] is functional without 
            // even adding it to the DOM, but that might not be the case in some older
            // or quirky browsers like IE, so you might want to add it to the DOM
            // just in case, and visually hide it. And do not forget do remove it
            // once you do not need it anymore.
        
            input.onchange = function() {
              var file = this.files[0];
              
              var reader = new FileReader();
              reader.readAsDataURL(file);
              reader.onload = function () {
                // Note: Now we need to register the blob in TinyMCEs image blob
                // registry. In the next release this part hopefully won't be
                // necessary, as we are looking to handle it internally.
                var id = 'blobid' + (new Date()).getTime();
                var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
                var base64 = reader.result.split(',')[1];
                var blobInfo = blobCache.create(id, file, base64);
                blobCache.add(blobInfo);
        
                // call the callback and populate the Title field with the file name
                cb(blobInfo.blobUri(), { title: file.name });
              };
            };
            
            input.click();
        }
    });

    function initSourceTags(){
        var source_ = new Array();
        @foreach ($tags as $value)
            source_.push('{{$value}}');
        @endforeach

        $('#tags-input').tagsinput({
            tagClass: 'label label-primary',
            maxTags: 5,
            typeahead: {
                afterSelect: function(val) { this.$element.val(""); },
                source: source_
            }
        });
    }

    $('#product').change(function() {
            getSub($('#product').val());
            console.log('test');
        });

    function getSub(_id_cat){
        $.ajax({
            @if(auth()->user()->roles->first()->name === 'admin')
            url: "{{ route('admin.campaign.children') }}",
            @else
            url: "{{ route('brand.campaign.children') }}",
            @endif
            method: "POST",
            data: {
                parent: _id_cat
            },
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            success: function(response){
                var counter = 0;
                var $el = $("#category_id");
                $el.empty(); // remove old options
                $.each(response, function(key,value) {
                  $el.append($("<option></option>")
                     .attr("value", key).text(value));
                  counter += 1;
                });
                
                if(counter > 0){
                    toastr.success("Mengambil Sub Category Berhasil");
                }else{
                    toastr.error("Sub Category tidak ada silahkan memilih Product yang lain.");
                }
            },
            error: function(response){
                console.log(response);
            }
        });
    }

    // $('#product').change(function(e) {
    //     var parent = e.target.value;
    //     $.get('/admin/campaign/children?parent=' + parent, function(data) {
    //         console.log(data);
    //         $('#category_id').empty();
    //         if (data != '') {
    //             $.each(data, function(key, value) {
    //                 var option = $("<option></option>")
    //                       .attr("value", key)                         
    //                       .text(value);

    //                 $('#category_id').html('<option disabled selected value>Select Sub Category').append(option);
    //                 $('#category_id').val('');
    //             });
    //         }else{
    //             $('#category_id').html('<option disabled selected value>Sub Category Empty');
    //             $('#category_id').val('');
    //         }
            
    //     });
    // });
</script>
@if($formType == 'edit')
@include('layouts.datatables_js')
<script>
    $.fn.dataTable.ext.buttons.create = {
        action: function (e, dt, button, config) {
            $('#myModal').modal('show');
        }
    };

    var hash = document.location.hash;
    if (hash) {
        console.log(hash);
        $('.nav-tabs a[href="'+hash+'"]').tab('show')
    }

    // Change hash for page-reload
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
    window.location.hash = e.target.hash;
    });
</script>
{!! $dataTable->scripts() !!}
@include('layouts.datatables_limit')
@endif
@endsection