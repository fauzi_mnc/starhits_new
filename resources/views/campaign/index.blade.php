@extends('layouts.crud')

@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Campaign Table</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Campaign</a>
            </li>
            <li class="active">
                <strong>Table</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
@endsection
@section('contentCrud')
@include('flash::message')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Campaign</h5>
                    {{-- @if(auth()->user()->roles->first()->name === 'admin')
                    <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{{route('admin.campaign.create')}}">Add New</a>
                    @else
                    <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{{route('brand.campaign.create')}}">Add New</a>
                    @endif --}}
                </div>

                <div class="ibox-content">
                    @if(auth()->user()->roles->first()->name === 'admin')
                    <div class="col-sm-3 m-b-xs">
                        <select class="input-sm form-control input-s-sm inline" id="status">
                            <option disabled selected>Search by status</option>
                            <option value="1">Draft</option>
                            <option value="2">Process</option>
                            <option value="3">Running</option>
                            <option value="4">Expired</option>
                        </select>
                    </div>
                    <div class="col-sm-3 m-b-xs">
                        <div class="input-group"><input type="text" id="query" placeholder="Search title" class="input-sm form-control"> <span class="input-group-btn">
                            <button type="button" class="btn btn-sm btn-primary" id="gas"> Go!</button> </span>
                        </div>
                    </div>
                    <div class="col-sm-6 m-b-xs">
                        @if(auth()->user()->roles->first()->name === 'admin')
                        <a class="btn btn-sm btn-primary pull-right" href="{{route('admin.campaign.create')}}">Add New</a>
                        @else
                        <a class="btn btn-sm btn-primary pull-right" href="{{route('brand.campaign.create')}}">Add New</a>
                        @endif
                    </div>
                    @include('campaign.table')
                    @else
                    <div class="col-sm-12 m-b-xs">
                            @if(auth()->user()->roles->first()->name === 'admin')
                            <a class="btn btn-sm btn-primary pull-right" href="{{route('admin.campaign.create')}}">Add New</a>
                            @else
                            <a class="btn btn-sm btn-primary pull-right" href="{{route('brand.campaign.create')}}">Add New</a>
                            @endif
                        </div>
                    @include('campaign.otable')
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
