@if(auth()->user()->roles->first()->name === 'admin')
{!! Form::open(['route' => ['admin.campaign.destroy', $camp_id], 'method' => 'delete']) !!}
@if($status == 3 || $status == 4)
<div class="btn-group">
    <a href="{{ route('admin.campaign.look', $camp_id) }}" class='btn btn-default btn-xs' title="Show Campaign">
        <i class="glyphicon glyphicon-bell"></i>
    </a>
</div>
@else
<div class='btn-group'>
    <a href="{{ route('admin.campaign.edit', $camp_id) }}" class='btn btn-default btn-xs' title="Edit Campaign">
        <i class="glyphicon glyphicon-edit"></i>
    </a>

    <a href="{{ route('admin.campaign.add', $camp_id) }}" class='btn btn-default btn-xs' title="Add Influencer">
        <i class="glyphicon glyphicon-user"></i>
    </a>
    <a href="{{ route('admin.campaign.budget', $camp_id) }}" class='btn btn-default btn-xs' title="Update Budget">
        <i class="glyphicon glyphicon-euro"></i>
    </a>
    <a href="{{ route('admin.campaign.look', $camp_id) }}" class='btn btn-default btn-xs' title="Show Campaign">
        <i class="glyphicon glyphicon-bell"></i>
    </a>
    @if($active == 1)
    {!! Form::button('<i class="glyphicon glyphicon-ban-circle"></i>', [
        'type' => 'submit',
        'title' => 'Inactive Campaign',
        'class' => 'btn btn-danger btn-xs',
        'onclick' => "return confirm('Do you want to inactive this campaign?')",
        'name' => 'action',
        'value' => 'inact'
    ]) !!}
    @else
    {!! Form::button('<i class="glyphicon glyphicon-ok"></i>', [
        'type' => 'submit',
        'title' => 'Activated Campaign',
        'class' => 'btn btn-success btn-xs',
        'onclick' => "return confirm('Do you want to activated this campaign?')",
        'name' => 'action',
        'value' => 'act'
    ]) !!}
    @endif
</div>
@endif
{!! Form::close() !!}
@elseif(auth()->user()->hasRole('influencer'))
<div class='btn-group'>
    <a href="{{ route('influencer.campaign.show', $camp_id) }}" class='btn btn-default btn-xs' title="Show Campaign">
        <i class="glyphicon glyphicon-eye-open"></i>
    </a>
</div>
@elseif(auth()->user()->hasRole('brand'))
    @if($status == 1 || $status == 2)
    <div class='btn-group'>
        <a href="{{ route('brand.campaign.edit', $camp_id) }}" class='btn btn-default btn-xs' title="Edit Campaign">
            <i class="glyphicon glyphicon-edit"></i>
        </a>
        <a href="{{ route('brand.campaign.add', $camp_id) }}" class='btn btn-default btn-xs' title="Add Influencer">
            <i class="glyphicon glyphicon-user"></i>
        </a>
    </div>
    @else
    <div class="btn-group">
        <a href="{{ route('brand.campaign.look', $camp_id) }}" class='btn btn-default btn-xs' title="Show Campaign">
            <i class="glyphicon glyphicon-bell"></i>
        </a>
    </div>
    @endif
@endif