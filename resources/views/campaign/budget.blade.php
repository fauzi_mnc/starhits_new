@extends('layouts.crud')

@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Campaign Update Budget</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Campaign</a>
            </li>
            <li class="active">
                <strong>Update Budget</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
@endsection
@section('contentCrud')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@include('flash::message')
            {!! Form::model($camp, ['route' => ['admin.campaign.upbudget'], 'method' => 'post', 'class' => 'form-horizontal', 'files' => true]) !!}
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Update Budget</h5>
                </div>
                <div class="ibox-content">
                        @include('campaign.nego', ['formType' => 'edit'])
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Search Influencer</h5>
                        </div>

                        <div class="ibox-content">
                            {!! $dataTable->table(['width' => '100%']) !!}
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
@endsection
