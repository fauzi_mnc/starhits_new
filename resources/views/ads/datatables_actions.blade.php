{!! Form::open(['route' => ['ads.destroy', $ads_id], 'method' => 'delete']) !!}
<div class='btn-group'>
    <a href="{{ route('ads.show', $ads_id) }}" class='btn btn-default btn-xs' title="Show Ads">
        <i class="glyphicon glyphicon-eye-open"></i>
    </a>
    <a href="{{ route('ads.edit', $ads_id) }}" class='btn btn-default btn-xs' title="Edit Ads">
        <i class="glyphicon glyphicon-edit"></i>
    </a>
    @if($status == 1)
    {!! Form::button('<i class="glyphicon glyphicon-ban-circle"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-danger btn-xs',
        'title' => 'Inactive Ads',
        'onclick' => "return confirm('Do you want to inactive this ads?')",
        'name' => 'action',
        'value' => 'inact'
    ]) !!}
    @else
    {!! Form::button('<i class="glyphicon glyphicon-ok"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-success btn-xs',
        'title' => 'Activated Ads',
        'onclick' => "return confirm('Do you want to activated this ads?')",
        'name' => 'action',
        'value' => 'act'
    ]) !!}
    @endif
</div>
{!! Form::close() !!}