@extends('layouts.crud')

@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Ads</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{route('ads.index')}}">Ads</a>
            </li>
            <li class="active">
                <strong>Detail</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
@endsection

@section('contentCrud')

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Ads</h5>
                </div>
                <div class="ibox-content">
                    {!! Form::model($ads, ['route' => ['ads.update', $ads->ads_id], 'class' => 'form-horizontal', 'files' => true]) !!}
                        @include('ads.show_fields', ['formType' => 'edit'])
                    {!! Form::close() !!}
                </div>
            </div>

@endsection

