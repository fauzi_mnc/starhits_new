<html>
<head>
<style>
body{
    margin: 10px;
}
table{
    margin: 20px 0px;
}
table, th, td{
    border-collapse: collapse;
}
.table-bordered table, .table-bordered th, .table-bordered td {
    border: 1px solid black;
}
th, td {
    padding: 5px;
    text-align: left;
    text-align: center;
}
.signature{
    padding: 40px 50px;
}
.silver{
    background-color: silver;
}
</style>
</head>
<body>

<table class="table-bordered" style="width:100%">
    <tr class="silver">
        <th rowspan="2">No</th>
        <th rowspan="2">Channel</th>
        <th colspan="6">Youtube Revenue Periode {{ $month }} {{$revenue->year}}</th>
    </tr>
    <tr class="silver">
        <!-- <th>Estimate Gross Revenue</th> -->
        <th>%</th>
        <th>Gross Revenue</th>
        <th>Cost Production</th>
        <th>Revenue</th>
        <th>Net Revenue Creator</th>
        <th>Net Revenue Starhits</th>
    </tr>
    <?php 
        $i = 0;
        $net_creators = 0;
        $net_starhits = 0;
    ?>
    @foreach($creators as $row)
        <?php
            $net_rev_creator = ($row->nett_revenue/100)*70;
            $net_rev_starhits = ($row->nett_revenue/100)*30;
            $net_creators = $net_creators + $net_rev_creator;
            $net_starhits = $net_starhits + $net_rev_starhits;
        ?>
    <tr>
        <td>{{++$i}}</td>
        <td>{{ $row->name }}</td>
        <!-- <td>Rp {{ number_format((float)$row->pivot->estimate_gross, 0,",",".") }}</td> -->
        <td>{{number_format((float)$row->percentage_revenue * 100, 2,",",".")}}%</td>
        <td>Rp {{number_format((float)$row->gross_revenue, 0,",",".")}}</td>
        <td>Rp {{ number_format((float)$row->pivot->cost_production, 0,",",".") }}</td>
        <td>Rp {{number_format((float)$row->nett_revenue, 0,",",".")}}</td>
        <td>Rp {{number_format((float)$net_rev_creator, 0,",",".")}}</td>
        <td>Rp {{number_format((float)$net_rev_starhits, 0,",",".")}}</td>
    </tr>
    @endforeach
    
    <tr class="silver">
        <td></td>
        <td>TOTAL</td>
        <!-- <td>Rp {{number_format((float)$creators->sum('pivot.estimate_gross'), 0,",",".")}}</td> -->
        <td>{{number_format((float)$creators->sum('percentage_revenue')* 100, 2,",",".")}}%</td>
        <td>Rp {{number_format((float)$creators->sum('gross_revenue'), 0,",",".")}}</td>
        <td>Rp {{number_format((float)$creators->sum('pivot.cost_production'), 0,",",".")}}</td>
        <td>Rp {{number_format((float)$creators->sum('nett_revenue'), 0,",",".")}}</td>

        <td>Rp {{number_format((float)$net_creators, 0,",",".")}}</td>
        <td>Rp {{number_format((float)$net_starhits, 0,",",".")}}</td>
    </tr>
</table>
*Adjust By starhits.id
<br>
<table class="table-bordered" style="width: 100%;">
    <tr>
        <td>Prepared By</td>
        <td colspan="3">Checked By</td>
        <td>Approved By</td>
    </tr>
    <tr>
        <td class="signature"></td>
        <td class="signature"></td>
        <td class="signature"></td>
        <td class="signature"></td>
        <td class="signature"></td>
    </tr>
</table>

</body>
</html>
