@section('css')
    @include('layouts.datatables_css')
@endsection

{!! $dataTable->table(['width' => '100%']) !!}

@section('scripts')
    @include('layouts.datatables_js')
    {!! $dataTable->scripts() !!}
    @include('layouts.datatables_limit')
    <script>
    function ConfirmDelete()
    {
    var x = confirm("Are you sure you want to delete?");
    if (x)
        return true;
    else
        return false;
    }
    $('.form-horizontal').find('.btn-search').on('click', function(e) {
        LaravelDataTables["dataTableBuilder"].draw();
        e.preventDefault();
    });
    $('.form-horizontal').find('.clear-filtering').on('click', function(e) {
        $('#month').val('');
        LaravelDataTables["dataTableBuilder"].draw();
        e.preventDefault();
    });

    LaravelDataTables["dataTableBuilder"].on('click', '.edit-revision', function(e){
        e.preventDefault();
        var url = $(this).data('url');
        $("#form-revision").attr('action', url);
        $('#modal-revision').modal('show');
        $('#btn-upload').click(function(e) {
            $('#edit-status').val('csv');
        });
    });

    </script>
@endsection