@extends('layouts.crud')

@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>List Revenue</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Revenue</a>
            </li>
            <li class="active">
                <strong>List</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
@endsection
@section('contentCrud')
@include('flash::message')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>List Revenue</h5>
                </div>

                <div class="ibox-content">
                    <form style="margin-bottom: 10px;" class="form-horizontal" role="form">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Revenue</label>
                                        <div class="col-md-4">
                                            {!! Form::select('month', $months, null, ['class' => 'form-control', 'id' => 'month']) !!}
                                        </div>
                                        {{-- <div class="col-md-4">
                                            {!! Form::text('year', null, ['class' => 'form-control', 'id' => 'year', 'placeholder' => 'Year']) !!}
                                        </div> --}}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="pull-right">
                                        <button type="button" class="btn btn-default clear-filtering"><i class="fa fa-undo"></i> Clear Filtering</button>
                                        <button type="button" class="btn btn-success btn-search"><i class="fa fa-search"></i> Search</button>
                                    </div>
                                </div>
                            </div>
                            <!-- /Row -->
                        </div>
                                                
                    </form>
                    @include('revenue.creator.table')
                </div>
            </div>
        </div>
    </div>

    <!-- <div class="modal fade" id="modal-approval" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Update</h4>
            </div>
            <div class="modal-body text-center">
                {!! Form::open(['files' => true, 'id' => 'form-approval','method' => 'put' ]) !!}
                <input type="hidden" name="status" id="status">
                <input type="hidden" name="reason" id="reason">
                <input type="hidden" name="approval" value="true" id="status">
                </form>
                <button type="button" class="btn btn-success" id="btn-approve">Approve</button>
                <button type="button" class="btn btn-warning" id="btn-revision">Revision</button>
                <button type="button" class="btn btn-danger" id="btn-reject">Reject</button>
            </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-reason" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Reason to Reject</h4>
            </div>
            <div class="modal-body">
            <form id="form-reject">
                <div class="form-horizontal">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <textarea class="form-control" name="reason_val" id="reason_val" rows="3" required></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" id="btn-submit">Submit</button>
            </div>
            </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-upload" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Approve Revenue</h4>
            </div>
            <div class="modal-body">
                {!! Form::open(['files' => true, 'id' => 'form-upload','method' => 'put' ]) !!}
                <div class="form-horizontal">
                    <input type="hidden" name="status" id="status-on-upload">
                    <div class="form-group">
                        {!! Form::label('evidence', 'Upload File:', ['class' => 'col-sm-3 control-label']) !!}
                        <input type="hidden" name="approval" value="true">
                        <input id="evidence" type="file" name="evidence" class="file" data-preview-file-type="text" >
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary" >Submit</button>
            </div>
            </form>
            </div>
        </div>
    </div> -->
@endsection
