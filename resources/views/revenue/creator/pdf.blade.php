<html><head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    
    <title>Printed document</title>
    
    <style type="text/css">
    
    body {
      font-family: sans-serif;
      /*font-size: 11pt;*/
    }
    
    div.absolute {
      position: absolute;
      text-align: center;
      vertical-align: middle;
    }
    
    </style>
    </head>
    <body marginwidth="0" marginheight="0" onload="print();" >
    
    <div class="absolute" style="top: 60px; left: 40%; ">
      <img src="{{ url('frontend/assets/img/black_logo.png') }}">
    </div>
    
      @foreach($revenue->creator as $creator)
            @if(($creator->id == auth()->user()->id) && ($creator->pivot->status_paid==3))
              <img src="{{ url('img/paid_.png')}}" width="100" class="" style="position: relative; left: 68%; top: 200px;">
          @endif
      @endforeach
    <div class="absolute" style="top: 150px; left: 60px; right: 60px;">
      <p><strong>Report Revenue {{{auth()->user()->name}}} {{{$months[$revenue->month]}}} {{{$revenue->year}}}</strong><br>Periode Cetak {{{$months[date('m')]}}} {{{date('Y')}}}</p>
    </div>
    
    
    <div class="absolute" style="top: 220px; left: 30%; right: 60px; bottom: 160px; ">
      <table class="detail" style="margin: 0px; border: none;">
    
        <tbody>
          @foreach($revenue->creator as $creator)
          @if($creator->id == auth()->user()->id)
          <tr>
            <td>Revenue</td>
            <td style="padding: 0px 20px 0px 20px;">:</td>
            <td>Rp. {{{number_format($creator->pivot->revenue,0,",",".")}}}</td>
          </tr>
    
          <tr>
            <td>Cost Production</td>
            <td style="padding: 0px 20px 0px 20px;">:</td>
            <td>Rp. {{{number_format($creator->pivot->cost_production,0,",",".")}}}</td>
          </tr>
    
          <tr>
            <td>Nett Revenue</td>
            <td style="padding: 0px 20px 0px 20px;">:</td>
            <td>Rp. {{{number_format($creator->pivot->nett_revenue,0,",",".")}}}</td>
          </tr>
          @if($creator->pivot->status_paid==3)
          <tr>
            <td>Payment Date</td>
            <td style="padding: 0px 20px 0px 20px;">:</td>
            <td>{{$creator->pivot->date_payment}}</td>
          </tr>
          @endif
    
          @endif
          @endforeach
        </tbody>
      </table>
    </div>
    <div class="absolute" style="top: 300px; left: 40px; right: 60px; bottom: 160px;font-size: 12px;">
      <p>*Nett Revenue Belum dipotong pajak</p>
    </div>
    </body></html>
    