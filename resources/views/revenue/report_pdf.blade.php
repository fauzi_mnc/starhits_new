<html>
<head>
<style>
body{
    margin: 10px;
}
table{
    margin: 20px 0px;
}
table, th, td{
    border-collapse: collapse;
}
.table-bordered table, .table-bordered th, .table-bordered td {
    border: 2px solid black;
}
th, td {
    padding: 5px;
    /*text-align: left;
    text-align: center;*/
}
.signature{
    padding: 40px 50px;
}
.silver{
    background-color: silver;
}
</style>
</head>
<body onload="print();">
    <?php
    $dates = $revenue->year."-".$revenue->month."-01";
    ?>
<table class="table-bordered" style="width:100%" border="1">
    <thead style="font-size: 16px;">
        <tr class="silver">
            <th rowspan="2">No</th>
            <th rowspan="2">Channel</th>
            <th colspan="4">Youtube Revenue Periode <?=date('F',strtotime($dates))?> {{$revenue->year}}</th>
        </tr>
        <tr class="silver">
            <th>Revenue</th>
            <th>Cost Production</th>
            <th>Net Revenue</th>
            <th>Share Revenue</th>
        </tr>
    </thead>
    <tbody style="font-size: 14px;">
        <?php 
        $total_revenue=0;
        $total_cost=0;
        $total_nett=0;
        $total_share=0;
        $i=1;
        ?>
        @foreach($revenue->creator as $row)
            <?php
                $total_revenue = $total_revenue+ $row->pivot->revenue;
                $total_cost = $total_cost+ $row->pivot->cost_production;
                $total_nett = $total_nett+ $row->pivot->nett_revenue;
                $total_share = $total_share+ $row->pivot->share_revenue;
            ?>
            <tr>
                <td align="center"><?=$i?></td>
                <td>{{$row->name}}</td>
                <td align="right">Rp. {{number_format((float)$row->pivot->revenue)}},-</td>
                <td align="right">Rp. {!!number_format((float)$row->pivot->cost_production)!!},-</td>
                <td align="right">Rp. {!!number_format((float)$row->pivot->nett_revenue)!!},-</td>
                <td align="right">Rp. {!!number_format((float)$row->pivot->share_revenue)!!},-</td>
            </tr>
        <?php $i++; ?>
        @endforeach
        <tr >
            <td colspan="2" align="center">
                <b>Total :</b> 
            </td>
            <td align="right">
                <b>Rp {{number_format((float)$total_revenue, 0,",",".")}}</b> 
            </td>
            <td align="right">
                <b>Rp {{number_format((float)$total_cost, 0,",",".")}}</b> 
            </td>
            <td align="right">
                <b>Rp {{number_format((float)$total_nett, 0,",",".")}}</b> 
            </td>
            <td align="right">
                <b>Rp {{number_format((float)$total_share, 0,",",".")}}</b> 
            </td>
        </tr>
    </tbody>
        
    
</table>
*Adjust By starhits.id
<br>
<table style="width: 100%; font-size: 15px; text-align: center;" border="1">
    <tr>
        <td>Prepared By</td>
        <td colspan="2">Checked By</td>
        <td colspan="2">Approved By</td>
    </tr>
    <tr>
        <td class="signature" width="150"><i style="font-size: 11px;">Print Date : {{ date('d F Y',strtotime(date("Y-m-d"))) }} </i></td>
        <td class="signature"></td>
        <td class="signature"></td>
        <td class="signature"></td>
        <td class="signature"></td>
    </tr>
    <tr style="font-weight: bold;font-size: 14px;">
        <td><i style="font-size: 11px;">Print by System Starhits</i></td>
        <td>Head Of Contents</td>
        <td>Head Of Finance</td>
        <td>CFO</td>
        <td>Director</td>
    </tr>
</table>

</body>
</html> 