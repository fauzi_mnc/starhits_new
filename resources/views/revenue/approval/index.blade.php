@extends('layouts.crud')

@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Pending Approval</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Revenue</a>
            </li>
            <li class="active">
                <strong>Pending Approval</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
@endsection
@section('contentCrud')
@include('flash::message')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Pending Approval Revenue</h5>
                </div>

                <div class="ibox-content">
                    <form style="margin-bottom: 10px;" class="form-horizontal" role="form">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Revenue</label>
                                        <div class="col-md-4">
                                            {!! Form::select('month', $months, null, ['class' => 'form-control', 'id' => 'month']) !!}
                                        </div>
                                        <div class="col-md-4">
                                            {!! Form::text('year', null, ['class' => 'form-control', 'id' => 'year', 'placeholder' => 'Year']) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="pull-right">
                                        <button type="button" class="btn btn-default clear-filtering"><i class="fa fa-undo"></i> Clear Filtering</button>
                                        <button type="button" class="btn btn-success btn-search"><i class="fa fa-search"></i> Search</button>
                                    </div>
                                </div>
                            </div>
                            <!-- /Row -->
                        </div>
                                                
                    </form>
                    @include('revenue.approval.table')
                </div>
            </div>
        </div>
    </div>
@endsection
