@section('css')
<link href="{{ asset('css/jquery-step.css') }}" rel="stylesheet">
<link href="{{ asset('js/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css') }}" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
<link href="{{ asset('css/plugins/bootstrap-select.min.css') }}" rel="stylesheet">
<style>
.wizard > .content > .body{
    position: relative !important;
}
strong{
    padding-right: 30px;
}
.footer{
    position: relative;
}
.pagination>li{
    display: inline !important;
}
</style>
@include('layouts.datatables_css')
@endsection
<div>
    <!-- @if($formType == 'create')
    <h3>Type Selection </h3>
    <section>
        <div class="form-group">
            <a href="{{route('finance.revenue.download.template')}}" class="btn btn-primary pull-right" id="btn-template">Download Template</a>
        </div>
        <div class="form-group">
            {!! Form::label('month_b', 'Month:', ['class' => 'month_b col-sm-2 control-label']) !!}

            <div class="col-sm-4">
            {!! Form::text('month_b', null, ['class' => 'month_b form-control monthpicker required']) !!}
            </div>

            {!! Form::label('year_b', 'Year:', ['class' => 'year_b col-sm-2 control-label']) !!}

            <div class="col-sm-4">
            {!! Form::text('year_b', null, ['class' => 'year_b form-control yearpicker required']) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('type', 'Input Revenue:', ['class' => 'col-sm-2 control-label']) !!}

            <div class="col-sm-10">
                {!! Form::select('type', ['' => 'Select', 1 => 'From CSV', 2 => 'Manual'], null, ['class' => 'form-control required']) !!}
            </div>
        </div>

        <div class="form-group" id="type-content" style="display:none">
            {!! Form::label('file', '', ['class' => 'col-sm-2 control-label']) !!}

            <div class="col-sm-10" >
                <input id="input-id" type="file" name="csv" data-preview-file-type="text" >
            </div>
        </div>
    </section>
    @endif -->

    
    <h3>Finance Revenue Input</h3>
    <section>
        <div>
            <input type="hidden" name="status" id="status">
            <input type="hidden" name="total_gross_revenue_val" id="total_gross_revenue_val" value="0">
            <div class="form-group">
                 <form class="hide" action="" method="get" name="myForm" id="myForm">
                    {{ csrf_field() }}
                </form> 


                <form action="{{route('finance.revenue.proses_finance')}}" method="post" name="frmProduct" id="frmProduct" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="col-sm-2">
                        <label class="col-sm-2 control-label">Month:</label>
                    </div>
                    <div class="col-sm-4">
                        <input type="text" name="month_a" id="month_a" value="<?php if(!empty($creators->month)){echo $creators->month;} ?>" class="year_a form-control monthpicker" onchange="submitDate();">
                    </div>
                    <div class="col-sm-2">
                        <label class="col-sm-2 control-label">Year:</label>
                    </div>
                    <div class="col-sm-4">
                        <input type="text" name="year_a" id="year_a" value="<?php if(!empty($creators->year)){echo $creators->year;} ?>" class="year_a form-control yearpicker" onchange="submitDate();">
                    </div>
                </form>
            </div>
        
            <script>
                function submitDate(){
                    var month_a = $("#month_a").val();
                    var year_a = $("#year_a").val();
                    //console.log(year_a);
                   if(month_a!="" && year_a!=""){
                         document.frmProduct.submit();
                    }
                }
            </script>
        
            <!-- <div class="form-group" id="creator-field" style="display:none">
                {!! Form::label('creator_id', 'Creator:', ['class' => 'col-sm-2 control-label']) !!}
                
                <div class="col-sm-10" >
                    {!! Form::select('creator_id', $creators, null, ['class' => 'form-control selectpicker rm-creator', 'data-live-search' => 'true', 'data-size' => 10]) !!}
                </div>
            </div>
            <div class="form-group" id="estimate-field" style="display:none">
                {!! Form::label('estimate_gross_revenue', 'Estimate Gross Revenue:', ['class' => 'col-sm-2 control-label ']) !!}

                <div class="col-sm-10">
                    {!! Form::text('estimate_gross_revenue', null, ['class' => 'form-control required']) !!}
                </div>
            </div>
            <div class="form-group" id="cost-field" style="display:none">
                {!! Form::label('cost_production', 'Cost Production:', ['class' => 'col-sm-2 control-label']) !!}

                <div class="col-sm-10">
                    {!! Form::text('cost_production', null, ['class' => 'form-control required']) !!}
                </div>
            </div>
            <div class="form-group" id="revenue-field" style="display:none">
                <div class="col-sm-12">
                    <button type="button" class="btn btn-default pull-right" id="btn-revenue">Submit</button>
                </div>
            </div> 
            @if(!empty($type))
                @if($type == 'csv')
                    <div class="form-group" id="type-content">
                        {!! Form::label('file', '', ['class' => 'col-sm-2 control-label']) !!}

                        <div class="col-sm-10" >
                            <input id="input-id" type="file" name="csv" data-preview-file-type="text" >
                        </div>
                    </div>
                    <button type="button" class="btn btn-success pull-right" disabled id="btn-process-csv">Process CSV</button>
                @endif
            @endif
            -->

            <div id="content-revenue"></div>
            <div class="form-group">
              <div class="col-sm-12">
                    <table class="table table-bordered" id="table-revenue-creator">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Creator</th>
                                <th>Estimate Gross</th>
                                <th>Cost Production</th>
                                <!-- <th>Action</th> -->
                            </tr>
                        </thead>
                        <tbody>     
                        <?php $i = 1; ?>
                        @if(!empty($revenue_creator))
                            <input type="text" class="hide" name="revenue_id" value="<?php if(!empty($creators->id)){echo $creators->id;} ?>">
                            
                            @foreach($revenue_creator as $revenue_creator)
                            <?php
                                $userCreator = DB::table('users')->where(['id'=>$revenue_creator->creator_id])->first();
                            ?>
                                <tr>
                                    <input type="hidden" name="creator_id[]" value="{{$revenue_creator->creator_id}}">
                                    <td>{!!$i++;!!}</td>
                                    <td>{{$userCreator->name}}</td>
                                    <td>
                                        <div class="input-group">
                                          <span class="input-group-addon" id="basic-addon1">Rp.</span>
                                          <input type="text"  name="estimate_gross[]" value="{{$revenue_creator->estimate_gross}}" class="form-control" placeholder="Username" aria-describedby="basic-addon1">
                                        </div>
                                    </td>
                                    <td>
                                        <div class="input-group">
                                          <span class="input-group-addon" id="basic-addon1">Rp.</span>
                                          <input type="text" name="cost_production[]" value="{{$revenue_creator->cost_production}}" class="form-control" placeholder="Username" aria-describedby="basic-addon1">
                                        </div>
                                    </td>
                                    <!-- <td><button type="button" data-id="{{$revenue_creator->creator_id}}" class="btn btn-default edit">Edit</button></td> -->
                                </tr>
                            @endforeach
                            <!-- <tr>
                                <td colspan="4">No data available in table</td>
                            </tr> -->
                        @endif
                                
                            
                        </tbody>
                        <!-- @if ($formType == 'view')
                            <tfoot>
                            <tr>
                                <td colspan="4">
                                    <b>Total Gross Revenue: Rp {{number_format((float)$revenue->total_gross_revenue, 0,",",".")}}</b> 
                                </td>
                            </tr>
                            </tfoot>
                        @endif -->
                    </table>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-12">
                @if($formType != 'view')
                    <button type="button" class="btn btn-primary pull-right" id="btn-add-total">{{ ($formType == 'create') ? 'Add Total Gross Revenue' : 'Update Total Gross Revenue' }}</button>
                @endif
            </div>
        </div>
        <!-- @if($formType == 'view')
            @if(auth()->user()->roles->first()->name == 'finance')
                <a href="{{route('finance.revenue.index')}}" class="btn btn-success pull-right" id="btn-add-total">Back</a>
            @else
                <a href="{{route('revenue.index')}}" class="btn btn-success pull-right" id="btn-add-total">Back</a>
            @endif
        @endif -->
        <button type="submit" class="btn btn-primary pull-left" id="btn-add-total">SUBMIT</button>
        
    </section>

</div>

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/js/plugins/piexif.min.js" type="text/javascript"></script>
    <!-- sortable.min.js is only needed if you wish to sort / rearrange files in initial preview. 
        This must be loaded before fileinput.min.js -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/js/plugins/sortable.min.js" type="text/javascript"></script>
    <!-- purify.min.js is only needed if you wish to purify HTML content in your preview for 
        HTML files. This must be loaded before fileinput.min.js -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/js/plugins/purify.min.js" type="text/javascript"></script>
    <!-- popper.min.js below is needed if you use bootstrap 4.x. You can also use the bootstrap js 
    3.3.x versions without popper.min.js. -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/js/fileinput.min.js"></script>
    <!-- optionally if you need a theme like font awesome theme you can include it as mentioned below -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/themes/fa/theme.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-steps/1.1.0/jquery.steps.js"></script>
    <script src="{{ asset('js/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('js/plugins/bootstrap-select.min.js') }}"></script>
    @include('layouts.datatables_js')

    
    <script>

        function readCsv(){
            /*var file_data = $('#input-id').prop('files')[0];
            var form_data = new FormData();                  
            form_data.append('file', file_data);
            
            $.ajax({
            <?php if(auth()->user()->roles->first()->name == 'finance'){ ?>
                    url: "{{ route('finance.revenue.read') }}",
            <?php }else{ ?>
                    url: "{{ route('revenue.read') }}",
            <?php } ?>
              
              method: "POST",
              data: form_data,
              processData: false,
              contentType: false,
              headers: {
                  'X-CSRF-TOKEN': '{{ csrf_token() }}'
              },
              success: function(response){
                toastr.success("Get data csv or excel success.");
                //console.log(response);
                var table = $('#table-revenue-creator').DataTable();
                $.each(response.data, function (index, value) {
                  console.log(value);
                  table.row.add([index+1, value.creator, value.estimate_gross,'<button type="button" data-id="'+value.creator+'" class="btn btn-default edit">Edit</button>'] )
                .draw()
                .node();

                $('#content-revenue').append('<input type="hidden" name="revenue_creator[]" value="'+value.creator+','+value.estimate_gross+'');
                });
              },
              error: function(response){
                toastr.error("Get data csv or excel error.");
              }
          });*/
        }

        $(document).ready(function(){
            $('.monthpicker').datepicker({
                keyboardNavigation: false,
                forceParse: false,
                autoclose: true,
                viewMode: 'months',
                minViewMode: "months",
                format: 'mm'
            });

            $('.yearpicker').datepicker({
                keyboardNavigation: false,
                forceParse: false,
                autoclose: true,
                viewMode: 'years',
                minViewMode: "years",
                format: 'yyyy'
            });
            
            $('#type').change(function(){
                if($(this).val() == 1){
                    $('#type-content').show();
                } else {
                    $('#type-content').hide();
                }
            });

            $("#input-id").fileinput({
                uploadUrl: "#",
                showUpload: false,
                allowedFileExtensions: ["csv", 'xlsx', 'xls'],
                fileActionSettings: {
                    showUpload: false,
                }
            });

            $('#input-id').change(function(){
                if($(this).get(0).files.length === 0){
                   $('#btn-process-csv').attr('disabled', true); 
                }else{
                    $('#btn-process-csv').attr('disabled', false);
                }
            });

            $('#btn-process-csv').click(function(){
                readCsv();
            });

            @if($formType == 'edit' || $formType == 'view')
            $('.manual').show();
            //$('.month_a').attr('disabled', true);
            //$('.year_a').attr('disabled', true);

            $(".actions ul").append('<li aria-hidden="false" style=""><a href="#" role="menuitem" id="btn-save">Save And Submit</a></li>');
            $('#total_gross_revenue').val('{{ $revenue->total_gross_revenue }}');
            $('#total_gross_revenue_val').val('{{ $revenue->total_gross_revenue }}');
            @endif

            @if($formType == 'edit')
            $('#creator-field').show();
            $('#estimate-field').show();
            $('#cost-field').show();
            $('#revenue-field').show();
            @endif
        });
    
        /*var form = $("#example-form");
        @if($formType != 'view')
            form.children("div").steps({
                labels: {finish: "Save As Draft"},
                headerTag: "h3",
                bodyTag: "section",
                transitionEffect: "slideLeft",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    if(currentIndex == 0){
                        if($('#type').val() == 1){
                            $('#creator-field').hide();
                            $('#estimate-field').hide();
                            $('#cost-field').hide();
                            $('#revenue-field').hide();
                            readCsv();
                        } else {    
                            $('#creator-field').show();
                            $('#estimate-field').show();
                            $('#cost-field').show();
                            $('#revenue-field').show();
                        }

                        if(form.valid() == true){
                            if($('#btn-save').length < 1){
                                $(".actions ul").append('<li aria-hidden="false" style=""><a href="#" role="menuitem" id="btn-save">Save And Submit</a></li>');
                            }

                            var month = $('#month_b').val();
                            var year = $('#year_b').val();
                            $('.month_a').val(month);
                            //$('.month_a').attr('disabled', true);
                            $('.year_a').val(year);
                            //$('.year_a').attr('disabled', true);
                        }
                    }
                    
                    form.validate().settings.ignore = ":disabled,:hidden";
                    return form.valid();
                },
                onFinishing: function (event, currentIndex)
                {
                    form.validate().settings.ignore = ":disabled";
                    $('#estimate_gross_revenue').removeClass('required');
                    $('#cost_production').removeClass('required');
                    
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    $('#status').val(1);
                    form.submit();
                }
            }); 
        @endif  */



        $(".actions").on("click","#btn-save",function(){
            $('#estimate_gross_revenue').removeClass('required');
            $('#cost_production').removeClass('required');
            $('#status').val(3);
            form.submit();
        });

        $(".actions").on("click","#btn-update",function(){
            $('#status').val(3);
            form.submit();
        });

    $('#btn-revenue').click(function(){
        form.validate().settings.ignore = ":disabled";

        if(form.valid()){
            table.row.add( [ index, $('#creator_id option:selected').html(), $('#estimate_gross_revenue').val(), $('#cost_production').val(), '<button type="button" data-id="'+$('#creator_id').val()+'" class="btn btn-default edit">Edit</button>'] )
                .draw()
                .node();
        
            $('#content-revenue').append('<input type="hidden" name="revenue_creator[]" value="'+[$('#creator_id').val(), $('#estimate_gross_revenue').val(), $('#cost_production').val()]+'">');
            $('.rm-creator').find('[value='+$('#creator_id').val()+']').remove();
            $('.rm-creator').selectpicker('refresh');
            $('#estimate_gross_revenue').val('');
            $('#cost_production').val('');
            index++;
        }
        
    });
    var table = $("#table-revenue-creator").DataTable();
    var revenueCreator = [];
    var index = 1;
    
    table.on('click', '.edit', function(e){
        var data = table.row( $(this).parents('tr') ).data();
        
        initModal($(this).attr('data-id'),data);
    });
        
    function initModal(id,arr){
        $('#myModal').modal('show');
        $('#m_index').val(arr[0]);
        $('#m_creator_id').val(arr[1]);
        $('#m_creator_id_val').val(id);
        $('#m_estimate_gross_revenue').val(arr[2]);
        $('#m_cost_production').val(arr[3]);
    }
    
    $('#update').click(function(){
        var idx = parseInt($('#m_index').val()) - 1;
        var d = table.row( idx ).data();
        d[2] = $('#m_estimate_gross_revenue').val();
        d[3] = $('#m_cost_production').val();
        table.row(idx).data(d).invalidate();
        
        $('#myModal').modal('hide');
        
        $('input[name="revenue_creator[]"').eq(idx).val([$('#m_creator_id_val').val(), d[2], d[3]])
    });

    $('#btn-add-total').click(function(){
        $('#modal-add-total').modal('show');
    });

    $('#modal-add-total').on('hidden.bs.modal', function () {
        $('#total_gross_revenue_val').val($('#total_gross_revenue').val());
    })
    </script>
@endsection