@extends('layouts.crud')

@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Revenue</h2>
        <ol class="breadcrumb">
            <li><a href="#">Revenue</a></li>
            <li class="active"><strong>Create</strong></li>
        </ol>
    </div>
</div>
@endsection

@section('contentCrud')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@include('flash::message')

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-info">
                <div class="panel-heading">Create Revenue</div>
                <div class="panel-body" >
               		{!! Form::open(['route' => ['finance.revenue.updated_finance'], 'files' => true, 'class' => 'form-horizontal', 'id' => 'example-form']) !!}
                	@include('revenue.finance.fields', ['formType' => 'create'])


                </div>
            </div>

        </div>
    </div>
    <div class="modal fade" id="myModal" role="dialog">
	    <div class="modal-dialog" role="document">
	        <div class="modal-content">
	        <div class="modal-header">
	            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	            <h4 class="modal-title">Edit</h4>
	        </div>
	        <div class="modal-body">
	            <div class="form-horizontal">
	                <input type="hidden" class="form-control" id="m_index" disabled>
	                <input type="hidden" class="form-control" id="m_creator_id_val" disabled>
	                <div class="form-group">
	                    {!! Form::label('type', 'Creator:', ['class' => 'col-sm-3 control-label']) !!}

	                    <div class="col-sm-9">
	                        <input type="text" class="form-control" id="m_creator_id" disabled>
	                    </div>
	                </div>
	                <div class="form-group">
	                    {!! Form::label('type', 'Estimate Gross Revenue:', ['class' => 'col-sm-3 control-label']) !!}
	                    <div class="col-sm-9">
	                        <input type="text" class="form-control" id="m_estimate_gross_revenue">
	                    </div>
	                </div>
	                <div class="form-group">
	                    {!! Form::label('type', 'Cross Production:', ['class' => 'col-sm-3 control-label']) !!}

	                    <div class="col-sm-9">
	                        <input type="text" class="form-control" id="m_cost_production">
	                    </div>
	                </div>
	            </div>
	        </div>
	        <div class="modal-footer">
	            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	            <button type="button" class="btn btn-primary" id="update">Save changes</button>
	        </div>
	        </div><!-- /.modal-content -->
	    </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div class="modal fade" id="modal-add-total" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add asasd</h4>
            </div>
            <div class="modal-body">
                <div class="form-horizontal">
                    <div class="form-group">
                        {!! Form::label('total_gross_revenue', 'Total Gross Revenue:', ['class' => 'col-sm-3 control-label']) !!}

                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="total_gross_revenue" value="<?php if(!empty($creators->total_gross_revenue)){echo $creators->total_gross_revenue;}?>" id="total_gross_revenue">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Save changes</button>
            </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endsection