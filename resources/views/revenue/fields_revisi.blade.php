@section('css')
<link href="{{ asset('css/jquery-step.css') }}" rel="stylesheet">
<link href="{{ asset('js/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css') }}" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
<link href="{{ asset('css/plugins/bootstrap-select.min.css') }}" rel="stylesheet">
<style>
    .wizard > .content > .body{
    position: relative !important;
    }
    strong{
    padding-right: 30px;
    }
    .footer{
    position: relative;
    }
    .pagination>li{
    display: inline !important;
    }
</style>
@include('layouts.datatables_css')
@endsection
<div>
    @if($formType == 'create')
    <h3>Type Selection</h3>
    <section>
        <div class="form-group">
            @if(auth()->user()->roles->first()->name == 'finance')
            <a href="{{route('finance.revenue.download.template')}}" class="btn btn-primary pull-right" id="btn-template">Download Template</a>
            @else
            <a href="{{route('revenue.download.template')}}" class="btn btn-primary pull-right" id="btn-template">Download Template</a>
            @endif
        </div>
        <div class="form-group">
            {!! Form::label('month_b', 'Month:', ['class' => 'month_b col-sm-2 control-label']) !!}
            <div class="col-sm-4">
                {!! Form::text('month_b', null, ['class' => 'month_b form-control monthpicker required']) !!}
            </div>
            {!! Form::label('year_b', 'Year:', ['class' => 'year_b col-sm-2 control-label']) !!}
            <div class="col-sm-4">
                {!! Form::text('year_b', null, ['class' => 'year_b form-control yearpicker required']) !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('type', 'Input Revenue:', ['class' => 'col-sm-2 control-label']) !!}
            <div class="col-sm-10">
                {!! Form::select('type', ['' => 'Select', 1 => 'Upload Excel and CSV'], null, ['class' => 'form-control required']) !!}
            </div>
        </div>
        <div class="form-group" id="type-content" style="display:none">
            {!! Form::label('file', '', ['class' => 'col-sm-2 control-label']) !!}
            <div class="col-sm-10" >
                <input id="input-id" type="file" name="csv" data-preview-file-type="text" >
            </div>
        </div>
    </section>
    @endif
    @if($formType != 'view')
    <h3>Revisi Revenue</h3>
    @endif
    <section>
        <div>
            <input type="hidden" name="status" id="status">
            @if($formType == 'edit')
                <input type="hidden" name="revenue_id" id="revenue_id" value="{{$revenue->id}}"> 
            @endif
            <input type="hidden" name="total_gross_revenue_val" id="total_gross_revenue_val" value="0">
            <div class="form-group">
                {!! Form::label('month_a', 'Month:', ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-sm-4">
                    {!! Form::text('month_a', ($formType == 'edit' || $formType == 'view' ) ? $revenue->month : null, ['class' => 'month_a form-control monthpicker']) !!}
                </div>
                {!! Form::label('year_a', 'Year:', ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-sm-4">
                    {!! Form::text('year_a', ($formType == 'edit' || $formType == 'view' ) ? $revenue->year : null, ['class' => 'year_a form-control yearpicker']) !!}
                </div>
                @if(!empty($type))
                    @if($type == 'csv')
                        <input type="hidden" name="reupload" value="true">
                    @endif
                @endif
            </div>
            @if(empty($revenue->status) OR $revenue->status == '7')
            <div class="form-group" id="creator-field" style="display:none">
                {!! Form::label('creator_id', 'Creator:', ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-sm-10" >
                    {!! Form::select('creator_id', $creators, null, ['class' => 'form-control selectpicker rm-creator', 'data-live-search' => 'true', 'data-size' => 10]) !!}
                </div>
            </div>
            <div class="form-group" id="estimate-field" style="display:none">
                {!! Form::label('estimate_gross_revenue', 'Estimate Gross Revenue:', ['class' => 'col-sm-2 control-label ']) !!}
                <div class="col-sm-10">
                    {!! Form::text('estimate_gross_revenue', null, ['class' => 'form-control required']) !!}
                </div>
            </div>
            @if(auth()->user()->roles->first()->name == 'finance')
            <div class="form-group" id="cost-field" style="display:none">
                {!! Form::label('cost_production', 'Cost Production:', ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-sm-10">
                    {!! Form::text('cost_production', null, ['class' => 'form-control']) !!}
                </div>
            </div>
            @endif
            <div class="form-group" id="revenue-field" style="display:none">
                <div class="col-sm-12">
                    <button type="button" class="btn btn-default pull-right" id="btn-revenue">Submit</button>
                </div>
            </div>
            @endif
            @if(!empty($type))
            @if($type == 'csv')
            <div class="form-group" id="type-content">
                {!! Form::label('file', '', ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-sm-10" >
                    <input id="input-id" type="file" name="csv" data-preview-file-type="text" >
                </div>
            </div>
            <button type="button" class="btn btn-success pull-right" disabled id="btn-process-csv">Process CSV</button>
            @endif
            @endif

            @if($formType == 'edit123')
                <div class="form-group" id="pay-field" >
                    {!! Form::label('date_payment', 'Date Payment:', ['class' => ' col-sm-2 control-label']) !!}
                    <div class="col-sm-10">
                        {!! Form::text('date_payment', ($formType == 'edit' || $formType == 'view' ) ? $revenue->date_payment : null, ['class' => 'date_payment form-control','autocomplete'=>'off']) !!}
                    </div>
                </div>
                <div class="form-group">
                {!! Form::label('type', 'Status Payment:', ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-sm-10">
                    {!! Form::select('status_paid', [1 => 'Pending', 2 => 'On Process', 3 => 'Paid'], null, ['class' => 'form-control', 'placeholder'=>'Select Status Payment']) !!}
                </div>
            @endif
            <div id="content-revenue"></div>
            <div class="form-group">
                <div class="col-sm-12">
                    <table class="table table-bordered" id="table-revenue-creator">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Creator</th>
                                <th>Revenue</th>
                                <th>Cost Production</th>
                                <th>Net Revenue</th>
                                <th>Share Revenue</th>
                                @if($formType == 'edit')
                                <th> Action</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                            @if($formType == 'edit' || $formType == 'view' )
                            @if(!empty($type))
                            @if($type != 'csv')
                                @foreach($revenue->creator as $row)
                                    <tr>
                                        <input type="text" name="revenue_creator[]" value="{{$row->pivot->creator_id}},{{$row->pivot->estimate_gross}},{{$row->pivot->cost_production}}">
                                        <input type="hidden" name="revenue_status" value="{{$revenue->status}}">
                                        <td>{!!$i++;!!}</td>
                                        <td>{{$row->name}}</td>
                                        <td>Rp. {!!number_format((float)$row->pivot->estimate_gross)!!},-</td>

                                        @if(auth()->user()->roles->first()->name == 'finance' && $revenue->status=='1')
                                            <td>{!!$row->pivot->cost_production;!!}</td>
                                        @elseif(auth()->user()->roles->first()->name == 'finance' && $revenue->status=='2')
                                            <td><input type="text" class="form-control" name="cost_production" id="cost_production"></td>
                                        @endif

                                        @if($formType == 'view' || auth()->user()->roles->first()->name == 'admin')
                                            @elseif(auth()->user()->roles->first()->name == 'finance' && $revenue->status=='3' || auth()->user()->roles->first()->name == 'finance' && $revenue->status=='2')
                                        @else
                                        <td><button type="button" data-id="{{$row->pivot->creator_id}}" class="btn btn-default edit">Edit</button></td>
                                        @endif
                                    </tr>
                                @endforeach
                            @endif

                            @else
                                <?php 
                                $no =0; 
                                $total_revenue=0;
                                $total_cost=0;
                                $total_nett=0;
                                $total_share=0;
                                ?>
                                @foreach($revenue->creator as $row)
                                    <?php
                                        $total_revenue = $total_revenue+ $row->pivot->revenue;
                                        $total_cost = $total_cost+ $row->pivot->cost_production;
                                        $total_nett = $total_nett+ $row->pivot->nett_revenue;
                                        $total_share = $total_share+ $row->pivot->share_revenue;
                                    ?>
                                    <tr>
                                        <!-- <input type="text" name="revenue_creator[]" value="{{$row->pivot->creator_id}},{{$row->name}},{{$row->pivot->revenue}},{{$row->pivot->cost_production}},{{$row->pivot->nett_revenue}},{{$row->pivot->share_revenue}}"> -->
                                        <!-- value="'+value.user_id+','+value.creator+','+value.revenue+','+value.cost+','+value.net_revenue+','+value.share_revenue+'" -->
                                        <!-- <input type="hidden" name="" id="rev_id_<?=$no?>" value="{{$row->pivot->creator_id}}"> -->
                                        <!-- <input type="text" name="revenue_status" value="{{$revenue->status}}"> -->
                                        <td>{!!$i++;!!}</td>
                                        <td>{{$row->name}}</td>
                                        <td>Rp. {{number_format((float)$row->pivot->revenue)}},-</td>
                                        <td>Rp. {!!number_format((float)$row->pivot->cost_production)!!},-</td>
                                        <td>Rp. {!!number_format((float)$row->pivot->nett_revenue)!!},-</td>
                                        <td>Rp. {!!number_format((float)$row->pivot->share_revenue)!!},-</td>
                                        @if($formType == 'edit') 
                                        <td align="center">
                                            <i
                                            data-toggle="modal" 
                                            data-target="#ContohModal" 
                                            data-a="{{$row->pivot->creator_id}}" 
                                            data-b="{{$row->pivot->revenue_id}}" 
                                            data-creator_txt="{{$row->name}}"
                                            data-revenue="{{$row->pivot->revenue}}"
                                            data-cost="{{$row->pivot->cost_production}}"
                                            data-netrev="{{$row->pivot->nett_revenue}}"
                                            data-sharerev="{{$row->pivot->share_revenue}}"
                                            class="btn btn-default fa fa-pencil"
                                            ></i>
                                            
                                        </td>
                                        @endif
                                    </tr>
                                <?php $no ++; ?>
                                @endforeach
                            @endif
                            @endif
                        </tbody>
                        @if ($formType == 'view')
                        <tfoot>
                            <tr>
                                <td colspan="2">
                                    <b>Total :</b> 
                                </td>
                                <td>
                                    <b>Rp {{number_format((float)$total_revenue, 0,",",".")}}</b> 
                                </td>
                                <td>
                                    <b>Rp {{number_format((float)$total_cost, 0,",",".")}}</b> 
                                </td>
                                <td>
                                    <b>Rp {{number_format((float)$total_nett, 0,",",".")}}</b> 
                                </td>
                                <td>
                                    <b>Rp {{number_format((float)$total_share, 0,",",".")}}</b> 
                                </td>
                            </tr>
                        </tfoot>
                        @endif
                    </table>
                    <script type="text/javascript">
                        /*function coba(i){
                            $('#revenue_creator_'+i+'').val( $('#revenuedump_'+i+'').val()+","+$('#cost_production_'+i+'').val() );
                        }*/
                    </script>
                </div>
            </div>
        </div>
        @if($formType == 'view' || $formType == 'edit')
            @if(auth()->user()->roles->first()->name == 'finance')
                <a href="{{route('finance.revenue.index')}}" class="btn btn-success pull-right" id="btn-add-total">Back</a>
            @else
                <a href="{{route('revenue.index')}}" class="btn btn-success pull-right" id="btn-add-total">Back</a>
            @endif
        @endif


<!-- Modal untuk merubah Status PAID -->
<div class="modal fade" id="ContohModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel">Revisi Revenue</h4>
      </div>
      <div class="modal-body">
        @if(auth()->user()->roles->first()->name == 'admin')
        <form action="{{route('revenue.post.revisi')}}" method="post" name="form_Payment" autocomplete='off'>
        @else
        <form action="{{route('finance.revenue.post.revisi')}}" method="post" name="form_Payment" autocomplete='off'>
        @endif
            <div class="form-group hide">
                <label for="recipient-name" class="control-label">Revenue ID:</label>
                <input type="text" class="form-control" name="id_creator_" id="id_creator_">
                <input type="text" class="form-control" name="id_revenue_" id="id_revenue_">
            </div>
            <div class="form-group">
            {!! Form::label('type', 'Creator:', ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-sm-10">
                    {!! Form::text('creator_txt', ($formType == 'edit' || $formType == 'view' ) ? $revenue->date_payment : null, ['class' => 'creator_txt form-control','autocomplete'=>'off','readonly'=>'readonly']) !!}
                </div>
            </div>
            <div class="form-group">
            {!! Form::label('type', 'Revenue:', ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-sm-10">
                    {!! Form::text('revenue_txt', ($formType == 'edit' || $formType == 'view' ) ? $revenue->date_payment : null, ['class' => 'revenue_txt form-control','autocomplete'=>'off']) !!}
                </div>
            </div>
            <div class="form-group">
            {!! Form::label('type', 'Cost Production:', ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-sm-10">
                    {!! Form::text('cost_txt', ($formType == 'edit' || $formType == 'view' ) ? $revenue->date_payment : null, ['class' => 'cost_txt form-control','autocomplete'=>'off']) !!}
                </div>
            </div>
            <div class="form-group">
            {!! Form::label('type', 'Net Revenue:', ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-sm-10">
                    {!! Form::text('net_rev_txt', ($formType == 'edit' || $formType == 'view' ) ? $revenue->date_payment : null, ['class' => 'net_rev_txt form-control','autocomplete'=>'off']) !!}
                </div>
            </div>
            <div class="form-group">
            {!! Form::label('type', 'Share Revenue:', ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-sm-10">
                    {!! Form::text('share_rev_txt', ($formType == 'edit' || $formType == 'view' ) ? $revenue->date_payment : null, ['class' => 'share_rev_txt form-control','autocomplete'=>'off']) !!}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
    </form>
    </div>
  </div>
</div>

    </section>
</div>

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/js/plugins/piexif.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/js/plugins/sortable.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/js/plugins/purify.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/js/fileinput.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/themes/fa/theme.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-steps/1.1.0/jquery.steps.js"></script>
<script src="{{ asset('js/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-select.min.js') }}"></script>
@include('layouts.datatables_js')
<script>
    function readCsv(){
        var file_data = $('#input-id').prop('files')[0];
        var form_data = new FormData();                  
        form_data.append('file', file_data);
        
        $.ajax({
            url: "{{ route('revenue.read') }}",
            method: "POST",
            data: form_data,
            processData: false,
            contentType: false,
            headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            success: function(response){
            toastr.success("Get data csv or excel success.");
            //console.log(value.user_id);
            
            var table = $('#table-revenue-creator').DataTable();
            $.each(response.data, function (index, value) {
                console.log(value.user_id);
                if(value.user_id!=null || value.user_id!=""){
                    table.row.add([index+1, value.creator, value.revenue,value.cost,value.net_revenue,value.share_revenue,])
                    .draw()
                    .node();
                    $('#content-revenue').append('<input type="hidden" name="revenue_creator[]" value="'+value.user_id+','+value.creator+','+value.revenue+','+value.cost+','+value.net_revenue+','+value.share_revenue+'">');  
                }
                
                //$('#contentrev').append('<input type="text" name="revenuedump[]" value="'+value.creator+','+value.estimate_gross+'">');
                //console.log(value);
            });
            },
            error: function(response){
            toastr.error("Get data csv or excel error.");
            }
        });
    }
    
    $(document).ready(function(){
        $('#ContohModal').on('shown.bs.modal', function (event) {
            var val = $(event.relatedTarget) // Button that triggered the modal
            var creator_id = val.data('a')
            var revenue_id = val.data('b')
            var creator_txt = val.data('creator_txt')
            var revenue_txt = val.data('revenue')
            var cost_txt = val.data('cost')
            var net_txt = val.data('netrev')
            var share_txt = val.data('sharerev')

            var modal = $(this)
            modal.find('.modal-body #id_creator_').val(creator_id)
            modal.find('.modal-body #id_revenue_').val(revenue_id)
            modal.find('.modal-body .creator_txt').val(creator_txt)
            modal.find('.modal-body .revenue_txt').val(revenue_txt)
            modal.find('.modal-body .cost_txt').val(cost_txt)
            modal.find('.modal-body .net_rev_txt').val(net_txt)
            modal.find('.modal-body .share_rev_txt').val(share_txt)
        })



        $('.date_payment').datepicker({
            keyboardNavigation: false,
            forceParse: false,
            autoclose: false,
            //viewMode: 'months',
           // minViewMode: 'months',
            format: 'yyyy-mm-dd'
        });

        $('.monthpicker').datepicker({
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true,
            viewMode: 'months',
            minViewMode: 'months',
            format: 'mm'
        });
    
        $('.yearpicker').datepicker({
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true,
            viewMode: 'years',
            minViewMode: 'years',
            format: 'yyyy'
        });
        
        $('#type').change(function(){
            if($(this).val() == 1){
                $('#type-content').show();
            } else {
                $('#type-content').hide();
            }
        });
    
        $("#input-id").fileinput({
            uploadUrl: "#",
            showUpload: false,
            allowedFileExtensions: ["csv", 'xlsx'],
            fileActionSettings: {
                showUpload: false,
            }
        });
    
        $('#input-id').change(function(){
            if($(this).get(0).files.length === 0){
                $('#btn-process-csv').attr('disabled', true); 
            }else{
                $('#btn-process-csv').attr('disabled', false);
            }
        });
    
        $('#btn-process-csv').click(function(){
            readCsv();
        });
    
        @if($formType == 'edit' || $formType == 'view')
        /*$('.manual').show();
        $('.month_a').attr('disabled', true);
        $('.year_a').attr('disabled', true);
        $(".actions ul").append('<li aria-hidden="false" style=""><a href="#" role="menuitem" id="btn-save">Save And Submit</a></li>');
        $('#total_gross_revenue').val('{{ $revenue->total_gross_revenue }}');
        $('#total_gross_revenue_val').val('{{ $revenue->total_gross_revenue }}');*/
        @endif
    
        @if($formType == 'edit')
        /*$('#creator-field').show();
        $('#estimate-field').show();
        $('#cost-field').show();
        $('#revenue-field').show();*/
        @endif
    });


    
    var form = $("#example-form");
    @if($formType == 'create' )
        form.children("div").steps({
            labels: {finish: "Save As Draft"},
            headerTag: "h3",
            bodyTag: "section",
            transitionEffect: "slideLeft",
            onStepChanging: function (event, currentIndex, newIndex)
            {
                if(currentIndex == 0){
                    if($('#type').val() == 1){
                        $('#creator-field').hide();
                        $('#estimate-field').hide();
                        @if(auth()->user()->roles->first()->name == 'finance')
                        $('#cost-field').hide();
                        @endif
                        $('#revenue-field').hide();
                        readCsv();
                    } else {    
                        $('#creator-field').show();
                        $('#estimate-field').show();
                        @if(auth()->user()->roles->first()->name == 'finance')
                        $('#cost-field').show();
                        @endif
                        $('#revenue-field').show();
                    }
    
                    if(form.valid() == true){
                        if($('#btn-save').length < 1){
                            $(".actions ul").append('<li aria-hidden="false" style=""><a href="#" role="menuitem" id="btn-save">Save And Submit</a></li>');
                        }
    
                        var month = $('#month_b').val();
                        var year = $('#year_b').val();
                        $('.month_a').val(month);
                        $('.month_a').attr('disabled', true);
                        $('.year_a').val(year);
                        $('.year_a').attr('disabled', true);
                    }
                }
                
                form.validate().settings.ignore = ":disabled,:hidden";
                return form.valid();
            },
            onFinishing: function (event, currentIndex)
            {
                form.validate().settings.ignore = ":disabled";
                $('#estimate_gross_revenue').removeClass('required');
                @if(auth()->user()->roles->first()->name == 'finance')
                $('#cost_production').removeClass('required');
                @endif
                
                return form.valid();
            },
            onFinished: function (event, currentIndex)
            {
                $('#status').val(7);
                form.submit();
            }
        });
    @endif   
    
    $(".actions").on("click","#btn-save",function(){
        $('#estimate_gross_revenue').removeClass('required');
        @if(auth()->user()->roles->first()->name == 'finance')
        $('#cost_production').removeClass('required');
        @endif
        $('#status').val(2);
        form.submit();
    });
    
    $(".actions").on("click","#btn-update",function(){
        $('#status').val(3);
        form.submit();
    });
    
    $('#btn-revenue').click(function(){
    form.validate().settings.ignore = ":disabled";
    
    if(form.valid()){
        table.row.add( [ index, $('#creator_id option:selected').html(), $('#estimate_gross_revenue').val(),'<button type="button" data-id="'+$('#creator_id').val()+'" class="btn btn-default edit">Edit</button>'] )
            .draw()
            .node();
    
        $('#content-revenue').append('<input type="hidden" name="revenue_creator[]" value="'+[$('#creator_id').val(), $('#estimate_gross_revenue').val()]+'">');
    
        $('.rm-creator').find('[value='+$('#creator_id').val()+']').remove();
        $('.rm-creator').selectpicker('refresh');
        $('#estimate_gross_revenue').val('');
        @if(auth()->user()->roles->first()->name == 'finance')
        $('#cost_production').val('');
        @endif
        index++;
    }
    
    });
    var table = $("#table-revenue-creator").DataTable();
    var revenueCreator = [];
    var index = 1;
    
    table.on('click', '.edit', function(e){
    var data = table.row( $(this).parents('tr') ).data();
    
    initModal($(this).attr('data-id'),data);
    });
    
    function initModal(id,arr){
    $('#myModal').modal('show');
    
    $('#m_index').val(arr[0]);
    $('#m_creator_id').val(arr[1]);
    $('#m_creator_id_val').val(id);
    $('#m_estimate_gross_revenue').val(arr[2]);
    @if(auth()->user()->roles->first()->name == 'finance')
    $('#m_cost_production').val(arr[3]);
    @endif
    }
    
    $('#update').click(function(){
    var idx = parseInt($('#m_index').val()) - 1;
    
    var d = table.row( idx ).data();
    d[2] = $('#m_estimate_gross_revenue').val();
    //d[3] = $('#m_cost_production').val();
    table.row(idx).data(d).invalidate();
    
    $('#myModal').modal('hide');
    
    $('input[name="revenue_creator[]"').eq(idx).val([$('#m_creator_id_val').val(), d[2], d[3]])
    });
    
    $('#btn-add-total').click(function(){
    $('#modal-add-total').modal('show');
    });
    
    $('#modal-add-total').on('hidden.bs.modal', function () {
    $('#total_gross_revenue_val').val($('#total_gross_revenue').val());
    })
</script>
@endsection