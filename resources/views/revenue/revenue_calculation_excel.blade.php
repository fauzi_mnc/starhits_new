<html>
<head>
<style>
.table-bordered table, .table-bordered th, .table-bordered td {
    border: 1px solid #000000;
}
th, td {
    padding: 5px;
    text-align: left;
    text-align: center;
}
.signature{
    padding: 40px 50px;
}

</style>
</head>
<body>

<table class="table-bordered" style="width:100%">
    <thead>
        <tr>
            <th></th>
            <th rowspan="2">No</th>
            <th rowspan="2">Channel</th>
            <th colspan="5">Youtube Revenue Periode {{$month}} {{$revenue->year}}</th>
        </tr>
        <tr>
            <th></th>
            <th></th>
            <th></th>
            <th>Estimate Gross Revenue</th>
            <th>%</th>
            <th>Gross Revenue</th>
            <th>Cost Production</th>
            <th>Revenue</th>
        </tr>
    </thead>
    <tbody>
        <?php $i = 0; ?>
        @foreach($creators as $row)
        <tr>
            <td></td>
            <td>{{++$i}}</td>
            <td>{{ $row->name }}</td>
            <td>Rp {{ number_format((float)$row->pivot->estimate_gross, 0,",",".") }}</td>
            <td>{{number_format((float)$row->percentage_revenue * 100, 2,",",".")}}%</td>
            <td>Rp {{number_format((float)$row->gross_revenue, 0,",",".")}}</td>
            <td>Rp {{ number_format((float)$row->pivot->cost_production, 0,",",".") }}</td>
            <td>Rp {{number_format((float)$row->nett_revenue, 0,",",".")}}</td>
        </tr>
        @endforeach  
        <tr>
            <td></td>
            <td></td>
            <td>TOTAL</td>
            <td>Rp {{number_format((float)$creators->sum('pivot.estimate_gross'), 0,",",".")}}</td>
            <td>{{number_format((float)$creators->sum('percentage_revenue')* 100, 2,",",".")}}%</td>
            <td>Rp {{number_format((float)$creators->sum('gross_revenue'), 0,",",".")}}</td>
            <td>Rp {{ number_format((float)$creators->sum('pivot.cost_production'), 0,",",".") }}</td>
            <td>Rp {{number_format((float)$creators->sum('nett_revenue'), 0,",",".")}}</td>
        </tr>
    </tbody>
</table>
<table style="width: 100%;">
    <tr>
        <td></td>
        <td>*Adjust By starhits.id</td>
    </tr>
</table>
<br>
<table class="table-bordered" style="width: 100%;">
    <tr>
        <td></td>
        <td>Prepared By</td>
        <td colspan="3">Checked By</td>
        <td>Approved By</td>
    </tr>
    <tr>
        <td></td>
        <td class="signature"></td>
        <td class="signature"></td>
        <td class="signature"></td>
        <td class="signature"></td>
        <td class="signature"></td>
    </tr>
    <tr>
        <td></td>
        <td>{{ auth()->user()->name }}</td>
        @foreach($checker as $user)
        <td>{{ $user->name }}</td>
        @endforeach
        <td>{{ $approver->name }}</td>
    </tr>
</table>

</body>
</html>
