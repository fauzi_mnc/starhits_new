@extends('layouts.crud')

@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Bookmark</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Bookmark</a>
            </li>
            <li class="active">
                <strong>List</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
@endsection
@section('contentCrud')
@include('flash::message')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Bookmark</h5>
                </div>

                <div class="ibox-content">
                    <div class="feed-activity-list">
                        @foreach($bookmarks as $row)
                        <div class="feed-element">
                            <a href="#" class="pull-left">
                                <img alt="image" height="100" class="img" src="{{ asset($row->image) }}">
                            </a>
                            <div class="media-body ">
                                <small class="pull-right text-navy">
                                    {!! Form::open(['route' => ['creator.bookmark.destroy', $row->id], 'method' => 'delete']) !!}

                                        {!! Form::button('<i class="fa fa-star"></i>', [
                                            'type' => 'submit',
                                            'class' => 'btn btn-outline btn-warning  dim',
                                            'onclick' => "return confirm('Are you sure want to remove this bookmark?')"
                                        ]) !!}
                                    {!! Form::close() !!}
                                </small>
                                {{ $row->title }}
                            </div>
                        </div>
                        @endforeach
                    </div>
                    {{ $bookmarks->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection
