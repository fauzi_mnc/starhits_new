<!-- Stored in resources/views/child.blade.php -->
@extends('layouts.frontend')

@section('title', 'Search Series')

@section('content')

<div class="container-fluid filter">
    <div class="row">
        <div class="col-sm-12">
            <div class="top-filter-area padding-tb-10">
                <div class="container-fluid">
                    <form action="" method="post">
                        {{ csrf_field() }}
                    <div class="row">
                        <!--<div class="col"></div>-->
                        <div class="col-sm-4">
                            <input name="type" class="form-control" placeholder="Video by Series" type="text">
                            </input>
                        </div>
                        <div class="col-sm-4">
                            <select name="sortby" class="select-box">
                                <option selected="selected" value="0">
                                    Sort by
                                </option>
                                <option value="1">
                                    Most Viewed
                                </option>
                                <option value="2">
                                    Most Liked
                                </option>
                                <option value="3">
                                    Most Commented
                                </option>
                                <option value="4">
                                    Published Date
                                </option>
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <button class="btn btn-filter btn-lg btn-block site-btn" name="search">
                                <i class="fa fa-search">
                                </i>
                                Search
                            </button>
                        </div>
                        <!--<div class="col"></div>-->
                    </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="search-result">
                <div class="container-fluid">
                    <div class="row">
                        @if(sizeof($models)>0)
                        <div class="col-md-12">
                            <h5>
                                {{ sizeof($models) }} matches sort by: {{ $type }} in most {{ $sort }} - {{ sizeof($models) }}
                            </h5>
                            <hr>
                                <ul class="updates">
                                    @foreach($models as $key => $value)
                                    <li>
                                        <a href="{{ url('/videos/'.$value['slug']) }}">
                                            {{ $value['title'] }}
                                            <span>
                                                {{ $value['created_at'] }}
                                            </span>
                                        </a>
                                    </li>
                                    @endforeach
                                </ul>
                                <div class="col-md-12 page">
                                    <div class="row navigation">
                                        {{-- $models->links('vendor.pagination.custom') --}}
                                    </div>
                                </div>
                            </hr>
                        </div>
                        @else
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection