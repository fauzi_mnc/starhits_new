<!-- Stored in resources/views/child.blade.php -->
@extends('layouts.frontend')

@section('title', 'Series')

@section('content')
<div class="container-fluid series">
    <div class="col-md-12">
        <div class="col-sm-12">
            <div class="title">
                <div class="col-sm-12" style="padding: 0;">
                    <div class="row">
                        <div class="col-md-8" style="padding: 0;">
                            <h6>
                                SERIES
                            </h6>
                        </div>
                        <div class="col-md-4" style="padding: 0;">
                            <a class="btn btn-filter filter" data-target=".bs-example-modal-lg-filter" data-toggle="modal" href="#">
                                <i class="fa fa-filter" style="font-size:24px">
                                </i>
                                Filter
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="{{ count($series)>=1? 'row item-series':'' }}">
            @forelse($series as $value)
            <div class="col-lg-6 col-series">
                <div class="row">
                    <div class="col-sm-6 inner-col-series-left">
                        <article class="caption">
                            <a href="{{ url('/series/'.$value->slug) }}">
                                <img class="caption__media img-fluid" src="{{ url('/'). Image::url($value->image,350,500,array('crop')) }}"/>
                            </a>
                            <a href="#">
                                <div class="caption__overlay">
                                    <h1 class="caption__overlay__title">
                                        {{ $value->title }}
                                    </h1>
                                    <p class="caption__overlay__content">
                                        {!! str_limit($value->content, 200) !!}
                                    </p>
                                </div>
                            </a>
                        </article>
                    </div>
                    <div class="col-sm-6 inner-col-series-right">
                        <div class="title-series">
                            <h7>
                                <a href="{{ url('/series/'.$value->slug) }}">
                                    {{ $value->title }}
                                </a>
                            </h7>
                        </div>
                        <div class="description text-justify">
                            <p>
                                {!! str_limit($value->content, 200) !!}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            @empty
            <div class="col-sm-12">
                <br>
                    <div class="alert alert-warning text-center" role="alert">
                        There is no data to display!
                    </div>
                <br>
            </div>
            @endforelse
        </div>
        <div class="col-md-12 page">
            <div class="row navigation">
                {{ $series->links('vendor.pagination.custom') }}
            </div>
        </div>
    </div>
</div>

@include('frontend.series.modals-filter')

@endsection
