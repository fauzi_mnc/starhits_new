@push('meta')    
        <meta property="og:url" content="{{ url()->current() }}" />
        <meta property="og:title" content="{{ $metaTag->name }}" />
        <meta property="og:description" content="{{ $metaTag->biodata }}" />
        <meta property="og:image" content="{{ url($metaTag->image) }}" />
        <meta property="fb:app_id" content="952765028218113" />
@endpush
<!-- Stored in resources/views/child.blade.php -->
@extends('layouts.frontend')

@section('title', $metaTag->name)

@push('preloader')
<div class="se-pre-con">
    <div class="col-sm-12 text-center pre-con">
        <div class="col">
            <div class="logo-preloader">
                <img alt="" class="img-fluid" src="{{ asset('frontend/assets/img/logo-home.png') }}">
                </img>
            </div>
        </div>
        <div class="col">
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg">
                <defs>
                    <filter id="gooey">
                        <fegaussianblur in="SourceGraphic" result="blur" stddeviation="10">
                        </fegaussianblur>
                        <fecolormatrix in="blur" mode="matrix" result="goo" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 18 -7">
                        </fecolormatrix>
                        <feblend in="SourceGraphic" in2="goo">
                        </feblend>
                    </filter>
                </defs>
            </svg>
            <div class="blob blob-0"></div>
            <div class="blob blob-1"></div>
            <div class="blob blob-2"></div>
            <div class="blob blob-3"></div>
            <div class="blob blob-4"></div>
            <div class="blob blob-5"></div>
        </div>
    </div>
</div>
@endpush

@section('content')
<div class="container-fluid single-creators">
    <div class="col-sm-12">
        <div class="item-single-creators">
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-2 item-single-creators-image">
                        <a class="d-block" href="#">
                            @if(!empty($creators->image))
                               <?php $links = explode("/", $creators->image);?>

                                @if(in_array($links[0], ['http:', 'https:']))
                                    <img src="{{ $creators->image }}" style="width: 150px; height: 150px;"/>
                                @else
                                    <img src="{{ url('/'). Image::url($creators->image,150,150,array('crop')) }}"/>
                                @endif
                            @else
                                <img src="{{ url('/'). Image::url('frontend/assets/img/icon/default.png',150,150,array('crop')) }}"/>
                            @endif
                        </a>
                    </div>
                    <div class="col-sm-5 item-single-creators-text">
                        <div class="item-single-creators-text-title">
                            <h4>
                                {{$creators->name}}
                            </h4>
                        </div>
                        <div class="item-single-creators-text-paragraph">
                            <p>
                                {{$creators->biodata}}
                            </p>
                        </div>
                        <div class="item-single-creators-text-videos">
                            <label>
                                <a class="subscriber" href="#">
                                    @if(!empty($creators->provider_id))
                                        {{ bd_nice_number($creators->subscribers) }} Subscriber
                                    @else
                                        0 Subscriber
                                    @endif
                                </a>
                                <a href="#">
                                    {{$creators->totalVideos}} Videos
                                </a>
                                |
                                <a href="#">
                                    {{$creators->totalSeries}} Series
                                </a>
                            </label>
                        </div>
                        <div class="item-single-creators-text-share">
                            <div class="entry-social">
                                @if(sizeof($creators->socials)>0)
                                    @foreach($creators->socials as $social)
                                        <div class="{{$social->name}}">
                                            @if(!empty($social->url))
                                                <a href="{{$social->url}}" target="_blank"></a>
                                            @endif
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="latest-creators-videos">
            <div class="col-md-12">
                <div class="title">
                    <h7>
                        LATEST {{ strtoupper($creators->name) }} VIDEOS
                    </h7>
                </div>
            </div>
            <div class="col-md-12">
                <div class="{{ count($latestVideos)>=1? 'slider-latest-creators-videos':'' }}">
                    @forelse($latestVideos as $value)
                    <div>
                        <div class="item">
                            <div class="imgTitle">
                                <div class="latest-creators-videos-overlay">
                                    <a href="{{ url('/videos/'.$value->slug) }}">
                                        @if(!empty($value->image))
                                            <img class="img-fluid latest-creators-videos-overlay" src="{{ url('/'). Image::url($value->image,400,225,array('crop')) }}"/>
                                        @else
                                            <img class="img-fluid latest-creators-videos-overlay" src="https://img.youtube.com/vi/{{ $value->attr_6}}/mqdefault.jpg" style="width: 400; height: 250;"/>
                                        @endif
                                    </a>
                                    @if(!empty($value->attr_7))
                                        <div class="item-latest-creators-videos-overlay">
                                            {{ $duration->formatted($value->attr_7) }}
                                        </div>
                                    @else

                                    @endif
                                </div>
                            </div>
                            <p>
                                <a href="{{ url('/videos/'.$value->slug) }}">
                                    {{$value->title}}
                                </a>
                            </p>
                        </div>
                    </div>
                    @empty
                    <div class="col-sm-12">
                        <br>
                            <div class="alert alert-warning text-center" role="alert">
                                There is no data to display!
                            </div>
                        </br>
                    </div>
                    @endforelse
                </div>
            </div>
        </div>
        <div class="popular-creators-videos">
            <div class="col-md-12">
                <div class="title">
                    <h7>
                        POPULAR {{ strtoupper($creators->name) }} VIDEOS
                    </h7>
                </div>
            </div>
            <div class="col-md-12">
                <div class="{{ count($popularVideos)>=1? 'slider-popular-creators-videos':'' }}">
                    @forelse($popularVideos as $value)
                    <div>
                        <div class="item">
                            <div class="imgTitle">
                                <div class="popular-creators-videos-overlay">
                                    <a href="{{ url('/videos/'.$value->slug) }}">
                                        @if(!empty($value->image))
                                            <img class="img-fluid latest-creators-videos-overlay" src="{{ url('/'). Image::url($value->image,400,225,array('crop')) }}"/>
                                        @else
                                            <img class="img-fluid latest-creators-videos-overlay" src="https://img.youtube.com/vi/{{ $value->attr_6}}/mqdefault.jpg" style="width: 400; height: 250;"/>
                                        @endif
                                    </a>
                                    @if(!empty($value->attr_7))
                                        <div class="item-popular-creators-videos-overlay">
                                            {{ $duration->formatted($value->attr_7) }}
                                        </div>
                                    @else

                                    @endif
                                </div>
                            </div>
                            <p>
                                <a href="{{ url('/videos/'.$value->slug) }}">
                                    {{$value->title}}
                                </a>
                            </p>
                        </div>
                    </div>
                    @empty
                    <div class="col-sm-12">
                        <br>
                            <div class="alert alert-warning text-center" role="alert">
                                There is no data to display!
                            </div>
                        </br>
                    </div>
                    @endforelse
                </div>
            </div>
        </div>
        <div class="latest-creators-series">
            <div class="col-sm-12">
                <div class="title">
                    <h7>
                        LATEST {{ strtoupper($creators->name) }} SERIES
                    </h7>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="{{ count($latestSeries)>=1? 'slider-latest-creators-series':'' }}">
                    @forelse($latestSeries as $value)
                    <div>
                        <div class="item">
                            <article class="caption">
                                <img class="caption__media img-fluid" src="{{ url('/'). Image::url($value->image,400,550,array('crop')) }}"/>
                                <a href="{{ url('/series/'.$value->slug) }}">
                                    <div class="caption__overlay">
                                        <h1 class="caption__overlay__title">
                                            {{$value->title}}
                                        </h1>
                                        <h7 class="caption__overlay__content">
                                            {{$value->totalVideo}} Videos
                                        </h7>
                                        @if(!empty($value->excerpt))
                                        <p class="caption__overlay__content">
                                            {!!strip_tags($value->excerpt)!!}
                                        </p>
                                        @else
                                            @if(!empty($value->excerpt))
                                            <p class="caption__overlay__content">
                                                {!!strip_tags($value->excerpt)!!}
                                            </p>
                                            @else
                                            <p class="caption__overlay__content">
                                                {!!strip_tags(str_limit($value->content, 100))!!}
                                            </p>
                                            @endif
                                        @endif
                                    </div>
                                </a>
                            </article>
                            <p>
                                <a href="{{ url('/series/'.$value->slug) }}">
                                    {{$value->title}}
                                </a>
                            </p>
                            <p class="videos">
                                <a href="#">
                                    {{$value->totalVideo}} Videos
                                </a>
                            </p>
                        </div>
                    </div>
                    @empty
                    <div class="col-sm-12">
                        <br>
                            <div class="alert alert-warning text-center" role="alert">
                                There is no data to display!
                            </div>
                        </br>
                    </div>
                    @endforelse
                </div>
            </div>
        </div>
        <div class="popular-creators-series">
            <div class="col-sm-12">
                <div class="title">
                    <h7>
                        POPULAR {{ strtoupper($creators->name) }} SERIES
                    </h7>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="{{ count($popularSeries)>=1? 'slider-popular-creators-series':'' }}">
                    @forelse($popularSeries as $value)
                    <div>
                        <div class="item">
                            <article class="caption">
                                <img class="caption__media img-fluid" src="{{ url('/'). Image::url($value->image,400,550,array('crop')) }}"/>
                                <a href="{{ url('/series/'.$value->slug) }}">
                                    <div class="caption__overlay">
                                        <h1 class="caption__overlay__title">
                                            {{$value->title}}
                                        </h1>
                                        <h7 class="caption__overlay__content">
                                            {{$value->totalVideo}} Videos
                                        </h7>
                                        @if(!empty($value->excerpt))
                                        <p class="caption__overlay__content">
                                            {!!strip_tags($value->excerpt)!!}
                                        </p>
                                        @else
                                        <p class="caption__overlay__content">
                                            {!!strip_tags(str_limit($value->content, 100))!!}
                                        </p>
                                        @endif
                                    </div>
                                </a>
                            </article>
                            <p>
                                <a href="{{ url('/series/'.$value->slug) }}">
                                    {{$value->title}}
                                </a>
                            </p>
                            <p class="videos">
                                <a href="#">
                                    {{$value->totalVideo}} Videos
                                </a>
                            </p>
                        </div>
                    </div>
                    @empty
                    <div class="col-sm-12">
                        <br>
                            <div class="alert alert-warning text-center" role="alert">
                                There is no data to display!
                            </div>
                        </br>
                    </div>
                    @endforelse
                </div>
            </div>
        </div>
        <div class="recommended-creators-videos">
            <div class="col-md-12">
                <div class="title">
                    <h7>
                        RECOMMENDED {{ strtoupper($creators->name) }} VIDEOS
                    </h7>
                </div>
            </div>
            <div class="col-md-12">
                <div class="{{ count($recomendedCreators)>=1? 'slider-recommended-creators-videos':'' }}">
                    @forelse($recomendedCreators as $value)
                    <div>
                        <div class="item">
                            <div class="imgTitle">
                                <div class="recommended-creators-videos-overlay">
                                    <a href="{{ url('/videos/'.$value->slug) }}">
                                        @if(!empty($value->image))
                                            <img class="img-fluid top-videos-overlay" src="{{ url('/'). Image::url($value->image,400,225,array('crop')) }}"/>
                                        @else
                                            <img class="img-fluid top-videos-overlay" src="https://img.youtube.com/vi/{{ $value->attr_6}}/mqdefault.jpg" style="width: 400; height: 250;"/>
                                        @endif
                                    </a>
                                    @if(!empty($value->attr_7))
                                        <div class="item-recommended-creators-videos-overlay">
                                            {{ $duration->formatted($value->attr_7) }}
                                        </div>
                                    @else

                                    @endif
                                </div>
                            </div>
                            <p>
                                <a href="{{ url('/videos/'.$value->slug) }}">
                                    {{$value->title}}
                                </a>
                            </p>
                        </div>
                    </div>
                    @empty
                    <div class="col-sm-12">
                        <br>
                            <div class="alert alert-warning text-center" role="alert">
                                There is no data to display!
                            </div>
                        </br>
                    </div>
                    @endforelse
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid others-videos">
    <div class="col-sm-12">
        <div class="col-sm-12">
            <div class="title">
                <h7>
                    OTHERS VIDEOS
                </h7>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="{{ count($otherVideos)>=1? 'row text-center text-lg-left item-others-videos':'' }}">
                @forelse($otherVideos as $value)
                <div class="col-sm-3 col-others-videos">
                    <div class="item">
                        <div class="imgTitle">
                            <div class="top-videos-overlay">
                                <a href="{{ url('/videos/'.$value->slug) }}">
                                    @if(!empty($value->image))
                                        <img class="img-fluid top-videos-overlay" src="{{ url('/'). Image::url($value->image,400,225,array('crop')) }}"/>
                                    @else
                                        <img class="img-fluid top-videos-overlay" src="https://img.youtube.com/vi/{{ $value->attr_6}}/mqdefault.jpg" style="width: 400; height: 250;"/>
                                    @endif
                                </a>
                                @if(!empty($value->attr_7))
                                    <div class="item-top-videos-overlay">
                                        {{ $duration->formatted($value->attr_7) }}
                                    </div>
                                @else

                                @endif
                            </div>
                        </div>
                        <p>
                            <a href="{{ url('/videos/'.$value->slug) }}">
                                {{$value->title}}
                            </a>
                        </p>
                    </div>
                </div>
                @empty
                <div class="col-sm-12">
                    <br>
                        <div class="alert alert-warning text-center" role="alert">
                            There is no data to display!
                        </div>
                    </br>
                </div>
                @endforelse
            </div>
        </div>
    </div>
</div>
<div class="container-fluid more-from-creators">
    <div class="col-sm-12">
        <div class="col-sm-12">
            <div class="title">
                <h7>
                    MORE FROM CREATOR
                </h7>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="{{ count($moreVideos)>=1? 'row text-center text-lg-left item-more-from-creators loadMore':'' }}">
                @forelse($moreVideos as $value)
                <div class="col-sm-3 col-more-from-creators">
                    <div class="item">
                        <div class="imgTitle">
                            <div class="top-videos-overlay">
                                <a href="{{ url('/videos/'.$value->slug) }}">
                                    @if(!empty($value->image))
                                        <img class="img-fluid top-videos-overlay" src="{{ url('/'). Image::url($value->image,400,225,array('crop')) }}"/>
                                    @else
                                        <img class="img-fluid top-videos-overlay" src="https://img.youtube.com/vi/{{ $value->attr_6}}/mqdefault.jpg" style="width: 400; height: 250;"/>
                                    @endif
                                </a>
                                <div class="item-top-videos-overlay">
                                    {{ $duration->formatted($value->attr_7) }}
                                </div>
                            </div>
                        </div>
                        <p>
                            <a href="{{ url('/videos/'.$value->slug) }}">
                                {{$value->title}}
                            </a>
                        </p>
                    </div>
                </div>
                @empty
                <div class="col-sm-12">
                    <br>
                        <div class="alert alert-warning text-center" role="alert">
                            There is no data to display!
                        </div>
                    </br>
                </div>
                @endforelse
            </div>
        </div>
    </div>
</div>
@endsection
