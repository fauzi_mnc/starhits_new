@extends('layouts.frontend')

@section('title', 'Create User')

@section('content')
<div class="container-fluid blank-page">
    <div class="title">
        <h2>
            Create Star Hits Account
        </h2>
    </div>
    <div class="w-75 mx-auto mt-5">
        @if ($errors->any())
            <div id="alertdanger" class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @include('flash::message')
        <form id="formReg" method="post" action="{{ route('register.store') }}" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <input class="form-control required" name="name" id="name" placeholder="Full Name" style="font-family:Fontawesome;" type="text">
                </input>
            </div>
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <input class="form-control required" name="email" id="email" placeholder="Email" style="font-family:Fontawesome;" type="email">
                </input>
            </div>
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <input class="form-control required" name="password" id="password" placeholder="Password" style="font-family:Fontawesome;" type="password">
                </input>
            </div>
            <div class="form-group">
                <input class="form-control required" name="password_confirmation" id="password_confirmation" placeholder="Confirm Password" style="font-family:Fontawesome;" type="password">
                </input>
            </div>
            <div class="form-group row">
                <div class="col-sm-12">
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" id="accept">
                                <p style="font-size: 10px; margin-top: 3px;">
                                    By Signing Up, You Agree to the
                                    <a href="#">
                                        Term of Use
                                    </a>
                                    and
                                    <a href="#">
                                        Privacy Policy
                                    </a>
                                </p>
                            </input>
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group text-center">
                <button class="btn login-btn" id="form-signup" type="submit" disabled="disabled">
                    Sign Up
                </button>
            </div>
            <div class="or-seperator">
            </div>
            <p class="text-center">
                Already a Star Hits User?
                <a aria-label="Close" data-dismiss="modal" data-target="#loginModal" data-toggle="modal" href="#">
                    Login now!
                </a>
            </p>
        </form>
    </div>
</div>

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function(){
                $(".alert-danger").fadeTo(2000, 500).slideUp(500, function(){
                    $(".alert-danger").slideUp(500);
                });
                $(".alert-success").fadeTo(2000, 500).slideUp(500, function(){
                    $(".alert-success").slideUp(500);
                });
        });
    </script>
    <script>
        jQuery(function ($) {
            $("#formReg").validate({
                //specify the validation rules
                rules: {
                    name: "required",
                    email: {
                    required: true,
                    email: true //email is required AND must be in the form of a valid email address
                    },
                    password: {
                    required: true,
                    minlength: 8
                    },
                    password_confirmation: {
                    required: true,
                    minlength: 8,
                    equalTo : "#password"
                    }
                },

                //specify validation error messages
                messages: {
                    name: "Please enter your Full Name.",
                    email: "Please enter Your Email.",
                    email: "Please enter a valid email address.",
                    password: {
                    required: "Password field cannot be blank!",
                    minlength: "Your password must be at least 8 characters long."
                    },
                    password_confirmation: {
                    required: "Password Confirm field cannot be blank!",
                    minlength: "Your password must be at least 8 characters long.",
                    equalTo : "Please enter the same password again."
                    }
                },
                
                submitHandler: function(form) {
                    form.submit();
                }
            });
        });
    </script>
    <script type="text/javascript">
        $('#accept').click(function() {
            if ($('#form-signup').is(':disabled')) {
                $('#form-signup').removeAttr('disabled');
            } else {
                $('#form-signup').attr('disabled', 'disabled');
            }
        });
    </script>
@endpush

@endsection
