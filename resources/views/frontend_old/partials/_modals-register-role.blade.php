<div aria-hidden="true" aria-labelledby="regRoleModalLabel" class="modal fade" id="regRoleModal" role="dialog" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="col text-center">
                    <div class="avatar">
                        <img alt="StarHits" src="{{asset('frontend/assets/img/black_logo.png')}}">
                    </div>
                </div>
                <div class="inline-block" style="display:absolute;">
                    <button type="button" class="close float-right" aria-label="Close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div> 
            <div class="text-center">
                <p style="font-size:14px; margin-top: 20px;">
                    Are you an infuencer ?
                </p>
                <div class="col-xs-12 text-center" style="margin: 0 auto;">
                    <div class="row">
                        <div class="col" style="margin: 0 auto;">
                        <a class="btn btn-primary login-btn" style="padding: 10px 30px; float:right;" href="{{ url('/register/influencer')}}">
                                Yes
                            </a>
                        </div>
                        <div class="col" style="margin: 0 auto;">
                            <a class="btn btn-primary login-btn" style="padding: 10px 30px; float:left;" href="{{ url('/register/user')}}">
                                No
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@push('scripts')
<script type="text/javascript">
    $('#accept').click(function() {
        if ($('#form-submitbtn').is(':disabled')) {
            $('#form-submitbtn').removeAttr('disabled');
        } else {
            $('#form-submitbtn').attr('disabled', 'disabled');
        }
    });
</script>
@endpush
