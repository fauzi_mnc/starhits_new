<!-- Stored in resources/views/child.blade.php -->

{{-- @php
dd($authUsers);    
@endphp --}}
@extends('layouts.frontend')
@if ($authCreators && $authUsers == 'creator')

    @section('title', 'already')

    @section('content')

    <div class="container-fluid mt-5" style="height:15em;">
        <div class="row">
            <div class="col-sm-12">
                <div class="alert alert-danger" role="alert">
                    <h4 class="text-center">Hy {{ $authCreators->name }}, you are already part of our creator. please visit your dashboard. <a style="text-decoration:none;" href="{{ url('/creator/serie') }}">Here</a></h4>
                </div>
            </div>
        </div>
    </div>

    <style>
        #footer{display: none !important}
    </style>

    @endsection

@else

    @if ($totalSubscriber > $minSubcriber && $statusSubcriber = 'false')

    @section('title', 'Join')

    @section('content')
    <!-- Page Content -->
    <div class="container-fluid access">
        <div class="row">
            <div class="col-sm-12">
                <div class="title">
                    <h2>
                        Join Here For Dashboard Access
                    </h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 box-one">
                    <div class="card">
                        <div class="card-body">
                            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                We have successfully connected with your Youtube channel and collected following information:
                            </div>
                            <?php $links = explode("/", $authCreators->image);?>

                            @if(in_array($links[0], ['http:', 'https:']))
                                <img class="img-fluid" src="{{ $authCreators->image }}" style="width: 150px; height: 150px; margin:0 auto; display:block; border-radius:5px;"/>
                            @else
                                <img class="img-fluid" src="{{ url('/'). Image::url($authCreators->image,150,150,array('crop')) }}" style="margin:0 auto; display:block; border-radius:5px;" />
                            @endif
                            <form>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">
                                        Channel Title
                                    </label>
                                    <div class="col-sm-8">
                                        <input type="text" readonly class="form-control-plaintext" id="#" value="{{$authCreators->name_channel}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">
                                        Email
                                    </label>
                                    <div class="col-sm-8">
                                        <input type="email" readonly class="form-control-plaintext" id="#" value="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">
                                        Channel View
                                    </label>
                                    <div class="col-sm-8">
                                        <input type="text" readonly class="form-control-plaintext" id="#" value="{{ $channelView }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">
                                        Subscribers
                                    </label>
                                    <div class="col-sm-8">
                                        <input type="text" readonly class="form-control-plaintext" id="#" value="{{ $totalSubscriber }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">
                                        Videos
                                    </label>
                                    <div class="col-sm-8">
                                        <input type="text" readonly class="form-control-plaintext" id="#" value="{{ $totalVideo }}">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 box-two">
                    <div class="card">
                        <div class="card-body">
                            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                Please fill out the following form in order to complete your application to starhits.id
                            </div>
                            @if(count($errors))
                                <div class="alert alert-danger slideup">
                                    <strong>Whoops!</strong>
                                    <br/>
                                    There were some problems with your input.
                                    <br/>
                                    <ul>
                                        @foreach($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <form method="POST" action="{{route('creators.validate.update',$authCreators->provider_id)}}" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                {{ method_field('PUT') }}
                                {{ method_field('PATCH') }}
                                <input name="subscribers" type="hidden" value="{{ $totalSubscriber }}">
                                <input name="channelview" type="hidden" value="{{ $channelView }}">
                                <input name="totalvideo" type="hidden" value="{{ $totalVideo }}">
                                <div class="form-group row{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <div class="col">
                                        <input class="form-control" name="name" value="{{ old('name') }}"  placeholder="Full Name**" type="text"  class="form-control required">
                                        </input>
                                        <span class="text-danger">{{ $errors->first('name') }}</span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6 dempet {{ $errors->has('gender') ? ' has-error' : '' }}">
                                        <select class="form-control" name="gender">
                                            <option value="" selected>
                                                Select Gender
                                            </option>
                                            <option value="male">
                                                Male
                                            </option>
                                            <option value="female">
                                                Female
                                            </option>
                                        </select>
                                        <span class="text-danger">{{ $errors->first('gender') }}</span>
                                    </div>
                                    <div class="col-sm-6 {{ $errors->has('dob') ? ' has-error' : '' }}">
                                        <input id="datepicker" name="dob" placeholder="Date Of Birth" class="form-control required"/>
                                        <span class="text-danger">{{ $errors->first('dob') }}</span>
                                    </div>
                                </div>
                                <div class="form-group row{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <div class="col-sm-6 dempet">
                                        <input class="form-control" name="email" placeholder="Email Address" type="email" class="form-control required">
                                        <span class="text-danger">{{ $errors->first('email') }}</span>
                                    </div>
                                    <div class="col-sm-6{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <input class="form-control" name="password" placeholder="Password" type="password" class="form-control required">
                                        <span class="text-danger">{{ $errors->first('password') }}</span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col{{ $errors->has('cover') ? ' has-error' : '' }}">
                                        <div class="custom-file">
                                            <input class="custom-file-input" type="file" name="cover">
                                                <label class="custom-file-label" for="customFile">
                                                    Upload Cover
                                                </label>
                                        </div>
                                        <span class="text-danger">{{ $errors->first('cover') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-12 box-three">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="form-check">
                                                <input class="form-check-input" id="accept" type="checkbox">
                                                    <label for="accept" class="form-check-label">
                                                        I agree to the General Terms and Conditions and understand that i am electronically signing this agreement to join the Starhits.id
                                                    </label>
                                                </input>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 box-four">
                                    <button class="btn btn-warning" id="form-submitbtn" disabled="disabled" type="submit">
                                        Apply Now
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @endsection

    @elseif($statusSubcriber == 'true')

    @section('title', 'failed')

    @section('content')

    <div class="container-fluid" style="height:15em;">
        <div class="row">
            <div class="col-sm-12">
                <div class="alert alert-danger" role="alert">
                    <h4 class="text-center">Your Subcribers Count is hidden, please display Your Subcribers Count. <a style="text-decoration:none;" href="https://www.youtube.com/advanced_settings">Here</a></h4>
                </div>
                <div class="text-center">
                    <p>Auto redirect to Home in <span id="count">10</span> second</p>
                    <p>or</p>
                    <p> Back to <a style="text-decoration:none;" href="{{ url('/logout') }}" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();" >Homepage</a>
                </div>
            </div>
        </div>
    </div>
    <form action="{{ url('/logout') }}" id="logout-form1" method="POST" style="display: none;">
        {{ csrf_field() }}
        <input style="display: none;" type="submit" value="logout">
        </input>
    </form>
    <script type="text/javascript">
        window.onload = function(){
    
        (function(){
            var counter = 10;
        
            setInterval(function() {
            counter--;
            if (counter >= 0) {
                span = document.getElementById("count");
                span.innerHTML = counter;
            }
            // Display 'counter' wherever you want to display it.
            if (counter === 0) {
                document.getElementById("logout-form1").submit();
                clearInterval(counter);
            }
            
            }, 1000);
            
        })();
            
        }
    </script>
    @endsection

    @else

    @section('title', 'failed')

    @section('content')

    <div class="container-fluid" style="height:15em;">
        <div class="row">
            <div class="col-sm-12">
                <div class="alert alert-danger" role="alert">
                    <h4 class="text-center"> You can't register because Your Subcribers less than 1000 subscribers. </h4>
                </div>
                <div class="text-center">
                    <p>Auto redirect to Home in <span id="count">5</span> second</p>
                    <p>or</p>
                    <p> Back to <a style="text-decoration:none;" href="{{ url('/logout') }}" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();" >Homepage</a>
                </div>
            </div>
        </div>
    </div>
    <form action="{{ url('/logout') }}" id="logout-form" method="POST" style="display: none;">
        {{ csrf_field() }}
        <input style="display: none;" type="submit" value="logout">
        </input>
    </form>
    <script type="text/javascript">
        window.onload = function(){
    
        (function(){
            var counter = 5;
        
            setInterval(function() {
            counter--;
            if (counter >= 0) {
                span = document.getElementById("count");
                span.innerHTML = counter;
            }
            // Display 'counter' wherever you want to display it.
            if (counter === 0) {
                document.getElementById("logout-form").submit();
                clearInterval(counter);
            }
            
            }, 1000);
            
        })();
            
        }
    </script>
    @endsection

    @endif

@endif

@push('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $(".slideup").fadeTo(2000, 500).slideUp(500, function(){
            $(".slideup").slideUp(500);
        });
        $(".alert-success").fadeTo(2000, 500).slideUp(500, function(){
            $(".alert-success").slideUp(500);
        });
    });
	$('#accept').click(function() {
		if ($('#form-submitbtn').is(':disabled')) {
	    	$('#form-submitbtn').removeAttr('disabled');
	    } else {
	    	$('#form-submitbtn').attr('disabled', 'disabled');
	    }
	});

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#showgambar').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#inputgambar").change(function () {
        readURL(this);
    });
</script>
@endpush