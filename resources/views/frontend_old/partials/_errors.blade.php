@extends('layouts.frontend')

@section('content')

<div class="container-fluid not-found">
    <div class="row">
        <div class="col-md-12 text-center">
            <label>
                This is somewhat embarrasing, isn't it?
                <br>
                    <small>
                        Sorry, we couldn't find what you were looking for. But, please enjoy these contents from our site.
                    </small>
                </br>
            </label>
            <h1>
                4
                <img src="{{asset('frontend/assets/img/icon/face.png')}}">
                    4
                </img>
            </h1>
        </div>
    </div>
</div>
<div class="container-fluid not-found-recommended">
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-12">
                <div class="title">
                    <h7>
                        RECOMMENDED VIDEOS
                    </h7>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row text-center text-lg-left item-others-videos">
                    @foreach($recomendedVideos as $value)
                    <div class="col-sm-3 col-others-videos">
                        <div class="item">
                            <div class="imgTitle">
                                <div class="top-videos-overlay">
                                    <a href="{{ url('/videos/'.$value->slug) }}">
                                        @if(!empty($value->image))
                                            <img class="img-fluid top-videos-overlay" src="{{ url('/'). Image::url($value->image,400,225,array('crop')) }}" />
                                        @else
                                            <img class="img-fluid top-videos-overlay" src="https://img.youtube.com/vi/{{ $value->attr_6}}/mqdefault.jpg" style="width: 400; height: 250;" />
                                        @endif
                                    </a>
                                    @if(!empty($value->attr_7))
                                        <div class="item-top-videos-overlay">
                                            {{ $duration->formatted($value->attr_7) }}
                                        </div>
                                    @else

                                    @endif
                                </div>
                            </div>
                            <p>
                                <a href="{{ url('/videos/'.$value->slug) }}">
                                    {{$value->title}}
                                </a>
                            </p>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid not-found-series">
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-12">
                <div class="title">
                    <h7>
                        RECOMMENDED SERIES
                    </h7>
                </div>
            </div>
            <div class="col-md-12">
                <div class=" row item-not-found-series">
                    @foreach($recomendedSeries as $value)
                    <div class="col-lg-3 col-sm-6 col-xs-6 item">
                        <article class="caption">
                            <img class="caption__media img-fluid" src="{{ url('/'). Image::url($value->image,400,550,array('crop')) }}" />
                            <a href="{{ url('/series/'.$value->slug) }}">
                                <div class="caption__overlay">
                                    <h1 class="caption__overlay__title">
                                        {{$value->title}}
                                    </h1>
                                    <h7 class="caption__overlay__content">
                                        {{$value->totalVideo}} Videos
                                    </h7>
                                    <p class="caption__overlay__content">
                                        {!!str_limit($value->content, 200)!!}
                                    </p>
                                </div>
                            </a>
                        </article>
                        <p>
                            <a href="{{ url('/series/'.$value->slug) }}">
                                {{$value->title}}
                            </a>
                        </p>
                        <p class="videos">
                            <a href="#">
                                {{$value->totalVideo}} Videos
                            </a>
                        </p>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid not-found-popular">
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-12">
                <div class="title">
                    <h7>
                        POPULAR CREATOR BASED ON VIEWS
                    </h7>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row text-center text-lg-left item-not-found-popular">
                    @foreach($popularCreators as $value)
                    <div class="col-lg-2 col-sm-6 col-xs-6 col-not-found-popular">
                        <div class="item">
                            <div class="imgTitle">
                                <div class="top-videos-overlay">
                                    <a href="{{ url('/creators/'.$value->slug) }}">
                                        <img class="img-fluid top-videos-overlay" src="{{ url('/'). Image::url($value->image,150,150,array('crop')) }}" />
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid recommended-videos">
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-12">
                <div class="title">
                    <h7>
                        LATEST VIDEOS
                    </h7>
                </div>
            </div>
            <div class="col-md-12">
                <div class="slider-recommended-videos">
                    @foreach($latestVideoStarHits as $value)
                    <div>
                        <div class="item">
                            <div class="imgTitle">
                                <div class="top-videos-overlay">
                                    <a href="{{ url('/videos/'.$value->slug) }}">
                                        @if(!empty($value->image))
                                            <img class="img-fluid top-videos-overlay" src="{{ url('/'). Image::url($value->image,400,225,array('crop')) }}" />
                                        @else
                                            <img class="img-fluid top-videos-overlay" src="https://img.youtube.com/vi/{{ $value->attr_6}}/mqdefault.jpg" style="width: 400; height: 250;" />
                                        @endif
                                    </a>
                                    @if(!empty($value->attr_7))
                                        <div class="item-top-videos-overlay">
                                            {{ $duration->formatted($value->attr_7) }}
                                        </div>
                                    @else

                                    @endif
                                </div>
                            </div>
                            <p>
                                <a href="{{ url('/videos/'.$value->slug) }}">
                                    {{$value->title}}
                                </a>
                            </p>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
