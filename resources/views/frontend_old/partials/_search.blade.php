<!-- Stored in resources/views/child.blade.php -->
@extends('layouts.frontend')

@section('title', 'Search')

@section('content')

<div class="container-fluid search">
    <div class="row">
        <div class="page-content small">
            <div class="container-fluid">
                <h3>
                    SEARCH RESULT FOR "{{ $search }}"
                </h3>
                <br>
                    @foreach($models as $name => $data)
                    <ul class="updates">
                        @foreach($data as $key => $value)
                        <li>
                            <a href="{{ url('/videos/'.$value['slug']) }}">
                                {{ $value['title'] }}
                                <span>
                                    {{ $value['created_at'] }}
                                </span>
                            </a>
                        </li>
                        @endforeach
                    </ul>
                    @endforeach
                </br>
            </div>
        </div>
        <div class="col-md-12 page">
            <div class="row navigation">
                {{-- $models->links('vendor.pagination.custom') --}}
            </div>
        </div>
    </div>
</div>

@endsection