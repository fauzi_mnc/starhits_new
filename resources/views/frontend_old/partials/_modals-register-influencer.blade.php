@extends('layouts.frontend')

@section('title', 'Register Influencer')

@section('content')
<div class="container-fluid blank-page wizard-form">
    <div class="col-sm-12">
        <div class="title">
            <h2>
                Create Influencer Account
            </h2>
        </div>
        @if ($errors->any())
            <div id="alertdanger" class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @include('flash::message')
        <form id="wizard1" class="wizard" method="POST" action="{{ route('registerInfluencer.store') }}" enctype="multipart/form-data">
            {{ csrf_field() }}
            <!-- <div class="wizard-header">
                <ul style="text-align:center; width:100%;">
                    <li role="presentation" class="wizard-step-indicator"><a href="#personalinformation">Personal Information</a></li>
                    <li role="presentation" class="wizard-step-indicator"><a href="#payment">Payment Information</a></li>
                    <li role="presentation" class="wizard-step-indicator"><a href="#instacon">Instagram Connection</a></li>
                </ul>
            </div> -->
            <div class="wizard-content" style="margin-bottom: 50px;">
                <div id="personalinformation" class="">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            {!! Form::text('name', null, [ 'id' => 'name', 'class' => 'form-control required', 'placeholder' => 'Full Name*']) !!}
                        </div>
                        <div class="form-group col-md-6">
                            {!! Form::number('phone', null, ['class' => 'form-control required', 'id' => 'phone', 'placeholder' => 'Phone*']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::email('email', null, [ 'id' => 'email', 'class' => 'form-control required', 'placeholder' => 'Your personal email (ex: johndoe@domain.com)']) !!}
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            {!! Form::select('country', array('' => '- Select Country -') + $countries, null, ['class' => 'form-control required']) !!}
                        </div>
                        <div class="form-group col">
                            {!! Form::select('country', array('' => '- Select Province -') + $province, null, ['class' => 'form-control required']) !!}
                        </div>
                        <div class="form-group col">
                            {!! Form::select('city', array('' => '- Select City -') + $cities, null, ['class' => 'form-control required', 'data-live-search' => 'true', 'data-style' => 'btn-selectpick', 'id' => 'selectcity']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::password('password', [ 'id' => 'password', 'class' => 'form-control required', 'placeholder' => 'Your Password*']) !!}
                    </div>
                    <div class="row">
                        <label class="inline" style="margin-right:5rem;">Gender</label>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="genderMale" name="gender" value="M" class="form-control custom-control-input required">
                            <label class="custom-control-label" for="genderMale">Male</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="genderFemale" name="gender" value="F" class="form-control custom-control-input required">
                            <label class="custom-control-label" for="genderFemale">Female</label>
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('Category', 'Influencer Category :', []) }}
                        <br>
                        <div class="row">
                        @foreach ($categories as $influencerCategory)
                        <div class="col-sm-6">
                        {{ Form::checkbox('category[]', $influencerCategory->title, null, ['class' => 'required', 'id' => 'category'.$influencerCategory->title]) }}
                        {{ Form::label('category'.$influencerCategory->title, $influencerCategory->title) }}
                        </div>
                        @endforeach
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('username', 'Username Instagram:', []) !!}
                        {!! Form::text('username', null, ['class' => 'form-control required', 'placeholder' => 'Input without "@"']) !!}
                        <input type="hidden" id="followers_ig" name="followers_ig">
                        <input type="hidden" id="profile_picture_ig" name="profile_picture_ig">
                        <br>
                        <button type="button" class="btn btn-primary text-center" id="btn-checkig">Check</button>
                        <br>
                        <br>
                        <div class="form-group" id="instagram-profile"></div>
                        <br>
                    </div>
                    <div class="form-group">
                        <!-- <button type="reset" class="wizard-prev btn btn-warning float-left">Reset</button> -->
                        <button type="submit" class="btn btn-success float-right" id="btn-finish" disabled="disabled">Finish</button>
                    </div>
                    <!-- <button type="button" class="wizard-prev btn btn-danger float-left" data-dismiss="modal">Cancel</button>
                    <button type="button" class="wizard-next btn btn-success float-right">Next</button> -->
                </div>
                <!-- <div id="payment" class="wizard-step">
                    <div class="form-group">
                        {!! Form::select('bank_id',  array('' => '- Select Bank -') + $banks, null, ['class' => 'form-control', 'id' => 'selectbank', 'data-live-search' => 'true', 'data-style' => 'btn-selectpick']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::number('bank_account_number', null, [ 'id' => 'bank_account_number', 'class' => 'form-control required', 'placeholder' => 'Your Bank Account Number*']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::text('bank_holder_name', null, [ 'id' => 'bank_holder_name', 'class' => 'form-control required', 'placeholder' => 'Your Bank Holder Name*']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::text('bank_location', null, [ 'id' => 'bank_location', 'class' => 'form-control required', 'placeholder' => 'Your Bank Location*']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::number('npwp', null, [ 'id' => 'npwp', 'class' => 'form-control required', 'placeholder' => 'NPWP*']) !!}
                    </div>
                    <button type="button" class="wizard-prev btn btn-warning float-left">Previous</button>
                    <button type="button" class="wizard-next btn btn-success float-right">Next</button>
                </div>
                <div id="instacon" class="wizard-step">
                    <div class="form-group">
                        {!! Form::label('username', 'Username Instagram:', []) !!}
                        {!! Form::text('username', null, ['class' => 'form-control required', 'placeholder' => 'Input without "@"']) !!}
                        <input type="hidden" id="followers_ig" name="followers_ig">
                        <input type="hidden" id="profile_picture_ig" name="profile_picture_ig">
                        <br>
                        <button type="button" class="btn btn-primary text-center" id="btn-checkig">Check</button>
                        <br>
                        <br>
                        <div class="form-group" id="instagram-profile"></div>
                        <br>
                    </div>
                    <div class="form-group">
                        <button type="button" class="wizard-prev btn btn-warning float-left">Previous</button>
                        <button type="submit" class="btn btn-success float-right" id="btn-finish" disabled="disabled">Finish</button>
                    </div>
                </div> -->
            </div>
        </form>
    </div>
</div>
<div class="clear"> </div>

@push('scripts')
    <script type="text/javascript">
        $('#selectcity').selectpicker();
        $('#selectbank').selectpicker();
    </script>
    @if(!empty(Session::get('errors')) OR Session::get('0') == 'success'))
    <script type="text/javascript" >
    $(document).ready(function(){
        $('#regInfModal').modal('show');
    });
    </script>
    @endif
    <script type="text/javascript" src="{{ asset('js/plugins/switchery/switchery.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/plugins/typeahed.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $(".alert-danger").fadeTo(2000, 500).slideUp(500, function(){
                $(".alert-danger").slideUp(500);
            });
            $(".alert-success").fadeTo(2000, 500).slideUp(500, function(){
                $(".alert-success").slideUp(500);
            });
            var elem = document.querySelector('.js-switch');
            var switchery = new Switchery(elem, { disabled: true });
            
    
            $('#btn-checkig').click(function(){
                $.get( "{{route('influencer.instagram.check')}}", { username:  $('#username').val()}, function(data) {
                    if(data.result == false){
                        $('#instagram-profile').prepend(
                            '<div class="alert alert-danger">Invalid Instagram Username</div>'
                        );
    
                        setTimeout(() => {
                            $('.alert-danger').remove();
                        }, 5000);
                        
                    } else {
                        $('#username').val(data.result.userName);
                        $('#followers_ig').val(data.result.followers);
                        $('#profile_picture_ig').val(data.result.profilePicture);
                        $('#instagram-profile').empty();
                        if (data.result.followers < 500) {
                            document.getElementById("btn-finish").disabled = true;
                            $('#instagram-profile').append(
                                '<div class="feed-element">'+
                                    '<a href="#" class="pull-left" style="margin-right:20px;">'+
                                        '<img alt="image" class="img-circle" src="'+data.result.profilePicture+'" style="height:100px;width:100px;border-radius: 50px;">'+
                                    '</a>'+
                                    '<div class="media-body">'+
                                        '<h3>'+data.result.userName+'</h3> <br>'+
                                        '<strong>'+data.result.mediaCount+' post</strong> <strong class="text-danger">'+data.result.followers+' followers</strong> <strong>'+ data.result.following +' following</small></strong>'+
                                        '<br><strong class="text-danger"><i>Followers anda kurang dari 500</i></strong>'+
                                    '</div>'+
                                '</div>'
                            );
                        } else {
                            document.getElementById("btn-finish").disabled = false;
                            $('#instagram-profile').append(
                                '<div class="feed-element">'+
                                    '<a href="#" class="pull-left" style="margin-right:20px;">'+
                                        '<img alt="image" class="img-circle" src="'+data.result.profilePicture+'" style="height:100px;width:100px;border-radius: 50px;">'+
                                    '</a>'+
                                    '<div class="media-body">'+
                                        '<h3>'+data.result.userName+'</h3> <br>'+
                                        '<strong>'+data.result.mediaCount+' post</strong> <strong>'+data.result.followers+' followers</strong> <strong>'+ data.result.following +' following</small></strong>'+
                                    '</div>'+
                                '</div>'
                            );
                        }
                    }
                });
                $("#wizard1").find("a:contains('Next')").show()
            });
        });
    </script>
    <script type="text/javascript">
        jQuery(function ($) {
            $("#wizard1").validate({
                //specify the validation rules
                rules: {
                    name: "required",
                    email: {
                    required: true,
                    email: true //email is required AND must be in the form of a valid email address
                    },
                    password: {
                    required: true,
                    minlength: 8
                    },
                    password_confirmation: {
                    required: true,
                    minlength: 8,
                    equalTo : "#password"
                    }
                },

                //specify validation error messages
                messages: {
                    name: "Please enter your Full Name.",
                    email: "Please enter Your Email.",
                    email: "Please enter a valid email address.",
                    password: {
                    required: "Password field cannot be blank!",
                    minlength: "Your password must be at least 8 characters long."
                    },
                    password_confirmation: {
                    required: "Password Confirm field cannot be blank!",
                    minlength: "Your password must be at least 8 characters long.",
                    equalTo : "Please enter the same password again."
                    }
                },
                
                submitHandler: function(form) {
                    form.submit();
                }
            });
        });
    </script>
@endpush

@endsection