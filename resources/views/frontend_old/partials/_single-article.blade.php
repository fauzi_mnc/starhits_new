<!-- Stored in resources/views/child.blade.php -->
@extends('layouts.frontend')

@section('title', $singleArticle->title)

@push('preloader')
<div class="se-pre-con">
    <div class="col-sm-12 text-center pre-con">
        <div class="col">
            <div class="logo-preloader">
                <img alt="" class="img-fluid" src="{{ asset('frontend/assets/img/logo-home.png') }}"></img>
            </div>
        </div>
        <div class="col">
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg">
                <defs>
                    <filter id="gooey">
                        <fegaussianblur in="SourceGraphic" result="blur" stddeviation="10">
                        </fegaussianblur>
                        <fecolormatrix in="blur" mode="matrix" result="goo" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 18 -7">
                        </fecolormatrix>
                        <feblend in="SourceGraphic" in2="goo">
                        </feblend>
                    </filter>
                </defs>
            </svg>
            <div class="blob blob-0"></div>
            <div class="blob blob-1"></div>
            <div class="blob blob-2"></div>
            <div class="blob blob-3"></div>
            <div class="blob blob-4"></div>
            <div class="blob blob-5"></div>
        </div>
    </div>
</div>
@endpush

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-8">
            <div class="top-content-single">
                <div class="top-slider">
                    <div class="item">
                        <div class="top-block">
                            <div class="top-media">
                                <div class="{{ !empty($singleArticle->attr_6) ? 'video-wrap' : '' }}">
                                    <div class="video">
                                        @if(empty($singleArticle->attr_6))
                                            <!-- <div class="thumb"> -->
                                            <img class="img-fluid" src="{{ url('/'). Image::url($singleArticle->image,886,490,array('crop')) }}"/>
                                            <!-- </div> -->
                                        @else
                                        <div class="youtube embed-responsive embed-responsive-16by9">
                                            {!! $singleArticle->link !!}
                                            <div class="thumb">
                                                <a class="target-big" href="{{ url('/article/'.$singleArticle->slug) }}">
                                                    {{ $singleArticle->title }}
                                                </a>
                                            </div>
                                        </div>
                                        @endif
                                    </div>
                                    <div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="articles">
                <div class="row">
                    <div class="col-md-12 main-article">
                        <div class="entry-social">
                            @guest
                            <div class="a2a_kit bm">
                                <!-- <a href="#" target="_blank">
                                    <div class="mobile-text">
                                        Bookmark
                                    </div>
                                </a> -->
                            </div>
                            @else
                            <div class="a2a_kit bm">
                                <a href="{{route('article.bookmark', ['id' => $singleArticle->id])}}" class="{{ $bookmarkClass }}">
                                    <div class="mobile-text">
                                        Bookmark
                                    </div>
                                </a>
                            </div>
                            @endguest
                            <div class="a2a_kit wa">
                                <a class="a2a_button_whatsapp link-social what-color" href="whatsapp://send?text=Hello World!" data-action="share/whatsapp/share" target="_blank">
                                    <div class="mobile-text">
                                        Share
                                    </div>
                                </a>
                            </div>
                            <div class="a2a_kit fb">
                                <a href="https://www.facebook.com/sharer/sharer.php?u={{ url('/article/'.$singleArticle->slug) }}" target="_blank">
                                    <div class="mobile-text">
                                        Share
                                    </div>
                                </a>
                            </div>
                            <div class="a2a_kit twitter">
                                <a href="https://twitter.com/intent/tweet?url={{ url('/article/'.$singleArticle->slug) }}" target="_blank">
                                    <div class="mobile-text">
                                        Tweet
                                    </div>
                                </a>
                            </div>
                            <div class="a2a_kit gplus">
                                <a href="https://plus.google.com/share?url={{ url('/article/'.$singleArticle->slug) }}" target="_blank">
                                    <div class="mobile-text">
                                        Share
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="title">
                            <h2>
                                {{$singleArticle->title}}
                            </h2>
                        </div>
                        <div class="sub-title">
                            <div class="row">
                                <div class="col-md-12 main-sub-title">
                                    <h4>
                                        {{$singleArticle->channelVideo->title}}
                                    </h4>
                                </div>
                            </div>
                        </div>
                        <div class="sub-title">
                            <div class="row">
                                <div class="col-md-12 views-2">
                                    <section>
                                        By
                                        <strong>
                                            {{$singleArticle->users->name}}
                                        </strong>
                                    </section>
                                </div>
                            </div>
                        </div>
                        <div class="paragraph">
                            <p class="main-paragraph">
                                {!!$singleArticle->content!!}
                                
                            </p>
                        </div>
                        <div class="col-sm-12 main-ads-articles text-center">
                            @foreach($SiteConfig['adsUnderContent'] as $value)
                                @if(empty($value->target))
                                <a href="{{$value->youtube_id}}" target="_blank">
                                    <img alt="" class="center-ads" src="{{ url('/'). Image::url($value->image,935,123,array('crop')) }}"/>
                                </a>
                                @else
                                <a href="{{$value->youtube_id}}">
                                    <img alt="" class="center-ads" src="{{ url('/'). Image::url($value->image,935,123,array('crop')) }}"/>
                                </a>
                                @endif
                            @endforeach
                        </div>
                        <br>
                        <div id="disqus_thread"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="listing-block">
                <div class="title">
                    <div class="row">
                        <div class="col">
                            <h7>
                                {{strtoupper($singleArticle->users->name)}}
                            </h7>
                        </div>
                    </div>
                </div>
                <div class="all-media">
                    <div class="all-media-child">
                        @if(sizeof($articles)>0)
                            @foreach($articles as $value)
                            <div class="media">
                                @if(!empty($value->attr_7))
                                    <div class="item-top-overlay">
                                        {{ $duration->formatted($value->attr_7) }}
                                    </div>
                                @else

                                @endif

                                @if(!empty($value->image))
                                <a href="{{ url('/article/'.$value->slug) }}">
                                    <img class="d-flex align-self-start img-fluid top-overlay" src="{{ url('/'). Image::url($value->image,513,320,array('crop')) }}"/>
                                </a>
                                @else
                                <a href="{{ url('/article/'.$value->slug) }}">
                                    <img class="d-flex align-self-start img-fluid top-overlay" src="https://img.youtube.com/vi/{{ $value->attr_6}}/hqdefault.jpg" style="width: 513; height: 350;"/>
                                </a>
                                @endif

                                <div class="media-body pl-3">
                                    <div class="media-title-series">
                                        <a href="{{ url('/article/'.$value->slug) }}">
                                            @if(!empty($value->series()->first()->title))
                                            <a href="#">
                                                    {{$value->series()->first()->title}}
                                                @else
                                                    {{$value->channelVideo()->first()->title}}
                                            </a>
                                            @endif
                                        </a>
                                    </div>
                                    <div class="media-title">
                                        <a href="{{ url('/article/'.$value->slug) }}">
                                            {{$value->title}}
                                        </a>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
            <div class="sidebar my-auto">
                <div class="main-ads-mobile text-center">
                    <div class="sidebar-ads-1">
                        @foreach($SiteConfig['adsRightContentI'] as $value)
                            @if(empty($value->target))
                            <a href="{{$value->youtube_id}}" target="_blank">
                                <img alt="" src="{{ url('/'). Image::url($value->image,144,542,array('crop')) }}"/>
                            </a>
                            @else
                            <a href="{{$value->youtube_id}}">
                                <img alt="" src="{{ url('/'). Image::url($value->image,144,542,array('crop')) }}"/>
                            </a>
                            @endif
                        @endforeach
                    </div>
                </div>
                <div class="main-ads text-center">
                    <div class="sidebar-ads-1">
                        @foreach($SiteConfig['adsRightContentI'] as $value)
                            @if(empty($value->target))
                            <a href="{{$value->youtube_id}}" target="_blank">
                                <img alt="" src="{{ url('/'). Image::url($value->image,144,542,array('crop')) }}"/>
                            </a>
                            @else
                            <a href="{{$value->youtube_id}}">
                                <img alt="" src="{{ url('/'). Image::url($value->image,144,542,array('crop')) }}"/>
                            </a>
                            @endif
                        @endforeach
                    </div>
                </div>
                <div class="main-ads text-center">
                    <div class="sidebar-ads-2">
                        @foreach($SiteConfig['adsRightContentII'] as $value)
                            @if(empty($value->target))
                            <a href="{{$value->youtube_id}}" target="_blank">
                                <img alt="" src="{{ url('/'). Image::url($value->image,144,542,array('crop')) }}"/>
                            </a>
                            @else
                            <a href="{{$value->youtube_id}}">
                                <img alt="" src="{{ url('/'). Image::url($value->image,144,542,array('crop')) }}"/>
                            </a>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid top-videos">
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-12">
                <div class="title">
                    <h7>
                        MORE FROM ARTICLE
                    </h7>
                </div>
            </div>
            <div class="col-md-12">
                <div class="{{ count($moreArticles)>=1? 'slider-top-videos':'' }}">
                    @forelse($moreArticles as $value)
                    <div>
                        <div class="item">
                            <div class="imgTitle">
                                <div class="top-videos-overlay">
                                    <a href="{{ url('/article/'.$value->slug) }}">
                                        @if(!empty($value->image))
                                            <img class="img-fluid top-videos-overlay" src="{{ url('/'). Image::url($value->image,886,490,array('crop')) }}"/>
                                        @else
                                            <img class="img-fluid top-videos-overlay" src="https://img.youtube.com/vi/{{ $value->attr_6}}/mqdefault.jpg" style="width: 886; height: 490;"/>
                                        @endif
                                    </a>
                                    @if(!empty($value->attr_7))
                                        <div class="item-top-videos-overlay">
                                            {{ $duration->formatted($value->attr_7) }}
                                        </div>
                                    @else

                                    @endif
                                </div>
                            </div>
                            <p>
                                <a href="{{ url('/article/'.$value->slug) }}">
                                    {{$value->title}}
                                </a>
                            </p>
                        </div>
                    </div>
                    @empty
                    <div class="col-sm-12">
                        <br>
                            <div class="alert alert-warning text-center" role="alert">
                                There is no data to display!
                            </div>
                        </br>
                    </div>
                    @endforelse
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid top-videos">
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-12">
                <div class="title">
                    <h7>
                        RELATED ARTICLE
                    </h7>
                </div>
            </div>
            <div class="col-md-12">
                <div class="{{ count($relatedArticles)>=1? 'slider-top-videos':'' }}">
                    @forelse($relatedArticles as $value)
                    <div>
                        <div class="item">
                            <div class="imgTitle">
                                <div class="top-videos-overlay">
                                    <a href="{{ url('/article/'.$value->slug) }}">
                                        @if(!empty($value->image))
                                            <img class="img-fluid top-videos-overlay" src="{{ url('/'). Image::url($value->image,886,490,array('crop')) }}"/>
                                        @else
                                            <img class="img-fluid top-videos-overlay" src="https://img.youtube.com/vi/{{ $value->attr_6}}/mqdefault.jpg" style="width: 886; height: 490;"/>
                                        @endif
                                    </a>
                                    @if(!empty($value->attr_7))
                                        <div class="item-top-videos-overlay">
                                            {{ $duration->formatted($value->attr_7) }}
                                        </div>
                                    @else

                                    @endif
                                </div>
                            </div>
                            <p>
                                <a href="{{ url('/article/'.$value->slug) }}">
                                    {{$value->title}}
                                </a>
                            </p>
                        </div>
                    </div>
                    @empty
                    <div class="col-sm-12">
                        <br>
                            <div class="alert alert-warning text-center" role="alert">
                                There is no data to display!
                            </div>
                        </br>
                    </div>
                    @endforelse
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid top-pro">
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-12">
                <div class="title">
                    <h7>
                        RECOMMENDED SERIES
                    </h7>
                </div>
            </div>
            <div class="col-md-12">
                <div class="{{ count($recomendedSeries)>=1? 'slider-top-pro':'' }}">
                    @forelse($recomendedSeries as $value)
                    <div>
                        <div class="item">
                            <article class="caption">
                                <img class="caption__media img-fluid" src="{{ url('/'). Image::url($value->image,400,550,array('crop')) }}"/>
                                <a href="{{ url('/series/'.$value->slug) }}">
                                    <div class="caption__overlay">
                                        <h1 class="caption__overlay__title">
                                            {{$value->title}}
                                        </h1>
                                        <h7 class="caption__overlay__content">
                                            {{$value->totalVideo}} Videos
                                        </h7>
                                        @if(!empty($value->excerpt))
                                        <p class="caption__overlay__content">
                                            {!!strip_tags($value->excerpt)!!}
                                        </p>
                                        @else
                                        <p class="caption__overlay__content">
                                            {!!strip_tags(str_limit($value->content, 100))!!}
                                        </p>
                                        @endif
                                    </div>
                                </a>
                            </article>
                            <p>
                                <a href="{{ url('/series/'.$value->slug) }}">
                                    {{$value->title}}
                                </a>
                            </p>
                            <p class="videos">
                                <a href="#">
                                    {{$value->totalVideo}} Videos
                                </a>
                            </p>
                        </div>
                    </div>
                    @empty
                    <div class="col-sm-12">
                        <br>
                            <div class="alert alert-warning text-center" role="alert">
                                There is no data to display!
                            </div>
                        </br>
                    </div>
                    @endforelse
                </div>
            </div>
        </div>
    </div>
</div>

@endsection