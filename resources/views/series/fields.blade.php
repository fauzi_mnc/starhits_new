
@section('css')
<link href="{{ asset('css/fileinput.min.css') }}" rel="stylesheet">

<style>
.kv-photo .krajee-default.file-preview-frame,.kv-photo .krajee-default.file-preview-frame:hover {
    margin: 0;
    padding: 0;
    border: none;
    box-shadow: none;
    text-align: center;
}
.kv-photo .file-input {
    display: table-cell;
    max-width: 220px;
}
.kv-reqd {
    color: red;
    font-family: monospace;
    font-weight: normal;
}
</style>
@include('layouts.datatables_css')
@endsection
@if(auth()->user()->roles->first()->name == 'admin')
<div class="form-group">
    {!! Form::label('user_id', 'Creator:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::select('user_id', $users, null, ['placeholder' => 'Select Creator', 'class' => 'form-control', 'required' => 'required']) !!}
    </div>
</div>
@elseif(auth()->user()->roles->first()->name == 'user')
    {{ Form::hidden('user_id', auth()->user()->id) }}
@endif
<div class="form-group">
    {!! Form::label('parent_id', 'Channel:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::select('parent_id', $channel, null, ['placeholder' => 'Select Channel', 'class' => 'form-control', 'required' => 'required']) !!}
    </div>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Title:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::text('title', null, ['class' => 'form-control', 'required' => 'required', 'maxlength' => 125]) !!}
    </div>
</div>

<!-- Excerpt Field -->
<div class="form-group">
    {!! Form::label('excerpt', 'Excerpt:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::textarea('excerpt', null, ['class' => 'form-control']) !!}
    </div>
</div>

@if(auth()->user()->roles->first()->name == 'admin')
<!-- Status Field -->
<div class="form-group">
    {!! Form::label('type', 'Type:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::select('type', ['starhits' => 'Starhits', 'starpro' => 'Starpro'], null, ['placeholder' => 'Select Type', 'class' => 'form-control', 'required' => 'required']) !!}
    </div>
</div>
@elseif(auth()->user()->roles->first()->name == 'user')
<!-- Status Field -->
<div class="form-group">
    {!! Form::label('type', 'Type:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::select('type', ['starpro' => 'Starpro'], null, ['class' => 'form-control', 'required' => 'required']) !!}
    </div>
</div>
@endif
<!-- Sub Title Field -->
<div class="form-group">
    {!! Form::label('content', 'Description:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::textarea('content', null, ['class' => 'form-control']) !!}
    </div>
</div>

<!-- Picture Field -->
<div class="form-group">
    {!! Form::label('image', 'Picture:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        <div class="kv-photo center-block text-center">
            <input id="photo" name="image" type="file" class="file-loading">
        </div>

        <div id="kv-photo-errors-1" class="center-block alert alert-block alert-danger" style="display:none"></div>
    </div>
</div>

<!-- Submit Field -->
<div class="form-group">
    <div class="col-sm-4 col-sm-offset-2">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    @if(auth()->user()->roles->first()->name === 'admin')
    <a href="{!! route('admin.serie.index') !!}" class="btn btn-default">Cancel</a>
    @elseif(auth()->user()->roles->first()->name === 'user')
    <a href="{!! route('user.serie.index') !!}" class="btn btn-default">Cancel</a>
    @else
    <a href="{!! route('creator.serie.index') !!}" class="btn btn-default">Cancel</a>
    @endif
    </div>
</div>

@section('scripts')
<script src="{{ asset('js/plugins/bootstrap-fileinput/piexif.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-fileinput/sortable.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-fileinput/purify.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-fileinput/fileinput.min.js') }}"></script>
<script src="{{ asset('js/plugins/tinymce/jquery.tinymce.min.js') }}"></script>
<script src="{{ asset('js/plugins/tinymce/tinymce.min.js') }}"></script>
<script>

    var formType = '{{ $formType }}';
    $('#photo').val('');

    $("#photo").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        browseLabel: '',
        removeLabel: '',
        browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
        removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-photo-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        defaultPreviewContent: '<img src="{{ ($formType == 'edit' && $series->image) ? $series->image : asset('img/default_avatar_male.jpg') }}" alt="Your icon" style="width:160px">',
        layoutTemplates: {main2: '{preview} {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png", "jpeg"]
    });

    tinymce.init({
        forced_root_block : "",
        selector: "textarea",
        setup: function (editor) {
            editor.on('change', function () {
                tinymce.triggerSave();
            });
        },
        file_picker_types: 'image',
        images_upload_credentials: true,
        automatic_uploads: false,
        theme: "modern",
        paste_data_images: true,
        plugins: [
        "advlist autolink lists link image media charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars code fullscreen",
        "insertdatetime nonbreaking save table contextmenu directionality",
        "template paste textcolor colorpicker textpattern"
        ],
        toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | forecolor | media image",
        image_advtab: true,
        file_picker_callback: function(cb, value, meta) {
            var input = document.createElement('input');
            input.setAttribute('type', 'file');
            input.setAttribute('accept', 'image/*');
            
            // Note: In modern browsers input[type="file"] is functional without 
            // even adding it to the DOM, but that might not be the case in some older
            // or quirky browsers like IE, so you might want to add it to the DOM
            // just in case, and visually hide it. And do not forget do remove it
            // once you do not need it anymore.
        
            input.onchange = function() {
                var file = this.files[0];
                
                var reader = new FileReader();
                reader.readAsDataURL(file);
                reader.onload = function () {
                // Note: Now we need to register the blob in TinyMCEs image blob
                // registry. In the next release this part hopefully won't be
                // necessary, as we are looking to handle it internally.
                var id = 'blobid' + (new Date()).getTime();
                var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
                var base64 = reader.result.split(',')[1];
                var blobInfo = blobCache.create(id, file, base64);
                blobCache.add(blobInfo);
        
                // call the callback and populate the Title field with the file name
                cb(blobInfo.blobUri(), { title: file.name });
              };
            };
            
            input.click();
        }
    });

    $("#btn-submit").click(function(e){
        e.preventDefault();
        if (getStats('excerpt').chars == 0) {
            tinymce.execCommand('mceFocus',false,'#excerpt');
            tinymce.activeEditor.windowManager.alert('Excerpt could not be empty');
        }else{
            $('#forms').submit();
        }
    });

    function getStats(id) {
        console.log('tes');
    var body = tinymce.get(id).getBody(), text = tinymce.trim(body.innerText || body.textContent);

    return {
        chars: text.length,
        words: text.split(/[\w\u2019]+/).length
        };
    }
    
</script>
@if($formType == 'edit')
@include('layouts.datatables_js')
<script>
    $.fn.dataTable.ext.buttons.create = {
        action: function (e, dt, button, config) {
            $('#myModal').modal('show');
        }
    };

    var hash = document.location.hash;
    if (hash) {
        console.log(hash);
        $('.nav-tabs a[href="'+hash+'"]').tab('show')
    }

    // Change hash for page-reload
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
    window.location.hash = e.target.hash;
    });
</script>
@endif
@endsection