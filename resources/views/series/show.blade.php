@extends('layouts.crud')

@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Series</h2>
        <ol class="breadcrumb">
            <li>
                @if(auth()->user()->roles->first()->name === 'admin')
                <a href="{{route('admin.serie.index')}}">Series</a>
                @elseif(auth()->user()->roles->first()->name === 'user')
                <a href="{{route('user.serie.index')}}">Series</a>
                @else
                <a href="{{route('creator.serie.index')}}">Series</a>
                @endif
            </li>
            <li class="active">
                <strong>Detail</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
@endsection

@section('contentCrud')

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Series</h5>
                </div>
                <div class="ibox-content">
                    <div class="form-horizontal">
                        @include('series.show_fields')
                    </div>
                </div>
            </div>

@endsection

