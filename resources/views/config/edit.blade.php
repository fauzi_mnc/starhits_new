@extends('layouts.crud')

@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Config</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{route('config.index')}}">Config</a>
            </li>
            <li class="active">
                <strong>Edit</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
@endsection

@section('contentCrud')

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Web Configuration</h5>
                </div>
                <div class="ibox-content">
                    {!! Form::model($config, ['route' => ['config.update', $config->id], 'method' => 'put', 'class' => 'form-horizontal', 'files' => true]) !!}
                        @include('config.fields', ['formType' => 'edit'])
                    {!! Form::close() !!}
                </div>
            </div>

@endsection

