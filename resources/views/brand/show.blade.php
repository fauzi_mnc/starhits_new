@extends('layouts.crud')

@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Brand</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{route('brand.index')}}">Brand</a>
            </li>
            <li class="active">
                <strong>Detail</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
@endsection

@section('contentCrud')

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Brand</h5>
                </div>
                <div class="ibox-content">
                    <div class="form-horizontal">
                        {!! Form::model($user, ['route' => ['brand.update', $user->id], 'method' => 'put', 'class' => 'form-horizontal', 'files' => true]) !!}
                        @include('brand.show_fields', ['formType' => 'edit'])
                    {!! Form::close() !!}
                    </div>
                </div>
            </div>

@endsection

