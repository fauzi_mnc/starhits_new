@extends('layouts.crud')
<style>
    .panel-heading .accordion-toggle:after {
    /* symbol for "opening" panels */
    font-family: 'Glyphicons Halflings';  /* essential for enabling glyphicon */
    content: "\e114";    /* adjust as needed, taken from bootstrap.css */
    float: right;        /* adjust as needed */
    color: grey;         /* adjust as needed */
}
.panel-heading .accordion-toggle.collapsed:after {
    /* symbol for "collapsed" panels */
    content: "\e080";   /* adjust as needed, taken from bootstrap.css */
}

</style>

@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Dashboard</h2>
    </div>
    <div class="col-lg-2">

    </div>
</div>
@endsection
@section('contentCrud')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@include('flash::message')
@if(auth()->user()->hasRole(['admin']))
<div class="wrapper wrapper-content">
    <div class="row">        
        <div class="col-lg-12">
            <h2> <i class="fa fa-newspaper-o"></i> Contents</h2>
            <hr style="border:1px solid #e7eaec;">
        </div>
        <div class="col-lg-3">
            <div class="ibox float-e-margins box-dashboard">
                <a href="{{route('admin.posts.index')}}">
                    <div class="ibox-title">
                        <h5>Videos</h5>
                    </div>
                    <div class="ibox-content text-center">
                        <i class="fa fa-play-circle-o fa-5x"></i><br />
                        <h1 class="no-margins">{{$videos}}</h1>
                        <small>Total Videos</small>
                    </div>
                </a>                
            </div>
        </div>
        <div class="col-lg-3">
            <div class="ibox float-e-margins box-dashboard">
                <a href="{{route('admin.posts.index')}}">
                    <div class="ibox-title">
                        <h5>Articles</h5>
                    </div>
                    <div class="ibox-content text-center">
                        <i class="fa fa-newspaper-o fa-5x"></i><br />
                        <h1 class="no-margins">{{$articles}}</h1>
                        <small>Total Articles</small>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="ibox float-e-margins box-dashboard">
                <a href="{{route('channel.index')}}">
                    <div class="ibox-title">
                        <h5>Channels</h5>
                    </div>
                    <div class="ibox-content text-center">
                        <i class="fa fa-adjust fa-5x"></i><br />
                        <h1 class="no-margins">{{$channels}}</h1>
                        <small>Total Channels</small>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="ibox float-e-margins box-dashboard">
                <a href="{{route('admin.serie.index')}}">
                    <div class="ibox-title">
                        <h5>Series</h5>
                    </div>
                    <div class="ibox-content text-center">
                        <i class="fa fa-star fa-5x"></i><br />
                        <h1 class="no-margins">{{$series}}</h1>
                        <small>Total Series</small>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <h2> <i class="fa fa-user-circle"></i> Users</h2>
            <hr style="border:1px solid #e7eaec;">
        </div>
        <div class="col-lg-4">
            <div class="ibox float-e-margins box-dashboard">
                <a href="{{route('creator.index')}}">
                    <div class="ibox-title">
                        <h5>Creators</h5>
                    </div>
                    <div class="ibox-content text-center">
                        <i class="fa fa-user-circle fa-5x"></i><br />
                        <h1 class="no-margins">{{$creators}}</h1>
                        <small>Total Creators</small>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="ibox float-e-margins box-dashboard">
                <a href="{{route('influencer.index')}}">
                    <div class="ibox-title">
                        <h5>Influencer</h5>
                    </div>
                    <div class="ibox-content text-center">
                        <i class="fa fa-instagram fa-5x"></i><br />
                        <h1 class="no-margins">{{$influencer}}</h1>
                        <small>Total Influencer</small>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="ibox float-e-margins box-dashboard">
                <a href="{{route('user.index')}}">
                    <div class="ibox-title">
                        <h5>Users</h5>
                    </div>
                    <div class="ibox-content text-center">
                        <i class="fa fa-user-circle-o fa-5x"></i><br />
                        <h1 class="no-margins">{{$users}}</h1>
                        <small>Total Users</small>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <h2> <i class="fa fa-newspaper-o"></i> Clients</h2>
            <hr style="border:1px solid #e7eaec;">
        </div>
        <div class="col-lg-6">
            <div class="ibox float-e-margins box-dashboard">
                <a href="{{route('admin.campaign.index')}}">
                    <div class="ibox-title">
                        <h5>Campaign</h5>
                    </div>
                    <div class="ibox-content text-center">
                        <i class="fa fa-handshake-o fa-5x"></i><br />
                        <h1 class="no-margins">{{$campaign}}</h1>
                        <small>Total Campaign</small>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="ibox float-e-margins box-dashboard">
                <a href="{{route('brand.index')}}">
                    <div class="ibox-title">
                        <h5>Brands</h5>
                    </div>
                    <div class="ibox-content text-center">
                        <i class="fa fa-user-circle-o fa-5x"></i><br />
                        <h1 class="no-margins">{{$brands}}</h1>
                        <small>Total Brands</small>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <h2> <i class="fa fa-file-audio-o"></i> Master Lagu</h2>
            <hr style="border:1px solid #e7eaec;">
        </div>
        <div class="col-lg-4">
            <div class="ibox float-e-margins box-dashboard">
                <a href="{{route('masterlagu.index')}}">
                    <div class="ibox-title">
                        <h5>Master Lagu</h5>
                    </div>
                    <div class="ibox-content text-center">
                        <i class="fa fa-music fa-5x"></i><br />
                        <h1 class="no-margins">{{$masterlagu}}</h1>
                        <small>Total Master Lagu</small>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="ibox float-e-margins box-dashboard">
                <a href="{{route('mastersinger.index')}}">
                    <div class="ibox-title">
                        <h5>Master Singer</h5>
                    </div>
                    <div class="ibox-content text-center">
                        <i class="fa fa-star fa-5x"></i><br />
                        <h1 class="no-margins">{{$singers}}</h1>
                        <small>Total Singer</small>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="ibox float-e-margins box-dashboard">
                <a href="{{route('mastersongwriter.index')}}">
                    <div class="ibox-title">
                        <h5>Master Songwriter / Publishing</h5>
                    </div>
                    <div class="ibox-content text-center">
                        <i class="fa fa-user fa-5x"></i><br />
                        <h1 class="no-margins">{{$songwriters}}</h1>
                        <small>Total Songwriter / Publishing</small>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <h2> <i class="fa fa-file-text-o"></i> Reporting</h2>
            <hr style="border:1px solid #e7eaec;">
        </div>
        <div class="col-lg-4">
            <div class="ibox float-e-margins box-dashboard">
                <a href="{{route('revenue.index')}}">
                    <div class="ibox-title">
                        <h5>Youtube Revenue</h5>
                    </div>
                    <div class="ibox-content text-center">
                        <i class="fa fa-youtube fa-5x"></i><br />
                        <h1 class="no-margins">{{$revenues}}</h1>
                        <small>Total Report Youtube Revenues</small>
                    </div>
                </a>                
            </div>
        </div>
        <div class="col-lg-4">
            <div class="ibox float-e-margins box-dashboard">
                <a href="{{route('youtube.analytics.index')}}">
                    <div class="ibox-title">
                        <h5>Youtube Weekly Analytics</h5>
                    </div>
                    <div class="ibox-content text-center">
                        <i class="fa fa-bar-chart fa-5x"></i><br />
                        {{-- <h1 class="no-margins">{{$ytanalytics}}</h1> --}}
                        <h1 class="no-margins">&nbsp;</h1>
                        <small>Youtube Weekly Analytics</small>
                    </div>
                </a>                
            </div>
        </div>
        <div class="col-lg-4">
                <div class="ibox float-e-margins box-dashboard">
                    <a href="{{route('dsp.index')}}">
                        <div class="ibox-title">
                            <h5>DSP Reports</h5>
                        </div>
                        <div class="ibox-content text-center">
                            <i class="fa fa-file-audio-o fa-5x"></i><br />
                            <h1 class="no-margins">{{$dsps}}</h1>
                            <small>Total DSP Reports</small>
                        </div>
                    </a>                
                </div>
            </div>
    </div>
</div>

@elseif(auth()->user()->hasRole(['user']))
<div class="wrapper wrapper-content">
    <div class="row">
        @php
        //dd($youtubePartner);    
        @endphp
        @if($youtubePartner==0)
        <div class="text-center">
            <h1>Grow With Us</h1>
            <h2>We need to connect to your YouTube Account.</h2>
            <p>Your data &amp; privacy is our prime priority. We will collect the following data only</p>
            <section id="busareas" class="wrapper">
                <div class="inner">
                    <div class="container">
                        <div class="row">
                            <div class="two columns">
                                <i class="fa fa-user fa-5x"></i>
                                <hr />
                                <p class="text-box small">Your Name</p>
                            </div>
                            <div class="two columns">
                                <i class="fa fa-envelope fa-5x"></i>
                                <hr />
                                <p class="text-box small">Email Address</p>
                            </div>
                            <div class="two columns">
                                <i class="fa fa-line-chart fa-5x"></i>
                                <hr />
                                <p class="text-box small">Change Statistic</p>
                            </div>
                            <div class="two columns">
                                <i class="fa fa-info-circle fa-5x"></i>
                                <hr />
                                <p class="text-box small">Info Statistic</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <div style="margin:10px 0;">
                <button class="btn btn-primary btn-lg">Authenticate Your Channel</button>
            </div>
            <p class="small">Authenticate Youtube CMS with Google Account, Please Connect Your Channel</p>
        </div>
        @else
        <div class="col-lg-3">
            <div class="ibox float-e-margins box-dashboard">
                <a href="{{route('user.posts.index')}}">
                    <div class="ibox-title">
                        <h5>Videos</h5>
                    </div>
                    <div class="ibox-content text-center">
                        <i class="fa fa-play-circle-o fa-5x"></i><br />
                        <h1 class="no-margins">{{$uservideos}}</h1>
                        <small>Total Videos</small>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="ibox float-e-margins box-dashboard">
                <a href="{{route('user.posts.index')}}">
                    <div class="ibox-title">
                        <h5>Articles</h5>
                    </div>
                    <div class="ibox-content text-center">
                        <i class="fa fa-newspaper-o fa-5x"></i><br />
                        <h1 class="no-margins">{{$userarticles}}</h1>
                        <small>Total Articles</small>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="ibox float-e-margins box-dashboard">
                <a href="{{route('user.serie.index')}}">
                    <div class="ibox-title">
                        <h5>Series</h5>
                    </div>
                    <div class="ibox-content text-center">
                        <i class="fa fa-star-o fa-5x"></i><br />
                        <h1 class="no-margins">{{$userseries}}</h1>
                        <small>Total Series</small>
                    </div>
                </a>
            </div>
        </div>
        @endif
    </div>
</div>
@elseif(auth()->user()->hasRole(['finance']))
  <div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <h2> <i class="fa fa-file-text-o"></i> Reporting</h2>
            <hr style="border:1px solid #e7eaec;">
        </div>
        <div class="col-lg-6">
            <div class="ibox float-e-margins box-dashboard">
                <a href="{{route('finance.revenue.index')}}">
                    <div class="ibox-title">
                        <h5>Youtube Revenue</h5>
                    </div>
                    <div class="ibox-content text-center">
                        <i class="fa fa-youtube fa-5x"></i><br />
                        <h1 class="no-margins">{{$revenues}}</h1>
                        <small>Total Report Youtube Revenues</small>
                    </div>
                </a>                
            </div>
        </div>
        <div class="col-lg-6">
            <div class="ibox float-e-margins box-dashboard">
                <a href="{{route('finance.dsp.index')}}">
                    <div class="ibox-title">
                        <h5>DSP Reports</h5>
                    </div>
                    <div class="ibox-content text-center">
                        <i class="fa fa-file-audio-o fa-5x"></i><br />
                        <h1 class="no-margins">{{$dsps}}</h1>
                        <small>Total DSP Reports</small>
                    </div>
                </a>                
            </div>
        </div>
    </div>
</div> 
@elseif(auth()->user()->hasRole(['legal']))
  <div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-4">
            <div class="ibox float-e-margins box-dashboard">
                <a href="{{url('')}}/{{auth::user()->roles->first()->name}}/masterlagu">
                    <div class="ibox-title">
                        <h5> Lagu</h5>
                    </div>
                    <div class="ibox-content text-center">
                        <i class="fa fa-music fa-5x"></i><br />
                        <h1 class="no-margins">{{$masterlagu}}</h1>
                        <small>Total Lagu</small>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="ibox float-e-margins box-dashboard">
                <a href="{{route('legal.mastersinger.index')}}">
                    <div class="ibox-title">
                        <h5>Master Singer</h5>
                    </div>
                    <div class="ibox-content text-center">
                        <i class="fa fa-star fa-5x"></i><br />
                        <h1 class="no-margins">{{$singers}}</h1>
                        <small>Total Singer</small>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="ibox float-e-margins box-dashboard">
                <a href="{{route('legal.mastersongwriter.index')}}">
                    <div class="ibox-title">
                        <h5>Master Singer</h5>
                    </div>
                    <div class="ibox-content text-center">
                        <i class="fa fa-user fa-5x"></i><br />
                        <h1 class="no-margins">{{$songwriters}}</h1>
                        <small>Total Songwriter / Publishing</small>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>
@elseif(auth()->user()->hasRole(['singer']))
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-6">
            <div class="ibox float-e-margins box-dashboard">
                <a href="{{route('singer.masterlagu.index')}}">
                    <div class="ibox-title">
                        <h5> Lagu</h5>
                    </div>
                    <div class="ibox-content text-center">
                        <i class="fa fa-music fa-5x"></i><br />
                        <h1 class="no-margins">{{$masterlagu}}</h1>
                        <small>Total Lagu</small>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div> 
@elseif(auth()->user()->hasRole(['songwriter']))
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-6">
            <div class="ibox float-e-margins box-dashboard">
                <a href="{{route('songwriter.masterlagu.index')}}">
                    <div class="ibox-title">
                        <h5> Lagu</h5>
                    </div>
                    <div class="ibox-content text-center">
                        <i class="fa fa-music fa-5x"></i><br />
                        <h1 class="no-margins">{{$masterlagu}}</h1>
                        <small>Total Lagu</small>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div> 
@elseif(auth()->user()->hasRole(['anr']))
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-4">
            <div class="ibox float-e-margins box-dashboard">
                <a href="{{url('')}}/{{auth::user()->roles->first()->name}}/masterlagu">
                    <div class="ibox-title">
                        <h5> Lagu</h5>
                    </div>
                    <div class="ibox-content text-center">
                        <i class="fa fa-music fa-5x"></i><br />
                        <h1 class="no-margins">{{$masterlagu}}</h1>
                        <small>Total Lagu</small>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="ibox float-e-margins box-dashboard">
                <a href="{{route('anr.mastersinger.index')}}">
                    <div class="ibox-title">
                        <h5>Master Singer</h5>
                    </div>
                    <div class="ibox-content text-center">
                        <i class="fa fa-star fa-5x"></i><br />
                        <h1 class="no-margins">{{$singers}}</h1>
                        <small>Total Singer</small>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="ibox float-e-margins box-dashboard">
                <a href="{{route('anr.mastersongwriter.index')}}">
                    <div class="ibox-title">
                        <h5>Master Songwriter / Publishing</h5>
                    </div>
                    <div class="ibox-content text-center">
                        <i class="fa fa-user fa-5x"></i><br />
                        <h1 class="no-margins">{{$songwriters}}</h1>
                        <small>Total Songwriter / Publishing</small>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div> 
@endif
@endsection

@if(auth()->user()->hasRole(['admin']))
@section('scripts')
    <script>
        (function(w,d,s,g,js,fs){
        g=w.gapi||(w.gapi={});g.analytics={q:[],ready:function(f){this.q.push(f);}};
        js=d.createElement(s);fs=d.getElementsByTagName(s)[0];
        js.src='https://apis.google.com/js/platform.js';
        fs.parentNode.insertBefore(js,fs);js.onload=function(){g.load('analytics');};
        }(window,document,'script'));
    </script>

    <script>

        gapi.analytics.ready(function() {
        
        /**
        * Authorize the user immediately if the user has already granted access.
        * If no access has been created, render an authorize button inside the
        * element with the ID "embed-api-auth-container".
        */
        gapi.analytics.auth.authorize({
            container: 'embed-api-auth-container',
            clientid: '516384708768-ecc7mo5tvv1ih90juqk0jo6i7m1jgvj2.apps.googleusercontent.com'
        });
        
        
        /**
        * Create a new ViewSelector instance to be rendered inside of an
        * element with the id "view-selector-container".
        */
        var viewSelector = new gapi.analytics.ViewSelector({
            container: 'view-selector-container'
        });
        
        // Render the view selector to the page.
        viewSelector.execute();
        
        
        /**
        * Create a new DataChart instance with the given query parameters
        * and Google chart options. It will be rendered inside an element
        * with the id "chart-container".
        */
        var dataChart = new gapi.analytics.googleCharts.DataChart({
            query: {
            metrics: 'ga:sessions',
            dimensions: 'ga:date',
            'start-date': '30daysAgo',
            'end-date': 'yesterday'
            },
            chart: {
            container: 'chart-container',
            type: 'LINE',
            options: {
                width: '100%'
            }
            }
        });
        
        
        /**
        * Render the dataChart on the page whenever a new view is selected.
        */
        viewSelector.on('change', function(ids) {
            dataChart.set({query: {ids: ids}}).execute();
        });
        
        });
    </script>    
@endsection
@endif