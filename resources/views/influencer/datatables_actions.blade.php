{!! Form::open(['route' => ['influencer.destroy', $id], 'method' => 'delete']) !!}
<div class='btn-group'>
    <a href="{{ route('influencer.edit', $id) }}" class='btn btn-default btn-xs' title="Edit Influencer">
        <i class="glyphicon glyphicon-edit"></i>
    </a>
 {!! Form::button('<i class="glyphicon glyphicon-remove"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-danger btn-xs',
        'title' => 'Delete Influencer',
        'onclick' => "return confirm('Do you want to delete this influencer?')",
        'name' => 'action'
    ]) !!}
</div>
{!! Form::close() !!}
