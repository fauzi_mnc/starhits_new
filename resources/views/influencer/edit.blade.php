@extends('layouts.crud')

@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Influencer</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Influencer</a>
            </li>
            <li class="active">
                <strong>Edit</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
@endsection

@section('contentCrud')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@include('flash::message')
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                Edit Influencer
            </div>
            <div class="panel-body">
                {!! Form::model($user, ['route' => ['influencer.update', $user->id], 'method' => 'put', 'class' => 'form', 'files' => true, 'id' => 'example-form']) !!}
                    @include('influencer.fields', ['formType' => 'edit'])
                </form>
            </div>

        </div>
        
    </div>
</div>
@endsection