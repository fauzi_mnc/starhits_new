<div class='btn-group'>
    <a href="{{ route('page.show', $id) }}" class='btn btn-default btn-xs' title="Show Page">
        <i class="glyphicon glyphicon-eye-open"></i>
    </a>
    <a href="{{ route('page.edit', $id) }}" class='btn btn-default btn-xs' title="Edit Page">
        <i class="glyphicon glyphicon-edit"></i>
    </a>
</div>