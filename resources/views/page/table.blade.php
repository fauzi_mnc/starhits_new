@section('css')
    @include('layouts.datatables_css')
@endsection

{!! $dataTable->table(['width' => '100%']) !!}

@section('scripts')
    @include('layouts.datatables_js')
    {!! $dataTable->scripts() !!}
    @include('layouts.datatables_limit')
    <script>
        $(document).ready(function () {
            $(".alert-danger").fadeTo(3000, 1000).slideUp(1000, function(){
                $(".alert-danger").slideUp(1000);
            });
            $(".alert-success").fadeTo(3000, 1000).slideUp(1000, function(){
                $(".alert-success").slideUp(1000);
            });
        });
        $(".dataTables_filter").show();
        //lengthmenu -> add a margin to the right and reset clear 
        $(".dataTables_length").css('clear', 'none');
        $(".dataTables_length").css('margin-right', '20px');

        //info -> reset clear and padding
        $(".dataTables_info").css('clear', 'none');
        $(".dataTables_info").css('padding', '0');        
    </script>
@endsection