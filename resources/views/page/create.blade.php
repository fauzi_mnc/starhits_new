@extends('layouts.crud')
@section('css')
<link href="{{ asset('css/fileinput.min.css') }}" rel="stylesheet">
<style>
.kv-photo .krajee-default.file-preview-frame,.kv-photo .krajee-default.file-preview-frame:hover {
    margin: 0;
    padding: 0;
    border: none;
    box-shadow: none;
    text-align: center;
}
.kv-photo .file-input {
    display: table-cell;
    max-width: 220px;
}
.kv-reqd {
    color: red;
    font-family: monospace;
    font-weight: normal;
}
</style>

@endsection
@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Page</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{route('page.index')}}">Page</a>
            </li>
            <li class="active">
                <strong>Create</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
@endsection

@section('contentCrud')

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Page</h5>
                </div>
                <div class="ibox-content">
                {!! Form::open(['route' => 'page.store', 'class' => 'form-horizontal', 'files' => true]) !!}
                
                @include('page.fields', ['formType' => 'create'])

                {!! Form::close() !!}
                </div>
            </div>
@endsection
