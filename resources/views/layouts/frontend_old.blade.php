<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <script type="text/javascript">
            var timerStart = Date.now();
        </script>

        <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="Star Hits is a digital network that develops creator, talent or influencers, to cooperate with the brand and to create the ecosystem that are connected in the content.">
        <meta name="author" content="Starhits">
        @if(Request::server ("SERVER_NAME") == 'starhits.id')
        <!-- starhits.id -->
        <meta name="google-site-verification" content="faXytG0YQOHovhpU4FQJQIfH8BzRG_AUYVMyxSj3x5Q" />
        @endif
        @if(Request::server ("SERVER_NAME") == 'starhits.com')
        <!-- starhits.com -->
        <meta name="google-site-verification" content="1LWlOlljjs-uqtRDTFDgpI1pAultCpoCe34OLsU_iuU" />
        @endif
        @if(Request::server ("SERVER_NAME") == 'devapp.mncgroup.com')
        <!-- devapp.mncgroup.com -->
        <meta name="google-site-verification" content="h6rKQ-J9ZHiljgaK49pXdJkQKAOj-cqzML6ALJ04fo0" />
        @endif
        @if(Request::server ("SERVER_NAME") == 'localhost/starhits/public')
        <!-- starhits.com -->
        <meta name="google-site-verification" content="1LWlOlljjs-uqtRDTFDgpI1pAultCpoCe34OLsU_iuU" />
        @endif

        @stack('meta')

        <!-- Email From Star Hits Official -->
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-92172449-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', 'UA-92172449-1');
        </script>
        <title>
            @if ($__env->yieldContent('title'))
                @yield('title') -
            @endif
            @if(isset($SiteConfig['configs']['website_title']))
                {{ $SiteConfig['configs']['website_title'] }}
            @endif
        </title>
        <!-- Bootstrap core CSS -->
        <link href="{{asset('frontend/assets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
        <link rel="stylesheet" href="{{asset('frontend/assets/vendor/font-awesome/css/font-awesome.css')}}">
        <link rel="stylesheet" href="{{asset('frontend/assets/vendor/slick/css/slick.css')}}">
        <link rel="stylesheet" href="{{asset('frontend/assets/vendor/slick/css/slick-theme.css')}}">
        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/css/gijgo.min.css" rel="stylesheet" type="text/css" />

        <!-- Custom styles for this template -->
        <link rel="stylesheet" href="{{asset('frontend/assets/css/style.css')}}">
        <link rel="stylesheet" href="{{asset('frontend/assets/css/media.min.css')}}">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css">
        <link rel="stylesheet" href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="{{ asset('frontend/assets/css/jquery.simplewizard.css')}}" rel="stylesheet" />
        <link rel="stylesheet" href="{{ asset('css/plugins/switchery/switchery.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('frontend/assets/css/bootstrap-select.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('frontend/assets/css/content-placeholder.css') }}" rel="stylesheet">

        <style>
        .btn-selectpick{
            background: #faffbe;
            border-color: #faffbe;
            box-shadow: none !important;
            border-radius: 4px;
        }
        .wizard > .content > .body{
            position: relative !important;
        }
        strong{
            padding-right: 30px;
        }
        </style>

        <script>
            var baseUrl = "{{ url('/') }}";
        </script>
        <style id="antiClickjack"></style>
        
    </head>
    <body>
        @if(env('APP_ENV') != 'local')
            @stack('preloader')
        @endif

        <!-- Page Content -->
        @include('frontend.partials._header')
        <div class="content">
            <!-- <amp-auto-ads type="adsense"
                data-ad-client="ca-pub-5195976246447231">
            </amp-auto-ads> -->
            
            <!-- <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>  -->
        </div>
        @include('frontend.partials._footer')

        <div class="modal fade bs-example-modal-lg" id="searchModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <form action="{{url('/search')}}" method="post">
                    {{ csrf_field() }}   
                    <div id="custom-search-input">
                        <div class="input-group col-md-12">
                            <input id="search" type="text" class="form-control search input-lg" name="search" placeholder="Search" autofocus />
                                <span class="input-group-btn">
                                    <button class="btn btn-info btn-lg" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>    

        @guest
            @include('frontend.partials._modals-login')

            @include('frontend.partials._modals-register-role')
            
            {{-- @include('frontend.partials._modals-register')

            @include('frontend.partials._modals-register-influencer') --}}

            @include('frontend.partials._modals-forgotpassword')
        @endguest

        <a href="javascript:" id="return-to-top"><i class="fa fa-chevron-up"></i></a>

        <!-- App scripts -->
        {{-- @stack('fixedban') --}}

        <!-- Bootstrap core JavaScript -->
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script type="text/javascript" src="{{ asset('frontend/assets/js/popper.js') }}"></script>
        <script type="text/javascript" src="{{asset('frontend/assets/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
        <script type="text/javascript" src="{{ asset('frontend/assets/js/bootstrap-select.js') }}"></script>
        <script type="text/javascript" src="{{asset('frontend/assets/vendor/navstrap/js/navstrap.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('frontend/assets/vendor/slick/js/slick.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('frontend/assets/vendor/loadmore/js/loadMoreResults.js')}}"></script>
        <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/js/gijgo.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="{{asset('frontend/assets/js/addtoany.js')}}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>

        <!-- jQuery UI -->
        <script src="{{ asset('js/plugins/jquery-ui/jquery-ui.min.js') }}"></script>

        <!-- Include this after the sweet alert js file -->
        @include('sweet::alert')
        
        <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/js/gijgo.min.js" type="text/javascript"></script>
        <script>
            $('#datepicker').datepicker({
                uiLibrary: 'bootstrap4'
            });
        </script>

        <script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>
        <script>
            //paste this code under head tag or in a seperate js file.
            // Wait for window load
            $(window).load(function() {
                // Animate loader off screen
                $(".se-pre-con").fadeOut("slow");;
            });
        </script>

        <!-- Custom JavaScript -->
        <script src="{{asset('frontend/assets/js/init.js?v=0.0.1')}}"></script>
        @stack('scripts')
        {{-- <script>
        /**
        *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.

        *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
        /*
        var disqus_config = function () {
        this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
        this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
        };
        */
        (function() { // DON'T EDIT BELOW THIS LINE
            var d = document, s = d.createElement('script');
                s.src = 'https://starhits-id.disqus.com/embed.js';
                s.setAttribute('data-timestamp', +new Date());
                (d.head || d.body).appendChild(s);
            })
        ();
        </script>
        <script id="dsq-count-scr" src="//starhits-id.disqus.com/count.js" async></script> --}}
        <script>
            $(document).ready(function() {
                $("#form-login").on("submit",function(e) {
                //e.preventDefault();
                var form = $(this);
                var field = form.find("input[name=password]");
                var hash = btoa(field.val());
                var hash2 = btoa(hash);
                var slice = hash2.substr(hash2.length - 2);
                hash2 = hash2.slice(0, -2);
                var random_char = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
                var slice_random = random_char.substring(0, 14); //only get 14 characters
                var new_str = hash2+slice_random+slice;
                field.val(new_str);
            });

                if (self === top) {
                    var antiClickjack = document.getElementById("antiClickjack");
                        antiClickjack.parentNode.removeChild(antiClickjack);
                } else {
                    top.location = self.location;
                }
            });
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.14.0/jquery.validate.min.js"></script>
        <script src="{{asset('frontend/assets/js/jquery.simplewizard.js')}}"></script>
        <script>
            $(function () {
                $("#wizard1").simpleWizard({
                    cssClassStepActive: "active",
                    cssClassStepDone: "done",
                    onFinish: function() {
                        alert("Wizard finished")
                    }
                });
            });
        </script>

        <script type="text/javascript">
             /* $(document).ready(function() {
                 console.log("Time until DOMready: ", Date.now()-timerStart);
             });
             $(window).load(function() {
                 console.log("Time until everything loaded: ", Date.now()-timerStart);
             }); */
        </script>
    </body>
</html>
