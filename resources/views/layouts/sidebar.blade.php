<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="logo" style="margin:20px 0; padding:5px;">                
                    <img alt="Starhits" width="100%" src="{{asset('images/starhits_new_2019.png')}}">
                </div>
                {{--  <div class="dropdown profile-element text-center" style="padding: 30px 0">
                    <span>
                        <img width="48" height="48" alt="image" class="img-circle" src="{{ asset(auth()->user()->image) }}">
                    </span>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#" aria-expanded="false">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">{{ auth()->user()->name }}</strong>
                            </span> <span class="text-muted text-xs block">{{ auth()->user()->roles()->where('name', request()->segment(1))->first()->display_name }} <b class="caret"></b></span> </span> 
                    </a>
                </div>  --}}
            </li>
            <!-- ++++++++++++++  ROLE ADMINISTRATOR  +++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

            @if(auth()->user()->hasRole(['admin']))
            <li class="{{ Request::is('admin/dashboard*') ? 'active' : '' }}">
                <a href="{{route('admin.dashboard.index')}}"><i class="fa fa-dashboard {{{ (Request::is('dashboard') ? 'active' : '') }}}"></i> <span class="nav-label">Dashboard</span></a>
            </li>
            <li class="{{ Request::is('admin/channel*') ? 'active' : '' }}">
                <a href="{{route('channel.index')}}"><i class="fa fa-adjust {{{ (Request::is('channel') ? 'active' : '') }}}"></i> <span class="nav-label">Channel</span></a>
            </li>
            <li class="{{ Request::is('admin/creator*') ? 'active' : '' }}">
                <a href="{{route('creator.index')}}"><i class="fa fa-group {{{ (Request::is('creator') ? 'active' : '') }}}"></i> <span class="nav-label">Creator</span></a>
            </li>
            <li class="{{ Request::is('admin/serie*') ? 'active' : '' }}">
                <a href="{{route('admin.serie.index')}}"><i class="fa fa-star {{{ (Request::is('serie') ? 'active' : '') }}}"></i> <span class="nav-label">Series</span></a>
            </li>
            <li class="{{ Request::is('admin/posts*') ? 'active' : '' }}">
                <a href="{{route('admin.posts.index')}}"><i class="fa fa-play {{{ (Request::is('posts') ? 'active' : '') }}}"></i> <span class="nav-label">Posts</span></a>
            </li>
            <li class="{{ Request::is('admin/ads*') ? 'active' : '' }}">
                <a href="{{route('ads.index')}}"><i class="fa fa-language {{{ (Request::is('ads') ? 'active' : '') }}}"></i> <span class="nav-label">Ads</span></a>
            </li>
            <li class="{{ Request::is('admin/brand*') ? 'active' : '' }}">
                <a href="{{route('brand.index')}}"><i class="fa fa-superpowers {{{ (Request::is('brand') ? 'active' : '') }}}"></i> <span class="nav-label">Brands</span></a>
            </li>
            <li class="{{ Request::is('admin/influencer*') ? 'active' : '' }}">
                <a href="#"><i class="fa fa-instagram"></i> <span class="nav-label">Influencers</span>
                <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li class="{{ Request::is('admin/influencer') ? 'active' : '' }}"><a href="{{route('influencer.index')}}"><i class="fa fa fa-diamond"></i> Influencers</a></li>
                    <li class="{{ Request::is('admin/influencer/browse-influencer*') ? 'active' : '' }}"><a href="{{route('admin.influencer.browse')}}"><i class="fa fa-users"></i> Browse Influencer</a></li>
                </ul>
            </li>
            <!-- <li class="{{ Request::is('admin/influencer*') ? 'active' : '' }}">
                <a href="{{route('influencer.index')}}"><i class="fa fa-instagram {{{ (Request::is('influencer') ? 'active' : '') }}}"></i> <span class="nav-label">Influencers</span></a>
            </li> -->
            <li class="{{ Request::is('admin/revenue*') ? 'active' : '' }}{{ Request::is('admin/dsp*') ? 'active' : '' }}{{ Request::is('admin/youtube/analytics*') ? 'active' : '' }}">
                <a href="#"><i class="fa fa-file-text-o"></i> <span class="nav-label">Reports</span>
                <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li class="{{ Request::is('admin/revenue*') ? 'active' : '' }} {{ Request::is('admin/youtube/analytics*') ? 'active' : '' }}">
                        <a href="#"><i class="fa fa-youtube"></i> <span class="nav-label">Youtube</span>
                        <span class="fa arrow"></span></a>
                        <ul class="nav nav-third-level">
                            <li class="{{ Request::is('admin/revenue') ? 'active' : '' }}{{ Request::is('admin/revenue/*/*') ? 'active' : '' }}"><a href="{{route('revenue.index')}}"><i class="fa fa-file-text"></i> Revenue</a></li>
                            <li class="{{ Request::is('admin/revenue/approval*') ? 'active' : '' }}"><a href="{{route('revenue.approval.index')}}"><i class="fa fa-check-square-o"></i> Pending Approval</a></li>
                            <li class="{{ Request::is('admin/revenue/approver*') ? 'active' : '' }}"><a href="{{route('revenue.approver.index')}}"><i class="fa fa-user-plus"></i> Approver</a></li>
                            <li class="{{ Request::is('admin/youtube/analytics*') ? 'active' : '' }}"><a href="{{route('youtube.analytics.index')}}"><i class="fa fa-bar-chart"></i> Analytics</a></li>
                        </ul>
                    </li>
                    <li class="{{ Request::is('admin/dsp*') ? 'active' : '' }}"><a href="{{route('dsp.index')}}"><i class="fa fa-file-audio-o"></i> DSP Reports</a></li>
                </ul>
            </li>
            <li class="{{ Request::is('admin/campaign*') ? 'active' : '' }}">
                <a href="{{route('admin.campaign.index')}}"><i class="fa fa-handshake-o {{ (Request::is('campaign') ? 'active' : '') }}"></i> <span class="nav-label">Campaign</span></a>
            </li>
            <li class="{{ Request::is('admin/masterlagu*') ? 'active' : '' }}{{ (Request::is('admin/mastersinger*') ? 'active' : '') }}{{ (Request::is('admin/mastersongwriter*') ? 'active' : '') }}">
                <a href="#"><i class="fa fa-music"></i> <span class="nav-label">Master Data</span>
                <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li class="{{ Request::is('admin/masterlagu*') ? 'active' : '' }}">
                        <a href="{{route('masterlagu.index')}}"><i class="fa fa-music {{ (Request::is('admin/masterlagu') ? 'active' : '') }}"></i> <span class="nav-label">Master Lagu</span></a>
                    </li>
                    <li class="{{ Request::is('admin/mastersinger*') ? 'active' : '' }}">
                        <a href="{{route('mastersinger.index')}}"><i class="fa fa-star {{ (Request::is('admin/mastersinger') ? 'active' : '') }}"></i> <span class="nav-label">Master Singer</span></a>
                    </li>
                    <li class="{{ Request::is('admin/mastersongwriter*') ? 'active' : '' }}">
                        <a href="{{route('mastersongwriter.index')}}"><i class="fa fa-user {{ (Request::is('admin/mastersongwriter') ? 'active' : '') }}"></i> <span class="nav-label">Master Songwriter</span></a>
                    </li>
                </ul>
            </li>
            <!-- <li class="{{ Request::is('admin/platforms*') ? 'active' : '' }}">
                <a href="{{route('platforms.index')}}"><i class="{{{ (Request::is('platforms') ? 'active' : '') }}}"></i> <span class="nav-label">Platforms</span></a>
            </li> -->
            <li class="{{ Request::is('admin/config*') ? 'active' : '' }}">
                <a href="{{route('config.index')}}"><i class="fa fa-gears {{{ (Request::is('config') ? 'active' : '') }}}"></i> <span class="nav-label">Website Config</span></a>
            </li>
            <li class="{{ Request::is('admin/email_config*') ? 'active' : '' }}">
                <a href="{{route('email_config.index')}}"><i class="fa fa-envelope {{{ (Request::is('email_config') ? 'active' : '') }}}"></i> <span class="nav-label">Email Config</span></a>
            </li>
            <li class="{{ Request::is('admin/personal*') ? 'active' : '' }}">
                <a href="{{route('admin.personal.index')}}"><i class="fa fa-user {{{ (Request::is('personal') ? 'active' : '') }}}"></i> <span class="nav-label">Personal Setting</span></a>
            </li>
            <!-- <li class="{{ Request::is('admin/payment*') ? 'active' : '' }}">
                <a href="{{route('payment.index')}}"><i class="{{{ (Request::is('payment') ? 'active' : '') }}}"></i> <span class="nav-label">Payment Setting</span></a>
            </li> -->
            <li class="{{ Request::is('admin/page*') ? 'active' : '' }}">
                <a href="{{route('page.index')}}"><i class="fa fa-file {{{ (Request::is('page') ? 'active' : '') }}}"></i> <span class="nav-label">Pages</span></a>
            </li>
            <li class="{{ Request::is('admin/menu*', 'admin/user*', 'admin/role*', 'admin/category*') ? 'active' : '' }}">
                <a href="#"><i class="fa fa-cog"></i> <span class="nav-label">System Admin</span>
                <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li class="{{ Request::is('admin/user*') ? 'active' : '' }}"><a href="{{route('user.index')}}"><i class="fa fa-user-plus"></i> User Management</a></li>
                    <!--<li class="{{ Request::is('admin/menu*') ? 'active' : '' }}"><a href="{{route('role.index')}}">Menu Management</a></li>-->
                    <li class="{{ Request::is('admin/role*') ? 'active' : '' }}"><a href="{{route('role.index')}}"><i class="fa fa-users"></i> Role Management</a></li>
                    <li class="{{ Request::is('admin/category*') ? 'active' : '' }}"><a href="{{route('category.index')}}"><i class="fa fa-clone"></i> Product Category</a></li>
                    <li class="{{ Request::is('admin/menu*') ? 'active' : '' }}"><a href="{{route('menu.index')}}"><i class="fa fa-navicon"></i> Menu Management</a></li>
                    <li class="{{ Request::is('admin/account_management*') ? 'active' : '' }}"><a href="{{route('account_management.index')}}"><i class="fa fa-navicon"></i> Akun Management</a></li>
                </ul>
            </li>
            @elseif(auth()->user()->hasRole(['user']))
            <li class="{{ Request::is('user/dashboard*') ? 'active' : '' }}">
                <a href="{{route('user.dashboard.index')}}"><i class="fa fa-dashboard {{{ (Request::is('dashboard') ? 'active' : '') }}}"></i> <span class="nav-label">Dashboard</span></a>
            </li>
            {{-- <li class="{{ Request::is('user/serie*') ? 'active' : '' }}">
                <a href="{{route('user.serie.index')}}"><i class="fa fa-star {{{ (Request::is('serie') ? 'active' : '') }}}"></i> <span class="nav-label">Series</span></a>
            </li>
            <li class="{{ Request::is('user/posts*') ? 'active' : '' }}">
                <a href="{{route('user.posts.index')}}"><i class="fa fa-play {{{ (Request::is('posts') ? 'active' : '') }}}"></i> <span class="nav-label">Posts</span></a>
            </li> --}}
            {{-- <li class="{{ Request::is('user/platforms*') ? 'active' : '' }}">
                <a href="{{route('user.platforms.index')}}"><i class="fa fa-globe {{{ (Request::is('platforms') ? 'active' : '') }}}"></i> <span class="nav-label">Platforms</span></a>
            </li> --}}
            <li class="{{ Request::is('user/cms*') ? 'active' : '' }}">
                <a href="#"><i class="fa fa-globe"></i> <span class="nav-label">Network</span>
                <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li class="{{ Request::is('user/cms') ? 'active' : '' }}"><a href="{{route('user.cms.index')}}"><i class="fa fa-list"></i> CMS List</a></li>
                </ul>
            </li>
            <li class="{{ Request::is('user/creator*') ? 'active' : '' }}">
                <a href="#"><i class="fa fa-star-o"></i> <span class="nav-label">Channels</span>
                <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li class="{{ Request::is('user/channels/leaderboard') ? 'active' : '' }}"><a href="{{route('user.channels.leaderboard.index')}}"><i class="fa fa-trophy"></i> Leaderboard</a></li>
                    <li class="{{ Request::is('user/channels/list') ? 'active' : '' }}"><a href="{{route('user.channels.list.index')}}"><i class="fa fa-star"></i>Channels List</a></li>
                </ul>
            </li>
            <li class="{{ Request::is('user/personal*') ? 'active' : '' }}">
                <a href="{{route('user.personal.index')}}"><i class="fa fa-user {{{ (Request::is('personal') ? 'active' : '') }}}"></i> <span class="nav-label">Personal Setting</span></a>
            </li>
            @elseif(auth()->user()->hasRole('creator') && request()->segment(1) == 'creator')
            {{-- @if (auth()->user()->is_show == 1) --}}
            <li class="{{ Request::is('creator/serie*') ? 'active' : '' }}">
                <a href="{{route('creator.serie.index')}}"><i class="fa fa-star {{{ (Request::is('serie') ? 'active' : '') }}}"></i> <span class="nav-label">Series</span></a>
            </li>
            <li class="{{ Request::is('creator/posts*') ? 'active' : '' }}">
                <a href="{{route('creator.posts.index')}}"><i class="fa fa-play {{{ (Request::is('posts') ? 'active' : '') }}}"></i> <span class="nav-label">Posts</span></a>
            <li class="{{ Request::is('creator/bookmark*') ? 'active' : '' }}">
                <a href="{{route('creator.bookmark.index')}}"><i class="fa fa-address-book {{{ (Request::is('bookmark') ? 'active' : '') }}}"></i> <span class="nav-label">Bookmark</span></a>
            </li>
            <li class="{{ Request::is('creator/platforms*') ? 'active' : '' }}">
                <a href="{{route('creator.platforms.index')}}"><i class="fa fa-globe {{{ (Request::is('platforms') ? 'active' : '') }}}"></i> <span class="nav-label">Platforms</span></a>
            </li>
            <li class="{{ Request::is('creator/multiUpload*') ? 'active' : '' }}">
                <a href="{{route('creator.multiUpload.getUpload')}}"><i class="fa fa-upload {{{ (Request::is('multiUpload') ? 'active' : '') }}}"></i> <span class="nav-label">Multi Upload</span></a>
            </li>
            <li class="{{ Request::is('creator/analytic*') ? 'active' : '' }}">
                <a href="{{route('creator.analytic.index')}}"><i class="fa fa-bar-chart {{{ (Request::is('analytic') ? 'active' : '') }}}"></i> <span class="nav-label">Analytic</span></a>
            </li>
            {{-- @endif --}}
            <li class="{{ Request::is('creator/revenue*') ? 'active' : '' }}">
                <a href="{{route('creator.revenue.index')}}"><i class="fa fa-money {{{ (Request::is('revenue') ? 'active' : '') }}}"></i> <span class="nav-label">Revenue</span></a>
            </li>
            {{-- <li class="{{ Request::is('creator/masterlagu*') ? 'active' : '' }}">
                <a href="{{route('creator.masterlagu.index')}}"><i class="fa fa-music {{{ (Request::is('masterlagu') ? 'active' : '') }}}"></i> <span class="nav-label">Master Lagu</span></a>
            </li> --}}
            <li class="{{ Request::is('creator/personal') ? 'active' : '' }}{{ Request::is('creator/setting*') ? 'active' : '' }}">
                <a href="#"><i class="fa fa-cog"></i> <span class="nav-label">Setting</span>
                <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li class="{{ Request::is('creator/personal') ? 'active' : '' }}"><a href="{{route('creator.personal.index')}}"><i class=""></i> Personal</a></li>
                    
                    <li class="{{ Request::is('creator/setting/*/payment*') ? 'active' : '' }}"><a href="{{route('self.creator.edit', [auth()->id(), 'payment'])}}"><i class=""></i> Payment</a></li>
                    <li class="{{ Request::is('creator/setting/*/rate_card*') ? 'active' : '' }}"><a href="{{route('self.creator.edit', [auth()->id(), 'rate_card'])}}"><i class=""></i> Rate Card</a></li>
                    <li class="{{ Request::is('creator/setting/*/social_media*') ? 'active' : '' }}"><a href="{{route('self.creator.edit', [auth()->id(), 'social_media'])}}"><i class=""></i> Social Media</a></li>
                </ul>
            </li>
            @elseif(auth()->user()->roles->first()->name === 'brand')
            <li class="{{ Request::is('brand/influencer*') ? 'active' : '' }}">
                <a href="{{route('brand.influencer.browse')}}"><i class="fa fa-users {{{ (Request::is('influencer') ? 'active' : '') }}}"></i> <span class="nav-label">Browse Influencer</span></a>
            </li>
            <li class="{{ Request::is('brand/campaign*') ? 'active' : '' }}">
                <a href="{{route('brand.campaign.index')}}"><i class="fa fa-handshake-o {{{ (Request::is('campaign') ? 'active' : '') }}}"></i> <span class="nav-label">Campaign</span></a>
            </li>
            <li class="{{ Request::is('brand/personal*') ? 'active' : '' }}">
                <a href="{{route('brand.personal.index')}}"><i class="fa fa-user {{{ (Request::is('personal') ? 'active' : '') }}}"></i> <span class="nav-label">Personal Setting</span></a>
            </li>
            @elseif(auth()->user()->hasRole('influencer') && request()->segment(1) == 'influencer')
            <li class="{{ Request::is('influencer/campaign*') ? 'active' : '' }}">
                <a href="{{route('influencer.campaign.index')}}"><i class="fa fa-handshake-o {{{ (Request::is('campaign') ? 'active' : '') }}}"></i> <span class="nav-label">Campaign</span></a>
            </li>
            <li class="{{ Request::is('influencer/setting*') ? 'active' : '' }}">
                <a href="#"><i class="fa fa-cog"></i> <span class="nav-label">Setting</span>
                <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li class="{{ Request::is('influencer/setting/*/personal*') ? 'active' : '' }}"><a href="{{route('self.influencer.edit', [auth()->id(), 'personal'])}}"><i class=""></i> Personal</a></li>
                    <li class="{{ Request::is('influencer/setting/*/payment*') ? 'active' : '' }}"><a href="{{route('self.influencer.edit', [auth()->id(), 'payment'])}}"><i class=""></i> Payment</a></li>
                    <li class="{{ Request::is('influencer/setting/*/rate_card*') ? 'active' : '' }}"><a href="{{route('self.influencer.edit', [auth()->id(), 'rate_card'])}}"><i class=""></i> Rate Card</a></li>
                    <li class="{{ Request::is('influencer/setting/*/social_media*') ? 'active' : '' }}"><a href="{{route('self.influencer.edit', [auth()->id(), 'social_media'])}}"><i class=""></i> Social Media</a></li>
                </ul>
            </li>
            <!-- ++++++++++++++  END ROLE ADMINISTRATOR  +++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

            <!-- ++++++++++++++  ROLE MEMBER  +++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

            @elseif(auth()->user()->hasRole('member'))
            <li class="{{ Request::is('member*') ? 'active' : '' }}">
                <a href="#"><i class="fa fa-cog"></i> <span class="nav-label">Setting</span>
                <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li class="{{ Request::is('member/*/edit*') ? 'active' : '' }}"><a href="{{route('self.member.edit', [auth()->id()])}}"><i class=""></i> Personal</a></li>
                    <li class="{{ Request::is('member/*/upgrade*') ? 'active' : '' }}"><a href="{{route('self.member.upgrade', [auth()->id()])}}"><i class=""></i> Upgrade Account</a></li>
                </ul>
            </li>
            <!-- ++++++++++++++  END ROLE MEMBER  +++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

            <!-- ++++++++++++++  ROLE FINANCE  +++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
            @elseif(auth()->user()->hasRole('finance'))
            <li class="{{ Request::is('finance/dashboard*') ? 'active' : '' }}">
                <a href="{{route('finance.dashboard.index')}}"><i class="fa fa-dashboard {{{ (Request::is('dashboard') ? 'active' : '') }}}"></i> <span class="nav-label">Dashboard</span></a>
            </li>
            <!-- <li class="{{ Request::is('finance/creator*') ? 'active' : '' }}">
                <a href="{{route('finance.creator.index')}}"><i class="fa fa-group {{{ (Request::is('creator') ? 'active' : '') }}}"></i> <span class="nav-label">Creator</span></a>
            </li> -->
            <li class="{{ Request::is('finance/revenue*') ? 'active' : '' }}{{ Request::is('finance/dsp*') ? 'active' : '' }}">
                <a href="#"><i class="fa fa-file-text-o"></i> <span class="nav-label">Reports</span>
                <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li class="{{ Request::is('finance/revenue') ? 'active' : '' }}{{ Request::is('finance/revenue/approval*') ? 'active' : '' }}{{ Request::is('finance/revenue/approver*') ? 'active' : '' }}">
                        <a href="#"><i class="fa fa-youtube"></i> <span class="nav-label">Youtube</span>
                        <span class="fa arrow"></span></a>
                        <ul class="nav nav-third-level">
                            <li class="{{ Request::is('finance/revenue') ? 'active' : '' }}"><a href="{{route('finance.revenue.index')}}"><i class="fa fa-file-text"></i> Revenue</a></li>
                            
                            <li class="{{ Request::is('finance/revenue/approval*') ? 'active' : '' }}"><a href="{{route('finance.revenue.approval.index')}}"><i class="fa fa-check-square-o"></i> Pending Approval</a></li>
                            {{--  <li class="{{ Request::is('finance/revenue/approver*') ? 'active' : '' }}"><a href="{{route('finance.revenue.approver.index')}}"><i class="fa fa-user-plus"></i> Approver</a></li>  --}}
                        </ul>
                    </li>
                    <li class="{{ Request::is('finance/dsp*') ? 'active' : '' }}"><a href="{{route('finance.dsp.index')}}"><i class="fa fa-file-audio-o"></i> DSP Reports</a></li>
                </ul>
            </li>
            <li class="{{ Request::is('finance/personal*') ? 'active' : '' }}">
                <a href="{{route('finance.personal.index')}}"><i class="fa fa-user {{{ (Request::is('personal') ? 'active' : '') }}}"></i> <span class="nav-label">Personal Setting</span></a>
            </li>
            {{--  <li class="{{ Request::is('finance/revenue') ? 'active' : '' }}"><a href="{{route('finance.revenue.create_finance')}}"><i class="fa fa-file-text"></i>Finance Revenue</a></li>  --}}

            <!-- ++++++++++++++  END ROLE FINANCE  +++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

            <!-- ++++++++++++++  ROLE LEGAL  +++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
            @elseif(auth()->user()->hasRole('legal'))
            <li class="{{ Request::is('legal/dashboard*') ? 'active' : '' }}">
                <a href="{{route('legal.dashboard.index')}}"><i class="fa fa-dashboard {{{ (Request::is('dashboard') ? 'active' : '') }}}"></i> <span class="nav-label">Dashboard</span></a>
            </li>
            <li class="{{ (Request::is('legal/masterlagu*') ? 'active' : '') }}{{ (Request::is('legal/mastersinger*') ? 'active' : '') }}{{ (Request::is('legal/mastersongwriter*') ? 'active' : '') }}">
                <a href="#"><i class="fa fa-music"></i> <span class="nav-label">Master Data</span>
                <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li class="{{ Request::is('legal/masterlagu*') ? 'active' : '' }}">
                        <a href="{{route('legal.masterlagu.index')}}"><i class="fa fa-music {{ (Request::is('legal/masterlagu') ? 'active' : '') }}"></i> <span class="nav-label">Master Lagu</span></a>
                    </li>
                    <li class="{{ Request::is('legal/mastersinger*') ? 'active' : '' }}">
                        <a href="{{route('legal.mastersinger.index')}}"><i class="fa fa-star {{ (Request::is('legal/mastersinger') ? 'active' : '') }}"></i> <span class="nav-label">Singer</span></a>
                    </li>
                    <li class="{{ Request::is('legal/mastersongwriter*') ? 'active' : '' }}">
                        <a href="{{route('legal.mastersongwriter.index')}}"><i class="fa fa-user {{ (Request::is('legal/mastersongwriter') ? 'active' : '') }}"></i> <span class="nav-label">Songwriter/Publisher</span></a>
                    </li>
                </ul>
            </li>
            <li class="{{ Request::is('legal/personal*') ? 'active' : '' }}">
                <a href="{{route('legal.personal.index')}}"><i class="fa fa-user {{{ (Request::is('personal') ? 'active' : '') }}}"></i> <span class="nav-label">Personal Setting</span></a>
            </li>

            <!-- ++++++++++++++  END ROLE LEGAL  +++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

            <!-- ++++++++++++++  ROLE SINGER  +++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
            @elseif(auth()->user()->hasRole('singer'))
            <li class="{{ Request::is('singer/dashboard*') ? 'active' : '' }}">
                <a href="{{route('singer.dashboard.index')}}"><i class="fa fa-dashboard {{{ (Request::is('dashboard') ? 'active' : '') }}}"></i> <span class="nav-label">Dashboard</span></a>
            </li>
            <li class="{{ Request::is('singer/masterlagu*') ? 'active' : '' }}">
                <a href="{{route('singer.masterlagu.index')}}"><i class="fa fa-music {{{ (Request::is('masterlagu') ? 'active' : '') }}}"></i> <span class="nav-label">Master Lagu</span></a>
            </li>
            <li class="{{ Request::is('singer/dsp*') ? 'active' : '' }}">
                <a href="{{route('singer.dsp.index')}}"><i class="fa fa-music {{{ (Request::is('dsp') ? 'active' : '') }}}"></i> <span class="nav-label">DSP Reports</span></a>
            </li>
            <li class="{{ Request::is('singer/personal*') ? 'active' : '' }}">
                <a href="{{route('singer.personal.index')}}"><i class="fa fa-user {{{ (Request::is('personal') ? 'active' : '') }}}"></i> <span class="nav-label">Personal Setting</span></a>
            </li>
            <!-- ++++++++++++++  END ROLE SINGER  +++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

            <!-- ++++++++++++++  ROLE SONGWRITER  +++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
            @elseif(auth()->user()->hasRole('songwriter'))
            <li class="{{ Request::is('songwriter/dashboard*') ? 'active' : '' }}">
                <a href="{{route('songwriter.dashboard.index')}}"><i class="fa fa-dashboard {{{ (Request::is('dashboard') ? 'active' : '') }}}"></i> <span class="nav-label">Dashboard</span></a>
            </li>
            <li class="{{ Request::is('songwriter/masterlagu*') ? 'active' : '' }}">
                <a href="{{route('songwriter.masterlagu.index')}}"><i class="fa fa-music {{{ (Request::is('masterlagu') ? 'active' : '') }}}"></i> <span class="nav-label">Master Lagu</span></a>
            </li>
            <li class="{{ Request::is('songwriter/dsp*') ? 'active' : '' }}">
                <a href="{{route('songwriter.dsp.index')}}"><i class="fa fa-music {{{ (Request::is('dsp') ? 'active' : '') }}}"></i> <span class="nav-label">DSP Reports</span></a>
            </li>
            <li class="{{ Request::is('songwriter/personal*') ? 'active' : '' }}">
                <a href="{{route('songwriter.personal.index')}}"><i class="fa fa-user {{{ (Request::is('personal') ? 'active' : '') }}}"></i> <span class="nav-label">Personal Setting</span></a>
            </li>

            <!-- ++++++++++++++  END ROLE SONGWRITER  +++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
            
            <!-- ++++++++++++++  ROLE ANR  +++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
            @elseif(auth()->user()->hasRole('anr'))
            <li class="{{ Request::is('anr/dashboard*') ? 'active' : '' }}">
                <a href="{{route('anr.dashboard.index')}}"><i class="fa fa-dashboard {{{ (Request::is('dashboard') ? 'active' : '') }}}"></i> <span class="nav-label">Dashboard</span></a>
            </li>
            <li class="{{ (Request::is('anr/masterlagu*') ? 'active' : '') }}{{ (Request::is('anr/mastersinger*') ? 'active' : '') }}{{ (Request::is('anr/mastersongwriter*') ? 'active' : '') }}">
                <a href="#"><i class="fa fa-music"></i> <span class="nav-label">Master Data</span>
                <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li class="{{ Request::is('anr/masterlagu*') ? 'active' : '' }}">
                        <a href="{{route('anr.masterlagu.index')}}"><i class="fa fa-music {{ (Request::is('anr/masterlagu') ? 'active' : '') }}"></i> <span class="nav-label">Master Lagu</span></a>
                    </li>
                    <li class="{{ Request::is('anr/mastersinger*') ? 'active' : '' }}">
                        <a href="{{route('anr.mastersinger.index')}}"><i class="fa fa-star {{ (Request::is('anr/mastersinger') ? 'active' : '') }}"></i> <span class="nav-label">Singer</span></a>
                    </li>
                    <li class="{{ Request::is('anr/mastersongwriter*') ? 'active' : '' }}">
                        <a href="{{route('anr.mastersongwriter.index')}}"><i class="fa fa-user {{ (Request::is('anr/mastersongwriter') ? 'active' : '') }}"></i> <span class="nav-label">Songwriter/Publisher</span></a>
                    </li>
                </ul>
            </li>
            {{-- <li class="{{ Request::is('anr/dsp*') ? 'active' : '' }}">
                <a href="{{route('anr.dsp.index')}}"><i class="fa fa-music {{{ (Request::is('dsp') ? 'active' : '') }}}"></i> <span class="nav-label">DSP Reports</span></a>
            </li> --}}
            <li class="{{ Request::is('anr/personal*') ? 'active' : '' }}">
                <a href="{{route('anr.personal.index')}}"><i class="fa fa-user {{{ (Request::is('personal') ? 'active' : '') }}}"></i> <span class="nav-label">Personal Setting</span></a>
            </li>

            <!-- ++++++++++++++  END ROLE ANR  +++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

            <!-- ++++++++++++++  ROLE CMS  +++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
            @elseif(auth()->user()->hasRole('cms'))
            <li class="{{ Request::is('cms/dashboard*') ? 'active' : '' }}">
                <a href="{{route('cms.dashboard.index')}}"><i class="fa fa-dashboard {{{ (Request::is('dashboard') ? 'active' : '') }}}"></i> <span class="nav-label">Dashboard</span></a>
            </li>
            {{-- <li class="{{ (Request::is('anr/masterlagu*') ? 'active' : '') }}{{ (Request::is('anr/mastersinger*') ? 'active' : '') }}{{ (Request::is('anr/mastersongwriter*') ? 'active' : '') }}">
                <a href="#"><i class="fa fa-music"></i> <span class="nav-label">Master Data</span>
                <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li class="{{ Request::is('anr/masterlagu*') ? 'active' : '' }}">
                        <a href="{{route('anr.masterlagu.index')}}"><i class="fa fa-music {{ (Request::is('anr/masterlagu') ? 'active' : '') }}"></i> <span class="nav-label">Master Lagu</span></a>
                    </li>
                    <li class="{{ Request::is('anr/mastersinger*') ? 'active' : '' }}">
                        <a href="{{route('anr.mastersinger.index')}}"><i class="fa fa-star {{ (Request::is('anr/mastersinger') ? 'active' : '') }}"></i> <span class="nav-label">Singer</span></a>
                    </li>
                    <li class="{{ Request::is('anr/mastersongwriter*') ? 'active' : '' }}">
                        <a href="{{route('anr.mastersongwriter.index')}}"><i class="fa fa-user {{ (Request::is('anr/mastersongwriter') ? 'active' : '') }}"></i> <span class="nav-label">Songwriter/Publisher</span></a>
                    </li>
                </ul>
            </li> --}}
            {{-- <li class="{{ Request::is('anr/dsp*') ? 'active' : '' }}">
                <a href="{{route('anr.dsp.index')}}"><i class="fa fa-music {{{ (Request::is('dsp') ? 'active' : '') }}}"></i> <span class="nav-label">DSP Reports</span></a>
            </li> --}}
            {{-- <li class="{{ Request::is('anr/personal*') ? 'active' : '' }}">
                <a href="{{route('anr.personal.index')}}"><i class="fa fa-user {{{ (Request::is('personal') ? 'active' : '') }}}"></i> <span class="nav-label">Personal Setting</span></a>
            </li> --}}

            <!-- ++++++++++++++  END ROLE CMS  +++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
            @endif
        </ul>
    </div>
</nav>