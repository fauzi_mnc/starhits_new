<script>
    $(".dataTables_filter input")
    .unbind() // Unbind previous default bindings
    .bind("input", function(e) { // Bind our desired behavior


        var value = $(this).val();

        if (this.value.indexOf('%') != -1) {
            $(this).val(value.replace(/\%/g, ""));
        }

        if (this.value.indexOf('*') != -1) {
            $(this).val(value.replace(/\*/g, ""));
        }
        // If the length is 3 or more characters, or the user pressed ENTER, search
        if(this.value.length >= 3 || e.keyCode == 13) {
            // Call the API search function
            LaravelDataTables["dataTableBuilder"].search(this.value).draw();
        }
        // Ensure we clear the search if they backspace far enough
        if(this.value.length == 0) {
            LaravelDataTables["dataTableBuilder"].search("").draw();
        }
        
        return;
    });
</script>