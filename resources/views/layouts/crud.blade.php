@extends('layouts.app')

@section('content')
    @yield('breadcrumb')
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                @yield('contentCrud')
            </div>
        </div>
    </div>
@endsection