@extends('layouts.crud')

@section('css')
<link href="{{ asset('css/plugins/switchery/switchery.css') }}" rel="stylesheet">
<link href="{{ asset('css/plugins/bootstrap-select.min.css') }}" rel="stylesheet">
@endsection

@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Member</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Member</a>
            </li>
            <li class="active">
                <strong>Setting</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
@endsection

@section('contentCrud')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@include('flash::message')
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                Setting
            </div>
            <div class="panel-body">
                {!! Form::model($user, ['route' => ['self.member.update', $user->id], 'method' => 'put', 'class' => 'form', 'files' => true, 'id' => 'example-form']) !!}
                <div>
                    
                    <h3>Personal Information</h3>
                    <section class="">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    {!! Form::label('name', 'Full Name:', []) !!}
                                    {!! Form::text('name', null, ['class' => 'form-control required']) !!}
                                    
                                </div>
                                <div class="form-group">
                                    {!! Form::label('phone', 'Phone Number:', []) !!}
                                    {!! Form::text('phone', null, ['class' => 'form-control required']) !!}
                                    
                                </div>
                                <div class="form-group">
                                    {!! Form::label('country', 'Country:', []) !!}
                                    {!! Form::select('country', $countries, null, ['class' => 'form-control', 'placholder' => 'Select City']) !!}
                                    
                                </div>
                                <div class="form-group">
                                    {!! Form::label('password', 'Password:', []) !!}
                                    
                                    <div class="input-group">
                                        {!! Form::password('password', ['class' => 'form-control required']) !!}
                                        <span class="input-group-btn"> 
                                            <button type="button" class="btn btn-warning pull-right" id="reset-pwd">Reset</button>
                                        </span>
                                    </div>    
                                    
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="form-group">
                                    {!! Form::label('email', 'Email:', []) !!}
                                    {!! Form::email('email', null, ['class' => 'form-control required']) !!}
                                    
                                </div>
                                <div class="form-group">
                                    {!! Form::label('gender', 'Gender:', []) !!}
                                    <br>
                                    <label class="radio-inline">
                                        <input type="radio" name="gender" value="F" {{ ($user->gender == 'F') ? 'checked' : '' }}>Female
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="gender" value="M" {{ ($user->gender == 'M') ? 'checked' : '' }}>Male
                                    </label>
                                    
                                </div>
                                <div class="form-group">
                                    {!! Form::label('city', 'Town/City:', []) !!}
                                    {!! Form::select('city', $cities, null, ['class' => 'form-control', 'placholder' => 'Select City']) !!}
                                    
                                </div>
                                <div class="form-group">
                                    {!! Form::label('is_active', 'Is Active:', []) !!}
                                    <br>
                                    {!! Form::checkbox('is_active', null, ($user->is_active == 1) ? 'on' : '',['class' => 'form-control js-switch required', 'data-switchery' => true]) !!}
                                
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <button type="submit" class="btn btn-primary">Save Change</button>
                <a href="{{route('self.member.edit', [auth()->id()])}}" class="btn btn-default">Cancel</a>
                </form>
            </div>

        </div>
        
    </div>
</div>
@endsection


@section('scripts')
    <script src="{{ asset('js/plugins/switchery/switchery.js') }}"></script>
    <script src="{{ asset('js/plugins/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('js/plugins/typeahed.min.js') }}"></script>
    
    <script>
        $(document).ready(function(){
            var elem = document.querySelector('.js-switch');
            var switchery = new Switchery(elem, { disabled: true });
        });
    
    
    $('#password').attr('disabled', true);
    $('#reset-pwd').click(function(){
        $('#password').val('');
        $('#password').attr('disabled', false);
    });
    
    </script>
@endsection