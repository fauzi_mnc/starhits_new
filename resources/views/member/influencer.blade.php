@extends('layouts.crud')

@section('css')
<link href="{{ asset('css/plugins/switchery/switchery.css') }}" rel="stylesheet">
<link href="{{ asset('css/plugins/bootstrap-select.min.css') }}" rel="stylesheet">
<style>
.wizard > .content > .body{
    position: relative !important;
}
strong{
    padding-right: 30px;
}
</style>
@endsection

@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Member</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Member</a>
            </li>
            <li class="active">
                <strong>Ugrade</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
@endsection

@section('contentCrud')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@include('flash::message')
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                Setting
            </div>
            <div class="panel-body">
                {!! Form::model($user, ['route' => ['self.member.role.store', $user->id, $role], 'method' => 'post', 'class' => 'form', 'files' => true, 'id' => 'example-form']) !!}
                <div>
                    <div class="form-group">
                        {!! Form::label('username', 'Username Instagram:', []) !!}
                        {!! Form::text('username', null, ['class' => 'form-control required']) !!}
                        <br>
                        <button type="button" class="btn btn-primary pull-right" id="btn-check">Connect</button>                                            
                    </div>
                    <div class="form-group" id="instagram-profile">
                    </div>
                    <div class="form-group">
                        {!! Form::label('category', 'Influencer Category:', []) !!}
                        {!! Form::select('category[]', $categories,  null, ['class' => 'form-control selectpicker required', 'multiple']) !!}
                        
                    </div>
                    
                </div>
                <button type="submit" class="btn btn-primary" id="btn-submit">Save Change</button>
                <a href="{{route('self.member.upgrade', [auth()->id()])}}" class="btn btn-default">Cancel</a>
                </form>
            </div>

        </div>
        
    </div>
</div>
@endsection


@section('scripts')
    <script src="{{ asset('js/plugins/switchery/switchery.js') }}"></script>
    <script src="{{ asset('js/plugins/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('js/plugins/typeahed.min.js') }}"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.js"></script>
    <script>
        $('#btn-submit').attr('disabled', true);
        $('#btn-check').click(function(){
            $.get( "{{route('influencer.instagram.check')}}", { username:  $('#username').val()}, function(data) {
                if(data.result == false){
                    $('#instagram-profile').prepend(
                        '<div class="alert alert-danger">Invalid Instagram Username</div>'
                    );

                    setTimeout(() => {
                        $('.alert-danger').remove();
                    }, 5000);
                    
                } else {
                    $('#username').val(data.result.userName);
                    $('#followers_ig').val(data.result.followers);
                    $('#profile_picture_ig').val(data.result.profilePicture);
                    $('#instagram-profile').empty();
                    $('#instagram-profile').append(
                        '<div class="feed-element">'+
                            '<a href="#" class="pull-left">'+
                                '<img alt="image" class="img-circle" src="'+data.result.profilePicture+'" style="height:100px;width:100px;">'+
                            '</a>'+
                            '<div class="media-body">'+
                                '<h3>'+data.result.userName+'</h3> <br>'+
                                '<strong>'+data.result.mediaCount+' post</strong><strong>'+data.result.followers+' followers</strong><strong>'+ data.result.following +' following</small></strong>'+
                            '</div>'+
                        '</div>'
                    );
                    $('#btn-submit').attr('disabled', false);
                }
            });

        });

        $('#btn-submit').click(function(){
            var valid = $('#example-form').valid();
            if(valid){
                $('#example-form').submit();
            }
        });
    </script>
@endsection