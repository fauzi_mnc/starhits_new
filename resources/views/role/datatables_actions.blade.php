<div class='btn-group'>
    <a href="{{ route('role.edit', $id) }}" class='btn btn-default btn-xs' title="Edit Role">
        <i class="glyphicon glyphicon-edit"></i>
    </a>
    {{--  {!! Form::button('<i class="glyphicon glyphicon-remove"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-danger btn-xs',
        'title' => 'Delete Post',
        'onclick' => "return confirm('Do you want to delete this role?')",
        'name' => 'action',
        'value' => 'del'
    ]) !!}  --}}
</div>