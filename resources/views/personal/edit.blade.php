@extends('layouts.crud')

@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Setting</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{url('')}}/{{auth()->user()->roles->first()->name}}/personal/">Personal</a>
            </li>
            <li class="active">
                <strong>Edit</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
@endsection

@section('contentCrud')

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Personal</h5>
                </div>
                <div class="ibox-content">
                 
                    <form method="POST" action="{{url('')}}/{{auth()->user()->roles->first()->name}}/personal/{{$user->id}}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                        <input name="_method" type="hidden" value="PUT">
                        {{ csrf_field() }}

                       @include('personal.fields', ['formType' => 'edit'])
                   </form>


                 

                   {{--

                   
                @if(auth()->user()->roles->first()->name === 'creator')
                    {!! Form::model($user, ['route' => ['creator.personal.update', $user->id], 'method' => 'put', 'class' => 'form-horizontal', 'files' => true]) !!}
                            
                    {!! Form::close() !!}
                @elseif(auth()->user()->roles->first()->name === 'brand')
                    {!! Form::model($user, ['route' => ['brand.personal.update', $user->id], 'method' => 'put', 'class' => 'form-horizontal', 'files' => true]) !!}
                            @include('personal.fields', ['formType' => 'edit'])
                    {!! Form::close() !!}
                @elseif(auth()->user()->roles->first()->name === 'user')
                    {!! Form::model($user, ['route' => ['user.personal.update', $user->id], 'method' => 'put', 'class' => 'form-horizontal', 'files' => true]) !!}
                            @include('personal.fields', ['formType' => 'edit'])
                    {!! Form::close() !!}
                @elseif(auth()->user()->roles->first()->name === 'finance')
                    {!! Form::model($user, ['route' => ['finance.personal.update', $user->id], 'method' => 'put', 'class' => 'form-horizontal', 'files' => true]) !!}
                            @include('personal.fields', ['formType' => 'edit'])
                    {!! Form::close() !!}

                @elseif(auth()->user()->roles->first()->name === 'legal')
                    {!! Form::model($user, ['route' => ['legal.personal.update', $user->id], 'method' => 'put', 'class' => 'form-horizontal', 'files' => true]) !!}
                            @include('personal.fields', ['formType' => 'edit'])
                    {!! Form::close() !!}
                @else
                    {!! Form::model($user, ['route' => ['admin.personal.update', $user->id], 'method' => 'put', 'class' => 'form-horizontal', 'files' => true]) !!}
                            @include('personal.fields', ['formType' => 'edit'])
                    {!! Form::close() !!}
                @endif
                --}}
                </div>
            </div>

@endsection

