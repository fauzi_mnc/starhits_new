<div class='btn-group'>
    <a href="{{ route('config.show', $id) }}" class='btn btn-default btn-xs'>
        <i class="glyphicon glyphicon-eye-open"></i>
    </a>
    <a href="{{ route('config.edit', $id) }}" class='btn btn-primary btn-xs'>
        <i class="glyphicon glyphicon-edit"></i>
    </a>
</div>