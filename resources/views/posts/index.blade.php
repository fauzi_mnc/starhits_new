@extends('layouts.crud')

@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Posts</h2>
        <ol class="breadcrumb">
            <li>
                Posts
            </li>
            @if(auth()->user()->roles->first()->name === 'admin')
            <li class="active">
                <a href="{{route('admin.posts.index')}}"><strong>Table</strong></a>
            </li>
            @elseif(auth()->user()->roles->first()->name === 'user')
            <li class="active">
                <a href="{{route('user.posts.index')}}"><strong>Table</strong></a>
            </li>
            @else
            <li class="active">
                <a href="{{route('creator.posts.index')}}"><strong>Table</strong></a>
            </li>
            @endif
        </ol>
    </div>
</div>
@endsection
@section('contentCrud')
@include('flash::message')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Posts</h5>
                    {{-- <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#modalAddPosts" style="margin-top: -10px;margin-bottom: 5px">
                        Add Post
                    </button> --}}
                    <div class="modal inmodal fade" id="modalAddPosts" tabindex="-1" role="dialog"  aria-hidden="true">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title">Choose Add Post</h4>
                                </div>
                                <div class="modal-body">
                                    @if(auth()->user()->roles->first()->name === 'admin')
                                    <a class="btn btn-primary" href="{{route('admin.posts.createManual')}}">Create Post Manual</a>
                                    <a class="btn btn-primary" href="{{route('admin.posts.createAutomatic')}}">Create Post Automatic</a>
                                    @elseif(auth()->user()->roles->first()->name === 'user')
                                    <a class="btn btn-primary" href="{{route('user.posts.createManual')}}">Create Post Manual</a>
                                    <a class="btn btn-primary" href="{{route('user.posts.createAutomatic')}}">Create Post Automatic</a>
                                    @else
                                    <a class="btn btn-primary" href="{{route('creator.posts.createManual')}}">Create Post Manual</a>
                                    <a class="btn btn-primary" href="{{route('creator.posts.createAutomatic')}}">Create Post Automatic</a>
                                    @endif
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="ibox-content">
                    @include('posts.table')
                </div>
            </div>
        </div>
    </div>
@endsection
