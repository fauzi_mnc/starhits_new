
@section('css')
<link href="{{ asset('css/fileinput.min.css') }}" rel="stylesheet">

<style>
.kv-photo .krajee-default.file-preview-frame,.kv-photo .krajee-default.file-preview-frame:hover {
    margin: 0;
    padding: 0;
    border: none;
    box-shadow: none;
    text-align: center;
}
.kv-photo .file-input {
    display: table-cell;
    max-width: 220px;
}
.kv-reqd {
    color: red;
    font-family: monospace;
    font-weight: normal;
}
</style>
@include('layouts.datatables_css')
@endsection

<div class="form-group">
    {!! Form::label('channel', 'Channel:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::select('parent_id', $channel, $series->parent_id, ['class' => 'form-control', 'disabled' => 'true']) !!}
    </div>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Title:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::text('title', $series->title, ['class' => 'form-control', 'disabled' => 'true']) !!}
    </div>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('type', 'Type:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::select('type', ['starhits' => 'Starhits', 'starpro' => 'Starpro'], $series->type, ['class' => 'form-control', 'disabled' => 'true']) !!}
    </div>
</div>

<!-- Sub Title Field -->
<div class="form-group">
    {!! Form::label('content', 'Description:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::textarea('content', $series->content, ['class' => 'form-control', 'disabled' => 'true']) !!}
    </div>
</div>

<!-- Picture Field -->
<div class="form-group">
    {!! Form::label('image', 'Picture:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        <div class="kv-photo center-block text-center">
            <input id="photo" name="image" type="file" class="file-loading">
        </div>

        <div id="kv-photo-errors-1" class="center-block alert alert-block alert-danger" style="display:none"></div>
    </div>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('is_active', 'Status:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::select('is_active', ['1' => 'Active', '0' => 'Inactive'], $series->is_active, ['class' => 'form-control', 'disabled' => true]) !!}
    </div>
</div>

<!-- Submit Field -->
<div class="form-group">
    <div class="col-sm-4 col-sm-offset-2">
    @if(auth()->user()->roles->first()->name === 'admin')
    <a href="{!! route('admin.posts.index') !!}" class="btn btn-default">Back</a>
    @elseif(auth()->user()->roles->first()->name === 'user')
    <a href="{!! route('user.posts.index') !!}" class="btn btn-default">Back</a>
    @else
    <a href="{!! route('creator.posts.index') !!}" class="btn btn-default">Back</a>
    @endif
    </div>
</div>

@section('scripts')
<script src="{{ asset('js/plugins/bootstrap-fileinput/piexif.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-fileinput/sortable.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-fileinput/purify.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-fileinput/fileinput.min.js') }}"></script>
<script src="{{ asset('js/plugins/tinymce/jquery.tinymce.min.js') }}"></script>
<script src="{{ asset('js/plugins/tinymce/tinymce.min.js') }}"></script>
<script>

    $('#photo').val('');

    $("#photo").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        browseLabel: '',
        removeLabel: '',
        browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
        removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-photo-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        defaultPreviewContent: '<img src="{{ ($series->image) ? $series->image : asset('img/default_avatar_male.jpg') }}" alt="Your icon" style="width:160px">',
        layoutTemplates: {main2: '{preview} {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png"]
    });
    
</script>
@endsection