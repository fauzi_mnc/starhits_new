@if(auth()->user()->roles->first()->name === 'admin')
	{!! Form::open(['route' => ['admin.posts.destroy', $id], 'method' => 'delete']) !!}
	@if($post_type == 'manual')
	<div class='btn-group'>
		{{-- <a href="{{ route('creator.serie.show', $id) }}" class='btn btn-info btn-xs' title="Show Series">
		Show
		</a> --}}
	    <a href="{{ route('admin.posts.editManual', $id) }}" class='btn btn-primary btn-xs' title="Edit Post">
	        <i class="glyphicon glyphicon-edit"></i>
		</a>
		@if($is_active == 1)
		{!! Form::button('<i class="glyphicon glyphicon-ban-circle"></i>', [
			'type' => 'submit',
			'class' => 'btn btn-warning btn-xs',
			'title' => 'Inactive Series',
			'onclick' => "return confirm('Do you want to inactive this series?')",
			'name' => 'action',
			'value' => 'inact'
		]) !!}
		@else
		{!! Form::button('<i class="glyphicon glyphicon-ok"></i>', [
			'type' => 'submit',
			'class' => 'btn btn-success btn-xs',
			'title' => 'Activated Series',
			'onclick' => "return confirm('Do you want to activated this series?')",
			'name' => 'action',
			'value' => 'act'
		]) !!}
		@endif
		{!! Form::button('<i class="glyphicon glyphicon-remove"></i>', [
			'type' => 'submit',
			'class' => 'btn btn-danger btn-xs',
			'title' => 'Delete Post',
			'onclick' => "return confirm('Do you want to delete this posts?')",
			'name' => 'action',
			'value' => 'del'
		]) !!}
	</div>
	@else
	<div class='btn-group'>
	    <a href="{{ route('admin.posts.editAutomatic', $id) }}" class='btn btn-primary btn-xs' title="Edit Post">
	        <i class="glyphicon glyphicon-edit"></i>
		</a>
		@if($is_active == 1)
		{!! Form::button('<i class="glyphicon glyphicon-ban-circle"></i>', [
			'type' => 'submit',
			'class' => 'btn btn-warning btn-xs',
			'title' => 'Inactive Series',
			'onclick' => "return confirm('Do you want to inactive this series?')",
			'name' => 'action',
			'value' => 'inact'
		]) !!}
		@else
		{!! Form::button('<i class="glyphicon glyphicon-ok"></i>', [
			'type' => 'submit',
			'class' => 'btn btn-success btn-xs',
			'title' => 'Activated Series',
			'onclick' => "return confirm('Do you want to activated this series?')",
			'name' => 'action',
			'value' => 'act'
		]) !!}
		@endif
		{!! Form::button('<i class="glyphicon glyphicon-remove"></i>', [
			'type' => 'submit',
			'class' => 'btn btn-danger btn-xs',
			'title' => 'Delete Post',
			'onclick' => "return confirm('Do you want to delete this posts?')",
			'name' => 'action',
			'value' => 'del'
		]) !!}
	</div>
	@endif
	{!! Form::close() !!}
@elseif(auth()->user()->roles->first()->name === 'user')
	{!! Form::open(['route' => ['user.posts.destroy', $id], 'method' => 'delete']) !!}
	@if($post_type == 'manual')
	<div class='btn-group'>
		<a href="{{ route('user.posts.editManual', $id) }}" class='btn btn-primary btn-xs' title="Edit Post">
			<i class="glyphicon glyphicon-edit"></i>
		</a>
		@if($is_active == 1)
		{!! Form::button('<i class="glyphicon glyphicon-ban-circle"></i>', [
			'type' => 'submit',
			'class' => 'btn btn-warning btn-xs',
			'title' => 'Inactive Series',
			'onclick' => "return confirm('Do you want to inactive this series?')",
			'name' => 'action',
			'value' => 'inact'
		]) !!}
		@else
		{!! Form::button('<i class="glyphicon glyphicon-ok"></i>', [
			'type' => 'submit',
			'class' => 'btn btn-success btn-xs',
			'title' => 'Activated Series',
			'onclick' => "return confirm('Do you want to activated this series?')",
			'name' => 'action',
			'value' => 'act'
		]) !!}
		@endif
		{!! Form::button('<i class="glyphicon glyphicon-remove"></i>', [
			'type' => 'submit',
			'class' => 'btn btn-danger btn-xs',
			'title' => 'Delete Post',
			'onclick' => "return confirm('Do you want to delete this posts?')",
			'name' => 'action',
			'value' => 'del'
		]) !!}
	</div>
	@else
	<div class='btn-group'>
	    <a href="{{ route('user.posts.editAutomatic', $id) }}" class='btn btn-primary btn-xs' title="Edit Post">
	        <i class="glyphicon glyphicon-edit"></i>
		</a>
		@if($is_active == 1)
		{!! Form::button('<i class="glyphicon glyphicon-ban-circle"></i>', [
			'type' => 'submit',
			'class' => 'btn btn-warning btn-xs',
			'title' => 'Inactive Series',
			'onclick' => "return confirm('Do you want to inactive this series?')",
			'name' => 'action',
			'value' => 'inact'
		]) !!}
		@else
		{!! Form::button('<i class="glyphicon glyphicon-ok"></i>', [
			'type' => 'submit',
			'class' => 'btn btn-success btn-xs',
			'title' => 'Activated Series',
			'onclick' => "return confirm('Do you want to activated this series?')",
			'name' => 'action',
			'value' => 'act'
		]) !!}
		@endif
		{!! Form::button('<i class="glyphicon glyphicon-remove"></i>', [
			'type' => 'submit',
			'class' => 'btn btn-danger btn-xs',
			'title' => 'Delete Post',
			'onclick' => "return confirm('Do you want to delete this posts?')",
			'name' => 'action',
			'value' => 'del'
		]) !!}
	</div>
	@endif
	{!! Form::close() !!}
@else
	{!! Form::open(['route' => ['creator.posts.destroy', $id], 'method' => 'delete']) !!}
	@if($post_type == 'manual')
	<div class='btn-group'>
	    <a href="{{ route('creator.posts.editManual', $id) }}" class='btn btn-primary btn-xs' title="Edit Post">
	        <i class="glyphicon glyphicon-edit"></i>
	    </a>
	</div>
	<div class='btn-group'>
	    @if($is_active == 1)
        {!! Form::button('<i class="glyphicon glyphicon-ban-circle"></i>', [
            'type' => 'submit',
            'class' => 'btn btn-warning btn-xs',
            'title' => 'Inactive Posts',
            'onclick' => "return confirm('Do you want to inactive this posts?')",
            'name' => 'action',
            'value' => 'inact'
        ]) !!}
        @endif
	</div>
	@else
	<div class='btn-group'>
	    <a href="{{ route('creator.posts.editAutomatic', $id) }}" class='btn btn-primary btn-xs' title="Edit Post">
	        <i class="glyphicon glyphicon-edit"></i>
	    </a>
	</div>
	<div class='btn-group'>
	    @if($is_active == 1)
        {!! Form::button('<i class="glyphicon glyphicon-ban-circle"></i>', [
            'type' => 'submit',
            'class' => 'btn btn-danger btn-xs',
            'title' => 'Inactive Posts',
            'onclick' => "return confirm('Do you want to inactive this posts?')",
            'name' => 'action',
            'value' => 'inact'
        ]) !!}
        @endif
	</div>
	@endif
	{!! Form::close() !!}
@endif