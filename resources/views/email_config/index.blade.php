@extends('layouts.crud')

@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Email Setting</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Email Config</a>
            </li>
            <li class="active">
                <strong>Table</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
@endsection
@section('contentCrud')
@include('flash::message')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Email Setting</h5>
                    <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{{route('email_config.create')}}">Add New</a>
                </div>

                <div class="ibox-content">
                    @include('email_config.table')
                </div>
            </div>
        </div>
    </div>
@endsection
