@extends('layouts.crud')

@section('css')
<link href="{{ asset('css/fileinput.min.css') }}" rel="stylesheet">
<style>
.kv-photo .krajee-default.file-preview-frame,.kv-photo .krajee-default.file-preview-frame:hover {
    margin: 0;
    padding: 0;
    border: none;
    box-shadow: none;
    text-align: center;
}
.kv-photo .file-input {
    display: table-cell;
    max-width: 220px;
}
.kv-reqd {
    color: red;
    font-family: monospace;
    font-weight: normal;
}
.btn-preview {
    margin-right: 15px;
}
</style>
@endsection

@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Email Setting</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{route('email_config.index')}}">Email Config</a>
            </li>
            <li class="active">
                <strong>Edit</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
@endsection
@section('contentCrud')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@include('flash::message')
<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>Email Config</h5>
    </div>
    <div class="ibox-content">
    {!! Form::open(['route' => 'email_config.store', 'class' => 'form-horizontal', 'files' => true]) !!}
    
    @section('css')
        <link href="{{ asset('css/fileinput.min.css') }}" rel="stylesheet">

        <style>
        .kv-photo .krajee-default.file-preview-frame,.kv-photo .krajee-default.file-preview-frame:hover {
            margin: 0;
            padding: 0;
            border: none;
            box-shadow: none;
            text-align: center;
        }
        .kv-photo .file-input {
            display: table-cell;
            max-width: 220px;
        }
        .kv-reqd {
            color: red;
            font-family: monospace;
            font-weight: normal;
        }
        </style>
        @include('layouts.datatables_css')
    @endsection
    @foreach($email_config as $con)
    <div class="form-group">
        {!! Form::label('value', $con->key, ['class' => 'col-sm-2 control-label']) !!}

        <div class="col-sm-10">
            {!! Form::textarea($con->key, $con->value, ['class' => 'form-control']) !!}
        </div>
    </div>
    @endforeach
    <!-- Submit Field -->
    <div class="form-group">
        <div class="col-sm-4 col-sm-offset-2">
        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
        <a href="{!! route('email_config.index') !!}" class="btn btn-default">Cancel</a>
        </div>
    </div>
                {!! Form::close() !!}
                </div>
            </div>
@endsection

@section('scripts')
<script src="{{ asset('js/plugins/bootstrap-fileinput/piexif.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-fileinput/sortable.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-fileinput/purify.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-fileinput/fileinput.min.js') }}"></script>
<script src="{{ asset('js/plugins/tinymce/jquery.tinymce.min.js') }}"></script>
<script src="{{ asset('js/plugins/tinymce/tinymce.min.js') }}"></script>
<script>
    $(document).ready(function(){
        initTinyMce();
    });
    // init tiny mce
    function initTinyMce(){
        tinymce.init({
            forced_root_block : "",
            selector: "textarea",
            setup: function (editor) {
                editor.on('change', function () {
                    tinymce.triggerSave();
                });
            },
            entity_encoding : "raw",
            file_picker_types: 'image',
            images_upload_credentials: true,
            automatic_uploads: false,
            theme: "modern",
            paste_data_images: true,
            plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime nonbreaking save table contextmenu directionality",
            "template paste textcolor colorpicker textpattern"
            ],
            toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | forecolor",
            image_advtab: true,
            file_picker_callback: function(cb, value, meta) {
                var input = document.createElement('input');
                input.setAttribute('type', 'file');
                input.setAttribute('accept', 'image/*');
                
                // Note: In modern browsers input[type="file"] is functional without 
                // even adding it to the DOM, but that might not be the case in some older
                // or quirky browsers like IE, so you might want to add it to the DOM
                // just in case, and visually hide it. And do not forget do remove it
                // once you do not need it anymore.
            
                input.onchange = function() {
                  var file = this.files[0];
                  
                  var reader = new FileReader();
                  reader.readAsDataURL(file);
                  reader.onload = function () {
                    // Note: Now we need to register the blob in TinyMCEs image blob
                    // registry. In the next release this part hopefully won't be
                    // necessary, as we are looking to handle it internally.
                    var id = 'blobid' + (new Date()).getTime();
                    var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
                    var base64 = reader.result.split(',')[1];
                    var blobInfo = blobCache.create(id, file, base64);
                    blobCache.add(blobInfo);
            
                    // call the callback and populate the Title field with the file name
                    cb(blobInfo.blobUri(), { title: file.name });
                  };
                };
                
                input.click();
            }
        });
    }
    // end tiny mce

    @foreach($email_config as $con)
    @if($con->key == 'website_logo_header')
        $('#photo').val('');

        $("#photo").fileinput({
            overwriteInitial: true,
            maxFileSize: 1500,
            showClose: false,
            showCaption: false,
            browseLabel: '',
            removeLabel: '',
            browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
            removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
            removeTitle: 'Cancel or reset changes',
            elErrorContainer: '#kv-photo-errors-1',
            msgErrorClass: 'alert alert-block alert-danger',
            defaultPreviewContent: '<img src="{{ ($con->value) ? $con->value : asset('img/default_avatar_male.jpg') }}" alt="Your icon" style="width:160px">',
            layoutTemplates: {main2: '{preview} {remove} {browse}'},
            allowedFileExtensions: ["jpg", "png"]
        });
    @endif
    @endforeach
</script>
@endsection