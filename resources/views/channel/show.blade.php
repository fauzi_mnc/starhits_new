@extends('layouts.crud')

@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Channel</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{route('channel.index')}}">Channel</a>
            </li>
            <li class="active">
                <strong>Detail</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
@endsection

@section('contentCrud')

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Channel</h5>
                </div>
                <div class="ibox-content">
                    <div class="form-horizontal">
                        @include('channel.show_fields')
                    </div>
                </div>
            </div>

@endsection

