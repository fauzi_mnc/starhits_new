@extends('layouts.frontend')

@section('css')
    <!-- Bootstrap CSS -->
    <style amp-custom>
        {{ include"frontend/assets/css/bootstrap.css" }}
        /*!
        * Navbar, Header and Footer styles
        */body{margin:0;font-family:Montserrat,sans-serif;font-size:1rem;font-weight:400;line-height:1.5;color:#212529;text-align:left;background-color:#fff;-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both;-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}a,a:hover{text-decoration:none}.nav-sidebar{padding:0}.nav-sidebar h6{margin-top:0;padding:15px 20px;background-color:#ccc;text-align:center;font-size:14px;margin-bottom:0}.nav-sidebar-close{position:absolute;right:15px;top:15px}.nav-sidebar ul{list-style:none;padding-left:0;margin:0}.nav-sidebar ul li{padding:15px;line-height:28px;border-bottom:1px solid #f0f0f0}.nav-sidebar ul li a{display:block}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}amp-sidebar{width:100%;height:100%}amp-sidebar a{text-decoration:none;cursor:pointer}amp-sidebar{background:#fff;color:#232323;fill:#232323;text-transform:uppercase;letter-spacing:.18rem;font-size:.875rem}amp-sidebar a{color:#232323;text-transform:none;letter-spacing:normal}amp-sidebar a:hover{text-decoration:underline;fill:#232323}.col-header{padding:0}.nav-menu-bar{display:inline-block;margin-right:15px;vertical-align:top;padding-top:3px}.nav-menu-logo{display:inline-block}.col-header-logo{padding-top:15px;padding-left:15px}.col-header-content{width:100%;z-index:2;top:0;padding:15px}.col-header-content .header-menu-1{font-size:10px;font-weight:700;color:rgba(0,0,0,.58);text-decoration:none}.col-header-content .header-menu-2{color:#000;font-weight:700}.col-header-content .header-menu{padding-top:15px;padding-bottom:15px}.col-header-content .header-menu-1:hover{text-decoration:none}.col-header-content .header-menu-1:first-child{margin-right:50px}.col-header-content .header-menu-1:hover,.col-header-content .header-menu-2:hover{color:#daa520;text-decoration:none}li.nav-item{padding-top:6px}li.group-menu{background-color:#000;border-top-left-radius:20px;border-bottom-left-radius:20px;border-top-right-radius:20px;border-bottom-right-radius:20px;padding:.5rem 1rem}li.group-menu a{color:#fff;font-weight:700}li.group-menu .delimiter{color:#fff;font-weight:700}li.group-menu a:hover{color:#f6c101;text-decoration:none}.col-footer{min-height:200px;background-color:#000;padding:20px}.col-footer-left p{font-size:12px;color:#fff;line-height:1.5;font-weight:300}.col-footer-center{padding-top:50px}.col-footer-right span{font-size:12px;font-weight:700;color:#fff}.col-footer-delimiter .delimeter{border-bottom:1px solid #fff}.col-footer-menu a:hover{color:#daa520}.col-footer-menu a{font-size:12px;font-weight:300;color:#fff;margin-right:30px}.col-footer-right-2{padding-right:60px}.col-footer-right-2 span{font-size:12px;font-weight:700;color:#fff;padding-top:15px;display:block}.icon-sosmed{font-size:18px;margin-right:15px;cursor:pointer}@media (max-width:991.98px){.col-header-content{background-color:#fcab2c;padding-top:0;padding-bottom:0;padding-left:15px;padding-right:15px}.col-header-content .header-menu{padding-bottom:0}.nav-sidebar .nav-item{text-align:left}.header-menu header div{display:inline-block;vertical-align:top}}@media (min-width:992px){.col-content{margin-top:0}.col-header-content{position:absolute}.col-header-logo{padding-top:35px;padding-left:60px}.col-footer{padding:40px}.col-footer-left{padding-top:24px;padding-left:60px}.col-footer-right{padding-top:24px;padding-right:60px}.col-footer-delimiter{padding-left:60px;padding-right:60px}.col-footer-menu{padding-left:60px;padding-top:10px}.col-footer-right-2 span{text-align:right}}[role=button]{cursor:pointer}

        h1,h2,h3,h4,h5,h6{font-weight:900}.font-weight-900{font-weight:900}.font-style-i{font-style:italic}.text-outline{color:transparent;text-shadow:-1px -1px 0 #000,1px -1px 0 #000,-1px 1px 0 #000,1px 1px 0 #000}.font-size-xl{font-size:24pt}@media(min-width:1200px){.font-size-xl{font-size:84pt}}@supports((text-stroke:1px black) or (-webkit-text-stroke:1px black)){.text-outline{color:transparent;-webkit-text-stroke:1px #000;text-stroke:1px #000;text-shadow:none}}:root{--color-primary:#005AF0;--space-1:.5rem;--space-4:2rem}.panel-title h5{background:rgba(255,165,0,.45);padding:1em;display:inline-block}[role=list]{-webkit-column-count:1;-moz-column-count:1;column-count:1;-webkit-column-gap:0;-moz-column-gap:0;column-gap:0;orphans:1;widows:1;}@media(min-width:992px){[role=list]{-webkit-column-count:2;-moz-column-count:2;column-count:2;-webkit-column-gap:0;-moz-column-gap:0;column-gap:0}}[role=list] img{object-fit:cover}.card.preview-popup{margin:0;border:none}.card.preview-popup .card-img{border-radius:0}.preview-popup-link::before{content:' ';border:1px solid;border-radius:50%;width:1rem;height:1rem;display:inline-block;margin-right:.5em;transform:translateY(20%)}.preview-popup .card-img-overlay{background:rgba(255,165,0,.45);-webkit-clip-path:polygon(0 0,100% 25%,100% 100%,0 100%);clip-path:polygon(0 0,100% 25%,100% 100%,0 100%);opacity:0;-webkit-transition:opacity .5s;-moz-transition:opacity .5s;-ms-transition:opacity .5s;-o-transition:opacity .5s;transition:opacity .5s;z-index:1050;cursor:pointer}@media(min-width:992px){.preview-popup .card-img-overlay{-webkit-clip-path:polygon(0 25%,100% 75%,100% 100%,0 100%);clip-path:polygon(0 25%,100% 75%,100% 100%,0 100%)}}.preview-popup:hover .card-img-overlay{opacity:1}.preview-popup-link{color:#000;font-weight:700}.load-less-link,.load-more-link{color:#000;font-weight:700;margin:auto;border:1px solid;border-radius:24px;text-decoration:none;padding:.5em 3em;margin-top:2rem}:-ms-input-placeholder{color:black;font-weight:700}::-ms-input-placeholder{color:black;font-weight:700}::placeholder{color:black;font-weight:700;opacity:1}input[type=search]:focus{background-color:transparent;outline:0;box-shadow:none}input[type=search]{background-image:url(pubs/icon/search-magnifier-interface-symbol.png);background-repeat:no-repeat;background-position:center right;background-size:contain;background-origin:content-box;background-color:transparent;border:0;border-bottom:1px solid;margin:0}[role=button]{cursor:pointer}.nav{display:flex;flex-direction:row-reverse;justify-content:center;align-items:flex-start;margin:3em 0;position:relative;-webkit-overflow-scrolling:touch;-ms-overflow-style:-ms-autohiding-scrollbar;background-color:orange;padding:0;height:50px}@media(max-width:1199px){.nav{margin:3em -30px}}.nav::-webkit-scrollbar{display:none}.nav__dropdown{position:absolute}.nav__dropdown__header,.nav__dropdown__header:focus{background-color:transparent;border:0;font-weight:700;width:220px;outline:0;padding:0 1em;line-height:50px;white-space:nowrap}.nav__dropdown__list{list-style:none;padding:1px 0 0;width:220px;z-index:1}.nav__dropdown__listitem{padding:1em;background-color:orange;width:220px}#main{background-color:orange;position:relative;list-style:none;font-weight:700;font-size:0;text-transform:uppercase;padding-left:120px;text-align:center;white-space:nowrap;overflow-x:auto;-webkit-overflow-scrolling:touch;-ms-overflow-style:-ms-autohiding-scrollbar}#main::-webkit-scrollbar{display:none}#main>li{background-color:orange;line-height:50px;font-size:1rem;display:inline-block;position:relative;cursor:pointer;min-width:220px;z-index:3}li{margin:0}.dropdown-symbol::before{content:"\25BC";padding:0 .5em}@media(max-width:1199px){.show-xl{display:none}}@media(min-width:1200px){#dropdown-container{position:relative;width:220px}}a:hover,a{color:#000}.col-header{background:white}@supports ((position:-webkit-sticky) or (position:sticky)){.sticky-top{position:-webkit-sticky;position:sticky;top:0;z-index:1020}}.list-overflow[overflow]{position:absolute;bottom:0;left:0;right:0}
    </style>
    <style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
@endsection

@section('content')
<div class="col-xs-12 col-sm-12 col-content">
    <div class="bg-circle" style="width:34vw;height:62vh;position: absolute;right:5%;top:10%">
        <amp-img src="frontend/assets/images/bg-circle.png" width="473" height="474" layout="responsive"
            style="transform: rotate(45deg)">
        </amp-img>
    </div>


    <div style="width: 50px;height:92px;position: absolute;left:5%;top:40%;z-index: -1;">
        <amp-img src="frontend/assets/images/bg-triangle.png" width="50" height="92" layout="responsive">
        </amp-img>
    </div>

    <div style="width: 34vw;height:62vh;position: absolute;left: 5%;top:75%">
        <amp-img src="frontend/assets/images/bg-circle.png" width="473" height="474" layout="responsive"
            style="transform: rotate(45deg)">
        </amp-img>
    </div>

    <div style="width: 34vw;height:62vh;position: absolute;right:1%;top:50%">
        <amp-img src="frontend/assets/images/bg-circle.png" width="473" height="474" layout="responsive"
            style="transform: rotate(45deg)">
        </amp-img>
    </div>

    <div style="margin: 0 -30px;position: relative">
        <amp-img src="frontend/assets/images/Layer-38.png" width="1280" height="607" layout="responsive">
        </amp-img>
        <div style="width: 50vw;position: absolute;bottom: 5%;right: 10%">
            <amp-img src="frontend/assets/images/Layer-39.png" width="670" height="447" layout="responsive"></amp-img>
        </div>
        <div style="width: 55vw;position: absolute;bottom: 5%;right: 12%;top: 0">
            <h1 class="font-size-xl font-weight-900 font-style-i text-outline">CREATOR</h1>
            <h1 class="font-size-xl font-weight-900 font-style-i text-right">PROFILE</h1>
        </div>

    </div>

    <div>
        <nav class="nav">
            <ul id="main">
                <li><a href="#">MOST SUBCRIBERS</a></li>
                <li><a href="#">MOST VIEWS</a></li>
                <li>
                    <form class="form-inline" method="POST" action-xhr="#" target="_top" style="margin: -0.45em 1em;">
                        <input class="form-control rounded-0" type="search" name="term" placeholder="SEARCH BY NAME"
                            required>
                    </form>
                </li>
            </ul>
            
            <div id="dropdown-container">
                <amp-accordion class="nav__dropdown">
                    <section>
                        <header class="nav__dropdown__header"><span class="show-xl">CHANNEL </span>CREATOR<span
                            class="dropdown-symbol"></span></header>
                            <ul class="nav__dropdown__list">
                                <li class="nav__dropdown__listitem">
                                    <a href="#">CORPORATE CREATOR</a>
                                </li>
                            </ul>
                        </section>
                    </amp-accordion>
            </div>
        </nav>

        <div style="position: relative" class="px-xl-5">
            <amp-list id="amp-list-tab1" class="list" reset-on-refresh layout="responsive" width="1" height="1.170"binding="no" src="http://starhits.com/creator.json">
                <template type="amp-mustache">
                    @foreach($creators as $value)
                    <div class="card preview-popup">
                        <amp-img class="card-img" src="{{ cover }}" layout="fixed-height" height="367">
                        </amp-img>
                        <div class="card-img-overlay d-flex flex-column justify-content-end px-xl-4">
                            <h5>{{name}}</h5>
                            <div class="d-flex">
                                <div class="d-flex align-items-center mr-3">
                                    <amp-img class="mr-2" src="frontend/assets/icon/001-youtube-play-button.png" width="18"
                                        height="18">
                                    </amp-img>
                                    <span>{{name}}</span>
                                </div>
                                <div class="d-flex align-items-center mr-3">
                                    <amp-img class="mr-2" src="frontend/assets/icon/004-instagram.png" width="18" height="18">
                                    </amp-img>
                                    <span>{{name}}</span>
                                </div>
                                <div class="d-flex align-items-center mr-3">
                                    <amp-img class="mr-2" src="frontend/assets/icon/002-twitter-logo.png" width="18" height="18">
                                    </amp-img>
                                    <span>{{name}}</span>
                                </div>
                                <div class="d-flex align-items-center mr-3">
                                    <amp-img class="mr-2" src="frontend/assets/icon/003-facebook-letter-logo.png" width="18"
                                        height="18">
                                    </amp-img>
                                    <span>{{name}}</span>
                                </div>
                            </div>
                            <div style="padding: 1em 0">{{name}}</div>
                            <a href="" class="preview-popup-link">see profile</a>
                        </div>
                    </div>
                    @endforeach
                </template>
            </amp-list>
        </div>

        <div class="text-center pt-5" style="margin-bottom: 5rem">
            <a href="creator-more.html" class="load-more-link">LOAD MORE</a>
        </div>
    </div>
</div>
@endsection