<!doctype html>
<html amp lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, minimum-scale=1,initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <link rel="preload" as="script" href="https://cdn.ampproject.org/v0.js">
    <link rel="preconnect dns-prefetch" href="https://fonts.gstatic.com/" crossorigin>
    <script async src="https://cdn.ampproject.org/v0.js"></script>
    <!-- Import other AMP Extensions here -->
    <script async custom-element="amp-carousel" src="https://cdn.ampproject.org/v0/amp-carousel-0.1.js"></script>
    <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>
    <script custom-element="amp-accordion" src="https://cdn.ampproject.org/v0/amp-accordion-0.1.js" async=""></script>
    <script async custom-element="amp-selector" src="https://cdn.ampproject.org/v0/amp-selector-0.1.js"></script>
    <script async custom-element="amp-fit-text" src="https://cdn.ampproject.org/v0/amp-fit-text-0.1.js"></script>

    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,900,900i&display=swap" rel="stylesheet">
    <style amp-custom>
        /*!
        * Bootstrap v4.3.1 (https://getbootstrap.com/)
        * Copyright 2011-2019 The Bootstrap Authors
        * Copyright 2011-2019 Twitter, Inc.
        * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
        */:root{--blue:#007bff;--indigo:#6610f2;--purple:#6f42c1;--pink:#e83e8c;--red:#dc3545;--orange:#fd7e14;--yellow:#ffc107;--green:#28a745;--teal:#20c997;--cyan:#17a2b8;--white:#fff;--gray:#6c757d;--gray-dark:#343a40;--primary:#007bff;--secondary:#6c757d;--success:#28a745;--info:#17a2b8;--warning:#ffc107;--danger:#dc3545;--light:#f8f9fa;--dark:#343a40;--breakpoint-xs:0;--breakpoint-sm:576px;--breakpoint-md:768px;--breakpoint-lg:992px;--breakpoint-xl:1200px;--font-family-sans-serif:-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,"Noto Sans",sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol","Noto Color Emoji";--font-family-monospace:SFMono-Regular,Menlo,Monaco,Consolas,"Liberation Mono","Courier New",monospace}*,::after,::before{box-sizing:border-box}html{font-family:sans-serif;line-height:1.15;-webkit-text-size-adjust:100%;-webkit-tap-highlight-color:transparent}header,nav{display:block}body{margin:0;font-family:-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,"Noto Sans",sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol","Noto Color Emoji";font-size:1rem;font-weight:400;line-height:1.5;color:#212529;text-align:left;background-color:#fff}h1,h6{margin-top:0;margin-bottom:.5rem}p{margin-top:0;margin-bottom:1rem}ul{margin-top:0;margin-bottom:1rem}a{color:#007bff;text-decoration:none;background-color:transparent}a:hover{color:#0056b3;text-decoration:underline}button{border-radius:0}button:focus{outline:1px dotted;outline:5px auto -webkit-focus-ring-color}button,input{margin:0;font-family:inherit;font-size:inherit;line-height:inherit}button,input{overflow:visible}button{text-transform:none}[type=button],button{-webkit-appearance:button}[type=button]:not(:disabled),[type=reset]:not(:disabled),[type=submit]:not(:disabled),button:not(:disabled){cursor:pointer}[type=button]::-moz-focus-inner,button::-moz-focus-inner{padding:0;border-style:none}::-webkit-file-upload-button{font:inherit;-webkit-appearance:button}h1,h6{margin-bottom:.5rem;font-weight:500;line-height:1.2}h1{font-size:2.5rem}h6{font-size:1rem}.container-fluid{width:100%;padding-right:15px;padding-left:15px;margin-right:auto;margin-left:auto}.row{display:-ms-flexbox;display:flex;-ms-flex-wrap:wrap;flex-wrap:wrap;margin-right:-15px;margin-left:-15px}.col-10,.col-12,.col-2,.col-sm-12,.col-sm-2,.col-sm-5,.col-sm-6,.col-xl-10,.col-xl-11,.col-xl-12,.col-xl-2,.col-xl-5,.col-xl-6{position:relative;width:100%;padding-right:15px;padding-left:15px}.col-2{-ms-flex:0 0 16.666667%;flex:0 0 16.666667%;max-width:16.666667%}.col-10{-ms-flex:0 0 83.333333%;flex:0 0 83.333333%;max-width:83.333333%}.col-12{-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}@media (min-width:576px){.col-sm-2{-ms-flex:0 0 16.666667%;flex:0 0 16.666667%;max-width:16.666667%}.col-sm-5{-ms-flex:0 0 41.666667%;flex:0 0 41.666667%;max-width:41.666667%}.col-sm-6{-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%}.col-sm-12{-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}}@media (min-width:1200px){.col-xl-2{-ms-flex:0 0 16.666667%;flex:0 0 16.666667%;max-width:16.666667%}.col-xl-5{-ms-flex:0 0 41.666667%;flex:0 0 41.666667%;max-width:41.666667%}.col-xl-6{-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%}.col-xl-10{-ms-flex:0 0 83.333333%;flex:0 0 83.333333%;max-width:83.333333%}.col-xl-11{-ms-flex:0 0 91.666667%;flex:0 0 91.666667%;max-width:91.666667%}.col-xl-12{-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}}.form-control{display:block;width:100%;height:calc(1.5em + .75rem + 2px);padding:.375rem .75rem;font-size:1rem;font-weight:400;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out}@media (prefers-reduced-motion:reduce){.form-control{transition:none}}.form-control::-ms-expand{background-color:transparent;border:0}.form-control:focus{color:#495057;background-color:#fff;border-color:#80bdff;outline:0;box-shadow:0 0 0 .2rem rgba(0,123,255,.25)}.form-control::-webkit-input-placeholder{color:#6c757d;opacity:1}.form-control::-moz-placeholder{color:#6c757d;opacity:1}.form-control:-ms-input-placeholder{color:#6c757d;opacity:1}.form-control::-ms-input-placeholder{color:#6c757d;opacity:1}.form-control:disabled{background-color:#e9ecef;opacity:1}.custom-control-input.is-valid:focus:not(:checked)~.custom-control-label::before,.was-validated .custom-control-input:valid:focus:not(:checked)~.custom-control-label::before{border-color:#28a745}.custom-control-input.is-invalid:focus:not(:checked)~.custom-control-label::before,.was-validated .custom-control-input:invalid:focus:not(:checked)~.custom-control-label::before{border-color:#dc3545}.btn{display:inline-block;font-weight:400;color:#212529;text-align:center;vertical-align:middle;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;background-color:transparent;border:1px solid transparent;padding:.375rem .75rem;font-size:1rem;line-height:1.5;border-radius:.25rem;transition:color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out}@media (prefers-reduced-motion:reduce){.btn{transition:none}}.btn:hover{color:#212529;text-decoration:none}.btn:focus{outline:0;box-shadow:0 0 0 .2rem rgba(0,123,255,.25)}.btn:disabled{opacity:.65}.btn-primary:not(:disabled):not(.disabled).active,.btn-primary:not(:disabled):not(.disabled):active{color:#fff;background-color:#0062cc;border-color:#005cbf}.btn-primary:not(:disabled):not(.disabled).active:focus,.btn-primary:not(:disabled):not(.disabled):active:focus{box-shadow:0 0 0 .2rem rgba(38,143,255,.5)}.btn-secondary:not(:disabled):not(.disabled).active,.btn-secondary:not(:disabled):not(.disabled):active{color:#fff;background-color:#545b62;border-color:#4e555b}.btn-secondary:not(:disabled):not(.disabled).active:focus,.btn-secondary:not(:disabled):not(.disabled):active:focus{box-shadow:0 0 0 .2rem rgba(130,138,145,.5)}.btn-success:not(:disabled):not(.disabled).active,.btn-success:not(:disabled):not(.disabled):active{color:#fff;background-color:#1e7e34;border-color:#1c7430}.btn-success:not(:disabled):not(.disabled).active:focus,.btn-success:not(:disabled):not(.disabled):active:focus{box-shadow:0 0 0 .2rem rgba(72,180,97,.5)}.btn-info:not(:disabled):not(.disabled).active,.btn-info:not(:disabled):not(.disabled):active{color:#fff;background-color:#117a8b;border-color:#10707f}.btn-info:not(:disabled):not(.disabled).active:focus,.btn-info:not(:disabled):not(.disabled):active:focus{box-shadow:0 0 0 .2rem rgba(58,176,195,.5)}.btn-warning:not(:disabled):not(.disabled).active,.btn-warning:not(:disabled):not(.disabled):active{color:#212529;background-color:#d39e00;border-color:#c69500}.btn-warning:not(:disabled):not(.disabled).active:focus,.btn-warning:not(:disabled):not(.disabled):active:focus{box-shadow:0 0 0 .2rem rgba(222,170,12,.5)}.btn-danger:not(:disabled):not(.disabled).active,.btn-danger:not(:disabled):not(.disabled):active{color:#fff;background-color:#bd2130;border-color:#b21f2d}.btn-danger:not(:disabled):not(.disabled).active:focus,.btn-danger:not(:disabled):not(.disabled):active:focus{box-shadow:0 0 0 .2rem rgba(225,83,97,.5)}.btn-light:not(:disabled):not(.disabled).active,.btn-light:not(:disabled):not(.disabled):active{color:#212529;background-color:#dae0e5;border-color:#d3d9df}.btn-light:not(:disabled):not(.disabled).active:focus,.btn-light:not(:disabled):not(.disabled):active:focus{box-shadow:0 0 0 .2rem rgba(216,217,219,.5)}.btn-dark:not(:disabled):not(.disabled).active,.btn-dark:not(:disabled):not(.disabled):active{color:#fff;background-color:#1d2124;border-color:#171a1d}.btn-dark:not(:disabled):not(.disabled).active:focus,.btn-dark:not(:disabled):not(.disabled):active:focus{box-shadow:0 0 0 .2rem rgba(82,88,93,.5)}.btn-outline-primary:not(:disabled):not(.disabled).active,.btn-outline-primary:not(:disabled):not(.disabled):active{color:#fff;background-color:#007bff;border-color:#007bff}.btn-outline-primary:not(:disabled):not(.disabled).active:focus,.btn-outline-primary:not(:disabled):not(.disabled):active:focus{box-shadow:0 0 0 .2rem rgba(0,123,255,.5)}.btn-outline-secondary{color:#6c757d;border-color:#6c757d}.btn-outline-secondary:hover{color:#fff;background-color:#6c757d;border-color:#6c757d}.btn-outline-secondary:focus{box-shadow:0 0 0 .2rem rgba(108,117,125,.5)}.btn-outline-secondary:disabled{color:#6c757d;background-color:transparent}.btn-outline-secondary:not(:disabled):not(.disabled).active,.btn-outline-secondary:not(:disabled):not(.disabled):active{color:#fff;background-color:#6c757d;border-color:#6c757d}.btn-outline-secondary:not(:disabled):not(.disabled).active:focus,.btn-outline-secondary:not(:disabled):not(.disabled):active:focus{box-shadow:0 0 0 .2rem rgba(108,117,125,.5)}.btn-outline-success:not(:disabled):not(.disabled).active,.btn-outline-success:not(:disabled):not(.disabled):active{color:#fff;background-color:#28a745;border-color:#28a745}.btn-outline-success:not(:disabled):not(.disabled).active:focus,.btn-outline-success:not(:disabled):not(.disabled):active:focus{box-shadow:0 0 0 .2rem rgba(40,167,69,.5)}.btn-outline-info:not(:disabled):not(.disabled).active,.btn-outline-info:not(:disabled):not(.disabled):active{color:#fff;background-color:#17a2b8;border-color:#17a2b8}.btn-outline-info:not(:disabled):not(.disabled).active:focus,.btn-outline-info:not(:disabled):not(.disabled):active:focus{box-shadow:0 0 0 .2rem rgba(23,162,184,.5)}.btn-outline-warning:not(:disabled):not(.disabled).active,.btn-outline-warning:not(:disabled):not(.disabled):active{color:#212529;background-color:#ffc107;border-color:#ffc107}.btn-outline-warning:not(:disabled):not(.disabled).active:focus,.btn-outline-warning:not(:disabled):not(.disabled):active:focus{box-shadow:0 0 0 .2rem rgba(255,193,7,.5)}.btn-outline-danger:not(:disabled):not(.disabled).active,.btn-outline-danger:not(:disabled):not(.disabled):active{color:#fff;background-color:#dc3545;border-color:#dc3545}.btn-outline-danger:not(:disabled):not(.disabled).active:focus,.btn-outline-danger:not(:disabled):not(.disabled):active:focus{box-shadow:0 0 0 .2rem rgba(220,53,69,.5)}.btn-outline-light:not(:disabled):not(.disabled).active,.btn-outline-light:not(:disabled):not(.disabled):active{color:#212529;background-color:#f8f9fa;border-color:#f8f9fa}.btn-outline-light:not(:disabled):not(.disabled).active:focus,.btn-outline-light:not(:disabled):not(.disabled):active:focus{box-shadow:0 0 0 .2rem rgba(248,249,250,.5)}.btn-outline-dark:not(:disabled):not(.disabled).active,.btn-outline-dark:not(:disabled):not(.disabled):active{color:#fff;background-color:#343a40;border-color:#343a40}.btn-outline-dark:not(:disabled):not(.disabled).active:focus,.btn-outline-dark:not(:disabled):not(.disabled):active:focus{box-shadow:0 0 0 .2rem rgba(52,58,64,.5)}.input-group{position:relative;display:-ms-flexbox;display:flex;-ms-flex-wrap:wrap;flex-wrap:wrap;-ms-flex-align:stretch;align-items:stretch;width:100%}.input-group>.form-control{position:relative;-ms-flex:1 1 auto;flex:1 1 auto;width:1%;margin-bottom:0}.input-group>.form-control:focus{z-index:3}.input-group>.form-control:not(:last-child){border-top-right-radius:0;border-bottom-right-radius:0}.input-group-append{display:-ms-flexbox;display:flex}.input-group-append .btn{position:relative;z-index:2}.input-group-append .btn:focus{z-index:3}.input-group-append{margin-left:-1px}.input-group-sm>.form-control:not(textarea){height:calc(1.5em + .5rem + 2px)}.input-group-sm>.form-control,.input-group-sm>.input-group-append>.btn{padding:.25rem .5rem;font-size:.875rem;line-height:1.5;border-radius:.2rem}.input-group>.input-group-append>.btn{border-top-left-radius:0;border-bottom-left-radius:0}.custom-control-input:focus:not(:checked)~.custom-control-label::before{border-color:#80bdff}.custom-control-input:not(:disabled):active~.custom-control-label::before{color:#fff;background-color:#b3d7ff;border-color:#b3d7ff}.nav-justified .nav-item{-ms-flex-preferred-size:0;flex-basis:0;-ms-flex-positive:1;flex-grow:1;text-align:center}.card{position:relative;display:-ms-flexbox;display:flex;-ms-flex-direction:column;flex-direction:column;min-width:0;word-wrap:break-word;background-color:#fff;background-clip:border-box;border:1px solid rgba(0,0,0,.125);border-radius:.25rem}.card-img-overlay{position:absolute;top:0;right:0;bottom:0;left:0;padding:1.25rem}.card-img{width:100%;border-radius:calc(.25rem - 1px)}.close:not(:disabled):not(.disabled):focus,.close:not(:disabled):not(.disabled):hover{opacity:.75}.bg-white{background-color:#fff}.d-none{display:none}.d-flex{display:-ms-flexbox;display:flex}@media (min-width:576px){.d-sm-none{display:none}.d-sm-block{display:block}}.justify-content-between{-ms-flex-pack:justify;justify-content:space-between}.align-items-center{-ms-flex-align:center;align-items:center}@supports ((position:-webkit-sticky) or (position:sticky)){}.w-100{width:100%}.mr-2{margin-right:.5rem}.mx-3{margin-right:1rem}.mb-3{margin-bottom:1rem}.mx-3{margin-left:1rem}.p-3{padding:1rem}.py-3{padding-top:1rem}.px-3{padding-right:1rem}.py-3{padding-bottom:1rem}.px-3{padding-left:1rem}.pt-5,.py-5{padding-top:3rem}.py-5{padding-bottom:3rem}.mx-auto{margin-right:auto}.mx-auto{margin-left:auto}@media (min-width:1200px){.ml-xl-5{margin-left:3rem}.pl-xl-5{padding-left:3rem}}.text-nowrap{white-space:nowrap}.text-right{text-align:right}.text-center{text-align:center}@media print{*,::after,::before{text-shadow:none;box-shadow:none}a:not(.btn){text-decoration:underline}p{orphans:3;widows:3}@page{size:a3}body{min-width:992px}}


        /*!
        * Custome Style
        * Code By Jawata System - kanganwar.itdev@gmail.com
        */body{margin:0;font-family:Montserrat,sans-serif;font-size:1rem;font-weight:400;line-height:1.5;color:#212529;text-align:left;background-color:#fff}.nav-sidebar{padding:15px}.nav-sidebar h3{padding-right:35px}.nav-sidebar-close{position:absolute;right:15px;top:15px}.nav-sidebar ul{list-style:none;padding-left:0}.col-header{padding:0}.nav-menu-bar{display:inline-block;margin-right:15px;vertical-align:top;padding-top:3px}.nav-menu-logo{display:inline-block}.col-header-logo{padding-top:15px;padding-left:15px}.col-header-content{width:100%;z-index:2;top:0;padding:15px}.col-header-content .header-menu-1{font-size:10px;font-weight:700;color:rgba(0,0,0,.58);text-decoration:none}.col-header-content .header-menu{padding-top:15px;padding-bottom:15px}.col-header-content .header-menu-1:hover{text-decoration:none}.col-header-content .header-menu-1:first-child{margin-right:50px}.col-header-content .header-menu-1:hover{color:#fcab2c;text-decoration:none}.col-header-content .header-menu-2{font-size:14px;font-weight:700;color:#000}.col-header-content .header-menu-2:hover{color:#fcab2c;text-decoration:none}li.nav-item{padding-top:6px}li.group-menu{background-color:#000;border-top-left-radius:20px;border-bottom-left-radius:20px;border-top-right-radius:20px;border-bottom-right-radius:20px;padding:10px 5px;font-size:10pt}li.group-menu a{color:#fff;font-weight:700}li.group-menu .delimiter{color:#fff;font-weight:700}li.group-menu a:hover{color:#f6c101;text-decoration:none}.col-content{padding-top:20px;padding-bottom:50px}.col-footer{min-height:200px;background-color:#000;padding:20px}.col-footer-left p{font-size:12px;color:#fff;line-height:1.5;font-weight:300}.col-footer-center{padding-top:50px}.col-footer-right span{font-size:12px;font-weight:700;color:#fff}.col-footer-delimiter .delimeter{border-bottom:1px solid #fff}.col-footer-menu a{font-size:12px;font-weight:300;color:#fff;margin-right:30px}.col-footer-right-2{padding-right:60px}.col-footer-right-2 span{font-size:12px;font-weight:700;color:#fff;padding-top:15px;display:block}@media (max-width:991.98px){.col-header-content{background-color:#fcab2c;padding-top:0;padding-bottom:0;padding-left:15px;padding-right:15px}.col-header-content .header-menu{padding-bottom:0}.nav-sidebar .nav-item{text-align:left}}@media (min-width:992px){.col-header-logo{padding-top:35px;padding-left:60px}.col-footer{padding:40px}.col-footer-left{padding-top:24px;padding-left:60px}.col-footer-right{padding-top:24px;padding-right:60px}.col-footer-delimiter{padding-left:60px;padding-right:60px}.col-footer-menu{padding-left:60px;padding-top:10px}.col-footer-right-2 span{text-align:right}}

        /*!
        * Custom page styles
        */h1,h2,h3,h4,h5,h6{font-weight:900}.font-weight-900{font-weight:900}.font-style-i{font-style:italic}@media(min-width:1200px){.font-size-xl{font-size:84pt}}.text-outline{color:#fff;text-shadow:-1px -1px 0 #000,1px -1px 0 #000,-1px 1px 0 #000,1px 1px 0 #000}@supports((text-stroke:1px black) or (-webkit-text-stroke:1px black)){.text-outline{color:transparent;-webkit-text-stroke:1px #000;text-stroke:1px #000;text-shadow:none}}#page-title{position:absolute;bottom:1rem;left:0;right:0}#page-title a{display:inline-block;line-height:55px;width:50px;height:50px;border:1px solid;border-radius:50%}#page-title a+a{margin-left:1em}.d-columns-2{column-count:2}#see-portofolio-link::before{content:' ';border:1px solid;border-radius:50%;width:1rem;height:1rem;display:inline-block;margin-right:.5em;transform:translateY(20%)}#see-portofolio-link:hover{color:#a9a9a9}#see-portofolio-link{font-weight:700;text-decoration:none;color:#000}#orange-box{background-color:#ffa500;margin:0 -15px;padding:15% 0;position:relative;-webkit-clip-path:polygon(0 calc(100vw * .161),100% 0,100% calc(100% - (100vw * .161)),0 100%);clip-path:polygon(0 calc(100vw * .161),100% 0,100% calc(100% - (100vw * .161)),0 100%)}@media(min-width:1200px){#orange-box{margin:-125px -15px 0}}.link-xl{border:1px solid #fff;color:#000;font-size:10pt;font-weight:700;border-radius:24px;text-decoration:none;line-height:24px;padding:1em 3em}.center-both{position:absolute;top:50%;left:50%;margin-right:-50%;transform:translate(-50%,-50%)}.card:hover amp-img{box-shadow:0 0 8px 4px #fff}.card:hover .card-img-overlay{opacity:1}#row-latest-video .card:hover{transform:scale(1.02)}.card-img-overlay{background-color:rgba(255,156,0,.7);display:flex;flex-direction:column;justify-content:flex-end;opacity:0;max-width:100%;width:100%;transition:all 2s;white-space:break-spaces}#row-latest-video .card-img-overlay{-webkit-clip-path:polygon(0 0,50% 0,85% 100%,0 100%);clip-path:polygon(0 0,50% 0,85% 100%,0 100%)}#row-latest-series .card-img-overlay{clip-path:polygon(0 10%,100% 25%,100% 100%,0 100%)}amp-fit-text{height:auto;white-space:normal}[role=button]{cursor:pointer}.view-videos-link::before{content:' ';border: 1px solid;border-radius:50%;width:1rem;height:1rem;display:inline-block;margin-right:.5em;transform: translateY(20%)}.view-videos-link{color:black;text-decoration:none}
    </style>
    <style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>

    <link rel="canonical" href=".">
    <title>Starhits</title>
</head>

<body>
    <amp-sidebar id="sidebar-left" class="nav-sidebar" layout="nodisplay" side="left">
        <h6>NAVIGATION</h6>
        <span tabindex="0" role="button" on="tap:sidebar-left.close" class="nav-sidebar-close">✕</span>
        <nav toolbar="(min-width: 784px)" toolbar-target="target-element-left">
            <ul class="w-100 nav-justified">
                <li class="nav-item"><a href="index.html" class="header-menu-2">HOME</a></li>
                <li class="nav-item"><a href="about-us.html" class="header-menu-2">ABOUT US</a></li>
                <li class="nav-item"><a href="creator.html" class="header-menu-2">CREATOR</a></li>
                <li class="nav-item"><a href="series.html" class="header-menu-2">SERIES</a></li>
                <li class="nav-item"><a href="channels.html" class="header-menu-2">CHANNEL</a></li>
                <li class="nav-item group-menu d-none d-sm-block text-nowrap px-3">
                    <span><a href="join-us.html">JOIN US</a></span>
                    <span class="delimiter">|</span>
                    <span><a href="login.html">LOGIN</a></span>
                </li>
            </ul>
        </nav>
        <ul>
            <li><a href="portofolio.html" class="header-menu-2">PORTOFOLIO</a></li>
            <li><a href="create-project.html" class="header-menu-2">CREATE PROJECT</a></li>
            <li><a href="join-us.html" class="header-menu-2">JOIN US</a></li>
            <li><a href="login.html" class="header-menu-2">LOGIN</a></li>
        </ul>
    </amp-sidebar>

    <div class="container-fluid">
        <div class="row">

            <!-- BEGIN HEADER -->
            <div class="col-xs-12 col-sm-12 col-header">
                <div class="col-header-bg"></div>
                <div class="col-header-content">
                    <div class="row">
                        <div class="d-none d-sm-block col-sm-6 col-header-logo">
                            <a href="index.html">
                                <amp-img src="pubs/images/logo.png" width="151" height="52"></amp-img>
                            </a>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="row">
                                <div class="col-sm-12 d-none d-sm-block text-right">
                                    <a href="portofolio.html" class="header-menu-1">PORTOFOLIO</a>
                                    <a href="#" class="header-menu-1">CREATE PROJECT</a>
                                </div>
                            </div>
                            <div class="header-menu">
                                <header class="d-flex d-sm-none">
                                    <div role="button" tabindex="0" on="tap:sidebar-left.toggle">
                                        <span class="nav-menu-bar">☰</span>
                                    </div>
                                    <amp-img src="pubs/images/logo.png" class="nav-menu-logo mx-auto" width="102" height="35">
                                    </amp-img>
                                </header>
                                <div id="target-element-left">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- BEGIN MAIN CONTENT -->
            <div class="col-xs-12 col-sm-12 col-content">
                <div class="d-flex justify-content-between" style="position: absolute;top: 0;right: 15Px;left: 15px">
                    <div>&nbsp;</div>
                    <div style="width: 473px;height:474px;padding-top: 150px;padding-right: 92px">
                        <amp-img src="pubs/images/bg-circle.png" width="473" height="474" layout="responsive"
                            style="transform: rotate(45deg)">
                        </amp-img>
                    </div>
                    <div style="width:50px;height:92px">
                        <amp-img src="pubs/images/bg-triangle.png" width="50" height="92">
                        </amp-img>
                    </div>
                </div>


                <div class="row">
                    <div class="col-12" style="padding:0;margin:0 -15px;position:relative">
                        <amp-img src="./pubs/images/Layer-42-copy.png" width="1280" height="607" layout="responsive">
                        </amp-img>

                        <div id="page-title">
                            <h1 class="font-style-i font-size-xl font-weight-900 text-center">
                                <span class="text-outline">JASMINE</span> WIJAYA
                            </h1>
                            <div class="text-center">
                                <a href="#" class="creator-details-assets-link">
                                    <amp-img src="pubs/icon/001-youtube-play-button.png" width="18" height="18"></amp-img>
                                </a>
                                <a href="#" class="creator-details-assets-link">
                                    <amp-img src="pubs/icon/004-instagram.png" width="18" height="18"></amp-img>
                                </a>
                                <a href="#" class="creator-details-assets-link">
                                    <amp-img src="pubs/icon/002-twitter-logo.png" width="18" height="18"></amp-img>
                                </a>
                                <a href="#" class="creator-details-assets-link">
                                    <amp-img src="pubs/icon/003-facebook-letter-logo.png" width="18" height="18"></amp-img>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-xl-5 pt-5 pl-xl-5 ml-xl-5">
                        <h1 class="mb-3"><span class="text-outline">GET TO</span> KNOW</h1>
                        <div>
                            Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat.                             
                        </div>
                        
                        <h1 class="py-3"><span class="text-outline">FOLLOWER</span></h1>
                        
                        <div class="d-columns-2">
                                <div class="d-flex align-items-center py-3">
                                    <amp-img class="mr-2" src="pubs/icon/001-youtube-play-button.png"
                                        width="24" height="24"></amp-img>
                                    <span>188</span>
                                </div>
                                <div class="d-flex align-items-center py-3">
                                    <amp-img class="mr-2" src="pubs/icon/004-instagram.png" width="24"
                                        height="24"></amp-img>
                                    <span>13M</span>
                                </div>
                                <div class="d-flex align-items-center py-3">
                                    <amp-img class="mr-2" src="pubs/icon/002-twitter-logo.png" width="24"
                                        height="24"></amp-img>
                                    <span>5M</span>
                                </div>
                                <div class="d-flex align-items-center py-3">
                                    <amp-img class="mr-2" src="pubs/icon/003-facebook-letter-logo.png"
                                        width="24" height="24"></amp-img>
                                    <span>582M</span>
                                </div>
                        </div>

                        <a id="see-portofolio-link" href="portofolio-details.html">see portofolio</a>

                    </div>
                    <div class="col-xl-5 pt-5">
                        <amp-img src="pubs/images/Layer-531.png" width="634" height="638" style="z-index: 2"></amp-img>
                    </div>
                </div>

                <div id="orange-box">
                    <div class="row">
                        <div class="col-xl-11" style="margin: auto;text-align:right;">
                            <a class="link-xl bg-white" href="creator-video-list.html">VIEW POPULAR VIDEO</a>
                        </div>
                    </div>
                    
                    <h1 class="text-center p-3"><span class="text-outline">LATEST</span> VIDEO</h1>
                    
                    <div class="row">
                        <div class="col-xl-10 mx-auto">
                            <amp-carousel id="row-latest-video" height="270" layout="fixed-height">
                                <div class="slide mx-3" style="padding: 10px">
                                    <div class="card" style="position: relative">
                                        <amp-img src="pubs/images/Layer-574.png" width="345" height="194"></amp-img>
                                        <div class="card-img-overlay">
                                            <a class="view-videos-link" href="creator-video-detail.html">view video</a>
                                        </div>
                                    </div>
                                    
                                    <div class="caption" style="padding:15px 0;">
                                        <h6>Card title</h6>
                                        <p>Some quick example text</p>
                                    </div>
                                </div>
                                
                                <div class="slide mx-3" style="padding: 10px">
                                    <div class="card" style="position: relative">
                                        <amp-img src="pubs/images/Layer-578.png" width="345" height="194"></amp-img>
                                        <div class="card-img-overlay">
                                            <a class="view-videos-link" href="creator-video-detail.html">view video</a>
                                        </div>
                                    </div>
                                    <div class="caption" style="padding:15px 0;">
                                        <h6>Card title</h6>
                                        <p>Some quick example text</p>
                                    </div>
                                </div>


                                <div class="slide mx-3">
                                    <div style="position: relative">
                                        <amp-img src="https://via.placeholder.com/345x194.png/ffa500/ffa500" width="345" height="194"></amp-img>
                                        <a class="link-xl center-both" href="creator-video-list.html">VIEW ALL</a>
                                    </div>
                                    <div class="caption">
                                        <h6>&nbsp;</h6>
                                        <p>&nbsp;</p>
                                    </div>
                                </div>
                            </amp-carousel>
                        </div>
                    </div>

                    <div class="bg-circle" style="width:34vw;height:62vh;position: absolute;left: -5vw;">
                        <amp-img src="pubs/images/bg-circle.png" width="473" height="474" layout="responsive"
                            style="transform: rotate(279deg)">
                        </amp-img>
                    </div>
                    </div>

                <h1 class="text-center py-5"><span class="text-outline">LATEST</span> SERIES</h1>
                
                <div class="row">
                    <div class="col-xl-10 mx-auto">
                        
                        <amp-carousel id="row-latest-series" height="584" layout="fixed-height" type="carousel">
                            <div class="card">
                                <amp-img class="card-img" src="pubs/images/Layer-586.png" width="350" height="584"></amp-img>

                                <div class="card-img-overlay">
                                    <p>8 Videos</p>
                                    <h6>The standard Lorem Ipsum passage</h6>
                                    <amp-fit-text height="200" layout="fixed-height" max-font-size="12">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                    </amp-fit-text>
                                    <a class="view-videos-link" href="creator-video-list.html">view videos</a>
                                </div>
                            </div>
                            
                            <div class="card">
                                <amp-img class="card-img" src="pubs/images/Layer-593.png" width="350" height="584"></amp-img>
                                
                                <div class="card-img-overlay">
                                        <p>8 Videos</p>
                                        <h6>The standard Lorem Ipsum passage</h6>
                                        <amp-fit-text height="200" layout="fixed-height" max-font-size="12">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                        </amp-fit-text>
                                        <a class="view-videos-link" href="creator-video-list.html">view videos</a>
                                    </div>
                            </div>

                            <div class="card">
                                <amp-img class="card-img" src="pubs/images/Layer-736.png" width="350" height="584"></amp-img>
                                
                                <div class="card-img-overlay">
                                        <p>8 Videos</p>
                                        <h6>The standard Lorem Ipsum passage</h6>
                                        <amp-fit-text height="200" layout="fixed-height" max-font-size="12">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                        </amp-fit-text>
                                        <a class="view-videos-link" href="creator-video-list.html">view videos</a>
                                    </div>
                            </div>
                        </amp-carousel>
                    </div>
                </div>
            </div>
            
            <!-- BEGIN FOOTER -->
            <div class="col-xs-12 col-sm-12 col-footer">
                <div class="row">
                    <div class="col-xl-5 col-sm-5 col-footer-left">
                        <amp-img src="pubs/images/logo-light.png" width="125" height="43"></amp-img>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                            ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                            ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                        </div>
                    <div class="col-xl-2 col-sm-2 text-center col-footer-center">
                        <amp-img src="pubs/icon/icon-yt-light.png" width="73" height="43"></amp-img>
                    </div>
                    <div class="col-xl-5 col-sm-5 col-footer-right text-center">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12">
                                <span>Subcribe our letter :</span>
                                    <div class="input-group input-group-sm mb-3">
                                    <input type="email" class="form-control">
                                    <div class="input-group-append">
                                        <button class="btn btn-outline-secondary" type="button">Send</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12">
                                <span class="icon-sosmed px-3">
                                    <a href="#">
                                        <amp-layout layout="fixed" width="24" height="24">
                                            <amp-img layout="responsive" width="1" height="1" src="pubs/svgs/youtube.svg"></amp-img> 
                                        </amp-layout>
                                    </a>
                                </span>
                                <span class="icon-sosmed px-3">
                                    <a href="#">
                                        <amp-layout layout="fixed" width="24" height="24">
                                            <amp-img layout="responsive" width="1" height="1" src="pubs/svgs/facebook-f.svg"></amp-img> 
                                        </amp-layout>
                                    </a>
                                </span>
                                <span class="icon-sosmed px-3">
                                    <a href="#">
                                        <amp-layout layout="fixed" width="24" height="24">
                                            <amp-img layout="responsive" width="1" height="1" src="pubs/svgs/twitter.svg"></amp-img> 
                                        </amp-layout>
                                    </a>
                                </span>
                                <span class="icon-sosmed px-3">
                                    <a href="#">
                                        <amp-layout layout="fixed" width="24" height="24">
                                            <amp-img layout="responsive" width="1" height="1" src="pubs/svgs/instagram.svg"></amp-img> 
                                        </amp-layout>
                                    </a>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-12 col-sm-12 col-footer-delimiter">
                        <p class="delimeter">&nbsp</p>
                    </div>

                    <div class="col-xl-6 col-sm-6 col-footer-menu">
                        <a href="#">COPYRIGHT &COPY; 2019 STARHITS</a>
                        <a href="policy.html">PRIVACY POLICY</a>
                        <a href="term.html">TERMS OF SERVICE</a>
                        <a href="faq.html">FAQ</a>
                    </div>

                    <div class="col-xl-6 col-sm-6 col-footer-right-2">
                        <div class="row">
                            <div class="col-10">
                                <span>SOUTHEAST ASIA'S LARGEST AND MOST INTEGRATED MEDIA GROUP</span>
                            </div>
                            <div class="col-2 text-right ">
                                <amp-img src="pubs/icon/icon-mnc-media.png" width="80" height="41"></amp-img>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>