@extends('frontend.channels.layout')

@section('data')
<div class="row bg-orange pt-5">
    <div class="col-xl-10 mx-auto">
        <h5 class="text-center" style="margin-bottom: 1.5em;">LATEST LIFESTYLE VIDEO</h5>

        <div class="latest-entertainment-video" style="position: relative">
        <amp-list id="amp-list-tab1-1" class="list" reset-on-refresh layout="fixed-height" height="156" src="//starhits.id/api/channel-latest-video/lifestyle" binding="refresh" load-more="manual" load-more-bookmark="next_page_url">
                <template type="amp-mustache">
                    <amp-img src="https://i3.ytimg.com/vi/@{{attr_6}}/mqdefault.jpg" width="256" height="156" layout="responsive"></amp-img>
                    <div class="preview-popup">
                        <a class="preview-popup-link" href="{{ route('channels') }}/@{{slug}}">watch video</a>
                    </div>
                    <div class="caption">
                        <amp-fit-text layout="fixed-height" height="50" max-font-size="20">
                            @{{subtitle}}</amp-fit-text>
                        <div class="title">By @{{users.name}}</div>
                    </div>
                </template>
                <div fallback>FALLBACK</div>
                <div placeholder>PLACEHOLDER</div>
                <amp-list-load-more load-more-failed>ERROR</amp-list-load-more>
                <amp-list-load-more load-more-end>END</amp-list-load-more>
                <amp-list-load-more load-more-button>
                    <div class="amp-load-more">
                        <button class="load-more-link"><label>LOAD MORE</label></button>
                    </div>
                </amp-list-load-more>                                    
            </amp-list>
        </div>                            
    </div>
</div>

<div class="row pt-5">
    <div class="col-xl-10 mx-auto">
        <h5 class="text-center" style="margin-bottom: 1.5em;">LATEST LIFESTYLE SERIES</h5>

        <div class="latest-entertainment-video" style="position: relative">
            <amp-list id="amp-list-tab1" class="list" reset-on-refresh layout="fixed-height"
                height="564" src="//starhits.id/api/channel-latest-series/lifestyle" binding="refresh" load-more="manual" load-more-bookmark="next_page_url">
                <template type="amp-mustache">
                    <amp-img src="@{{image}}" width="258" height="408" layout="responsive"></amp-img>
                    <div class="preview-popup">
                        <div class="preview-popup-description">
                            <small>@{{totalVideo}} Videos</small>
                            <p class="subtitle">
                                <strong>
                                    @{{title}}
                                </strong>
                            </p>
                            <amp-fit-text layout="fixed-height" height="150" max-font-size="12"
                                style="font-weight: 300">@{{excerpt}}</amp-fit-text>
                        </div>
                        <a class="preview-popup-link" href="{{ route('series') }}/@{{slug}}">watch video</a>
                    </div>
                    <div class="caption">
                        <h6>@{{title}}</h6>
                        <p><small>@{{totalVideo}} Videos</small></p>
                    </div>
                </template>
                <div fallback>FALLBACK</div>
                <div placeholder>PLACEHOLDER</div>
                <amp-list-load-more load-more-failed>ERROR</amp-list-load-more>
                <amp-list-load-more load-more-end>END</amp-list-load-more>
                <amp-list-load-more load-more-button>
                        <div class="amp-load-more">
                            <button class="load-more-link"><label>LOAD MORE</label></button>
                        </div>
                </amp-list-load-more>  
            </amp-list>
        </div>

        
    </div>
</div>
@endsection