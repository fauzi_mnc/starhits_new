@extends('layouts.frontend')

@section('css')
    @include('frontend.includes.extensions.amp-channel')
@endsection

@section('content')
<!-- BEGIN MAIN CONTENT -->
<div class="col-xs-12 col-sm-12 col-content">
    <div style="width: 34vw;height:62vh;position: absolute;top:360px;">
        <amp-img src="{{asset('pubs/images/bg-circle.png')}}" width="473" height="474" layout="responsive"
            style="transform: rotate(45deg)">
        </amp-img>
    </div>

    <div style="width: 34vw;height:62vh;position: absolute;top:132px;right:0">
        <amp-img src="{{asset('pubs/images/bg-circle.png')}}" width="473" height="474" layout="responsive"
            style="transform: rotate(45deg)">
        </amp-img>
    </div>

    <div style="width: 34vw;height:62vh;position: absolute;bottom:50px;transform: rotate(-90deg)">
        <amp-img src="{{asset('pubs/images/bg-circle.png')}}" width="473" height="474" layout="responsive"
            style="transform: rotate(45deg)">
        </amp-img>
    </div>

    <div class="profile">
        <div class="profile__container">
            <div class="profile__picture">
            <amp-img src="{{asset('pubs/images/Layer-987.png')}}" width="363" height="567" layout="responsive">
                </amp-img>
            </div>
            <div class="profile__picture">
                <amp-img src="{{asset('pubs/images/Layer-986.png')}}" width="363" height="567" layout="responsive">
                </amp-img>
            </div>
            <div class="profile__picture">
                <amp-img src="{{asset('pubs/images/Layer-988.png')}}" width="363" height="567" layout="responsive">
                </amp-img>
            </div>
        </div>
        
        <div class="profile__container">
            <div class="page-title">
                <h1 class="text-outline font-style-i font-weight-900 font-size-xl">CHANNEL</h1>
                <h1 class="font-style-i font-weight-900 font-size-xl">CHANNEL</h1>
            </div>
        </div>
    </div>

    <div>
        <nav class="nav">
            <ul id="main">
                @foreach ($SiteConfig['_channel'] as $channels)
                <li><a href="{{ route('channels') }}/{{ $channels->slug }}">{{ $channels->title }}</a></li>
                @endforeach
            </ul>
        </nav>
    </div>

    @yield('data')

</div>
@endsection