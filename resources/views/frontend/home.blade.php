@extends('layouts.frontend')

@section('css')
    <!-- Bootstrap CSS -->
    <style amp-custom>
        {{ include"frontend/assets/css/bootstrap.css" }}
        {{ include"frontend/assets/css/style.css" }}
    </style>
    <style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
@endsection

@section('content')
<div class="col-header-bg"></div>
<div class="col-home-slider">
    <amp-carousel id="carouselWithPreview"
        width="1665"
        layout="responsive"
        height="662"
        autoplay
        delay="10000"
        type="slides"
        on="slideChange:carouselWithPreviewSelector.toggle(index=event.index, value=true, class=selected)">

        <div class="slide">
            <h3 class="heading-slider">
                <span class="text-outline">LEADING <br>DIGITAL</span> 
                <span>NETWORK A</span>
            </h3>
            <amp-img src="frontend/assets/images/slider01.png"
                layout="responsive"
                width="1665"
                height="662"
                layout="responsive"></amp-img>]
        </div>

        <div class="slide">
            <h3 class="heading-slider">&nbsp;</h3>
            <amp-img src="frontend/assets/images/slider03.png"
                layout="responsive"
                width="665"
                height="662"
                layout="responsive"></amp-img>
        </div>

    </amp-carousel>
    <amp-selector id="carouselWithPreviewSelector"
        class="carousel-preview"
        on="select:carouselWithPreview.goToSlide(index=event.targetOption)"
        layout="container">

        <amp-img option="0"
            selected
            src="frontend/assets/images/img-circle.png"
            width="22"
            height="22"></amp-img>

        <amp-img option="1"
            src="frontend/assets/images/img-circle.png"
            width="22"
            height="22"></amp-img>

    </amp-selector>
</div>
<div class="col-home-info">
    <div class="row">
        <div class="col-4 col-sm-12">
            <span class="p1">Watchtime</span>
            <span class="p2">12.2B</span>
        </div>

        <div class="col-4 col-sm-12">
            <span class="p1">Views</span>
            <span class="p2">112B</span>
        </div>

        <div class="col-4 col-sm-12">
            <span class="p1">Subscribers</span>
            <span class="p2">1227M</span>
        </div>
    </div>
</div>
</div>

<!-- BEGIN MAIN CONTENT -->
<div class="col-xs-12 col-sm-12 col-content">

<div class="row">
    <!-- BEGIN OUR BUSINESS-->
    <div class="col-xs-12 col-sm-12 col-our-business">
        <div class="row">
            <div class="col-our-business-bg"></div>
            <div class="col-12 text-center">
                <h3 class="title-our-business"><span class="text-outline">OUR</span> BUSINESS</h3>
            </div>

            <div class="col-xs-12 col-sm-5 col-our-business-img">
                <div class="our-business-img-01">
                    <amp-img src="frontend/assets/icon/icon-yt.png" class="img-icon-yt" width="36" height="26"></amp-img>
                    <amp-img src="frontend/assets/images/demo01.jpg" class="img-our-bussiness" width="255" height="170"></amp-img>
                </div>

                <div class="our-business-img-02">
                    <amp-img src="frontend/assets/icon/icon-love.png" class="img-icon-love" width="35" height="26"><span>1</span></amp-img>
                    <amp-img src="frontend/assets/images/demo02.jpg" class="img-our-bussiness" width="200" height="300"></amp-img>
                </div>
                <div class="our-business-img-03">
                    <amp-img src="frontend/assets/icon/icon-msg.png" class="img-icon-msg" width="43" height="32"><span>17</span></amp-img>
                    <amp-img src="frontend/assets/images/demo03.jpg" class="img-our-bussiness" width="309" height="206"></amp-img>
                </div>
            </div>
            <div class="col-xs-12 col-sm-7 col-our-business-list">
                <div class="row">
                    <div class="col-12 col-sm-6">
                        <div class="row">
                            <div class="col-12 our-bussines-01">
                                <h4 class="our-business-heading">Multi Channel Network</h4>
                                <p class="our-business-content">Lorem ipsum dolor sit amet, sectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ut enim ad minim.</p>
                            </div>

                            <div class="col-12 our-bussines-02">
                                <h4 class="our-business-heading">Influencer Management</h4>
                                <p class="our-business-content">Lorem ipsum dolor sit amet, sectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ut enim ad minim.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6">
                        <div class="row">
                            <div class="col-12 our-bussines-03">
                                <h4 class="our-business-heading">Creative Digital Campaign</h4>
                                <p class="our-business-content">Lorem ipsum dolor sit amet, sectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ut enim ad minim.</p>
                            </div>

                            <div class="col-12 our-bussines-04">
                                <h4 class="our-business-heading">Production Service</h4>
                                <p class="our-business-content">Lorem ipsum dolor sit amet, sectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ut enim ad minim.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- BEGIN RECOMENDED VIDEO-->
    <div class="col-xs-12 col-sm-12 col-recomended-video">
        <div class="row">
            <div class="col-xs-12 col-sm-12 text-center">
                <h3 class="title-recomended-video">RECOMENDED <span class="text-outline">VIDEO</span></h3>
            </div>
            <div class="col-xs-12 col-sm-12 content-slider">
                <amp-carousel height="340" class="hidden-scroll" type="carousel">
                    <div class="col-sm-4 col-xs-12 slider-box">
                        <div class="slider-box__wrapper">
                            <div class="slider-box__img">
                                <amp-img src="frontend/assets/images/demo04.png" width="320" height="180" layout="responsive" class="img-cover">
                                </amp-img>
                            </div>
                            <div class="overlay"></div>
                        </div>
                
                        <p class="title-video">Meja Panas 2</p>
                        <amp-fit-text layout="responsive" width="300" height="50" min-font-size="12" max-font-size="14">
                            <span class="desc-video">Cabe-cabean Ala Dessert King reynold Pornomo</span>
                        </amp-fit-text>
                
                    </div>
                
                    <div class="col-sm-4 col-xs-12 slider-box">
                        <div class="slider-box__wrapper">
                            <div class="slider-box__img">
                                <amp-img src="frontend/assets/images/demo05.jpg" width="320" height="180" layout="responsive" class="img-cover">
                                </amp-img>
                                <div class="overlay"></div>
                            </div>
                        </div>
                
                        <p class="title-video">Album Name 2</p>
                        <amp-fit-text layout="responsive" width="300" height="50" min-font-size="12" max-font-size="14">
                            <span class="desc-video">Album description here</span>
                        </amp-fit-text>
                    </div>
                
                    <div class="col-sm-4 col-xs-12 slider-box">
                        <div class="slider-box__wrapper">
                            <div class="slider-box__img">
                                <amp-img src="frontend/assets/images/demo06.jpg" width="320" height="180" layout="responsive" class="img-cover">
                                </amp-img>
                                <div class="overlay"></div>
                            </div>
                        </div>
                
                        <p class="title-video">Album Name 3</p>
                        <amp-fit-text layout="responsive" width="300" height="50" min-font-size="12" max-font-size="14">
                            <span class="desc-video">Album description here</span>
                        </amp-fit-text>
                    </div>
                
                    <div class="col-sm-4 col-xs-12 slider-box">
                        <div class="slider-box__wrapper">
                            <div class="slider-box__img">
                                <amp-img src="frontend/assets/images/demo04.png" width="300" height="180" layout="responsive" class="img-cover">
                                </amp-img>
                                <div class="overlay"></div>
                            </div>
                        </div>
                
                        <p class="title-video">Album Name 4</p>
                        <amp-fit-text layout="responsive" width="300" height="50" min-font-size="12" max-font-size="14">
                            <span class="desc-video">Album description here</span>
                        </amp-fit-text>
                    </div>
                
                    <div class="col-sm-4 col-xs-12 slider-box">
                        <div class="slider-box__wrapper">
                            <div class="slider-box__img">
                                <amp-img src="frontend/assets/images/demo05.jpg" width="320" height="180" layout="responsive" class="img-cover">
                                </amp-img>
                                <div class="overlay"></div>
                            </div>
                        </div>
                
                        <p class="title-video">Album Name 5</p>
                        <amp-fit-text layout="responsive" width="300" height="50" min-font-size="12" max-font-size="14">
                            <span class="desc-video">Album description here</span>
                        </amp-fit-text>
                    </div>
                </amp-carousel>
            </div>
        </div>

    </div>

    <!-- BEGIN CONTENT CREATOR-->
    <div class="col-xs-12 col-sm-12 col-content-creator">
        <div class="col-recomended-video-footer">&nbsp;</div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 bg-content-creator">

                <div class="col-xs-12 col-sm-12 text-center">
                    <h3 class="title-recomended-video">CONTENT <span class="text-outline">CREATOR</span></h3>
                </div>

                <div class="col-xs-12 col-sm-12 col-recomended-video-slider">
                    <amp-carousel type="carousel"
                        height="680"
                        layout="fixed-height"
                        class="hidden-scroll">

                        <div class="col-sm-4 col-xs-1 slider-box">
                            <div class="col-recomended-video-slider__wrapper">
                                <div class="col-recomended-video-slider__img">
                                    <h3 class="title-creator-content">angelica 01</h3>
                                    <amp-img src="frontend/assets/images/creator02.png" layout="responsive" width="259" height="428"></amp-img>
                            
                                    <amp-img class="btn-circle" src="frontend/assets/images/btn-plus-circle.png" width="69" height="69"></amp-img>
                            
                                    <div class="content-text">
                                        <div class="social-media">
                                            <p><i class="fab fa-youtube"></i></p>
                                            <p><i class="fab fa-facebook-f"></i></p>
                                            <p><i class="fab fa-twitter"></i></p>
                                            <p><i class="fab fa-instagram"></i></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-4 col-xs-1 slider-box">
                            <div class="col-recomended-video-slider__wrapper">
                                <div class="col-recomended-video-slider__img">
                                    <h3 class="title-creator-content">angelica 02</h3>
                                    <amp-img src="frontend/assets/images/creator02.png" layout="responsive" width="259" height="428"></amp-img>
                            
                                    <amp-img class="btn-circle" src="frontend/assets/images/btn-plus-circle.png" width="69" height="69"></amp-img>
                            
                            
                                    <div class="content-text">
                                        <div class="social-media">
                                            <p><i class="fab fa-youtube"></i></p>
                                            <p><i class="fab fa-facebook-f"></i></p>
                                            <p><i class="fab fa-twitter"></i></p>
                                            <p><i class="fab fa-instagram"></i></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-4 col-xs-1 slider-box">
                            <div class="col-recomended-video-slider__wrapper">
                                <div class="col-recomended-video-slider__img">
                                    <h3 class="title-creator-content">angelica 03</h3>
                                    <amp-img src="frontend/assets/images/creator02.png" layout="responsive" width="259" height="428"></amp-img>
                            
                                    <amp-img class="btn-circle" src="frontend/assets/images/btn-plus-circle.png" width="69" height="69"></amp-img>
                            
                            
                                    <div class="content-text">
                                        <div class="social-media">
                                            <p><i class="fab fa-youtube"></i></p>
                                            <p><i class="fab fa-facebook-f"></i></p>
                                            <p><i class="fab fa-twitter"></i></p>
                                            <p><i class="fab fa-instagram"></i></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-4 col-xs-1 slider-box">
                            <div class="col-recomended-video-slider__wrapper">
                                <div class="col-recomended-video-slider__img">
                                    <h3 class="title-creator-content">angelica 04</h3>
                                    <amp-img src="frontend/assets/images/creator02.png" layout="responsive" width="259" height="428"></amp-img>
                            
                                    <amp-img class="btn-circle" src="frontend/assets/images/btn-plus-circle.png" width="69" height="69"></amp-img>
                            
                                    <div class="content-text">
                                        <div class="social-media">
                                            <p><i class="fab fa-youtube"></i></p>
                                            <p><i class="fab fa-facebook-f"></i></p>
                                            <p><i class="fab fa-twitter"></i></p>
                                            <p><i class="fab fa-instagram"></i></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </amp-carousel>
                </div>
                <div class="col-xs-12 col-sm-12 text-center col-recomended-video_link"><a href="" class="link-view-all">VIEW ALL</a></div>
            </div>
        </div>
    </div>
</div>
@endsection