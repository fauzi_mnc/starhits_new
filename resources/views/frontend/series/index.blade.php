@extends('layouts.frontend')

@section('css')
    @include('frontend.includes.extensions.amp-series')
@endsection

@section('content')
<!-- BEGIN MAIN CONTENT -->
<div class="row">
    <div class="col-xs-12 col-sm-12 col-content">
        <div id="profile-picture">
            <div style="width: 34vw;height:62vh;position: absolute;left:0;bottom:0;transform:translate(-50%,50%)">
                <amp-img src="pubs/images/bg-circle.png" width="473" height="474" layout="responsive"
                    style="transform: rotate(45deg)">
                </amp-img>
            </div>

            <amp-img src="pubs/images/Layer-706-2.png" width="1280" height="357" layout="responsive"></amp-img>

            <div id="page-title">
                <h1 style="color:#fff">STARHITS</h1>
                <h1 class="font-style-i font-size-xl font-weight-900">
                    <span class="text-outline">SERIES</span>
                </h1>
            </div>
        </div>

        <div class="col-12">
                <nav class="nav">
                    <ul id="main">
                        <li><a href="#">MOST SUBCRIBERS</a></li>
                        <li><a href="#">MOST VIEWS</a></li>
                        <li>
                            <form class="form-inline" method="POST" action-xhr="#" target="_top"
                                style="margin: -0.45em 1em;">
                                <input class="form-control rounded-0" type="search" name="term"
                                    placeholder="SEARCH BY NAME" required>
                            </form>
                        </li>
                    </ul>

                    <div id="dropdown-container">
                        <amp-accordion class="nav__dropdown">
                            <section>
                                <header class="nav__dropdown__header"><span class="show-xl">CHANNEL
                                    </span>CREATOR<span class="dropdown-symbol"></span></header>
                                <ul class="nav__dropdown__list">
                                    <li class="nav__dropdown__listitem">
                                        <a href="#">CORPORATE CREATOR</a>
                                    </li>
                                </ul>
                            </section>
                        </amp-accordion>
                    </div>
                </nav>

            <div class="col-xl-10 mx-auto" style="position: relative">
                <amp-list id="amp-list-tab1" class="list" reset-on-refresh layout="fixed-height" width="auto" height="488" src="https://starhits.id/api/series" binding="refresh" load-more="manual" load-more-bookmark="next_page_url">
                    <div overflow role="button" aria-label="SHOW MORE" class="text-center pt-5">
                            <span class="load-more-link">SHOW MORE</span>
                    </div>
                    <template type="amp-mustache"> 
                        <div class="card">
                            <amp-img class="card-img" src="@{{image}}" width="258" height="403" layout="responsive"></amp-img>
                            <div class="card-img-overlay">
                                <div class="preview-popup">
                                    <div class="preview-popup-description">
                                        <small>@{{totalVideo}} Videos</small>
                                        <p class="subtitle">
                                            <strong>
                                                @{{title}}
                                            </strong>
                                        </p>
                                        <amp-fit-text layout="fixed-height" height="150" max-font-size="12" style="font-weight: 300">@{{excerpt}}</amp-fit-text>
                                    </div>
                                    <a class="preview-popup-link" href="{{ route('series') }}/@{{slug}}">watch video</a>
                                </div>
                            </div>
                        </div>
                        <div class="caption">
                            <h6>@{{title}}</h6>
                            <p><small>@{{totalVideo}} Videos</small></p>
                        </div>
                    </template>
                    <div fallback>FALLBACK</div>
                        <div placeholder>PLACEHOLDER</div>
                        <amp-list-load-more load-more-failed>ERROR</amp-list-load-more>
                        <amp-list-load-more load-more-end>END</amp-list-load-more>
                        <amp-list-load-more load-more-button>
                            <div class="amp-load-more">
                                <button class="load-more-link"><label>LOAD MORE</label></button>
                            </div>
                        </amp-list-load-more>  
                </amp-list>
            </div>
        </div>
    </div>
</div>
@endsection
