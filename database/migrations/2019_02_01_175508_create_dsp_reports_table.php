<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDspReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dsp_reports', function (Blueprint $table) {
            $table->increments('id');
            $table->datetime('reporting_date');
            $table->datetime('sales_date');
            $table->string('platform');
            $table->integer('id_country');
            $table->string('artis_name', 100);
            $table->string('release_title', 100);
            $table->string('track_title', 100);
            $table->string('upc', 100);
            $table->string('isrc', 100);
            $table->string('release_catalog', 100)->nullable();
            $table->string('release_type', 50)->nullable();
            $table->string('sales_type', 50)->nullable();
            $table->integer('quantity');
            $table->integer('unit_price');
            $table->integer('client_share_rate');
            $table->integer('net_revenue', 50);
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dsp_reports');
    }
}
