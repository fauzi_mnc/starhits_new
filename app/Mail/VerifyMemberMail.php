<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Model\SiteConfig;
use App\Model\User;
use App\Model\VerifyUser;
use Mail;
use Auth;

class VerifyMemberMail extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $email_config = SiteConfig::where('key', 'like', '%'.'email_member'.'%')->get();
        return $this->view('email.registerMemberEmail')->with(['email_config'=> $email_config]);
    }
}