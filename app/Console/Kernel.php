<?php

namespace App\Console;

use App\Console\Commands\UpdateCampaign;
use App\Console\Commands\YoutubeGetAPI;
use App\Console\Commands\SitemapGenerate;
use App\Console\Commands\InstagramGetAPI;
use App\Console\Commands\YoutubeGetAPIWeek;
use App\Console\Commands\YoutubeGetAPIMonthly;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        YoutubeGetAPI::class,
        UpdateCampaign::class,
        SitemapGenerate::class,
        InstagramGetAPI::class,
        YoutubeGetAPIWeek::class,
        YoutubeGetAPIMonthly::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $filePath_YoutubeGetAPI = base_path()."/storage/logs/cron_YoutubeGetAPI.log";
        //Fungsi command untuk data youtube get api
        $schedule->command('YoutubeGetAPI')
            ->timezone('Asia/Jakarta')
            ->dailyAt('00:15')
            ->emailOutputTo('it.starhits@gmail.com');
            //->sendOutputTo($filePath_YoutubeGetAPI)
            //->appendOutputTo($filePath_YoutubeGetAPI);

        $filePath_YoutubeGetAPIWeek = base_path()."/storage/logs/cron_YoutubeGetAPIWeek.log";
        //Fungsi command untuk data youtube get api
        $schedule->command('YoutubeGetAPIWeek')
            ->timezone('Asia/Jakarta')
            ->weekly()
            ->emailOutputTo('it.starhits@gmail.com');
            //->sendOutputTo($filePath_YoutubeGetAPI)
            //->appendOutputTo($filePath_YoutubeGetAPI);

        $filePath_YoutubeGetAPIMonthly = base_path()."/storage/logs/cron_YoutubeGetAPIMonthly.log";
        //Fungsi command untuk data youtube get api
        $schedule->command('YoutubeGetAPIMonthly')
            ->timezone('Asia/Jakarta')
            ->monthly()
            ->emailOutputTo('it.starhits@gmail.com');
            //->sendOutputTo($filePath_YoutubeGetAPI)
            //->appendOutputTo($filePath_YoutubeGetAPI);

        $filePath_UpdateCampaign = base_path()."/storage/logs/cron_UpdateCampaign.log";
       /* $schedule->call(function () {
            DB::table('campaign')->whereIn('status', [1, 2, 3])->whereDate('end_date', '<', Carbon::today())->update(['status' => 4]);
        })->hourlyAt(5);*/
        $schedule->command('UpdateCampaign')
        ->timezone('Asia/Jakarta')
        ->twiceDaily(1, 13)
        ->emailOutputTo('it.starhits@gmail.com');
        //->sendOutputTo($filePath_UpdateCampaign)
        //->appendOutputTo($filePath_UpdateCampaign);

        $filePath_SitemapGenerate = base_path()."/storage/logs/cron_SitemapGenerate.log";
        $schedule->command('SitemapGenerate')
        ->timezone('Asia/Jakarta')
        ->twiceDaily(9, 21)
        ->emailOutputTo('it.starhits@gmail.com');
        //->sendOutputTo($filePath_SitemapGenerate)
        //->appendOutputTo($filePath_SitemapGenerate);

        $filePath_InstagramGetAPI = base_path()."/storage/logs/cron_InstagramGetAPI.log";
        $schedule->command('InstagramGetAPI')
        ->timezone('Asia/Jakarta')
        ->hourlyAt(45)
        ->emailOutputTo('it.starhits@gmail.com');
        //->sendOutputTo($filePath_SitemapGenerate)
        //->appendOutputTo($filePath_SitemapGenerate);

    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
