<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Model\DetailInfluencer;
use App\Model\InfluencerDiagram;

class InstagramGetAPI extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'InstagramGetAPI';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Instagram API';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $detailInfluencers = DetailInfluencer::get();
        foreach ($detailInfluencers as $value) {
            $name = $value->username;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://www.instagram.com/$name/");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $result = curl_exec($ch);
            $http = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);
            if ($http=="200") {
                $doc = new \DOMDocument();
                libxml_use_internal_errors(true);
                $doc->loadHTML($result);
                $xpath = new \DOMXPath($doc);
                $js = $xpath->query('//body/script[@type="text/javascript"]')->item(0)->nodeValue;
                $start = strpos($js, '{');
                $end = strrpos($js, ';');
                $json = substr($js, $start, $end - $start);
                $data = json_decode($json, true);
                //ProfilePage
                $user_id = $data["entry_data"]["ProfilePage"][0]["graphql"]["user"]["id"];
                $username = $data["entry_data"]["ProfilePage"][0]["graphql"]["user"]["username"];
                $user_followers = $data["entry_data"]["ProfilePage"][0]["graphql"]["user"]["edge_followed_by"]["count"];
                $user_pic = $data["entry_data"]["ProfilePage"][0]["graphql"]["user"]["profile_pic_url"];
                $user_pic_hd = $data["entry_data"]["ProfilePage"][0]["graphql"]["user"]["profile_pic_url_hd"];
                $biography =$data["entry_data"]["ProfilePage"][0]["graphql"]["user"]["biography"];
                $external_url =$data["entry_data"]["ProfilePage"][0]["graphql"]["user"]["external_url"];
                $business_category =$data["entry_data"]["ProfilePage"][0]["graphql"]["user"]["business_category_name"];



                $countLike = 0;
                $countComment = 0;
                $totalPost = 3;
                for($i=0; $i<=$totalPost; $i++){                
                    $liked_by = $data["entry_data"]["ProfilePage"][0]["graphql"]["user"]["edge_owner_to_timeline_media"]["edges"][$i]["node"]["edge_liked_by"]["count"];

                    $media_to_comment = $data["entry_data"]["ProfilePage"][0]["graphql"]["user"]["edge_owner_to_timeline_media"]["edges"][$i]["node"]["edge_media_to_comment"]["count"];
                    $countLike= $countLike+$liked_by;
                    $countComment = $countComment + $media_to_comment;
                }

                
                $avgLike = (float)$countLike/$totalPost;
                $avgComment = (float)$countComment/$totalPost;
                $engagement = (($avgLike+$avgComment)/$user_followers)*100;

                $this->info(var_dump($json));
                $value->where('username', $username)->update([
                    'followers_ig' => $user_followers,
                    'profile_picture_ig' => $user_pic_hd,
                    'countlike' => $countLike,
                    'countcomment' => $countComment,
                    'avglike' => $avgLike,
                    'avgcomment' => $avgComment,
                    'engagement' => $engagement,
                    'biography' => $biography,
                    'external_url' => $external_url,
                    'business_category_name' => $business_category,
                ]);


                $insert = new InfluencerDiagram();
                $insert->user_id = $value->user_id;
                $insert->followers_ig = $user_followers;
                $insert->avglike = $avgLike;
                $insert->avgcomment = $avgComment;
                $insert->engagement = $engagement;
                $insert->save();
                
            }//http 200


        }
    }
}
