<?php

namespace App\Console\Commands;

use App\Model\Content;
use App\Model\User;
use App\Model\Video;
use App\Model\YoutubeAnalitic;
use App\Model\YoutubeAnaliticLink;
use Illuminate\Console\Command;
use Youtube;
use App\Http\Controllers\Backend\YoutubeController;
use App\Http\Controllers\Backend\GoogleSheetController;

class YoutubeGetAPI extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'YoutubeGetAPI';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Youtube API';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(YoutubeController $youtubeAnalitic, GoogleSheetController $googleSheet)
    {
        parent::__construct();
        $this->youtubeAnalitic = $youtubeAnalitic;
        $this->googleSheet = $googleSheet;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $singleCreators = User::get();

        foreach ($singleCreators as $value) {
            if (!is_null($value->provider_id)) {
                if(empty(Youtube::getChannelById($value->provider_id))){
                    $channel = Youtube::getChannelByName($value->provider_id);
                }else{
                    $channel = Youtube::getChannelById($value->provider_id);
                }
                
                if (!empty($channel)) {
                    $value->update(
                        [   
                            'name'          => $channel->snippet->title,
                            'name_master'   => $channel->snippet->title,
                            'name_channel'  => $channel->snippet->title,
                            'subscribers'   => $channel->statistics->subscriberCount,
                            'provider_id'   => $channel->id
                        ]
                    );
                }
            }
        }

        $youtubeAnalitics = YoutubeAnalitic::get();

        foreach ($youtubeAnalitics as $valueA) {
            if (!is_null($valueA->provider_id)) {
                if(empty(Youtube::getChannelById($valueA->provider_id))){
                    $channelA = Youtube::getChannelByName($valueA->provider_id);
                }else{
                    $channelA = Youtube::getChannelById($valueA->provider_id);
                }
                
                if (!empty($channelA)) {
                    $valueA->update(
                        [   
                            'name'          => $channelA->snippet->title,
                            'name_channel'  => $channelA->snippet->title
                        ]
                    );
                }
            }
        }

        $singleVideos = Video::where('attr_5', '=', 'youtube')->get();
        $singleVideos->map(function ($sing) {
            $youtube  = Youtube::getVideoInfo($sing->attr_6);
            if($youtube){
                // $this->info($sing->id);
                //$this->info(var_dump($youtube));
                if (!empty($youtube->statistics)) {
                    Content::where('id', $sing->id)
                        ->update(
                            [
                                'viewed' => isset($youtube->statistics->viewCount) ? $youtube->statistics->viewCount:0,
                                'attr_2' => isset($youtube->statistics->likeCount) ? $youtube->statistics->likeCount:0,
                                'attr_3' => isset($youtube->statistics->dislikeCount) ? $youtube->statistics->dislikeCount:0,
                                'attr_4' => isset($youtube->statistics->commentCount) ? $youtube->statistics->commentCount:0,
                            ]
                        );
                }
            }

            $youtube2 = Youtube::getVideoInfo($sing->attr_6);
            if($youtube){
                if (!empty($youtube2->contentDetails)) {
                    Content::where('id', $sing->id)
                        ->update(
                            [
                                'attr_7' => $youtube2->contentDetails->duration,
                            ]
                        );
                }
            }
        });

        $date = date("Y-m");
        $getFileID = YoutubeAnaliticLink::where('created_at', 'like', '%'.$date.'%')->first();

        $values = $this->googleSheet->updateEntry($getFileID->fileid);
    }
}
