<?php

namespace App\Console\Commands;

use App\Model\Content;
use App\Model\User;
use App\Model\Video;
use App\Model\YoutubeAnalitic;
use App\Model\YoutubeAnaliticLink;

use Illuminate\Console\Command;
use Youtube;
use App\Http\Controllers\Backend\YoutubeController;
use App\Http\Controllers\Backend\GoogleSheetController;


class YoutubeGetAPIWeek extends Command
{

    protected $signature = 'YoutubeGetAPIWeek';
    protected $description = 'Youtube API Weekly';
    protected $youtubeAnalitic;
    protected $googleSheet;

    public function __construct(YoutubeController $youtubeAnalitic, GoogleSheetController $googleSheet)
    {
        parent::__construct();
        $this->youtubeAnalitic = $youtubeAnalitic;
        $this->googleSheet = $googleSheet;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $singleCreators = User::where('provider_id', '!=', '')->orWhereNotNull('provider_id')->orderBy('unit','asc')->get();

        foreach ($singleCreators as $value) {
            if (!is_null($value->provider_id)) {
                if(empty(Youtube::getChannelById($value->provider_id))){
                    $channel = Youtube::getChannelByName($value->provider_id);
                }else{
                    $channel = Youtube::getChannelById($value->provider_id);
                }
                var_dump($channel);
                if (!empty($channel)) {
                    $viewer = Content::where('user_id', $value->id)->count('viewed');
                    $like_count = Content::where('user_id', $value->id)->count('attr_2');
                    $dislike_count = Content::where('user_id', $value->id)->count('attr_3');
                    $comment_count = Content::where('user_id', $value->id)->count('attr_4');

                    $insertt = YoutubeAnalitic::insert([   
                        'user_id'       => $value->id,
                        'name'          => $channel->snippet->title,
                        'name_channel'  => $channel->snippet->title,
                        'subscribers'   => $channel->statistics->subscriberCount,
                        'viewer'        => $viewer,
                        'like_count'    => $like_count,
                        'dislike_count' => $dislike_count,
                        'comment_count' => $comment_count,
                        'created_at'    => date("Y-m-d H:i:s"),
                        'updated_at'    => date("Y-m-d H:i:s")
                    ]);
                   //var_dump($insertt);
                }
            }
        }
        $date = date("Y-m");
        $getFileID = YoutubeAnaliticLink::where('created_at', 'like', '%'.$date.'%')->first();

        $values = $this->googleSheet->updateEntry($getFileID->fileid);

    }
}
