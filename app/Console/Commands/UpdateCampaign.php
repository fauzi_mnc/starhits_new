<?php

namespace App\Console\Commands;

use App\Model\Campaign;
use Illuminate\Console\Command;
use Carbon;

class UpdateCampaign extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'UpdateCampaign';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Campaign';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Campaign::whereIn('status', [1, 2, 3])->whereDate('end_date', '<', Carbon::today())->update(['status' => 4]);
    }
}
