<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Spatie\Sitemap\SitemapGenerator;

class SitemapGenerate extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'SitemapGenerate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate the sitemap';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // modify this to your own needs
        $path = base_path().'/public/sitemap.xml';
        SitemapGenerator::create('https://starhits.id')->writeToFile($path);
    }
}
