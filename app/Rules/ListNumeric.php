<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ListNumeric implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        foreach($value as $revenue_creator){
            $data = explode(',', $revenue_creator);

            if(is_numeric($data[1]) || is_numeric($data[2])){
                return true;
            }
            
            return false;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Estimate Gross Revenue And Cost Production must be an integer.';
    }
}
