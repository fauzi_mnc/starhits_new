<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class BrandEmail implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (!empty($value)) {
            $one = explode('@', $value);
            $two = explode('.', $one[1]);
            if(($two[0] != 'gmail') && ($two[0] != 'hotmail') && ($two[0] != 'yahoo')){
                return true;
            }
        }
        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Can not use gmail, hotmail, and yahoo.';
    }
}
