<?php

namespace App\Providers;

use App\DataTables\Util\Request;
use App\Model\Ads;
use App\Model\Channel;
use App\Model\Page;
use App\Model\SiteConfig;
use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        config(['app.locale' => 'id']);
        \Carbon\Carbon::setLocale('id');
        
        Schema::defaultStringLength(191);

        $globalPage = Page::where(function ($query) {
            $query->where('is_active', '=', '1')
                ->where('type', '=', 'page');
        })->get();

        $adsOnTopVideo = Ads::where(function ($query) {
            $query->where('status', '=', '1')
                ->where('position', '=', '0')
                ->where('start_date', '<=', Carbon::now())
                ->where('end_date', '>=', Carbon::now());
        })->inRandomOrder()->limit(2)->get();

        $adsInsideHeaders = Ads::where(function ($query) {
            $query->where('status', '=', '1')
                ->where('position', '=', '1')
                ->where('start_date', '<=', Carbon::now())
                ->where('end_date', '>=', Carbon::now());
        })->inRandomOrder()->limit(2)->get();

        $adsRightContentI = Ads::where(function ($query) {
            $query->where('status', '=', '1')
                ->where('position', '=', '3')
                ->where('start_date', '<=', Carbon::now())
                ->where('end_date', '>=', Carbon::now());
        })->inRandomOrder()->limit(1)->get();

        $adsRightContentII = Ads::where(function ($query) {
            $query->where('status', '=', '1')
                ->where('position', '=', '4')
                ->where('start_date', '<=', Carbon::now())
                ->where('end_date', '>=', Carbon::now());
        })->inRandomOrder()->limit(1)->get();

        $adsUnderContent = Ads::where(function ($query) {
            $query->Where('status', '=', '1')
                ->where('position', '=', '2')
                ->where('start_date', '<=', Carbon::now())
                ->where('end_date', '>=', Carbon::now());
        })->inRandomOrder()->limit(1)->get();

        $configs = SiteConfig::get();

        $channel = Channel::where('is_active', '=', '1')->get();
        
        $bind                      = array();
        $bind['pages']             = $globalPage;
        $bind['adsInsideHeaders']  = $adsInsideHeaders;
        $bind['adsRightContentI']  = $adsRightContentI;
        $bind['adsRightContentII'] = $adsRightContentII;
        $bind['adsUnderContent']   = $adsUnderContent;
        $bind['adsOnTopVideo']     = $adsOnTopVideo;
        $bind['_channel']           = $channel;
        
        $bind['configs'] = [];
        if (sizeof($configs) > 0) {
            foreach ($configs as $key => $value) {
                $bind['configs'][$value->key] = $value->value;
            }
        }

        View::share('SiteConfig', $bind);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('datatables.request', function () {
            return new Request;
        });
    }
}
