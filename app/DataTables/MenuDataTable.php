<?php

namespace App\DataTables;

use App\Model\Menu;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class MenuDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('parent', function($m){
                            return $m->parent? $m->parent->title : '';
                        })->addColumn('position', function($m){
                            return $m->position? 'Bottom' : 'Top';
                        })->addColumn('is_active', function($m){
                            return $m->is_active? 'Ya' : 'Tidak';
                        })->addColumn('action', 'menu.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Post $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Menu $model)
    {
        return $model->with('parent')->newQuery()->orderBy('updated_at', 'desc');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '80px'])
            ->parameters([
                'dom'     => 'Bfrtlip',
                'order'   => [[0, 'desc']],
                'responsive' => true,
                'autoWidth' => false,
                'buttons' => [
                    [ 
                        "extend" => 'create', 
                        "text" => '<i class="fa fa-plus"></i> Add Menu Parent',
                        "className" => 'btn-primary',
                        'action' => 'function( e, dt, button, config){ 
                            window.location = "menu/create-parent";
                        }'
                    ],
                    [ 
                        "extend" => 'create', 
                        "text" => '<i class="fa fa-plus"></i> Add Menu Child',
                        "className" => 'btn-primary',
                        'action' => 'function( e, dt, button, config){ 
                            window.location = "menu/create-child";
                        }'
                    ],
                    [ 
                        "extend" => 'reset', 
                        "text" => '<i class="fa fa-undo"></i> Reset',
                        "className" => 'btn-primary'
                    ],
                    [ 
                        "extend" => 'reload', 
                        "text" => '<i class="fa fa-refresh"></i> Reload',
                        "className" => 'btn-primary'
                    ]
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'title',
            'parent',
            'url',
            'position',
            'is_active',
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'menu_datatable_' . time();
    }
}