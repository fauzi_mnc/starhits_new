<?php

namespace App\DataTables;

use App\Model\User;
use App\Model\CampaignPost;
use App\Model\CampaignCategory;
use App\Model\Category;
use App\Model\Campaign;
use App\Model\Influencer;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Illuminate\Http\Request;
use Auth;

class OInfluencerDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);
        $campaign = Campaign::where('camp_id', request()->segment(3))->first();
        
        if (Auth::user()->roles->first()->name == 'admin') {
            return $dataTable->addColumn('select', function($m){
                return '<a href="/admin/campaign/'.$m->id.'/'.request()->segment(3).'/addtotable" class="add"><i class="fa fa-check text-navy"></i></a>';
            })->addColumn('instagram_video_posting_rate', function($m){
                return $m->detailInfluencer->instagram_video_posting_rate;
            })->addColumn('instagram_story_posting_rate', function($m){
                return $m->detailInfluencer->instagram_story_posting_rate;
            })->addColumn('instagram_photo_posting_rate', function($m){
                return $m->detailInfluencer->instagram_photo_posting_rate;
            })->addColumn('youtube_posting_rate', function($m){
                return $m->detailInfluencer->youtube_posting_rate;
            })->addColumn('ig_highlight_rate', function($m){
                return $m->detailInfluencer->ig_highlight_rate;
            })->addColumn('package_rate', function($m){
                return $m->detailInfluencer->package_rate;
            })->rawColumns(['select']);
        }else{
            return $dataTable->addColumn('select', function($m) use ($campaign){
                if($campaign->status != 3){
                return '<a href="/brand/campaign/'.$m->id.'/'.request()->segment(3).'/addtotable" class="add"><i class="fa fa-check text-navy"></i></a>';
                }else{
                    return '';
                }
            })->addColumn('instagram_video_posting_rate', function($m){
                return $m->detailInfluencer->instagram_video_posting_rate;
            })->addColumn('instagram_story_posting_rate', function($m){
                return $m->detailInfluencer->instagram_story_posting_rate;
            })->addColumn('instagram_photo_posting_rate', function($m){
                return $m->detailInfluencer->instagram_photo_posting_rate;
            })->addColumn('youtube_posting_rate', function($m){
                return $m->detailInfluencer->youtube_posting_rate;
            })->addColumn('ig_highlight_rate', function($m){
                return $m->detailInfluencer->ig_highlight_rate;
            })->addColumn('package_rate', function($m){
                return $m->detailInfluencer->package_rate;
            })->rawColumns(['select']);
        }
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Posts $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(User $model)
    {
        $post = ['', 'instagram_video_posting_rate', 'instagram_story_posting_rate', 'instagram_photo_posting_rate', 'youtube_posting_rate', 'ig_highlight_rate', 'package_rate'];
        $camp = CampaignPost::where('campaign_id', request()->segment(3))
        ->get()
        ->pluck('post_id')
        ->toArray();
        $get_ca = CampaignCategory::where('campaign_id', request()->segment(3))->pluck('category_id')->toArray();
        $get_cat = Category::whereIn('id', $get_ca)->get();
        $cf = Campaign::where('camp_id', request()->segment(3))->first();
        $cff = Campaign::where([['category_id', $cf->category_id], ['type', '=', '1'], ['status', '=', '3']])->get()->pluck('camp_id')->toArray();
        $if = Influencer::whereIn('campaign_id', $cff)->get()->pluck('user_id')->toArray();
        $filter = [];
        foreach ($camp as $val) {
            $filter[] = $post[$val];
        }

        return User::where('is_active', 1)->whereNotIn('id', $if)
            ->with('accessrole')
            ->whereHas('accessrole', function($query){
                $query->where('role_id', '6');
            })->with('detailInfluencer')
            ->whereHas('detailInfluencer', function($query) use ($filter, $get_cat){
                foreach ($filter as $test) {
                    $query->where($test, '!=', NULL)->where($test, '>', '0');   
                }
            })
            ->whereHas('detailInfluencer', function ($q) use ($get_cat){
                        for ($i=0; $i < sizeof($get_cat); $i++) {
                            $q->orWhere('category', 'like', '%'.$get_cat[$i]->title.'%');
                        }
                    });
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->parameters([
                'dom'     => 'Bfrtip',
                'order'   => [[0, 'desc']],
                'responsive' => true,
                'autoWidth' => false,
                'buttons' => [
                    // 'create',
                    // 'export',
                    // 'print',
                    'reset',
                    'reload',
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'name' => ['title' => 'Influencer'],
            'engagement_rate',
            'instagram_video_posting_rate',
            'instagram_story_posting_rate',
            'instagram_photo_posting_rate',
            'youtube_posting_rate',
            'ig_highlight_rate',
            'package_rate',
            'select'
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'influencer_datatable' . time();
    }
}