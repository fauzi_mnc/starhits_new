<?php

namespace App\DataTables;

use App\Model\Revenue;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use App\Enum\RevenueStatus;

class RevenueDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);
        return $dataTable ->addColumn('month', function($m){
                                $monthName = date('F', mktime(0, 0, 0, $m->month, 10)); // March
                                return $monthName; 
                            })
                        
                        ->addColumn('status', function($m){ return RevenueStatus::getText($m->status);})
                        ->orderColumns(['month', 'status'], '-:column $1')
                        ->addColumn('action', 'revenue.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Post $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Revenue $model)
    {
        $query = $model->newQuery();
        if(request()->month){
            $query->where('month', request()->month);
            $query->orderBy('updated_at', 'desc');
        }

        return $query;

    }

    /*public function query(Revenue $model)
    {
        return $model->newQuery();
    }*/

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->ajax([
                'url'  => '',
                'data' => "function(d) {
                    d.month  = $('#month').val();
                }",
            ])
            ->addAction(['width' => '80px'])
            ->parameters([
                'dom'     => 'Bfrtip',
                'order'   => [[1, 'desc'],[0, 'asc']],
                'responsive' => true,
                'autoWidth' => false,
                'buttons' => [
                   //      'create',
                   //      'export',
                   //      'print',
                   //      'reset',
                   //      'reload',
                ],
            ]);
    }


    /*public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px'])
            ->parameters([
                'dom'     => 'Bfrtip',
                'order'   => [[0, 'desc']],
                'buttons' => [
                    // 'create',
                    // 'export',
                    // 'print',
                    'reset',
                    'reload',
                ],
            ]);
    }*/
    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'month',
            'year',
            'status',
        ];
    }

}