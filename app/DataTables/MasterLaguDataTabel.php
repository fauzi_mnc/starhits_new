<?php

namespace App\DataTables;

use App\Model\MasterLagu;
use App\Model\User;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Illuminate\Http\Request;
use Auth;

class MasterLaguDataTabel extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);
        if (Auth::user()->roles->first()->name == 'admin' || Auth::user()->roles->first()->name == 'legal') {
            return $dataTable
            ->addColumn('judul', function($m){
                return '<a href="'.route("masterlagu.detail.lagu", $m->id).'">'.$m->track_title.'</a>';
            })
            ->addColumn('album', function($m){
                return $m->release_title;
            })
            ->addColumn('tanggal release', function($m){
                if($m->release_date == '0000-00-00'){
                    $daterelease = '';
                }else{
                    $daterelease = date('d-m-Y', strtotime($m->release_date));
                }
                return $daterelease;
            })
            ->addColumn('penyanyi', function($m){
                if(!empty($m->rolelagu[0]->penyanyi->name_master) AND empty($m->rolelagu[1]->penyanyi->name_master) AND empty($m->rolelagu[2]->penyanyi->name_master) AND empty($m->rolelagu[3]->penyanyi->name_master) AND empty($m->rolelagu[4]->penyanyi->name_master) AND empty($m->rolelagu[5]->penyanyi->name_master)){
                
                    return '<a href="'.route("masterlagu.detail.singer", $m->rolelagu[0]->penyanyi->id).'">'.$m->rolelagu[0]->penyanyi->name_master.'</a>';

                }elseif(empty($m->rolelagu[0]->penyanyi->name_master) AND !empty($m->rolelagu[1]->penyanyi->name_master) AND empty($m->rolelagu[2]->penyanyi->name_master) AND empty($m->rolelagu[3]->penyanyi->name_master) AND empty($m->rolelagu[4]->penyanyi->name_master) AND empty($m->rolelagu[5]->penyanyi->name_master)){
                    
                    return '<a href="'.route("masterlagu.detail.singer", $m->rolelagu[1]->penyanyi->id).'">'.$m->rolelagu[1]->penyanyi->name_master.'</a>';

                }elseif(empty($m->rolelagu[0]->penyanyi->name_master) AND empty($m->rolelagu[1]->penyanyi->name_master) AND !empty($m->rolelagu[2]->penyanyi->name_master) AND empty($m->rolelagu[3]->penyanyi->name_master) AND empty($m->rolelagu[4]->penyanyi->name_master) AND empty($m->rolelagu[5]->penyanyi->name_master)){
                    
                    return '<a href="'.route("masterlagu.detail.singer", $m->rolelagu[2]->penyanyi->id).'">'.$m->rolelagu[2]->penyanyi->name_master.'</a>';
                
                }elseif(empty($m->rolelagu[0]->penyanyi->name_master) AND empty($m->rolelagu[1]->penyanyi->name_master) AND empty($m->rolelagu[2]->penyanyi->name_master) AND !empty($m->rolelagu[3]->penyanyi->name_master) AND empty($m->rolelagu[4]->penyanyi->name_master) AND empty($m->rolelagu[5]->penyanyi->name_master)){
                    
                    return '<a href="'.route("masterlagu.detail.singer", $m->rolelagu[3]->penyanyi->id).'">'.$m->rolelagu[3]->penyanyi->name_master.'</a>';
                
                }elseif(empty($m->rolelagu[0]->penyanyi->name_master) AND empty($m->rolelagu[1]->penyanyi->name_master) AND empty($m->rolelagu[2]->penyanyi->name_master) AND empty($m->rolelagu[3]->penyanyi->name_master) AND !empty($m->rolelagu[4]->penyanyi->name_master) AND empty($m->rolelagu[5]->penyanyi->name_master)){
                    
                    return '<a href="'.route("masterlagu.detail.singer", $m->rolelagu[4]->penyanyi->id).'">'.$m->rolelagu[4]->penyanyi->name_master.'</a>';

                }elseif(empty($m->rolelagu[0]->penyanyi->name_master) AND empty($m->rolelagu[1]->penyanyi->name_master) AND empty($m->rolelagu[2]->penyanyi->name_master) AND empty($m->rolelagu[3]->penyanyi->name_master) AND empty($m->rolelagu[4]->penyanyi->name_master) AND !empty($m->rolelagu[5]->penyanyi->name_master)){
                    
                    return '<a href="'.route("masterlagu.detail.singer", $m->rolelagu[5]->penyanyi->id).'">'.$m->rolelagu[5]->penyanyi->name_master.'</a>';

                }elseif(!empty($m->rolelagu[0]->penyanyi->name_master) AND !empty($m->rolelagu[1]->penyanyi->name_master) AND empty($m->rolelagu[2]->penyanyi->name_master) AND empty($m->rolelagu[3]->penyanyi->name_master) AND empty($m->rolelagu[4]->penyanyi->name_master) AND empty($m->rolelagu[5]->penyanyi->name_master)){

                    return '<a href="'.route("masterlagu.detail.singer", $m->rolelagu[0]->penyanyi->id).'">'.$m->rolelagu[0]->penyanyi->name_master.'</a> & <a href="'.route("masterlagu.detail.singer", $m->rolelagu[1]->penyanyi->id).'">'.$m->rolelagu[1]->penyanyi->name_master.'</a>';

                }elseif(!empty($m->rolelagu[0]->penyanyi->name_master) AND !empty($m->rolelagu[1]->penyanyi->name_master) AND !empty($m->rolelagu[2]->penyanyi->name_master) AND empty($m->rolelagu[3]->penyanyi->name_master) AND empty($m->rolelagu[4]->penyanyi->name_master) AND empty($m->rolelagu[5]->penyanyi->name_master)){

                    return '<a href="'.route("masterlagu.detail.singer", $m->rolelagu[0]->penyanyi->id).'">'.$m->rolelagu[0]->penyanyi->name_master.'</a> & <a href="'.route("masterlagu.detail.singer", $m->rolelagu[1]->penyanyi->id).'">'.$m->rolelagu[1]->penyanyi->name_master.'</a> & <a href="'.route("masterlagu.detail.singer", $m->rolelagu[2]->penyanyi->id).'">'.$m->rolelagu[2]->penyanyi->name_master.'</a>';

                }elseif(!empty($m->rolelagu[0]->penyanyi->name_master) AND !empty($m->rolelagu[1]->penyanyi->name_master) AND !empty($m->rolelagu[2]->penyanyi->name_master) AND !empty($m->rolelagu[3]->penyanyi->name_master) AND empty($m->rolelagu[4]->penyanyi->name_master) AND empty($m->rolelagu[5]->penyanyi->name_master)){

                    return '<a href="'.route("masterlagu.detail.singer", $m->rolelagu[0]->penyanyi->id).'">'.$m->rolelagu[0]->penyanyi->name_master.'</a> & <a href="'.route("masterlagu.detail.singer", $m->rolelagu[1]->penyanyi->id).'">'.$m->rolelagu[1]->penyanyi->name_master.'</a> & <a href="'.route("masterlagu.detail.singer", $m->rolelagu[2]->penyanyi->id).'">'.$m->rolelagu[2]->penyanyi->name_master.'</a> & <a href="'.route("masterlagu.detail.singer", $m->rolelagu[3]->penyanyi->id).'">'.$m->rolelagu[3]->penyanyi->name_master.'</a>';

                }elseif(!empty($m->rolelagu[0]->penyanyi->name_master) AND !empty($m->rolelagu[1]->penyanyi->name_master) AND !empty($m->rolelagu[2]->penyanyi->name_master) AND !empty($m->rolelagu[3]->penyanyi->name_master) AND !empty($m->rolelagu[4]->penyanyi->name_master) AND empty($m->rolelagu[5]->penyanyi->name_master)){

                    return '<a href="'.route("masterlagu.detail.singer", $m->rolelagu[0]->penyanyi->id).'">'.$m->rolelagu[0]->penyanyi->name_master.'</a> & <a href="'.route("masterlagu.detail.singer", $m->rolelagu[1]->penyanyi->id).'">'.$m->rolelagu[1]->penyanyi->name_master.'</a> & <a href="'.route("masterlagu.detail.singer", $m->rolelagu[2]->penyanyi->id).'">'.$m->rolelagu[2]->penyanyi->name_master.'</a> & <a href="'.route("masterlagu.detail.singer", $m->rolelagu[3]->penyanyi->id).'">'.$m->rolelagu[3]->penyanyi->name_master.'</a> & <a href="'.route("masterlagu.detail.singer", $m->rolelagu[4]->penyanyi->id).'">'.$m->rolelagu[4]->penyanyi->name_master.'</a>';

                }elseif(!empty($m->rolelagu[0]->penyanyi->name_master) AND !empty($m->rolelagu[1]->penyanyi->name_master) AND !empty($m->rolelagu[2]->penyanyi->name_master) AND !empty($m->rolelagu[3]->penyanyi->name_master) AND !empty($m->rolelagu[4]->penyanyi->name_master) AND !empty($m->rolelagu[5]->penyanyi->name_master)){
                    
                    return '<a href="'.route("masterlagu.detail.singer", $m->rolelagu[0]->penyanyi->id).'">'.$m->rolelagu[0]->penyanyi->name_master.'</a> & <a href="'.route("masterlagu.detail.singer", $m->rolelagu[1]->penyanyi->id).'">'.$m->rolelagu[1]->penyanyi->name_master.'</a> & <a href="'.route("masterlagu.detail.singer", $m->rolelagu[2]->penyanyi->id).'">'.$m->rolelagu[2]->penyanyi->name_master.'</a> & <a href="'.route("masterlagu.detail.singer", $m->rolelagu[3]->penyanyi->id).'">'.$m->rolelagu[3]->penyanyi->name_master.'</a> & <a href="'.route("masterlagu.detail.singer", $m->rolelagu[4]->penyanyi->id).'">'.$m->rolelagu[4]->penyanyi->name_master.'</a> & <a href="'.route("masterlagu.detail.singer", $m->rolelagu[5]->penyanyi->id).'">'.$m->rolelagu[5]->penyanyi->name_master.'</a>';

                }else{
                    return '&nbsp;';
                }
            })
            ->addColumn('pencipta', function($m){
                if(!empty($m->rolelagu[0]->pencipta->name_master) && empty($m->rolelagu[1]->pencipta->name_master) && empty($m->rolelagu[2]->pencipta->name_master) && empty($m->rolelagu[3]->pencipta->name_master) && empty($m->rolelagu[4]->pencipta->name_master) && empty($m->rolelagu[5]->pencipta->name_master) && empty($m->rolelagu[6]->pencipta->name_master) && empty($m->rolelagu[7]->pencipta->name_master) && empty($m->rolelagu[8]->pencipta->name_master) && empty($m->rolelagu[9]->pencipta->name_master)){
                
                    return '<a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[0]->pencipta->id).'">'.$m->rolelagu[0]->pencipta->name_master.'</a>';

                }elseif(!empty($m->rolelagu[0]->pencipta->name_master) && !empty($m->rolelagu[1]->pencipta->name_master) && empty($m->rolelagu[2]->pencipta->name_master) && empty($m->rolelagu[3]->pencipta->name_master) && empty($m->rolelagu[4]->pencipta->name_master) && empty($m->rolelagu[5]->pencipta->name_master) && empty($m->rolelagu[6]->pencipta->name_master) && empty($m->rolelagu[7]->pencipta->name_master) && empty($m->rolelagu[8]->pencipta->name_master) && empty($m->rolelagu[9]->pencipta->name_master)){
                
                    return '<a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[0]->pencipta->id).'">'.$m->rolelagu[0]->pencipta->name_master.'</a> & <a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[1]->pencipta->id).'">'.$m->rolelagu[1]->pencipta->name_master.'</a>';

                }elseif(!empty($m->rolelagu[0]->pencipta->name_master) && !empty($m->rolelagu[1]->pencipta->name_master) && !empty($m->rolelagu[2]->pencipta->name_master) && empty($m->rolelagu[3]->pencipta->name_master) && empty($m->rolelagu[4]->pencipta->name_master) && empty($m->rolelagu[5]->pencipta->name_master) && empty($m->rolelagu[6]->pencipta->name_master) && empty($m->rolelagu[7]->pencipta->name_master) && empty($m->rolelagu[8]->pencipta->name_master) && empty($m->rolelagu[9]->pencipta->name_master)){
                
                    return '<a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[0]->pencipta->id).'">'.$m->rolelagu[0]->pencipta->name_master.'</a> & <a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[1]->pencipta->id).'">'.$m->rolelagu[1]->pencipta->name_master.'</a> & <a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[2]->pencipta->id).'">'.$m->rolelagu[2]->pencipta->name_master.'</a>';

                }elseif(!empty($m->rolelagu[0]->pencipta->name_master) && !empty($m->rolelagu[1]->pencipta->name_master) && !empty($m->rolelagu[2]->pencipta->name_master) && !empty($m->rolelagu[3]->pencipta->name_master) && empty($m->rolelagu[4]->pencipta->name_master) && empty($m->rolelagu[5]->pencipta->name_master) && empty($m->rolelagu[6]->pencipta->name_master) && empty($m->rolelagu[7]->pencipta->name_master) && empty($m->rolelagu[8]->pencipta->name_master) && empty($m->rolelagu[9]->pencipta->name_master)){
                
                    return '<a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[0]->pencipta->id).'">'.$m->rolelagu[0]->pencipta->name_master.'</a> & <a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[1]->pencipta->id).'">'.$m->rolelagu[1]->pencipta->name_master.'</a> & <a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[2]->pencipta->id).'">'.$m->rolelagu[2]->pencipta->name_master.'</a> & <a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[3]->pencipta->id).'">'.$m->rolelagu[3]->pencipta->name_master.'</a>';
                
                }elseif(!empty($m->rolelagu[0]->pencipta->name_master) && !empty($m->rolelagu[1]->pencipta->name_master) && !empty($m->rolelagu[2]->pencipta->name_master) && !empty($m->rolelagu[3]->pencipta->name_master) && !empty($m->rolelagu[4]->pencipta->name_master) && empty($m->rolelagu[5]->pencipta->name_master) && empty($m->rolelagu[6]->pencipta->name_master) && empty($m->rolelagu[7]->pencipta->name_master) && empty($m->rolelagu[8]->pencipta->name_master) && empty($m->rolelagu[9]->pencipta->name_master)){
                
                    return '<a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[0]->pencipta->id).'">'.$m->rolelagu[0]->pencipta->name_master.'</a> & <a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[1]->pencipta->id).'">'.$m->rolelagu[1]->pencipta->name_master.'</a> & <a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[2]->pencipta->id).'">'.$m->rolelagu[2]->pencipta->name_master.'</a> & <a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[3]->pencipta->id).'">'.$m->rolelagu[3]->pencipta->name_master.'</a> & <a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[4]->pencipta->id).'">'.$m->rolelagu[4]->pencipta->name_master.'</a>';

                }elseif(empty($m->rolelagu[0]->pencipta->name_master) && !empty($m->rolelagu[1]->pencipta->name_master) && empty($m->rolelagu[2]->pencipta->name_master) && empty($m->rolelagu[3]->pencipta->name_master) && empty($m->rolelagu[4]->pencipta->name_master) && empty($m->rolelagu[5]->pencipta->name_master) && empty($m->rolelagu[6]->pencipta->name_master) && empty($m->rolelagu[7]->pencipta->name_master) && empty($m->rolelagu[8]->pencipta->name_master) && empty($m->rolelagu[9]->pencipta->name_master)){
                
                    return '<a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[1]->pencipta->id).'">'.$m->rolelagu[1]->pencipta->name_master.'</a>';

                }elseif(empty($m->rolelagu[0]->pencipta->name_master) && !empty($m->rolelagu[1]->pencipta->name_master) && !empty($m->rolelagu[2]->pencipta->name_master) && empty($m->rolelagu[3]->pencipta->name_master) && empty($m->rolelagu[4]->pencipta->name_master) && empty($m->rolelagu[5]->pencipta->name_master) && empty($m->rolelagu[6]->pencipta->name_master) && empty($m->rolelagu[7]->pencipta->name_master) && empty($m->rolelagu[8]->pencipta->name_master) && empty($m->rolelagu[9]->pencipta->name_master)){
                
                    return '<a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[1]->pencipta->id).'">'.$m->rolelagu[1]->pencipta->name_master.'</a> & <a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[2]->pencipta->id).'">'.$m->rolelagu[2]->pencipta->name_master.'</a>';

                }elseif(empty($m->rolelagu[0]->pencipta->name_master) && !empty($m->rolelagu[1]->pencipta->name_master) && !empty($m->rolelagu[2]->pencipta->name_master) && !empty($m->rolelagu[3]->pencipta->name_master) && empty($m->rolelagu[4]->pencipta->name_master) && empty($m->rolelagu[5]->pencipta->name_master) && empty($m->rolelagu[6]->pencipta->name_master) && empty($m->rolelagu[7]->pencipta->name_master) && empty($m->rolelagu[8]->pencipta->name_master) && empty($m->rolelagu[9]->pencipta->name_master)){
                
                    return '<a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[1]->pencipta->id).'">'.$m->rolelagu[1]->pencipta->name_master.'</a> & <a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[2]->pencipta->id).'">'.$m->rolelagu[2]->pencipta->name_master.'</a> & <a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[3]->pencipta->id).'">'.$m->rolelagu[3]->pencipta->name_master.'</a>';

                }elseif(empty($m->rolelagu[0]->pencipta->name_master) && !empty($m->rolelagu[1]->pencipta->name_master) && !empty($m->rolelagu[2]->pencipta->name_master) && !empty($m->rolelagu[3]->pencipta->name_master) && !empty($m->rolelagu[4]->pencipta->name_master) && empty($m->rolelagu[5]->pencipta->name_master) && empty($m->rolelagu[6]->pencipta->name_master) && empty($m->rolelagu[7]->pencipta->name_master) && empty($m->rolelagu[8]->pencipta->name_master) && empty($m->rolelagu[9]->pencipta->name_master)){
                
                    return '<a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[1]->pencipta->id).'">'.$m->rolelagu[1]->pencipta->name_master.'</a> & <a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[2]->pencipta->id).'">'.$m->rolelagu[2]->pencipta->name_master.'</a> & <a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[3]->pencipta->id).'">'.$m->rolelagu[3]->pencipta->name_master.'</a> & <a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[4]->pencipta->id).'">'.$m->rolelagu[4]->pencipta->name_master.'</a>';
                
                }elseif(empty($m->rolelagu[0]->pencipta->name_master) && !empty($m->rolelagu[1]->pencipta->name_master) && !empty($m->rolelagu[2]->pencipta->name_master) && !empty($m->rolelagu[3]->pencipta->name_master) && !empty($m->rolelagu[4]->pencipta->name_master) && !empty($m->rolelagu[5]->pencipta->name_master) && empty($m->rolelagu[6]->pencipta->name_master) && empty($m->rolelagu[7]->pencipta->name_master) && empty($m->rolelagu[8]->pencipta->name_master) && empty($m->rolelagu[9]->pencipta->name_master)){
                
                    return '<a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[1]->pencipta->id).'">'.$m->rolelagu[1]->pencipta->name_master.'</a> & <a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[2]->pencipta->id).'">'.$m->rolelagu[2]->pencipta->name_master.'</a> & <a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[3]->pencipta->id).'">'.$m->rolelagu[3]->pencipta->name_master.'</a> & <a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[4]->pencipta->id).'">'.$m->rolelagu[4]->pencipta->name_master.'</a> & <a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[5]->pencipta->id).'">'.$m->rolelagu[5]->pencipta->name_master.'</a>';

                }elseif(empty($m->rolelagu[0]->pencipta->name_master) && empty($m->rolelagu[1]->pencipta->name_master) && !empty($m->rolelagu[2]->pencipta->name_master) && empty($m->rolelagu[3]->pencipta->name_master) && empty($m->rolelagu[4]->pencipta->name_master) && empty($m->rolelagu[5]->pencipta->name_master) && empty($m->rolelagu[6]->pencipta->name_master) && empty($m->rolelagu[7]->pencipta->name_master) && empty($m->rolelagu[8]->pencipta->name_master) && empty($m->rolelagu[9]->pencipta->name_master)){
                
                    return '<a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[2]->pencipta->id).'">'.$m->rolelagu[2]->pencipta->name_master.'</a>';

                }elseif(empty($m->rolelagu[0]->pencipta->name_master) && empty($m->rolelagu[1]->pencipta->name_master) && !empty($m->rolelagu[2]->pencipta->name_master) && !empty($m->rolelagu[3]->pencipta->name_master) && empty($m->rolelagu[4]->pencipta->name_master) && empty($m->rolelagu[5]->pencipta->name_master) && empty($m->rolelagu[6]->pencipta->name_master) && empty($m->rolelagu[7]->pencipta->name_master) && empty($m->rolelagu[8]->pencipta->name_master) && empty($m->rolelagu[9]->pencipta->name_master)){
                
                    return '<a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[2]->pencipta->id).'">'.$m->rolelagu[2]->pencipta->name_master.'</a> & <a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[3]->pencipta->id).'">'.$m->rolelagu[3]->pencipta->name_master.'</a>';

                }elseif(empty($m->rolelagu[0]->pencipta->name_master) && empty($m->rolelagu[1]->pencipta->name_master) && !empty($m->rolelagu[2]->pencipta->name_master) && !empty($m->rolelagu[3]->pencipta->name_master) && !empty($m->rolelagu[4]->pencipta->name_master) && empty($m->rolelagu[5]->pencipta->name_master) && empty($m->rolelagu[6]->pencipta->name_master) && empty($m->rolelagu[7]->pencipta->name_master) && empty($m->rolelagu[8]->pencipta->name_master) && empty($m->rolelagu[9]->pencipta->name_master)){
                
                    return '<a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[2]->pencipta->id).'">'.$m->rolelagu[2]->pencipta->name_master.'</a> & <a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[3]->pencipta->id).'">'.$m->rolelagu[3]->pencipta->name_master.'</a> & <a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[4]->pencipta->id).'">'.$m->rolelagu[4]->pencipta->name_master.'</a>';

                }elseif(empty($m->rolelagu[0]->pencipta->name_master) && empty($m->rolelagu[1]->pencipta->name_master) && !empty($m->rolelagu[2]->pencipta->name_master) && !empty($m->rolelagu[3]->pencipta->name_master) && !empty($m->rolelagu[4]->pencipta->name_master) && !empty($m->rolelagu[5]->pencipta->name_master) && empty($m->rolelagu[6]->pencipta->name_master) && empty($m->rolelagu[7]->pencipta->name_master) && empty($m->rolelagu[8]->pencipta->name_master) && empty($m->rolelagu[9]->pencipta->name_master)){
                
                    return '<a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[2]->pencipta->id).'">'.$m->rolelagu[2]->pencipta->name_master.'</a> & <a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[3]->pencipta->id).'">'.$m->rolelagu[3]->pencipta->name_master.'</a> & <a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[4]->pencipta->id).'">'.$m->rolelagu[4]->pencipta->name_master.'</a> & <a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[5]->pencipta->id).'">'.$m->rolelagu[5]->pencipta->name_master.'</a>';
                
                }elseif(empty($m->rolelagu[0]->pencipta->name_master) && empty($m->rolelagu[1]->pencipta->name_master) && !empty($m->rolelagu[2]->pencipta->name_master) && !empty($m->rolelagu[3]->pencipta->name_master) && !empty($m->rolelagu[4]->pencipta->name_master) && !empty($m->rolelagu[5]->pencipta->name_master) && !empty($m->rolelagu[6]->pencipta->name_master) && empty($m->rolelagu[7]->pencipta->name_master) && empty($m->rolelagu[8]->pencipta->name_master) && empty($m->rolelagu[9]->pencipta->name_master)){
                
                    return '<a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[2]->pencipta->id).'">'.$m->rolelagu[2]->pencipta->name_master.'</a> & <a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[3]->pencipta->id).'">'.$m->rolelagu[3]->pencipta->name_master.'</a> & <a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[4]->pencipta->id).'">'.$m->rolelagu[4]->pencipta->name_master.'</a> & <a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[5]->pencipta->id).'">'.$m->rolelagu[5]->pencipta->name_master.'</a> & <a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[6]->pencipta->id).'">'.$m->rolelagu[6]->pencipta->name_master.'</a>';

                }elseif(empty($m->rolelagu[0]->pencipta->name_master) && empty($m->rolelagu[1]->pencipta->name_master) && empty($m->rolelagu[2]->pencipta->name_master) && !empty($m->rolelagu[3]->pencipta->name_master) && empty($m->rolelagu[4]->pencipta->name_master) && empty($m->rolelagu[5]->pencipta->name_master) && empty($m->rolelagu[6]->pencipta->name_master) && empty($m->rolelagu[7]->pencipta->name_master) && empty($m->rolelagu[8]->pencipta->name_master) && empty($m->rolelagu[9]->pencipta->name_master)){
                
                    return '<a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[3]->pencipta->id).'">'.$m->rolelagu[3]->pencipta->name_master.'</a>';

                }elseif(empty($m->rolelagu[0]->pencipta->name_master) && empty($m->rolelagu[1]->pencipta->name_master) && empty($m->rolelagu[2]->pencipta->name_master) && !empty($m->rolelagu[3]->pencipta->name_master) && !empty($m->rolelagu[4]->pencipta->name_master) && empty($m->rolelagu[5]->pencipta->name_master) && empty($m->rolelagu[6]->pencipta->name_master) && empty($m->rolelagu[7]->pencipta->name_master) && empty($m->rolelagu[8]->pencipta->name_master) && empty($m->rolelagu[9]->pencipta->name_master)){
                
                    return '<a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[3]->pencipta->id).'">'.$m->rolelagu[3]->pencipta->name_master.'</a> & <a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[4]->pencipta->id).'">'.$m->rolelagu[4]->pencipta->name_master.'</a>';

                }elseif(empty($m->rolelagu[0]->pencipta->name_master) && empty($m->rolelagu[1]->pencipta->name_master) && empty($m->rolelagu[2]->pencipta->name_master) && !empty($m->rolelagu[3]->pencipta->name_master) && !empty($m->rolelagu[4]->pencipta->name_master) && !empty($m->rolelagu[5]->pencipta->name_master) && empty($m->rolelagu[6]->pencipta->name_master) && empty($m->rolelagu[7]->pencipta->name_master) && empty($m->rolelagu[8]->pencipta->name_master) && empty($m->rolelagu[9]->pencipta->name_master)){
                
                    return '<a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[3]->pencipta->id).'">'.$m->rolelagu[3]->pencipta->name_master.'</a> & <a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[4]->pencipta->id).'">'.$m->rolelagu[4]->pencipta->name_master.'</a> & <a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[5]->pencipta->id).'">'.$m->rolelagu[5]->pencipta->name_master.'</a>';

                }elseif(empty($m->rolelagu[0]->pencipta->name_master) && empty($m->rolelagu[1]->pencipta->name_master) && empty($m->rolelagu[2]->pencipta->name_master) && !empty($m->rolelagu[3]->pencipta->name_master) && !empty($m->rolelagu[4]->pencipta->name_master) && !empty($m->rolelagu[5]->pencipta->name_master) && !empty($m->rolelagu[6]->pencipta->name_master) && empty($m->rolelagu[7]->pencipta->name_master) && empty($m->rolelagu[8]->pencipta->name_master) && empty($m->rolelagu[9]->pencipta->name_master)){
                
                    return '<a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[3]->pencipta->id).'">'.$m->rolelagu[3]->pencipta->name_master.'</a> & <a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[4]->pencipta->id).'">'.$m->rolelagu[4]->pencipta->name_master.'</a> & <a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[5]->pencipta->id).'">'.$m->rolelagu[5]->pencipta->name_master.'</a> & <a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[6]->pencipta->id).'">'.$m->rolelagu[6]->pencipta->name_master.'</a>';
                
                }elseif(empty($m->rolelagu[0]->pencipta->name_master) && empty($m->rolelagu[1]->pencipta->name_master) && empty($m->rolelagu[2]->pencipta->name_master) && !empty($m->rolelagu[3]->pencipta->name_master) && !empty($m->rolelagu[4]->pencipta->name_master) && !empty($m->rolelagu[5]->pencipta->name_master) && !empty($m->rolelagu[6]->pencipta->name_master) && !empty($m->rolelagu[7]->pencipta->name_master) && empty($m->rolelagu[8]->pencipta->name_master) && empty($m->rolelagu[9]->pencipta->name_master)){
                
                    return '<a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[3]->pencipta->id).'">'.$m->rolelagu[3]->pencipta->name_master.'</a> & <a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[4]->pencipta->id).'">'.$m->rolelagu[4]->pencipta->name_master.'</a> & <a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[5]->pencipta->id).'">'.$m->rolelagu[5]->pencipta->name_master.'</a> & <a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[6]->pencipta->id).'">'.$m->rolelagu[6]->pencipta->name_master.'</a> & <a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[7]->pencipta->id).'">'.$m->rolelagu[7]->pencipta->name_master.'</a>';

                }elseif(empty($m->rolelagu[0]->pencipta->name_master) && empty($m->rolelagu[1]->pencipta->name_master) && empty($m->rolelagu[2]->pencipta->name_master) && empty($m->rolelagu[3]->pencipta->name_master) && !empty($m->rolelagu[4]->pencipta->name_master) && empty($m->rolelagu[5]->pencipta->name_master) && empty($m->rolelagu[6]->pencipta->name_master) && empty($m->rolelagu[7]->pencipta->name_master) && empty($m->rolelagu[8]->pencipta->name_master) && empty($m->rolelagu[9]->pencipta->name_master)){
                
                    return '<a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[4]->pencipta->id).'">'.$m->rolelagu[4]->pencipta->name_master.'</a>';

                }elseif(empty($m->rolelagu[0]->pencipta->name_master) && empty($m->rolelagu[1]->pencipta->name_master) && empty($m->rolelagu[2]->pencipta->name_master) && empty($m->rolelagu[3]->pencipta->name_master) && !empty($m->rolelagu[4]->pencipta->name_master) && !empty($m->rolelagu[5]->pencipta->name_master) && empty($m->rolelagu[6]->pencipta->name_master) && empty($m->rolelagu[7]->pencipta->name_master) && empty($m->rolelagu[8]->pencipta->name_master) && empty($m->rolelagu[9]->pencipta->name_master)){
                
                    return '<a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[4]->pencipta->id).'">'.$m->rolelagu[4]->pencipta->name_master.'</a> & <a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[5]->pencipta->id).'">'.$m->rolelagu[5]->pencipta->name_master.'</a>';

                }elseif(empty($m->rolelagu[0]->pencipta->name_master) && empty($m->rolelagu[1]->pencipta->name_master) && empty($m->rolelagu[2]->pencipta->name_master) && empty($m->rolelagu[3]->pencipta->name_master) && !empty($m->rolelagu[4]->pencipta->name_master) && !empty($m->rolelagu[5]->pencipta->name_master) && !empty($m->rolelagu[6]->pencipta->name_master) && empty($m->rolelagu[7]->pencipta->name_master) && empty($m->rolelagu[8]->pencipta->name_master) && empty($m->rolelagu[9]->pencipta->name_master)){
                
                    return '<a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[4]->pencipta->id).'">'.$m->rolelagu[4]->pencipta->name_master.'</a> & <a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[5]->pencipta->id).'">'.$m->rolelagu[5]->pencipta->name_master.'</a> & <a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[6]->pencipta->id).'">'.$m->rolelagu[6]->pencipta->name_master.'</a>';

                }elseif(empty($m->rolelagu[0]->pencipta->name_master) && empty($m->rolelagu[1]->pencipta->name_master) && empty($m->rolelagu[2]->pencipta->name_master) && empty($m->rolelagu[3]->pencipta->name_master) && !empty($m->rolelagu[4]->pencipta->name_master) && !empty($m->rolelagu[5]->pencipta->name_master) && !empty($m->rolelagu[6]->pencipta->name_master) && !empty($m->rolelagu[7]->pencipta->name_master) && empty($m->rolelagu[8]->pencipta->name_master) && empty($m->rolelagu[9]->pencipta->name_master)){
                
                    return '<a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[4]->pencipta->id).'">'.$m->rolelagu[4]->pencipta->name_master.'</a> & <a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[5]->pencipta->id).'">'.$m->rolelagu[5]->pencipta->name_master.'</a> & <a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[6]->pencipta->id).'">'.$m->rolelagu[6]->pencipta->name_master.'</a> & <a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[7]->pencipta->id).'">'.$m->rolelagu[7]->pencipta->name_master.'</a>';
                
                }elseif(empty($m->rolelagu[0]->pencipta->name_master) && empty($m->rolelagu[1]->pencipta->name_master) && empty($m->rolelagu[2]->pencipta->name_master) && empty($m->rolelagu[3]->pencipta->name_master) && !empty($m->rolelagu[4]->pencipta->name_master) && !empty($m->rolelagu[5]->pencipta->name_master) && !empty($m->rolelagu[6]->pencipta->name_master) && !empty($m->rolelagu[7]->pencipta->name_master) && !empty($m->rolelagu[8]->pencipta->name_master) && empty($m->rolelagu[9]->pencipta->name_master)){
                
                    return '<a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[4]->pencipta->id).'">'.$m->rolelagu[4]->pencipta->name_master.'</a> & <a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[5]->pencipta->id).'">'.$m->rolelagu[5]->pencipta->name_master.'</a> & <a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[6]->pencipta->id).'">'.$m->rolelagu[6]->pencipta->name_master.'</a> & <a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[7]->pencipta->id).'">'.$m->rolelagu[7]->pencipta->name_master.'</a> & <a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[8]->pencipta->id).'">'.$m->rolelagu[8]->pencipta->name_master.'</a>';

                }elseif(empty($m->rolelagu[0]->pencipta->name_master) && empty($m->rolelagu[1]->pencipta->name_master) && empty($m->rolelagu[2]->pencipta->name_master) && empty($m->rolelagu[3]->pencipta->name_master) && empty($m->rolelagu[4]->pencipta->name_master) && !empty($m->rolelagu[5]->pencipta->name_master) && empty($m->rolelagu[6]->pencipta->name_master) && empty($m->rolelagu[7]->pencipta->name_master) && empty($m->rolelagu[8]->pencipta->name_master) && empty($m->rolelagu[9]->pencipta->name_master)){
                
                    return '<a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[5]->pencipta->id).'">'.$m->rolelagu[5]->pencipta->name_master.'</a>';

                }elseif(empty($m->rolelagu[0]->pencipta->name_master) && empty($m->rolelagu[1]->pencipta->name_master) && empty($m->rolelagu[2]->pencipta->name_master) && empty($m->rolelagu[3]->pencipta->name_master) && empty($m->rolelagu[4]->pencipta->name_master) && !empty($m->rolelagu[5]->pencipta->name_master) && !empty($m->rolelagu[6]->pencipta->name_master) && empty($m->rolelagu[7]->pencipta->name_master) && empty($m->rolelagu[8]->pencipta->name_master) && empty($m->rolelagu[9]->pencipta->name_master)){
                
                    return '<a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[5]->pencipta->id).'">'.$m->rolelagu[5]->pencipta->name_master.'</a> & <a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[6]->pencipta->id).'">'.$m->rolelagu[6]->pencipta->name_master.'</a>';

                }elseif(empty($m->rolelagu[0]->pencipta->name_master) && empty($m->rolelagu[1]->pencipta->name_master) && empty($m->rolelagu[2]->pencipta->name_master) && empty($m->rolelagu[3]->pencipta->name_master) && empty($m->rolelagu[4]->pencipta->name_master) && !empty($m->rolelagu[5]->pencipta->name_master) && !empty($m->rolelagu[6]->pencipta->name_master) && !empty($m->rolelagu[7]->pencipta->name_master) && empty($m->rolelagu[8]->pencipta->name_master) && empty($m->rolelagu[9]->pencipta->name_master)){
                
                    return '<a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[5]->pencipta->id).'">'.$m->rolelagu[5]->pencipta->name_master.'</a> & <a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[6]->pencipta->id).'">'.$m->rolelagu[6]->pencipta->name_master.'</a> & <a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[7]->pencipta->id).'">'.$m->rolelagu[7]->pencipta->name_master.'</a>';

                }elseif(empty($m->rolelagu[0]->pencipta->name_master) && empty($m->rolelagu[1]->pencipta->name_master) && empty($m->rolelagu[2]->pencipta->name_master) && empty($m->rolelagu[3]->pencipta->name_master) && empty($m->rolelagu[4]->pencipta->name_master) && !empty($m->rolelagu[5]->pencipta->name_master) && !empty($m->rolelagu[6]->pencipta->name_master) && !empty($m->rolelagu[7]->pencipta->name_master) && !empty($m->rolelagu[8]->pencipta->name_master) && empty($m->rolelagu[9]->pencipta->name_master)){
                
                    return '<a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[5]->pencipta->id).'">'.$m->rolelagu[5]->pencipta->name_master.'</a> & <a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[6]->pencipta->id).'">'.$m->rolelagu[6]->pencipta->name_master.'</a> & <a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[7]->pencipta->id).'">'.$m->rolelagu[7]->pencipta->name_master.'</a> & <a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[8]->pencipta->id).'">'.$m->rolelagu[8]->pencipta->name_master.'</a>';
                
                }elseif(empty($m->rolelagu[0]->pencipta->name_master) && empty($m->rolelagu[1]->pencipta->name_master) && empty($m->rolelagu[2]->pencipta->name_master) && empty($m->rolelagu[3]->pencipta->name_master) && empty($m->rolelagu[4]->pencipta->name_master) && !empty($m->rolelagu[5]->pencipta->name_master) && !empty($m->rolelagu[6]->pencipta->name_master) && !empty($m->rolelagu[7]->pencipta->name_master) && !empty($m->rolelagu[8]->pencipta->name_master) && !empty($m->rolelagu[9]->pencipta->name_master)){
                
                    return '<a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[5]->pencipta->id).'">'.$m->rolelagu[5]->pencipta->name_master.'</a> & <a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[6]->pencipta->id).'">'.$m->rolelagu[6]->pencipta->name_master.'</a> & <a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[7]->pencipta->id).'">'.$m->rolelagu[7]->pencipta->name_master.'</a> & <a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[8]->pencipta->id).'">'.$m->rolelagu[8]->pencipta->name_master.'</a> & <a href="'.route("masterlagu.detail.songwriter", $m->rolelagu[9]->pencipta->id).'">'.$m->rolelagu[9]->pencipta->name_master.'</a>';

                }else{
                    return "&nbsp;";
                }
            })
            ->escapeColumns([])
            // ->addColumn('isrc', function($m){
            //     return $m->isrc;
            // })
            // ->addColumn('upc', function($m){
            //     return $m->upc;
            // })
            // ->addColumn('% pencipta / publishing', function($m){
            //     return sprintf("%.0f%%", $m->percentage_pencipta * 100);
            // })
            // ->addColumn('% penyanyi', function($m){
            //     return sprintf("%.0f%%", $m->percentage_penyanyi * 100);
            // })
            // ->addColumn('country', function($m){/* 
            //     for ($x=0; $x < count($m->rolelagu); $x++) { 
            //         if(!empty($m->rolelagu[$x]->penyanyi->name_master)){
            //             return $m->rolelagu[$x]->penyanyi->name_master;
            //         }else{
            //             return '&nbsp;';
            //         }
            //     } */
            //     if(!empty($m->rolelagucountry[0]->country->country_name) AND empty($m->rolelagucountry[1]->country->country_name) AND empty($m->rolelagucountry[2]->country->country_name) AND empty($m->rolelagucountry[3]->country->country_name) AND empty($m->rolelagucountry[4]->country->country_name) AND empty($m->rolelagucountry[5]->country->country_name)){
                
            //         return $m->rolelagucountry[0]->country->country_name;

            //     }elseif(empty($m->rolelagucountry[0]->country->country_name) AND !empty($m->rolelagucountry[1]->country->country_name) AND empty($m->rolelagucountry[2]->country->country_name) AND empty($m->rolelagucountry[3]->country->country_name) AND empty($m->rolelagucountry[4]->country->country_name) AND empty($m->rolelagucountry[5]->country->country_name)){
                    
            //         return $m->rolelagucountry[1]->country->country_name;

            //     }elseif(empty($m->rolelagucountry[0]->country->country_name) AND empty($m->rolelagucountry[1]->country->country_name) AND !empty($m->rolelagucountry[2]->country->country_name) AND empty($m->rolelagucountry[3]->country->country_name) AND empty($m->rolelagucountry[4]->country->country_name) AND empty($m->rolelagucountry[5]->country->country_name)){
                    
            //         return $m->rolelagucountry[2]->country->country_name;
                
            //     }elseif(empty($m->rolelagucountry[0]->country->country_name) AND empty($m->rolelagucountry[1]->country->country_name) AND empty($m->rolelagucountry[2]->country->country_name) AND !empty($m->rolelagucountry[3]->country->country_name) AND empty($m->rolelagucountry[4]->country->country_name) AND empty($m->rolelagucountry[5]->country->country_name)){
                    
            //         return $m->rolelagucountry[3]->country->country_name;
                
            //     }elseif(empty($m->rolelagucountry[0]->country->country_name) AND empty($m->rolelagucountry[1]->country->country_name) AND empty($m->rolelagucountry[2]->country->country_name) AND empty($m->rolelagucountry[3]->country->country_name) AND !empty($m->rolelagucountry[4]->country->country_name) AND empty($m->rolelagucountry[5]->country->country_name)){
                    
            //         return $m->rolelagucountry[4]->country->country_name;

            //     }elseif(empty($m->rolelagucountry[0]->country->country_name) AND empty($m->rolelagucountry[1]->country->country_name) AND empty($m->rolelagucountry[2]->country->country_name) AND empty($m->rolelagucountry[3]->country->country_name) AND empty($m->rolelagucountry[4]->country->country_name) AND !empty($m->rolelagucountry[5]->country->country_name)){
                    
            //         return $m->rolelagucountry[5]->country->country_name;

            //     }elseif(!empty($m->rolelagucountry[0]->country->country_name) AND !empty($m->rolelagucountry[1]->country->country_name) AND empty($m->rolelagucountry[2]->country->country_name) AND empty($m->rolelagucountry[3]->country->country_name) AND empty($m->rolelagucountry[4]->country->country_name) AND empty($m->rolelagucountry[5]->country->country_name)){

            //         return $m->rolelagucountry[0]->country->country_name.' & '.$m->rolelagucountry[1]->country->country_name;

            //     }elseif(!empty($m->rolelagucountry[0]->country->country_name) AND !empty($m->rolelagucountry[1]->country->country_name) AND !empty($m->rolelagucountry[2]->country->country_name) AND empty($m->rolelagucountry[3]->country->country_name) AND empty($m->rolelagucountry[4]->country->country_name) AND empty($m->rolelagucountry[5]->country->country_name)){

            //         return $m->rolelagucountry[0]->country->country_name.' & '.$m->rolelagucountry[1]->country->country_name.' & '.$m->rolelagucountry[2]->country->country_name;

            //     }elseif(!empty($m->rolelagucountry[0]->country->country_name) AND !empty($m->rolelagucountry[1]->country->country_name) AND !empty($m->rolelagucountry[2]->country->country_name) AND !empty($m->rolelagucountry[3]->country->country_name) AND empty($m->rolelagucountry[4]->country->country_name) AND empty($m->rolelagucountry[5]->country->country_name)){

            //         return $m->rolelagucountry[0]->country->country_name.' & '.$m->rolelagucountry[1]->country->country_name.' & '.$m->rolelagucountry[2]->country->country_name.' & '.$m->rolelagucountry[3]->country->country_name;

            //     }elseif(!empty($m->rolelagucountry[0]->country->country_name) AND !empty($m->rolelagucountry[1]->country->country_name) AND !empty($m->rolelagucountry[2]->country->country_name) AND !empty($m->rolelagucountry[3]->country->country_name) AND !empty($m->rolelagucountry[4]->country->country_name) AND empty($m->rolelagucountry[5]->country->country_name)){

            //         return $m->rolelagucountry[0]->country->country_name.' & '.$m->rolelagucountry[1]->country->country_name.' & '.$m->rolelagucountry[2]->country->country_name.' & '.$m->rolelagucountry[3]->country->country_name.' & '.$m->rolelagucountry[4]->country->country_name;

            //     }elseif(!empty($m->rolelagucountry[0]->country->country_name) AND !empty($m->rolelagucountry[1]->country->country_name) AND !empty($m->rolelagucountry[2]->country->country_name) AND !empty($m->rolelagucountry[3]->country->country_name) AND !empty($m->rolelagucountry[4]->country->country_name) AND !empty($m->rolelagucountry[5]->country->country_name)){
                    
            //         return $m->rolelagucountry[0]->country->country_name.' & '.$m->rolelagucountry[1]->country->country_name.' & '.$m->rolelagucountry[2]->country->country_name.' & '.$m->rolelagucountry[3]->country->country_name.' & '.$m->rolelagucountry[4]->country->country_name.' & '.$m->rolelagucountry[5]->country->country_name;

            //     }else{
            //         return '&nbsp;';
            //     }
            // })
            // ->addColumn('% rights recording', function($m){
            //     return sprintf("%.0f%%", $m->percentage_rights * 100);
            // })
            ->addColumn('action', 'masterlagu.datatables_actions');
        }else{
            return $dataTable
            ->addColumn('judul', function($m){
                return $m->track_title;
            })
            ->addColumn('album', function($m){
                return $m->release_title;
            })
            ->addColumn('tanggal release', function($m){
                if($m->release_date == '0000-00-00'){
                    $daterelease = '';
                }else{
                    $daterelease = date('d-m-Y', strtotime($m->release_date));
                }
                return $daterelease;
            })
            ->addColumn('penyanyi', function($m){
                if(!empty($m->rolelagu[0]->penyanyi->name_master) AND empty($m->rolelagu[1]->penyanyi->name_master) AND empty($m->rolelagu[2]->penyanyi->name_master) AND empty($m->rolelagu[3]->penyanyi->name_master) AND empty($m->rolelagu[4]->penyanyi->name_master) AND empty($m->rolelagu[5]->penyanyi->name_master)){
                    
                    return $m->rolelagu[0]->penyanyi->name_master;

                }elseif(empty($m->rolelagu[0]->penyanyi->name_master) AND !empty($m->rolelagu[1]->penyanyi->name_master) AND empty($m->rolelagu[2]->penyanyi->name_master) AND empty($m->rolelagu[3]->penyanyi->name_master) AND empty($m->rolelagu[4]->penyanyi->name_master) AND empty($m->rolelagu[5]->penyanyi->name_master)){
                    
                    return $m->rolelagu[1]->penyanyi->name_master;

                }elseif(empty($m->rolelagu[0]->penyanyi->name_master) AND empty($m->rolelagu[1]->penyanyi->name_master) AND !empty($m->rolelagu[2]->penyanyi->name_master) AND empty($m->rolelagu[3]->penyanyi->name_master) AND empty($m->rolelagu[4]->penyanyi->name_master) AND empty($m->rolelagu[5]->penyanyi->name_master)){
                    
                    return $m->rolelagu[2]->penyanyi->name_master;
                
                }elseif(empty($m->rolelagu[0]->penyanyi->name_master) AND empty($m->rolelagu[1]->penyanyi->name_master) AND empty($m->rolelagu[2]->penyanyi->name_master) AND !empty($m->rolelagu[3]->penyanyi->name_master) AND empty($m->rolelagu[4]->penyanyi->name_master) AND empty($m->rolelagu[5]->penyanyi->name_master)){
                    
                    return $m->rolelagu[3]->penyanyi->name_master;
                
                }elseif(empty($m->rolelagu[0]->penyanyi->name_master) AND empty($m->rolelagu[1]->penyanyi->name_master) AND empty($m->rolelagu[2]->penyanyi->name_master) AND empty($m->rolelagu[3]->penyanyi->name_master) AND !empty($m->rolelagu[4]->penyanyi->name_master) AND empty($m->rolelagu[5]->penyanyi->name_master)){
                    
                    return $m->rolelagu[4]->penyanyi->name_master;

                }elseif(empty($m->rolelagu[0]->penyanyi->name_master) AND empty($m->rolelagu[1]->penyanyi->name_master) AND empty($m->rolelagu[2]->penyanyi->name_master) AND empty($m->rolelagu[3]->penyanyi->name_master) AND empty($m->rolelagu[4]->penyanyi->name_master) AND !empty($m->rolelagu[5]->penyanyi->name_master)){
                    
                    return $m->rolelagu[5]->penyanyi->name_master;

                }elseif(!empty($m->rolelagu[0]->penyanyi->name_master) AND !empty($m->rolelagu[1]->penyanyi->name_master) AND empty($m->rolelagu[2]->penyanyi->name_master) AND empty($m->rolelagu[3]->penyanyi->name_master) AND empty($m->rolelagu[4]->penyanyi->name_master) AND empty($m->rolelagu[5]->penyanyi->name_master)){

                    return $m->rolelagu[0]->penyanyi->name_master.' & '.$m->rolelagu[1]->penyanyi->name_master;

                }elseif(!empty($m->rolelagu[0]->penyanyi->name_master) AND !empty($m->rolelagu[1]->penyanyi->name_master) AND !empty($m->rolelagu[2]->penyanyi->name_master) AND empty($m->rolelagu[3]->penyanyi->name_master) AND empty($m->rolelagu[4]->penyanyi->name_master) AND empty($m->rolelagu[5]->penyanyi->name_master)){

                    return $m->rolelagu[0]->penyanyi->name_master.' & '.$m->rolelagu[1]->penyanyi->name_master.' & '.$m->rolelagu[2]->penyanyi->name_master;

                }elseif(!empty($m->rolelagu[0]->penyanyi->name_master) AND !empty($m->rolelagu[1]->penyanyi->name_master) AND !empty($m->rolelagu[2]->penyanyi->name_master) AND !empty($m->rolelagu[3]->penyanyi->name_master) AND empty($m->rolelagu[4]->penyanyi->name_master) AND empty($m->rolelagu[5]->penyanyi->name_master)){

                    return $m->rolelagu[0]->penyanyi->name_master.' & '.$m->rolelagu[1]->penyanyi->name_master.' & '.$m->rolelagu[2]->penyanyi->name_master.' & '.$m->rolelagu[3]->penyanyi->name_master;

                }elseif(!empty($m->rolelagu[0]->penyanyi->name_master) AND !empty($m->rolelagu[1]->penyanyi->name_master) AND !empty($m->rolelagu[2]->penyanyi->name_master) AND !empty($m->rolelagu[3]->penyanyi->name_master) AND !empty($m->rolelagu[4]->penyanyi->name_master) AND empty($m->rolelagu[5]->penyanyi->name_master)){

                    return $m->rolelagu[0]->penyanyi->name_master.' & '.$m->rolelagu[1]->penyanyi->name_master.' & '.$m->rolelagu[2]->penyanyi->name_master.' & '.$m->rolelagu[3]->penyanyi->name_master.' & '.$m->rolelagu[4]->penyanyi->name_master;

                }elseif(!empty($m->rolelagu[0]->penyanyi->name_master) AND !empty($m->rolelagu[1]->penyanyi->name_master) AND !empty($m->rolelagu[2]->penyanyi->name_master) AND !empty($m->rolelagu[3]->penyanyi->name_master) AND !empty($m->rolelagu[4]->penyanyi->name_master) AND !empty($m->rolelagu[5]->penyanyi->name_master)){
                    
                    return $m->rolelagu[0]->penyanyi->name_master.' & '.$m->rolelagu[1]->penyanyi->name_master.' & '.$m->rolelagu[2]->penyanyi->name_master.' & '.$m->rolelagu[3]->penyanyi->name_master.' & '.$m->rolelagu[4]->penyanyi->name_master.' & '.$m->rolelagu[5]->penyanyi->name_master;

                }else{
                    return '&nbsp;';
                }
            })
            ->addColumn('pencipta / publishing', function($m){
                if(!empty($m->rolelagu[0]->pencipta->name_master) && empty($m->rolelagu[1]->pencipta->name_master) && empty($m->rolelagu[2]->pencipta->name_master) && empty($m->rolelagu[3]->pencipta->name_master) && empty($m->rolelagu[4]->pencipta->name_master) && empty($m->rolelagu[5]->pencipta->name_master) && empty($m->rolelagu[6]->pencipta->name_master) && empty($m->rolelagu[7]->pencipta->name_master) && empty($m->rolelagu[8]->pencipta->name_master) && empty($m->rolelagu[9]->pencipta->name_master)){
                    
                    return $m->rolelagu[0]->pencipta->name_master;

                }elseif(!empty($m->rolelagu[0]->pencipta->name_master) && !empty($m->rolelagu[1]->pencipta->name_master) && empty($m->rolelagu[2]->pencipta->name_master) && empty($m->rolelagu[3]->pencipta->name_master) && empty($m->rolelagu[4]->pencipta->name_master) && empty($m->rolelagu[5]->pencipta->name_master) && empty($m->rolelagu[6]->pencipta->name_master) && empty($m->rolelagu[7]->pencipta->name_master) && empty($m->rolelagu[8]->pencipta->name_master) && empty($m->rolelagu[9]->pencipta->name_master)){
                
                    return $m->rolelagu[0]->pencipta->name_master.' & '.$m->rolelagu[1]->pencipta->name_master;

                }elseif(!empty($m->rolelagu[0]->pencipta->name_master) && !empty($m->rolelagu[1]->pencipta->name_master) && !empty($m->rolelagu[2]->pencipta->name_master) && empty($m->rolelagu[3]->pencipta->name_master) && empty($m->rolelagu[4]->pencipta->name_master) && empty($m->rolelagu[5]->pencipta->name_master) && empty($m->rolelagu[6]->pencipta->name_master) && empty($m->rolelagu[7]->pencipta->name_master) && empty($m->rolelagu[8]->pencipta->name_master) && empty($m->rolelagu[9]->pencipta->name_master)){
                
                    return $m->rolelagu[0]->pencipta->name_master.' & '.$m->rolelagu[1]->pencipta->name_master.' & '.$m->rolelagu[2]->pencipta->name_master;

                }elseif(!empty($m->rolelagu[0]->pencipta->name_master) && !empty($m->rolelagu[1]->pencipta->name_master) && !empty($m->rolelagu[2]->pencipta->name_master) && !empty($m->rolelagu[3]->pencipta->name_master) && empty($m->rolelagu[4]->pencipta->name_master) && empty($m->rolelagu[5]->pencipta->name_master) && empty($m->rolelagu[6]->pencipta->name_master) && empty($m->rolelagu[7]->pencipta->name_master) && empty($m->rolelagu[8]->pencipta->name_master) && empty($m->rolelagu[9]->pencipta->name_master)){
                
                    return $m->rolelagu[0]->pencipta->name_master.' & '.$m->rolelagu[1]->pencipta->name_master.' & '.$m->rolelagu[2]->pencipta->name_master.' & '.$m->rolelagu[3]->pencipta->name_master;
                
                }elseif(!empty($m->rolelagu[0]->pencipta->name_master) && !empty($m->rolelagu[1]->pencipta->name_master) && !empty($m->rolelagu[2]->pencipta->name_master) && !empty($m->rolelagu[3]->pencipta->name_master) && !empty($m->rolelagu[4]->pencipta->name_master) && empty($m->rolelagu[5]->pencipta->name_master) && empty($m->rolelagu[6]->pencipta->name_master) && empty($m->rolelagu[7]->pencipta->name_master) && empty($m->rolelagu[8]->pencipta->name_master) && empty($m->rolelagu[9]->pencipta->name_master)){
                
                    return $m->rolelagu[0]->pencipta->name_master.' & '.$m->rolelagu[1]->pencipta->name_master.' & '.$m->rolelagu[2]->pencipta->name_master.' & '.$m->rolelagu[3]->pencipta->name_master.' & '.$m->rolelagu[4]->pencipta->name_master;

                }elseif(empty($m->rolelagu[0]->pencipta->name_master) && !empty($m->rolelagu[1]->pencipta->name_master) && empty($m->rolelagu[2]->pencipta->name_master) && empty($m->rolelagu[3]->pencipta->name_master) && empty($m->rolelagu[4]->pencipta->name_master) && empty($m->rolelagu[5]->pencipta->name_master) && empty($m->rolelagu[6]->pencipta->name_master) && empty($m->rolelagu[7]->pencipta->name_master) && empty($m->rolelagu[8]->pencipta->name_master) && empty($m->rolelagu[9]->pencipta->name_master)){
                
                    return $m->rolelagu[1]->pencipta->name_master;

                }elseif(empty($m->rolelagu[0]->pencipta->name_master) && !empty($m->rolelagu[1]->pencipta->name_master) && !empty($m->rolelagu[2]->pencipta->name_master) && empty($m->rolelagu[3]->pencipta->name_master) && empty($m->rolelagu[4]->pencipta->name_master) && empty($m->rolelagu[5]->pencipta->name_master) && empty($m->rolelagu[6]->pencipta->name_master) && empty($m->rolelagu[7]->pencipta->name_master) && empty($m->rolelagu[8]->pencipta->name_master) && empty($m->rolelagu[9]->pencipta->name_master)){
                
                    return $m->rolelagu[1]->pencipta->name_master.' & '.$m->rolelagu[2]->pencipta->name_master;

                }elseif(empty($m->rolelagu[0]->pencipta->name_master) && !empty($m->rolelagu[1]->pencipta->name_master) && !empty($m->rolelagu[2]->pencipta->name_master) && !empty($m->rolelagu[3]->pencipta->name_master) && empty($m->rolelagu[4]->pencipta->name_master) && empty($m->rolelagu[5]->pencipta->name_master) && empty($m->rolelagu[6]->pencipta->name_master) && empty($m->rolelagu[7]->pencipta->name_master) && empty($m->rolelagu[8]->pencipta->name_master) && empty($m->rolelagu[9]->pencipta->name_master)){
                
                    return $m->rolelagu[1]->pencipta->name_master.' & '.$m->rolelagu[2]->pencipta->name_master.' & '.$m->rolelagu[3]->pencipta->name_master;

                }elseif(empty($m->rolelagu[0]->pencipta->name_master) && !empty($m->rolelagu[1]->pencipta->name_master) && !empty($m->rolelagu[2]->pencipta->name_master) && !empty($m->rolelagu[3]->pencipta->name_master) && !empty($m->rolelagu[4]->pencipta->name_master) && empty($m->rolelagu[5]->pencipta->name_master) && empty($m->rolelagu[6]->pencipta->name_master) && empty($m->rolelagu[7]->pencipta->name_master) && empty($m->rolelagu[8]->pencipta->name_master) && empty($m->rolelagu[9]->pencipta->name_master)){
                
                    return $m->rolelagu[1]->pencipta->name_master.' & '.$m->rolelagu[2]->pencipta->name_master.' & '.$m->rolelagu[3]->pencipta->name_master.' & '.$m->rolelagu[4]->pencipta->name_master;
                
                }elseif(empty($m->rolelagu[0]->pencipta->name_master) && !empty($m->rolelagu[1]->pencipta->name_master) && !empty($m->rolelagu[2]->pencipta->name_master) && !empty($m->rolelagu[3]->pencipta->name_master) && !empty($m->rolelagu[4]->pencipta->name_master) && !empty($m->rolelagu[5]->pencipta->name_master) && empty($m->rolelagu[6]->pencipta->name_master) && empty($m->rolelagu[7]->pencipta->name_master) && empty($m->rolelagu[8]->pencipta->name_master) && empty($m->rolelagu[9]->pencipta->name_master)){
                
                    return $m->rolelagu[1]->pencipta->name_master.' & '.$m->rolelagu[2]->pencipta->name_master.' & '.$m->rolelagu[3]->pencipta->name_master.' & '.$m->rolelagu[4]->pencipta->name_master.' & '.$m->rolelagu[5]->pencipta->name_master;

                }elseif(empty($m->rolelagu[0]->pencipta->name_master) && empty($m->rolelagu[1]->pencipta->name_master) && !empty($m->rolelagu[2]->pencipta->name_master) && empty($m->rolelagu[3]->pencipta->name_master) && empty($m->rolelagu[4]->pencipta->name_master) && empty($m->rolelagu[5]->pencipta->name_master) && empty($m->rolelagu[6]->pencipta->name_master) && empty($m->rolelagu[7]->pencipta->name_master) && empty($m->rolelagu[8]->pencipta->name_master) && empty($m->rolelagu[9]->pencipta->name_master)){
                
                    return $m->rolelagu[2]->pencipta->name_master;

                }elseif(empty($m->rolelagu[0]->pencipta->name_master) && empty($m->rolelagu[1]->pencipta->name_master) && !empty($m->rolelagu[2]->pencipta->name_master) && !empty($m->rolelagu[3]->pencipta->name_master) && empty($m->rolelagu[4]->pencipta->name_master) && empty($m->rolelagu[5]->pencipta->name_master) && empty($m->rolelagu[6]->pencipta->name_master) && empty($m->rolelagu[7]->pencipta->name_master) && empty($m->rolelagu[8]->pencipta->name_master) && empty($m->rolelagu[9]->pencipta->name_master)){
                
                    return $m->rolelagu[2]->pencipta->name_master.' & '.$m->rolelagu[3]->pencipta->name_master;

                }elseif(empty($m->rolelagu[0]->pencipta->name_master) && empty($m->rolelagu[1]->pencipta->name_master) && !empty($m->rolelagu[2]->pencipta->name_master) && !empty($m->rolelagu[3]->pencipta->name_master) && !empty($m->rolelagu[4]->pencipta->name_master) && empty($m->rolelagu[5]->pencipta->name_master) && empty($m->rolelagu[6]->pencipta->name_master) && empty($m->rolelagu[7]->pencipta->name_master) && empty($m->rolelagu[8]->pencipta->name_master) && empty($m->rolelagu[9]->pencipta->name_master)){
                
                    return $m->rolelagu[2]->pencipta->name_master.' & '.$m->rolelagu[3]->pencipta->name_master.' & '.$m->rolelagu[4]->pencipta->name_master;

                }elseif(empty($m->rolelagu[0]->pencipta->name_master) && empty($m->rolelagu[1]->pencipta->name_master) && !empty($m->rolelagu[2]->pencipta->name_master) && !empty($m->rolelagu[3]->pencipta->name_master) && !empty($m->rolelagu[4]->pencipta->name_master) && !empty($m->rolelagu[5]->pencipta->name_master) && empty($m->rolelagu[6]->pencipta->name_master) && empty($m->rolelagu[7]->pencipta->name_master) && empty($m->rolelagu[8]->pencipta->name_master) && empty($m->rolelagu[9]->pencipta->name_master)){
                
                    return $m->rolelagu[2]->pencipta->name_master.' & '.$m->rolelagu[3]->pencipta->name_master.' & '.$m->rolelagu[4]->pencipta->name_master.' & '.$m->rolelagu[5]->pencipta->name_master;
                
                }elseif(empty($m->rolelagu[0]->pencipta->name_master) && empty($m->rolelagu[1]->pencipta->name_master) && !empty($m->rolelagu[2]->pencipta->name_master) && !empty($m->rolelagu[3]->pencipta->name_master) && !empty($m->rolelagu[4]->pencipta->name_master) && !empty($m->rolelagu[5]->pencipta->name_master) && !empty($m->rolelagu[6]->pencipta->name_master) && empty($m->rolelagu[7]->pencipta->name_master) && empty($m->rolelagu[8]->pencipta->name_master) && empty($m->rolelagu[9]->pencipta->name_master)){
                
                    return $m->rolelagu[2]->pencipta->name_master.' & '.$m->rolelagu[3]->pencipta->name_master.' & '.$m->rolelagu[4]->pencipta->name_master.' & '.$m->rolelagu[5]->pencipta->name_master.' & '.$m->rolelagu[6]->pencipta->name_master;

                }elseif(empty($m->rolelagu[0]->pencipta->name_master) && empty($m->rolelagu[1]->pencipta->name_master) && empty($m->rolelagu[2]->pencipta->name_master) && !empty($m->rolelagu[3]->pencipta->name_master) && empty($m->rolelagu[4]->pencipta->name_master) && empty($m->rolelagu[5]->pencipta->name_master) && empty($m->rolelagu[6]->pencipta->name_master) && empty($m->rolelagu[7]->pencipta->name_master) && empty($m->rolelagu[8]->pencipta->name_master) && empty($m->rolelagu[9]->pencipta->name_master)){
                
                    return $m->rolelagu[3]->pencipta->name_master;

                }elseif(empty($m->rolelagu[0]->pencipta->name_master) && empty($m->rolelagu[1]->pencipta->name_master) && empty($m->rolelagu[2]->pencipta->name_master) && !empty($m->rolelagu[3]->pencipta->name_master) && !empty($m->rolelagu[4]->pencipta->name_master) && empty($m->rolelagu[5]->pencipta->name_master) && empty($m->rolelagu[6]->pencipta->name_master) && empty($m->rolelagu[7]->pencipta->name_master) && empty($m->rolelagu[8]->pencipta->name_master) && empty($m->rolelagu[9]->pencipta->name_master)){
                
                    return $m->rolelagu[3]->pencipta->name_master.' & '.$m->rolelagu[4]->pencipta->name_master;

                }elseif(empty($m->rolelagu[0]->pencipta->name_master) && empty($m->rolelagu[1]->pencipta->name_master) && empty($m->rolelagu[2]->pencipta->name_master) && !empty($m->rolelagu[3]->pencipta->name_master) && !empty($m->rolelagu[4]->pencipta->name_master) && !empty($m->rolelagu[5]->pencipta->name_master) && empty($m->rolelagu[6]->pencipta->name_master) && empty($m->rolelagu[7]->pencipta->name_master) && empty($m->rolelagu[8]->pencipta->name_master) && empty($m->rolelagu[9]->pencipta->name_master)){
                
                    return $m->rolelagu[3]->pencipta->name_master.' & '.$m->rolelagu[4]->pencipta->name_master.' & '.$m->rolelagu[5]->pencipta->name_master;

                }elseif(empty($m->rolelagu[0]->pencipta->name_master) && empty($m->rolelagu[1]->pencipta->name_master) && empty($m->rolelagu[2]->pencipta->name_master) && !empty($m->rolelagu[3]->pencipta->name_master) && !empty($m->rolelagu[4]->pencipta->name_master) && !empty($m->rolelagu[5]->pencipta->name_master) && !empty($m->rolelagu[6]->pencipta->name_master) && empty($m->rolelagu[7]->pencipta->name_master) && empty($m->rolelagu[8]->pencipta->name_master) && empty($m->rolelagu[9]->pencipta->name_master)){
                
                    return $m->rolelagu[3]->pencipta->name_master.' & '.$m->rolelagu[4]->pencipta->name_master.' & '.$m->rolelagu[5]->pencipta->name_master.' & '.$m->rolelagu[6]->pencipta->name_master;
                
                }elseif(empty($m->rolelagu[0]->pencipta->name_master) && empty($m->rolelagu[1]->pencipta->name_master) && empty($m->rolelagu[2]->pencipta->name_master) && !empty($m->rolelagu[3]->pencipta->name_master) && !empty($m->rolelagu[4]->pencipta->name_master) && !empty($m->rolelagu[5]->pencipta->name_master) && !empty($m->rolelagu[6]->pencipta->name_master) && !empty($m->rolelagu[7]->pencipta->name_master) && empty($m->rolelagu[8]->pencipta->name_master) && empty($m->rolelagu[9]->pencipta->name_master)){
                
                    return $m->rolelagu[3]->pencipta->name_master.' & '.$m->rolelagu[4]->pencipta->name_master.' & '.$m->rolelagu[5]->pencipta->name_master.' & '.$m->rolelagu[6]->pencipta->name_master.' & '.$m->rolelagu[7]->pencipta->name_master;

                }elseif(empty($m->rolelagu[0]->pencipta->name_master) && empty($m->rolelagu[1]->pencipta->name_master) && empty($m->rolelagu[2]->pencipta->name_master) && empty($m->rolelagu[3]->pencipta->name_master) && !empty($m->rolelagu[4]->pencipta->name_master) && empty($m->rolelagu[5]->pencipta->name_master) && empty($m->rolelagu[6]->pencipta->name_master) && empty($m->rolelagu[7]->pencipta->name_master) && empty($m->rolelagu[8]->pencipta->name_master) && empty($m->rolelagu[9]->pencipta->name_master)){
                
                    return $m->rolelagu[4]->pencipta->name_master;

                }elseif(empty($m->rolelagu[0]->pencipta->name_master) && empty($m->rolelagu[1]->pencipta->name_master) && empty($m->rolelagu[2]->pencipta->name_master) && empty($m->rolelagu[3]->pencipta->name_master) && !empty($m->rolelagu[4]->pencipta->name_master) && !empty($m->rolelagu[5]->pencipta->name_master) && empty($m->rolelagu[6]->pencipta->name_master) && empty($m->rolelagu[7]->pencipta->name_master) && empty($m->rolelagu[8]->pencipta->name_master) && empty($m->rolelagu[9]->pencipta->name_master)){
                
                    return $m->rolelagu[4]->pencipta->name_master.' & '.$m->rolelagu[5]->pencipta->name_master;

                }elseif(empty($m->rolelagu[0]->pencipta->name_master) && empty($m->rolelagu[1]->pencipta->name_master) && empty($m->rolelagu[2]->pencipta->name_master) && empty($m->rolelagu[3]->pencipta->name_master) && !empty($m->rolelagu[4]->pencipta->name_master) && !empty($m->rolelagu[5]->pencipta->name_master) && !empty($m->rolelagu[6]->pencipta->name_master) && empty($m->rolelagu[7]->pencipta->name_master) && empty($m->rolelagu[8]->pencipta->name_master) && empty($m->rolelagu[9]->pencipta->name_master)){
                
                    return $m->rolelagu[4]->pencipta->name_master.' & '.$m->rolelagu[5]->pencipta->name_master.' & '.$m->rolelagu[6]->pencipta->name_master;

                }elseif(empty($m->rolelagu[0]->pencipta->name_master) && empty($m->rolelagu[1]->pencipta->name_master) && empty($m->rolelagu[2]->pencipta->name_master) && empty($m->rolelagu[3]->pencipta->name_master) && !empty($m->rolelagu[4]->pencipta->name_master) && !empty($m->rolelagu[5]->pencipta->name_master) && !empty($m->rolelagu[6]->pencipta->name_master) && !empty($m->rolelagu[7]->pencipta->name_master) && empty($m->rolelagu[8]->pencipta->name_master) && empty($m->rolelagu[9]->pencipta->name_master)){
                
                    return $m->rolelagu[4]->pencipta->name_master.' & '.$m->rolelagu[5]->pencipta->name_master.' & '.$m->rolelagu[6]->pencipta->name_master.' & '.$m->rolelagu[7]->pencipta->name_master;
                
                }elseif(empty($m->rolelagu[0]->pencipta->name_master) && empty($m->rolelagu[1]->pencipta->name_master) && empty($m->rolelagu[2]->pencipta->name_master) && empty($m->rolelagu[3]->pencipta->name_master) && !empty($m->rolelagu[4]->pencipta->name_master) && !empty($m->rolelagu[5]->pencipta->name_master) && !empty($m->rolelagu[6]->pencipta->name_master) && !empty($m->rolelagu[7]->pencipta->name_master) && !empty($m->rolelagu[8]->pencipta->name_master) && empty($m->rolelagu[9]->pencipta->name_master)){
                
                    return $m->rolelagu[4]->pencipta->name_master.' & '.$m->rolelagu[5]->pencipta->name_master.' & '.$m->rolelagu[6]->pencipta->name_master.' & '.$m->rolelagu[7]->pencipta->name_master.' & '.$m->rolelagu[8]->pencipta->name_master;

                }elseif(empty($m->rolelagu[0]->pencipta->name_master) && empty($m->rolelagu[1]->pencipta->name_master) && empty($m->rolelagu[2]->pencipta->name_master) && empty($m->rolelagu[3]->pencipta->name_master) && empty($m->rolelagu[4]->pencipta->name_master) && !empty($m->rolelagu[5]->pencipta->name_master) && empty($m->rolelagu[6]->pencipta->name_master) && empty($m->rolelagu[7]->pencipta->name_master) && empty($m->rolelagu[8]->pencipta->name_master) && empty($m->rolelagu[9]->pencipta->name_master)){
                
                    return $m->rolelagu[5]->pencipta->name_master;

                }elseif(empty($m->rolelagu[0]->pencipta->name_master) && empty($m->rolelagu[1]->pencipta->name_master) && empty($m->rolelagu[2]->pencipta->name_master) && empty($m->rolelagu[3]->pencipta->name_master) && empty($m->rolelagu[4]->pencipta->name_master) && !empty($m->rolelagu[5]->pencipta->name_master) && !empty($m->rolelagu[6]->pencipta->name_master) && empty($m->rolelagu[7]->pencipta->name_master) && empty($m->rolelagu[8]->pencipta->name_master) && empty($m->rolelagu[9]->pencipta->name_master)){
                
                    return $m->rolelagu[5]->pencipta->name_master.' & '.$m->rolelagu[6]->pencipta->name_master;

                }elseif(empty($m->rolelagu[0]->pencipta->name_master) && empty($m->rolelagu[1]->pencipta->name_master) && empty($m->rolelagu[2]->pencipta->name_master) && empty($m->rolelagu[3]->pencipta->name_master) && empty($m->rolelagu[4]->pencipta->name_master) && !empty($m->rolelagu[5]->pencipta->name_master) && !empty($m->rolelagu[6]->pencipta->name_master) && !empty($m->rolelagu[7]->pencipta->name_master) && empty($m->rolelagu[8]->pencipta->name_master) && empty($m->rolelagu[9]->pencipta->name_master)){
                
                    return $m->rolelagu[5]->pencipta->name_master.' & '.$m->rolelagu[6]->pencipta->name_master.' & '.$m->rolelagu[7]->pencipta->name_master;

                }elseif(empty($m->rolelagu[0]->pencipta->name_master) && empty($m->rolelagu[1]->pencipta->name_master) && empty($m->rolelagu[2]->pencipta->name_master) && empty($m->rolelagu[3]->pencipta->name_master) && empty($m->rolelagu[4]->pencipta->name_master) && !empty($m->rolelagu[5]->pencipta->name_master) && !empty($m->rolelagu[6]->pencipta->name_master) && !empty($m->rolelagu[7]->pencipta->name_master) && !empty($m->rolelagu[8]->pencipta->name_master) && empty($m->rolelagu[9]->pencipta->name_master)){
                
                    return $m->rolelagu[5]->pencipta->name_master.' & '.$m->rolelagu[6]->pencipta->name_master.' & '.$m->rolelagu[7]->pencipta->name_master.' & '.$m->rolelagu[8]->pencipta->name_master;
                
                }elseif(empty($m->rolelagu[0]->pencipta->name_master) && empty($m->rolelagu[1]->pencipta->name_master) && empty($m->rolelagu[2]->pencipta->name_master) && empty($m->rolelagu[3]->pencipta->name_master) && empty($m->rolelagu[4]->pencipta->name_master) && !empty($m->rolelagu[5]->pencipta->name_master) && !empty($m->rolelagu[6]->pencipta->name_master) && !empty($m->rolelagu[7]->pencipta->name_master) && !empty($m->rolelagu[8]->pencipta->name_master) && !empty($m->rolelagu[9]->pencipta->name_master)){
                
                    return $m->rolelagu[5]->pencipta->name_master.' & '.$m->rolelagu[6]->pencipta->name_master.' & '.$m->rolelagu[7]->pencipta->name_master.' & '.$m->rolelagu[8]->pencipta->name_master.' & '.$m->rolelagu[9]->pencipta->name_master;

                }else{
                    return "&nbsp;";
                }
            });
        }
    }

    public function query(Request $request, MasterLagu $model)
    {
        //dd($request->all());
        //$query = $model->with('penyanyi', 'pencipta')->newQuery()->orderBy('master_lagu.created_at', 'DESC');
        if (Auth::user()->roles->first()->name == 'admin' || Auth::user()->roles->first()->name == 'legal') {
            if(!empty($request->album)){
                $query = $model->newQuery()->with('rolelagu','rolelagu.penyanyi','rolelagu.pencipta','rolelagucountry.country')
                ->where('release_title', 'LIKE', '%'.$request->album.'%')
                ->orderBy('master_lagu.id', 'DESC');
            }else{
                $query = $model->newQuery()->with('rolelagu','rolelagu.penyanyi','rolelagu.pencipta','rolelagucountry.country')
                ->orderBy('master_lagu.updated_at', 'DESC');
            }

        }elseif (Auth::user()->roles->first()->name == 'singer') {
            $query = $model->with('rolelagu','rolelagu.penyanyi','rolelagu.pencipta')
            ->newQuery()
            ->whereHas('rolelagu', function($q){
                $q->where('role_lagu.id_user', Auth::user()->id);
            })
            ->orderBy('master_lagu.updated_at', 'DESC');
        }elseif (Auth::user()->roles->first()->name == 'songwriter') {
            $query = $model->with('rolelagu','rolelagu.penyanyi','rolelagu.pencipta')
            ->newQuery()
            ->whereHas('rolelagu', function($q){
                $q->where('role_lagu.id_user', Auth::user()->id);
            })
            ->orderBy('master_lagu.updated_at', 'DESC');
        }
        return $query;
    }

    public function html()
    {
        if (Auth::user()->roles->first()->name == 'admin') {
            return $this->builder()
                ->columns($this->getColumns())
                ->minifiedAjax()
                ->ajax([
                    'url'  => '',
                    'data' => "function(d) {
                        d.album      = $('#album').val();
                        d.judul      = $('#judul').val();
                    }",
                ])
                ->addAction(['printable' => false],['width' => '80px'])
                ->parameters([
                    'dom'     => 'Brtlip',
                    'targets' => 'no-sort',
                    'bSort' =>  false,
                    'order'   => [[1,'desc']],
                    'responsive' => true,
                    'autoWidth' => false,
                    'aLengthMenu' => [[10, 50, 100, 250, -1], [10, 50, 100, 250, 'All']],
                    'columnDefs' => [
                        [
                            "targets" => [0,3], // your case first column
                            "className" => "text-center",
                        ]
                    ],
                    // 'language' => [
                    //     'search' => 'Search By Album:'
                    // ],
                    'buttons' => [
                        [ 
                            "extend" => 'create', 
                            "text" => '<i class="fa fa-plus"></i> Add New',
                            "className" => 'btn-primary',
                            'action' => 'function( e, dt, button, config){ 
                                window.location = "masterlagu/create";
                            }'
                        ],
                        [ 
                            "extend" =>'csv', 
                            "text" =>'<i class="fa fa-file-excel-o"></i> CSV',
                            "className" => 'btn-primary',
                            'action' => 'function( e, dt, button, config){ 
                                window.location = "masterlagu/export_csv";
                            }'
                        ],
                        [ 
                            "extend" =>'excel', 
                            "text" =>'<i class="fa fa-file-excel-o"></i> Excel',
                            "className" => 'btn-primary',
                            'action' => 'function( e, dt, button, config){ 
                                window.location = "masterlagu/export_excell";
                            }'
                        ],
                        [ 
                            "extend" => 'pdf', 
                            "text" =>'<i class="fa fa-file-pdf-o"></i> PDF',
                            "className" =>'btn-primary',
                            'action' => 'function( e, dt, button, config){ 
                                window.location = "masterlagu/export_print";
                            }'
                        ]
                    ],
                ]);
        }elseif (Auth::user()->roles->first()->name == 'legal') {
            return $this->builder()
                ->columns($this->getColumns())
                ->minifiedAjax()
                ->ajax([
                    'url'  => '',
                    'data' => "function(d) {
                        d.album      = $('#album').val();
                        d.judul      = $('#judul').val();
                    }",
                ])
                ->parameters([
                    'dom'     => 'Bfrtlip',
                    'order'   => [[0,'desc']],
                    'responsive' => true,
                    'autoWidth' => false,
                    'buttons' => [
                        [ 
                            "extend" => 'create', 
                            "text" => '<i class="fa fa-plus"></i> Add New',
                            "className" => 'btn-primary'
                        ],
                        [ 
                            "extend" =>'excel', 
                            "text" =>'<i class="fa fa-file-excel-o"></i> Excel',
                            "className" => 'btn-primary',
                            'action' => 'function( e, dt, button, config){ 
                                window.location = "masterlagu/export_excell";
                            }'
                        ],
                        [ 
                            "extend" => 'pdf', 
                            "text" =>'<i class="fa fa-file-pdf-o"></i> PDF',
                            "className" =>'btn-primary',
                            'action' => 'function( e, dt, button, config){ 
                                window.location = "masterlagu/export_print";
                            }'
                        ]
                    ],
                ]);
        }else{
            return $this->builder()
                ->columns($this->getColumns())
                ->minifiedAjax()
                ->parameters([
                    'dom'     => 'Brtlip',
                    'order'   => [[0,'desc']],
                    'responsive' => true,
                    'autoWidth' => false,
                    'buttons' => [
                        [ 
                            "extend" =>'excel', 
                            "text" =>'<i class="fa fa-file-excel-o"></i> Excel',
                            "className" => 'btn-primary',
                            'action' => 'function( e, dt, button, config){ 
                                window.location = "masterlagu/export_excell";
                            }'
                        ],
                        [ 
                            "extend" => 'pdf', 
                            "text" =>'<i class="fa fa-file-pdf-o"></i> PDF',
                            "className" =>'btn-primary',
                            'action' => 'function( e, dt, button, config){ 
                                window.location = "masterlagu/export_print";
                            }'
                        ]
                    ],
                ]);
        }
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            /* 'id', */
            'judul' => [
                'name' => 'judul',
                'data' => 'judul',
                'title' => 'Judul',
                'defaultContent' => '',
            ],
            [
                'name' => 'release_title',
                'data' => 'album',
                'title' => 'Album',
                'defaultContent' => '',
            ],
            [
                'name' => 'release_date',
                'data' => 'tanggal release',
                'title' => 'Tanggal Release',
                'defaultContent' => '',
            ],
            [
                'name' => 'penyanyi',
                'data' => 'penyanyi',
                'title' => 'Penyanyi',
                'defaultContent' => '',
            ],
            [
                'name' => 'pencipta',
                'data' => 'pencipta',
                'title' => 'Pencipta / Publishing',
                'defaultContent' => '',
            ],
            // 'isrc' => [
            //     'isrc' => 'isrc',
            // ],
            // 'upc' => [
            //     'upc' => 'upc',
            // ],
            // '% pencipta / publishing' => [
            //     '% pencipta / publishing' => 'percentage_pencipta',
            // ],
            // '% penyanyi' => [
            //     '% penyanyi' => 'percentage_penyanyi',
            // ],
            // 'country' => [
            //     'country' => 'country_name',
            // ],
            // '% rights recording' => [
            //     '% rights recording' => 'percentage_rights',
            // ],
        ];
    }

}