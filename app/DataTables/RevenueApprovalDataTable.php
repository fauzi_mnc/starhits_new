<?php

namespace App\DataTables;

use App\Model\Revenue;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use App\Enum\RevenueStatus;

class RevenueApprovalDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
                        ->addColumn('month', function($m){
                            $monthName = date('F', mktime(0, 0, 0, $m->month, 10)); // March
                            return $monthName; 
                        })->addColumn('year', function($m){
                            return $m->year; 
                        })->addColumn('status', function($m){
                            return RevenueStatus::getText($m->status);
                        })
                        ->orderColumns(['month', 'year'], '-:column $1')
                        ->addColumn('action', 'revenue.approval.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Post $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Revenue $model)
    {
        $query = $model->newQuery();

        $query->where('status', '<>', RevenueStatus::CEKER());

        if(request()->month){
            $query->where('month', request()->month);
        }

        if(request()->year){
            $query->where('year', request()->year);
        }

        return $query;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->ajax([
                'url'  => '',
                'data' => "function(d) {
                    d.month  = $('#month').val();
                    
                }",
            ])
            ->addAction(['width' => '80px'])
            ->parameters([
                'dom'     => 'Brtlip',
                'order'   => [[0, 'desc']],
                'responsive' => true,
                'autoWidth' => false,
                'buttons' => [
                    /*'create',
                    'export',
                    'print',
                    'reset',
                    'reload',*/
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'month',
            'year',
            'status',
        ];
    }

}