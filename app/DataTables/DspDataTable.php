<?php

namespace App\DataTables;

use App\Model\Dsp;
use App\Model\DspDetails;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class DspDataTable extends DataTable
{
   
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('ReportMonth', function($m){
                            $reportingMonth = date('d F Y', strtotime($m->reporting_month));
                            return $reportingMonth;
                        
                        })->addColumn('action', 'dsp.datatables_actions');
    }
 
    public function query(Dsp $model)
    {
        $query = $model->newQuery();

       if(request()->y){
            $query->where('reporting_month', 'LIKE', '%'.request()->y.'%');
        }

        return $query;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax([
                'url'  => '',
                'data' => "function(d) {
                    d.reporting_month   = $('#ReportMonth').val();
                }",
            ])
            ->addAction(['width' => '280px'])
            ->parameters([
                'dom'     => 'Bfrtlip',
                'order'   => [[0, 'desc']],
                'responsive' => true,
                'autoWidth' => false,
                'buttons' => [
                    [ 
                        "extend" => 'create', 
                        "text" => '<i class="fa fa-plus"></i> Add New',
                        "className" => 'btn-primary',
                        'action' => 'function( e, dt, button, config){ 
                            window.location = "dsp/create";
                        }'
                    ]
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'ReportMonth' => [
                'ReportMonth' => 'reporting_month',
                'name' => 'reporting_month'
            ],
        ];
    }

}