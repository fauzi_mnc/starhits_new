<?php

namespace App\DataTables;

use App\Model\User;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class BrandDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('is_active', function($m){
                            return $m->is_active? 'Ya' : 'Tidak';
                        })->addColumn('action', 'brand.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Post $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(User $model)
    {   
        return $model->newQuery()->whereHas('accessrole', function ($query) {
            $query->where('role_id', '5');
        })->orderBy('updated_at', 'desc');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '80px'])
            ->parameters([
                'dom'     => 'Bfrtip',
                'order'   => [[0, 'desc']],
                'responsive' => true,
                'autoWidth' => false,
                'buttons' => [
                    [ 
                        "extend" => 'create', 
                        "text" => '<i class="fa fa-plus"></i> Add New',
                        "className" => 'btn-primary',
                    ],
                    [ 
                        "extend" => 'reset', 
                        "text" => '<i class="fa fa-undo"></i> Reset',
                        "className" => 'btn-primary'
                    ],
                    [ 
                        "extend" => 'reload', 
                        "text" => '<i class="fa fa-refresh"></i> Reload',
                        "className" => 'btn-primary'
                    ]
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'brand_name',
            'name' => ['title' => 'PIC'],
            'email',
            'phone',
            'is_active'
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'brand_datatable_' . time();
    }
}