<?php

namespace App\DataTables;

use App\Model\User;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class MasterSongwriterDataTabel extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
        ->addColumn('name', function($m){
            return $m->name_master;
        })
        ->addColumn('action', 'masterlagu.songwriter.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Post $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(User $model)
    {
        return $model->newQuery()->whereHas('accessrole', function ($query) {
            $query->whereIn('role_id', ['10']);
        })->orderBy('updated_at', 'desc');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '180px'])
            ->parameters([
                'dom'     => 'Bfrtlip',
                'order'   => [[0, 'asc']],
                'responsive' => true,
                'autoWidth' => false,
                'buttons' => [
                    [ 
                        "extend" => 'create', 
                        "text" => '<i class="fa fa-plus"></i> Add New',
                        "className" => 'btn-primary'
                    ],
                    'reset',
                    //'reload',
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'name' => [
                'name' => 'name_master',
            ],
            'email'
        ];
    }

}