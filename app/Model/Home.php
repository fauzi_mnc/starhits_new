<?php

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Home extends Content
{
    protected static function boot()
    {
        parent::boot();
        $type = ['starhits','video','article'];
        $map  = [

        ];

        static::addGlobalScope('', function (Builder $builder) use ($type) {
            $builder->whereIn('type', $type);
        });

        self::creating(function ($model) use ($type, $map) {
            $model->type = $type;
            foreach ($map as $k => $v) {
                $model->$v = request()->$k;
            }
        });

        self::retrieved(function ($model) use ($map) {
            foreach ($map as $k => $v) {
                $model->$k = $model->$v;
            }
        });
    }

    /**
     * undocumented function
     *
     * @return void
     * @author
     **/

    public function channelVideo()
    {
        return $this->hasOne(Channel::class, 'attr_1', 'attr_1');
    }

    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function series()
    {
        return $this->belongsTo(Series::class, 'parent_id');
    }

    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function starhits()
    {
        return $this->belongsTo(Starhits::class, 'parent_id');
    }
}
