<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Analytics extends Model
{
	protected $table    = 'analytics';
	protected $fillable = [
        'type', 'date', 'user_id', 'views', 'comments', 'shares', 'likes', 'dislikes', 'males', 'females', 'subscribers_gained', 'subscribers_lost', 'estimate_minutes_watched', 'average_view_duration', 'annotation_impressions', 'close_impressions', 'annotation_close', 'annotation_click_through_rate', 'click_impressions', 'annotation_click','annotation_close_rate', 'created_at', 'updated_at'
    ];
}	
