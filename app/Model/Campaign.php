<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Campaign extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $table    = 'campaign';
    protected $primaryKey    = 'camp_id';
    protected $fillable = ['brand_id', 'category_id', 'type', 'title', 'start_date', 'end_date', 'socio_economic_status', 'brief_campaign', 'budget', 'status', 'active', 'notes'];
    
    protected $auditInclude = [
        'title',
        'content',
    ];


    public function user()
    {
        return $this->belongsTo(User::class, 'brand_id');
    }

    function influencer()
    {
        return $this->hasMany(Influencer::class, 'campaign_id');
    }

    function campaign_post()
    {
        return $this->hasMany(CampaignPost::class, 'campaign_id');
    }
}
