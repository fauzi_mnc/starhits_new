<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Serie extends Content
{
    protected $appends = ['totalVideo'];

    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    protected static function boot()
    {
        parent::boot();

        $type = ['starpro', 'starhits'];
        $map  = [
            'user_id' => 'user_id',
            // 'userID' => 'user_id',
        ];

        static::addGlobalScope('', function (Builder $builder) use ($type) {
            $builder->whereIn('type', $type);
            // $builder->orderBy('content', 'desc');
        });

        // self::creating(function ($model) use ($type, $map) {
        //     $model->type = $type;
        //     foreach ($map as $k => $v) {
        //         $model->$v = request()->$k;
        //     }
        // });

        self::retrieved(function ($model) use ($map) {
            foreach ($map as $k => $v) {
                $model->$k = $model->$v;
            }
        });
    }

    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function videos()
    {
        return $this->hasMany(Video::class, 'parent_id')
                    ->where('is_active', '=', '1');
    }

    /**
     * undocumented function
     *
     * @return void
     * @author
     **/

    public function channels()
    {
        return $this->belongsTo(Channel::class, 'parent_id', 'attr_1');
    }

    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function users()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function getTotalVideoAttribute()
    {
        return $this->videos()->count();
    }
}
