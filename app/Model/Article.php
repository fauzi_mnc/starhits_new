<?php

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Article extends Content
{
    protected static function boot()
    {
        parent::boot();
        $type = 'article';
        $map  = [
              'series' => 'parent_id',
        ];

        static::addGlobalScope('', function (Builder $builder) use ($type) {
            $builder->where('type', $type);
        });

        self::creating(function ($model) use ($type, $map) {
            $model->type = $type;
            foreach ($map as $k => $v) {
                $model->$v = request()->$k;
            }
        });

        self::retrieved(function ($model) use ($map) {
            foreach ($map as $k => $v) {
                $model->$k = $model->$v;
            }
        });
    }

    /**
     * undocumented function
     *
     * @return void
     * @author
     **/

    public function channelVideo()
    {
        return $this->hasOne(Channel::class, 'attr_1', 'attr_1');
    }

    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function users()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function series()
    {
        return $this->belongsTo(Series::class, 'series');
    }
}
