<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Content extends Model implements Auditable
{
	use \OwenIt\Auditing\Auditable;
	
	protected $table    = 'contents';
    protected $fillable = [
        'user_id', 'type', 'title', 'subtitle', 'slug', 'excerpt', 'content', 'image', 'order', 'seo_title',
        'seo_keyword', 'seo_description', 'parent_id', 'is_active', 'is_featured', 'is_popular', 'token', 'post_type', 'created_by',
        'viewed', 'attr_1', 'attr_2', 'attr_3', 'attr_4', 'attr_5', 'attr_6',
    ];

    protected $auditInclude = [
        'title',
        'content',
    ];
}
