<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserGroup extends Model
{
    protected $table    = 'users_group';
	public $timestamps	= false;
	protected $fillable = [
		'user_master', 'user_child'
	];

	public function user_master(){
        return $this->belongsTo(User::class, 'user_master', 'id');
    }
	public function user_child(){
        return $this->belongsTo(User::class, 'user_child', 'id');
    }
}
