<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class YoutubeAnaliticLink extends Model
{

    protected $table    = 'youtube_analitic_link';
    public $timestamps  = true;

	protected $fillable = [
		'link'
	];
    
}
