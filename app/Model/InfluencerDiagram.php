<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class InfluencerDiagram extends Model
{
    protected $table    = 'influencer_diagram';
    protected $primaryKey = "id";
    ///public $incrementing = false;
    protected $fillable = [
        'user_id', 'followers_ig', 'avglike', 'avgcomment', 'engagement'
    ];

}
