<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class CampaignTags extends Model implements Auditable
{
	use \OwenIt\Auditing\Auditable;
	
	protected $table    = 'campaign_tags';
	public $timestamps  = false;
    protected $fillable = [
        'content_id', 'term_id', 'created_at', 'updated_at'
    ];

    protected $auditInclude = [
        'title',
        'content',
    ];
}
