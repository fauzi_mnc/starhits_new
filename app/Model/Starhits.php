<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Starhits extends Content
{
	/**
     * undocumented function
     *
     * @return void
     * @author
     **/
    protected static function boot()
    {
        parent::boot();

        $type = ['starpro'];
        $map  = [

        ];

        static::addGlobalScope('', function (Builder $builder) use ($type) {
            $builder->whereNotIn('type', $type);
        });

        self::retrieved(function ($model) use ($map) {
            foreach ($map as $k => $v) {
                $model->$k = $model->$v;
            }
        });
    }

}
