<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Socials extends Model implements Auditable
{
	use \OwenIt\Auditing\Auditable;
    protected $table    = 'socials';
    public $timestamps = false;
    protected $fillable = [
        'user_id', 'name', 'url',
    ];

    protected $auditInclude = [
        'title',
        'content',
    ];
}
