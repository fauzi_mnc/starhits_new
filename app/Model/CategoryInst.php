<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class CategoryInst extends Model implements Auditable
{
	use \OwenIt\Auditing\Auditable;

	protected $table    = 'category_inst';
	protected $fillable = ['title'];

	protected $auditInclude = [
        'title',
        'content',
    ];
}
