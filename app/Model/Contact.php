<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
	protected $fillable = ['name', 'email', 'message'];
    public $timestamps  = false;

    /**
     * undocumented function
     *
     * @return void
     * @author 
     **/
    public static function boot()
    {
    	parent::boot();

        static::creating(function ($model) {
            $model->created_at = $model->freshTimestamp();
        });
    }
}
