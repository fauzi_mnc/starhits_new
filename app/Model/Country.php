<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{

    protected $table    = 'country';
    public $timestamps  = false;

	protected $fillable = [
		'id', 'country_code', 'country_name'
	];
    
    function rolelagucountry(){
        return $this->hasMany(RoleLaguCountry::class, 'id_country');
    }
}
