<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class RoleUser extends Model implements Auditable
{
	use \OwenIt\Auditing\Auditable;

	protected $table    = 'role_user';
	public $timestamps	= false;
	protected $fillable = [
		'user_id', 'role_id'
	];

	protected $auditInclude = [
        'title',
        'content',
    ];
}
