<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RoleLagu extends Model
{
	protected $table    = 'role_lagu';
	public $timestamps  = false;

	protected $fillable = [
		'id_master_lagu', 'id_user'
	];

	public function penyanyi()
	{
			//return $this->belongsTo(User::class, 'id_user');
			return $this->belongsTo(User::class, 'id_user')
				->whereHas('accessrole', function($q){
					$q->where('role_id', '9');
			});
    }
	public function pencipta()
	{
			//return $this->belongsTo(User::class, 'id_user');
			return $this->belongsTo(User::class, 'id_user')
				->whereHas('accessrole', function($q){
					$q->where('role_id', '10');
			});
    }

    public function user(){
		return $this->belongsTo(User::class, 'id_user');
	}

	public function masterlagu(){
		return $this->belongsTo(MasterLagu::class, 'id_master_lagu');
	}
}