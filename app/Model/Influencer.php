<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Influencer extends Model
{
	// use \OwenIt\Auditing\Auditable;

	protected $table    = 'influencer';
    protected $primaryKey = null;
    public $incrementing = false;
    protected $fillable = [
        'campaign_id', 'user_id', 'approval', 'reason'
    ];

    // protected $auditInclude = [
    //     'title',
    //     'content',
    // ];

    public function campaign()
    {
        return $this->belongsTo(Campaign::class, 'campaign_id');
    }

    public function user()
    {
        return $this->hasMany(User::class, 'user_id', 'id');
    }
}
