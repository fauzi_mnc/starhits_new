<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Tags extends Model implements Auditable
{
	use \OwenIt\Auditing\Auditable;
	protected $table    = 'tags';
	public $timestamps  = false;
    protected $fillable = [
        'name', 'slug', 'image', 'logo', 'type', 'order', 'is_active', 'is_popular', 'parent_id', 'created_at', 'updated_at', 'creted_by'
    ];

    protected $auditInclude = [
        'title',
        'content',
    ];
}
