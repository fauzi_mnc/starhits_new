<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class ContentsTerms extends Model implements Auditable
{
	use \OwenIt\Auditing\Auditable;
	
	protected $table    = 'contents_terms';
	public $timestamps  = false;
    protected $fillable = [
        'content_id', 'term_id', 'created_at', 'updated_at'
    ];

    protected $auditInclude = [
        'title',
        'content',
    ];
}
