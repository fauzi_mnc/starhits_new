<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Ads extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $table    = 'ads';
    protected $primaryKey = 'ads_id';
    protected $dateFormat = 'Y-m-d H:i:s';
    protected $dates = ['start_date', 'end_date'];
    protected $fillable = ['title', 'category', 'type', 'image', 'video', 'youtube_id', 'target', 'position', 'status', 'start_date', 'end_date', 'script'];

    protected $auditInclude = [
        'title',
        'content',
    ];
}
