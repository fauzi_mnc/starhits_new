<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class MasterLagu extends Model
{
    protected $table    = 'master_lagu';
    public $timestamps = true;

    public function penyanyi()
	{
			//return $this->belongsTo(User::class, 'id_user');
			return $this->belongsTo(RoleLagu::class, 'id_user');
    }
    public function pencipta()
	{
			return $this->belongsTo(RoleLagu::class, 'id_user');
	}
	public function rolelagu(){
		return $this->hasMany(RoleLagu::class, 'id_master_lagu');
	}
	public function rolelagucountry(){
		return $this->hasMany(RoleLaguCountry::class, 'id_master_lagu');
	}
}
