<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class YoutubeAnalitic extends Model
{

    protected $table    = 'youtube_analitic';
    public $timestamps  = true;

	protected $fillable = [
		'id', 'name', 'name_channel', 'subscribers', 'viewer', 'like_count', 'dislike_count', 'comment_count'
	];
    
}
