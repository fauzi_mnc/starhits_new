<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RoleLaguCountry extends Model
{
	protected $table    = 'role_lagu_country';
	public $timestamps  = false;

	protected $fillable = [
		'id_master_lagu', 'id_country'
	];

    public function country(){
		return $this->belongsTo(Country::class, 'id_country');
	}
}