<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class CampaignPost extends Model implements Auditable
{
	use \OwenIt\Auditing\Auditable;

	protected $table    = 'campaign_post';
    protected $fillable = [
        'campaign_id', 'post_id'
    ];

    protected $auditInclude = [
        'title',
        'content',
    ];

    public function campaign()
	{
		return $this->belongsTo('App\model\Campaign', 'campaign_id');
	}
}
