<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class SiteConfig extends Model implements Auditable
{
	use \OwenIt\Auditing\Auditable;

	protected $table    = 'configs';
    protected $fillable = [
        'key', 'value'
    ];
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $auditInclude = [
        'title',
        'content',
    ];
}
