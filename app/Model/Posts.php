<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Posts extends Content
{
    protected $primaryKey = 'id';

    protected static function boot()
    {
        parent::boot();

        $type = ['video', 'article'];

        static::addGlobalScope('', function (Builder $builder) use ($type) {
            $builder->whereIn('type', $type);
        });

    }

    /**
     * undocumented function
     *
     * @return void
     * @author
     **/

    public function channel()
    {
        return $this->belongsTo(Channel::class, 'attr_1', 'attr_1');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function bookmarkUser()
    {
        return $this->belongsToMany('App\model\User', 'bookmark', 'content_id', 'user_id')->withPivot('user_id', 'content_id');
    }
}
