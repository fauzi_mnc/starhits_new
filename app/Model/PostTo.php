<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class PostTo extends Model implements Auditable
{
	use \OwenIt\Auditing\Auditable;

	protected $table    = 'post_to';
	protected $fillable = ['title'];

	protected $auditInclude = [
        'title',
        'content',
    ];
}
