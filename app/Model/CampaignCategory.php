<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class CampaignCategory extends Model implements Auditable
{
	use \OwenIt\Auditing\Auditable;

	protected $table    = 'campaign_category';
    protected $fillable = [
        'campaign_id', 'category_id'
    ];

    protected $auditInclude = [
        'title',
        'content',
    ];
}
