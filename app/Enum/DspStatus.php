<?php
namespace App\Enum;

use MyCLabs\Enum\Enum;

/**
 * Action enum
 */
class RevenueStatus extends Enum
{
    private const DRAFT = 1;
    private const WTA = 3;
    private const APPROVED = 4;
    private const REVISION = 5;
    private const REJECTED = 6;

    public static function getText($value) {
        $mapping = [
            self::DRAFT     => 'Draft',
            self::WTA       => 'Waiting To Approve',
            self::APPROVED  => 'Approved',
            self::REVISION  => 'Revision',
            self::REJECTED  => 'Rejected',
        ];

        return $mapping[$value];
    }

}