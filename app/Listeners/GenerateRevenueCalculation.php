<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\RevenueWaitingToApproved;
use PDF;
use Carbon;
use App\Model\User;
use App\Model\RevenueApprover;

class GenerateRevenueCalculation
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(RevenueWaitingToApproved $event)
    {
        $filename = sprintf('Penghitungan-Revenue-%s-%s.pdf', $event->revenue->month, $event->revenue->year);
        $month = Carbon::createFromDate($event->revenue->year, $event->revenue->month, null)->format('F');
        $approver = RevenueApprover::where('type', 1)->where('is_active', 1)->first();
        $checker = RevenueApprover::where('type', 2)->where('is_active', 1)->get();
        $creators = $event->revenue->creator;
        $total_gross_revenue = $event->revenue->total_gross_revenue;
        $sum_estimate_gross = $creators->sum(function ($creator) {
            return $creator->pivot->estimate_gross;
        });
        
        $creators->map(function ($creator) use($sum_estimate_gross, $total_gross_revenue) {
            $creator->percentage_revenue = ($creator->pivot->estimate_gross / $sum_estimate_gross);
            $creator->gross_revenue = $creator->percentage_revenue * $total_gross_revenue;
            $creator->share_revenue = $creator->gross_revenue - $creator->pivot->cost_production;
            $creator->nett_revenue = $creator->share_revenue * ($creator->percentage / 100);
            
            return $creator;
        });
        
        $pdf = PDF::loadView('revenue.revenue_calculation', [
            'revenue' => $event->revenue,
            'creators' => $creators,
            'month' => $month,
            'approver' => $approver,
            'checker' => $checker])
                    ->setPaper('a3', 'portrait')
                    ->setWarnings(false)
                    ->save(public_path('uploads/revenue/pdf/'.$filename));
        
        $event->revenue->update(['pdf' => 'uploads/revenue/pdf/'.$filename]);
    }
}
