<?php
namespace App\Extend;

use DB;

class DailymotionCli extends \Dailymotion
{
    public $timeout = 60;

    /**
     * Maximum number of seconds to wait for connection establishment of HTTP requests.
     * @var int
     */
    public $connectionTimeout = 5;

    /**
     * Where to store the current application session.
     * @var string
     */
    protected $table = 'user_token';

    /**
     * Define where to store the session on the file system.
     */
    public function __construct()
    {
        
    }
    /**
     * Overloading the default implementation with file system implementation.
     * `readSession` is used to restore the session from its storage medium.
     * @return array Restored session information.
     */
    protected function readSession()
    {
        $token = DB::table($this->table)->where('user_id', auth()->user()->id)->where('type', 'dailymotion')->whereNotNull('access_token')->first();

        if($token){
            $token = collect($token)->except(['id', 'user_id', 'type', 'created', 'token_type', 'token_secret'])->toArray();

            $token['scope'] = explode(chr(32), $token['scopes']);
            $token['expires'] = $token['expires_in'];
            $token['grant_type'] = 1;

            return $token;
        }

        return [];
    }
    /**
     * Overloading the default implementation with file system implementation.
     * `storeSession` is used to store the session to its storage medium.
     *
     * @param array $session Session information to store.
     * @return DailymotionCli $this
     */
    protected function storeSession(array $accessToken = array())
    {
        $accessTokenDb = DB::table($this->table)->where('user_id', auth()->user()->id)->where('type', 'dailymotion')->get();

        if(!$accessTokenDb->isEmpty()){
            DB::table($this->table)->where('user_id', auth()->user()->id)->where('type', 'dailymotion')->update([
                'access_token'  => $accessToken['access_token'],
                'refresh_token' => $accessToken['refresh_token'],
                'expires_in'    => $accessToken['expires'],
                'scopes'        => implode(chr(32), $accessToken['scope'])
            ]);
        } else {
            DB::table($this->table)->insert([
                'user_id'       => auth()->user()->id,
                'type'          => 'dailymotion',
                'access_token'  => isset($accessToken['access_token']) ? $accessToken['access_token'] : null,
                'refresh_token' => isset($accessToken['refresh_token']) ? $accessToken['refresh_token'] : null,
                'expires_in'    => isset($accessToken['expires']) ? $accessToken['expires'] : null,
                'scopes'        => isset($accessToken['scope']) ? implode(chr(32), $accessToken['scope']) : null
            ]);
        }
        
        return $this;
    }

    public function clearSession()
    {
        DB::table($this->table)->where('user_id', auth()->user()->id)->where('type', 'dailymotion')->delete();

        return $this;
    }
}