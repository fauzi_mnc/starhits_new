<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Backend\Controller;
use App\Repositories\RevenueRepository;
use App\Repositories\UserRepository;
use App\DataTables\RevenueApprovalDataTable;
use Flash;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;

class RevenueApprovalController extends Controller
{
    /**
     * The user repository instance.
     */
    protected $revenue;
    protected $users;

    /**
     * Create a new controller instance.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct(RevenueRepository $revenue, UserRepository $users)
    {
        $this->revenue = $revenue;
        $this->users = $users;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(RevenueApprovalDataTable $revenueApproval)
    {
        $months[''] = 'Month';
        $months['01'] = 'January';
        $months['02'] = 'February';
        $months['03'] = 'March';
        $months['04'] = 'April';
        $months['05'] = 'May';
        $months['06'] = 'June';
        $months['07'] = 'July';
        $months['08'] = 'August';
        $months['09'] = 'September';
        $months['10'] = 'October';
        $months['11'] = 'November';
        $months['12'] = 'December';
        
        return $revenueApproval->render('revenue.approval.index', ['months' => $months]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $creators = $this->users->getlistCreator();
        
        return view('revenue.create', [
            'creators' => $creators
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $revenueInput = [
            'month'                 => $request->month_b,
            'year'                  => $request->year_b,
            'status'                => $request->status,
            'total_gross_revenue'   => $request->total_gross_revenue
        ];
        
        $revenue = $this->revenue->create($revenueInput);
        
        foreach($request->revenue_creator as $revenue_creator){
            $data = explode(',', $revenue_creator);
            $revenueCreatorInput[] = [
                'revenue_id'        => $revenue->id,
                'creator_id'        => $data[0],
                'estimate_gross'    => $data[1],
                'cost_production'  => $data[2],
            ];
        }
        $revenue->creator()->attach($revenueCreatorInput);
        
        Flash::success('Revenue saved successfully.');

        return redirect(route('revenue.approval.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = $this->users->find($id);

        if (empty($user)) {
            Flash::error('User not found');

            return redirect(route('revenue.index'));
        }
        
        $listRole = $this->roles->getList();

        return view('revenue.edit', [
            'user'  => $user,
            'roles' => $listRole
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, $id)
    {
        $user = $this->users->find($id);
        
        if (empty($user)) {
            Flash::error('user not found');

            return redirect(route('revenue.index'));
        }

        $input = $request->except(['_method', '_token']);
        
        if($request->has('image')){
            $result = Storage::disk('local_public')->delete($user->image);
            $image = $request->image->store('uploads/creators/image', 'local_public');
            $input['image'] = $image;
        }

        if($request->has('roles')){
            $user->roles()->sync($request->input('roles'));
        }
        
        $user = $this->users->update($input, $id);

        Flash::success('user updated successfully.');

        return redirect(route('revenue.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        if ($request->action == 'act') {
            $creator = User::find($id);

            if (empty($creator)) {
                Flash::error('Creator not found');

                return redirect(route('creator.index'));
            }
            $creator['is_active'] = 1;

            $creator->save();

            Flash::success('Creator activated successfully.');
        }else{
            $creator = User::find($id);

            if (empty($creator)) {
                Flash::error('Creator not found');

                return redirect(route('creator.index'));
            }
            $creator['is_active'] = 0;
            $channel = Channels::where('user_id', $creator->id)->get();
            foreach ($channel as $seri) {
                $ser = Channels::find($seri->id);
                $ser['is_active'] = 0;
                $ser->save();
            } 
            $series = Serie::where('user_id', $creator->id)->get();
            foreach ($series as $seri) {
                $ser = Serie::find($seri->id);
                $ser['is_active'] = 0;
                $ser->save();
            }
            $video = Vids::where('user_id', $creator->id)->get();
            foreach ($video as $seri) {
                $ser = Vids::find($seri->id);
                $ser['is_active'] = 0;
                $ser->save();
            }

            $creator->save();

            Flash::success('Creator inactivated successfully.');
        }

        

        return redirect(route('creator.index'));
    }

    /**
     * Read csv file.
     *
     */
    public function readCsv(Request $request){

        $data = new \stdClass();
        $array_revenue = array();
        if ($request->file != NULL){

            // Moves file to folder on server
            $request->file->storeAs('uploads/csv', $request->file->getClientOriginalName(), 'local_public');
            
            $file_path = public_path().'/uploads/csv/'.$request->file->getClientOriginalName();
            $reader = ReaderFactory::create(Type::CSV); // for CSV files

            $reader->open($file_path);

            foreach ($reader->getSheetIterator() as $sheet) {
                foreach ($sheet->getRowIterator() as $row) {
                    $records = explode(";", $row[0]);
                    $record = new \stdClass();
                    $record->creator = $records[0];
                    $record->estimate_gross = $records[1];
                    $record->cost_production = $records[2];
                    array_push($array_revenue, $record);
                }
            }

            $reader->close();

        }
        $data = $array_revenue;
        return response()->json([
            'result' => $data
        ]); 
    }
}
