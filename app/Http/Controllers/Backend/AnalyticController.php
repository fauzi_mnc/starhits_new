<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Backend\Controller;
use Alaouy\Youtube\Facades\Youtube;
use Flash;
use Storage;
use Auth;
use Carbon;
use App\Model\Analytics;
use DB;

class AnalyticController extends Controller
{

    public function index()
    {
        $carbon_today = Carbon::now();
        $carbon_date1 = Carbon::now()->subWeek();
        $carbon_date2 = Carbon::now()->subWeeks(2);
        $today = $carbon_today->toDateString();
        $date_one = $carbon_date1->toDateString();
        $date_two = $carbon_date2->toDateString();

        $token_yt = DB::table('user_token')->where('user_id', auth()->user()->id)->where('type', 'youtube')->first();
        $token_tw = DB::table('user_token')->where('user_id', auth()->user()->id)->where('type', 'twitter')->first();
        $token_dm = DB::table('user_token')->where('user_id', auth()->user()->id)->where('type', 'dailymotion')->first();
        if(empty($token_yt) && empty($token_tw) && empty($token_dm)){
            Flash::error('Must connect to platform first.');
            return redirect()->route('creator.platforms.index');
        }

        $arr_videos = array();
        //$sort_videos = array();
        $dates = array();
        $views_yt = array();
        $subscribers_yt = array();
        $estimate_minutes_watched_yt = array();
        $average_view_duration_yt= array();
        $all_interactions_yt = array();
        $shares_yt = array();
        $likes_yt = array();
        $subscribers_yt = array();
        $comments_yt = array();
        $annotation_impressions_yt = array();
        $annotation_click_through_rate_yt = array();
        $annotation_close_rate_yt = array();
        $click_impressions_yt = array();
        $close_impressions_yt = array();
        $annotation_click_yt = array();
        $annotation_close_yt = array();
        $views_dm = array();
        $subscribers_dm = array();
        $estimate_minutes_watched_dm = array();
        $average_view_duration_dm= array();
        $all_interactions_dm = array();
        $shares_dm = array();
        $likes_dm = array();
        $subscribers_dm = array();
        $comments_dm = array();
        $annotation_impressions_dm = array();
        $annotation_click_through_rate_dm = array();
        $annotation_close_rate_dm = array();
        $click_impressions_dm = array();
        $close_impressions_dm = array();
        $annotation_click_dm = array();
        $annotation_close_dm = array();
        $old_analytics = Analytics::where('user_id', Auth::id())->whereBetween('date', [$date_two, $date_one])->get();
        $analytics = Analytics::where('user_id', Auth::id())->whereBetween('date', [$date_one, $carbon_today])->orderBy('date', 'asc')->get();
        
        $data = new \stdClass();
        $old = new \stdClass();
        $old->total_views = 0;
        $old->total_interactions = 0;
        $old->total_followers = 0;
        $data->total_views_yt = 0;
        $data->total_interactions_yt = 0;
        $data->total_followers_yt = 0;
        $data->total_likes_yt = 0;
        $data->total_shares_yt = 0;
        $data->total_comments_yt = 0;
        $data->females_yt = 0;
        $data->males_yt = 0;
        $data->total_views_dm = 0;
        $data->total_interactions_dm = 0;
        $data->total_followers_dm = 0;
        $data->total_likes_dm = 0;
        $data->total_shares_dm = 0;
        $data->total_comments_dm = 0;
        $data->females_dm = 0;
        $data->males_dm = 0;
        $data->info = 'Last 7 Days';
        $counter = 0;

        if(!empty($token_yt)){
            $client = new \Google_Client();
            $client->setClientId(env('GOOGLE_CLIENT_ID'));
            $client->setClientSecret(env('GOOGLE_CLIENT_SECRET'));
            $client->setScopes('https://www.googleapis.com/auth/youtube');
            $client->addScope(\Google_Service_YouTubeAnalytics::YT_ANALYTICS_READONLY);
            $client->setRedirectUri(env('GOOGLE_REDIRECT'));
            $client->setAccessType('offline');
            $client->setPrompt('consent select_account');

            $token = collect($token_yt)->except(['id', 'user_id', 'type'])->toArray();
            $client->setAccessToken($token);
            $analytic = new \Google_Service_YouTubeAnalytics($client);

            $videos_result = $analytic->reports->query([
                'ids'           => 'channel==MINE',
                'startDate'     => $date_one,
                'metrics'       => 'views,likes,shares,comments',
                'endDate'       => $today, 
                'sort'          => '-views',
                'maxResults'    => 1,
                'dimensions'    => 'video'
            ]);

            foreach ($videos_result->rows as $value){
                $video = new \stdClass();
                $video->video_id = $value[0];
                array_push($arr_videos, $video);
            }
        }
        

        if(!empty($arr_videos)){
            $video_info = Youtube::getVideoInfo($arr_videos[0]->video_id);
            $channel_info = Youtube::getChannelById($video_info->snippet->channelId);
            $total_videos = $channel_info->statistics->videoCount;
        }else{
            $total_videos = 0;
        }
        

        //$sort_videos = $arr_videos;
        //array_multisort(array_column($sort_videos, 'engangement'), SORT_DESC, $sort_videos);
        
        foreach ($old_analytics as $value) {
            $old->total_views += $value->views;
            $old->total_interactions += $value->likes + $value->shares + $value->comments;
            $old->total_followers += $value->subscribers_gained + $value->subscribers_lost;
        }

        foreach ($analytics as $value) {
            
            if(!in_array($value->date, $dates)){
                array_push($dates, $value->date);
                if($value->type == 'dailymotion'){
                    $data->total_views_dm += $value->views;
                    $data->total_likes_dm += $value->likes;
                    $data->total_followers_dm += $value->subscribers_gained;
                    $data->total_interactions_dm += $value->likes + $value->shares + $value->comments;
                    array_push($views_dm, $value->views);
                    array_push($subscribers_dm, $value->subscribers_gained);
                    array_push($estimate_minutes_watched_dm, 0);
                    array_push($average_view_duration_dm, 0);
                    array_push($all_interactions_dm, $value->likes + $value->shares + $value->comments);
                    array_push($shares_dm, 0);
                    array_push($likes_dm, $value->likes);
                    array_push($comments_dm, 0);
                    array_push($annotation_impressions_dm, 0);
                    array_push($annotation_click_through_rate_dm, 0);
                    array_push($annotation_close_rate_dm, 0);
                    array_push($click_impressions_dm, 0);
                    array_push($close_impressions_dm, 0);
                    array_push($annotation_click_dm, 0);
                    array_push($annotation_close_dm, 0);
                    array_push($views_yt, 0);
                    array_push($subscribers_yt, 0);
                    array_push($estimate_minutes_watched_yt, 0);
                    array_push($average_view_duration_yt, 0);
                    array_push($all_interactions_yt, 0);
                    array_push($shares_yt, 0);
                    array_push($likes_yt, 0);
                    array_push($comments_yt, 0);
                    array_push($annotation_impressions_yt, 0);
                    array_push($annotation_click_through_rate_yt, 0);
                    array_push($annotation_close_rate_yt, 0);
                    array_push($click_impressions_yt, 0);
                    array_push($close_impressions_yt, 0);
                    array_push($annotation_click_yt, 0);
                    array_push($annotation_close_yt, 0);
                }elseif($value->type == 'youtube'){
                    $data->total_views_yt += $value->views;
                    $data->total_likes_yt += $value->likes;
                    $data->total_shares_yt += $value->shares;
                    $data->total_comments_yt += $value->comments;
                    $data->total_interactions_yt += $value->likes + $value->shares + $value->comments;
                    $data->total_followers_yt += $value->subscribers_gained + $value->subscribers_lost;
                    $data->females_yt += $value->females;
                    $data->males_yt += $value->males;
                    array_push($views_yt, $value->views);
                    array_push($subscribers_yt, $value->subscribers_gained);
                    array_push($estimate_minutes_watched_yt, $value->estimate_minutes_watched);
                    array_push($average_view_duration_yt, $value->average_view_duration);
                    array_push($all_interactions_yt, $value->likes + $value->shares + $value->comments);
                    array_push($shares_yt, $value->shares);
                    array_push($likes_yt, $value->likes);
                    array_push($comments_yt, $value->comments);
                    array_push($annotation_impressions_yt, $value->annotation_impressions);
                    array_push($annotation_click_through_rate_yt, $value->annotation_click_through_rate);
                    array_push($annotation_close_rate_yt, $value->annotation_close_rate);
                    array_push($click_impressions_yt, $value->click_impressions);
                    array_push($close_impressions_yt, $value->close_impressions);
                    array_push($annotation_click_yt, $value->annotation_click);
                    array_push($annotation_close_yt, $value->annotation_close);
                    array_push($views_dm, 0);
                    array_push($subscribers_dm, 0);
                    array_push($estimate_minutes_watched_dm, 0);
                    array_push($average_view_duration_dm, 0);
                    array_push($all_interactions_dm, 0);
                    array_push($shares_dm, 0);
                    array_push($likes_dm, 0);
                    array_push($comments_dm, 0);
                    array_push($annotation_impressions_dm, 0);
                    array_push($annotation_click_through_rate_dm, 0);
                    array_push($annotation_close_rate_dm, 0);
                    array_push($click_impressions_dm, 0);
                    array_push($close_impressions_dm, 0);
                    array_push($annotation_click_dm, 0);
                    array_push($annotation_close_dm, 0);
                }
            }
        }

        //dd($data->total_views_dm);


        //$data->most_viewed_videos = $arr_videos;
        //$data->sort_engangement_videos = $sort_videos;
        $data->dates = $dates;
        $data->views_yt = $views_yt;
        $data->subscribers_yt = $subscribers_yt;
        $data->estimate_minutes_watched_yt = $estimate_minutes_watched_yt;
        $data->average_view_duration_yt = $average_view_duration_yt;
        $data->all_interactions_yt = $all_interactions_yt;
        $data->shares_yt = $shares_yt;
        $data->likes_yt = $likes_yt;
        $data->subscribers_yt = $subscribers_yt;
        $data->comments_yt = $comments_yt;
        $data->annotation_impressions_yt = $annotation_impressions_yt;
        $data->annotation_click_through_rate_yt = $annotation_click_through_rate_yt;
        $data->annotation_close_rate_yt = $annotation_close_rate_yt;
        $data->click_impressions_yt = $click_impressions_yt;
        $data->close_impressions_yt = $close_impressions_yt;
        $data->annotation_click_yt = $annotation_click_yt;
        $data->annotation_close_yt = $annotation_close_yt;
        $data->views_dm = $views_dm;
        $data->subscribers_dm = $subscribers_dm;
        $data->estimate_minutes_watched_dm = $estimate_minutes_watched_dm;
        $data->average_view_duration_dm = $average_view_duration_dm;
        $data->all_interactions_dm = $all_interactions_dm;
        $data->shares_dm = $shares_dm;
        $data->likes_dm = $likes_dm;
        $data->subscribers_dm = $subscribers_dm;
        $data->comments_dm = $comments_dm;
        $data->annotation_impressions_dm = $annotation_impressions_dm;
        $data->annotation_click_through_rate_dm = $annotation_click_through_rate_dm;
        $data->annotation_close_rate_dm = $annotation_close_rate_dm;
        $data->click_impressions_dm = $click_impressions_dm;
        $data->close_impressions_dm = $close_impressions_dm;
        $data->annotation_click_dm = $annotation_click_dm;
        $data->annotation_close_dm = $annotation_close_dm; 
        
        if($data->total_followers_yt+$data->total_followers_dm == 0) {
            $data->likes_subs = 0;
            $data->shares_subs = 0;
            $data->comments_subs = 0;
        }else{
            $data->likes_subs = round(($data->total_likes_yt+$data->total_likes_dm)/($data->total_followers_yt+$data->total_followers_dm), 2);
            $data->shares_subs = round($data->total_shares_yt/$data->total_followers_yt, 2);
            $data->comments_subs = round($data->total_comments_yt/$data->total_followers_yt, 2);
        }

        if($total_videos == 0) {
            $data->likes_videos = 0;
            $data->shares_videos = 0;
            $data->comments_videos = 0;
        }else{
            $data->likes_videos = round($data->total_likes_yt/$total_videos, 2);
            $data->shares_videos = round($data->total_shares_yt/$total_videos, 2);
            $data->comments_videos = round($data->total_comments_yt/$total_videos, 2);
        }
        
        if($old->total_views == 0){
            $data->percent_views = 0;
        }else{
            $data->percent_views = round((($data->total_views_yt + $data->total_views_dm - $old->total_views)*100)/$old->total_views, 2);
        }
        
        if($old->total_interactions == 0){
            $data->percent_interactions = 0;
        }else{
            $data->percent_interactions = round((($data->total_interactions_yt + $data->total_interactions_dm - $old->total_interactions)*100)/$old->total_interactions,2);
        }
        
        if($old->total_followers == 0){
            $data->percent_followers = 0;
        }else{
            $data->percent_followers = round((($data->total_followers_yt + $data->total_followers_dm - $old->total_followers)*100)/$old->total_followers, 2);
        }
        
        if($data->females_yt+$data->males_yt == 0){
            $data->percent_females = 0;
            $data->percent_males = 0;
        }else{
            $data->percent_females = round(($data->females_yt*100)/($data->females_yt + $data->males_yt),1);
            $data->percent_males = round(($data->males_yt*100)/($data->females_yt + $data->males_yt),1);
        }
        
        $data->total_views_info = number_format($data->total_views_yt+$data->total_views_dm, null, null, ".");
        $data->total_interactions_info = number_format($data->total_interactions_yt+$data->total_interactions_dm, null, null, ".");
        $data->total_followers_info = number_format($data->total_followers_yt+$data->total_followers_dm, null, null, ".");

        return view('analytic.index', ['data' => $data]);
    }

    public function getData(Request $request) {

        if($request->type == "week")
        {
            $carbon_today = Carbon::now();
            $carbon_date1 = Carbon::now()->subWeek();
            $carbon_date2 = Carbon::now()->subWeeks(2);
            $today = $carbon_today->toDateString();
            $date_one = $carbon_date1->toDateString();
            $date_two = $carbon_date2->toDateString();
        }elseif($request->type == "twoweeks")
        {
            $carbon_today = Carbon::now();
            $carbon_date1 = Carbon::now()->subWeek(2);
            $carbon_date2 = Carbon::now()->subWeeks(4);
            $today = $carbon_today->toDateString();
            $date_one = $carbon_date1->toDateString();
            $date_two = $carbon_date2->toDateString();
        }elseif($request->type == "thirtydays")
        {
            $carbon_today = Carbon::now();
            $carbon_date1 = Carbon::now()->subDays(30);
            $carbon_date2 = Carbon::now()->subDays(60);
            $today = $carbon_today->toDateString();
            $date_one = $carbon_date1->toDateString();
            $date_two = $carbon_date2->toDateString();
        }

        $token_yt = DB::table('user_token')->where('user_id', auth()->user()->id)->where('type', 'youtube')->first();
        $token_tw = DB::table('user_token')->where('user_id', auth()->user()->id)->where('type', 'twitter')->first();
        $token_dm = DB::table('user_token')->where('user_id', auth()->user()->id)->where('type', 'dailymotion')->first();
        if(empty($token_yt) && empty($token_tw) && empty($token_dm))
        {
            Flash::error('Must connect to platform first.');
            return redirect()->route('creator.platforms.index');
        }

        $arr_videos = array();
        //$sort_videos = array();
        $dates = array();
        $views_yt = array();
        $subscribers_yt = array();
        $estimate_minutes_watched_yt = array();
        $average_view_duration_yt= array();
        $all_interactions_yt = array();
        $shares_yt = array();
        $likes_yt = array();
        $subscribers_yt = array();
        $comments_yt = array();
        $annotation_impressions_yt = array();
        $annotation_click_through_rate_yt = array();
        $annotation_close_rate_yt = array();
        $click_impressions_yt = array();
        $close_impressions_yt = array();
        $annotation_click_yt = array();
        $annotation_close_yt = array();
        $views_dm = array();
        $subscribers_dm = array();
        $estimate_minutes_watched_dm = array();
        $average_view_duration_dm= array();
        $all_interactions_dm = array();
        $shares_dm = array();
        $likes_dm = array();
        $subscribers_dm = array();
        $comments_dm = array();
        $annotation_impressions_dm = array();
        $annotation_click_through_rate_dm = array();
        $annotation_close_rate_dm = array();
        $click_impressions_dm = array();
        $close_impressions_dm = array();
        $annotation_click_dm = array();
        $annotation_close_dm = array();
        $old_analytics = Analytics::where('user_id', Auth::id())->whereBetween('date', [$date_two, $date_one])->get();
        $analytics = Analytics::where('user_id', Auth::id())->whereBetween('date', [$date_one, $carbon_today])->orderBy('date', 'asc')->get();
        
        $data = new \stdClass();
        $old = new \stdClass();
        $old->total_views = 0;
        $old->total_interactions = 0;
        $old->total_followers = 0;
        $data->total_views_yt = 0;
        $data->total_interactions_yt = 0;
        $data->total_followers_yt = 0;
        $data->total_likes_yt = 0;
        $data->total_shares_yt = 0;
        $data->total_comments_yt = 0;
        $data->females_yt = 0;
        $data->males_yt = 0;
        $data->total_views_dm = 0;
        $data->total_interactions_dm = 0;
        $data->total_followers_dm = 0;
        $data->total_likes_dm = 0;
        $data->total_shares_dm = 0;
        $data->total_comments_dm = 0;
        $data->females_dm = 0;
        $data->males_dm = 0;
        $data->info = 'Last 7 Days';
        $counter = 0;

        if(!empty($token_yt)){
            $client = new \Google_Client();
            $client->setClientId(env('GOOGLE_CLIENT_ID'));
            $client->setClientSecret(env('GOOGLE_CLIENT_SECRET'));
            $client->setScopes('https://www.googleapis.com/auth/youtube');
            $client->addScope(\Google_Service_YouTubeAnalytics::YT_ANALYTICS_READONLY);
            $client->setRedirectUri(env('GOOGLE_REDIRECT'));
            $client->setAccessType('offline');
            $client->setPrompt('consent select_account');

            $token = collect($token_yt)->except(['id', 'user_id', 'type'])->toArray();
            $client->setAccessToken($token);
            $analytic = new \Google_Service_YouTubeAnalytics($client);

            $videos_result = $analytic->reports->query([
                'ids'           => 'channel==MINE',
                'startDate'     => $date_one,
                'metrics'       => 'views,likes,shares,comments',
                'endDate'       => $today, 
                'sort'          => '-views',
                'maxResults'    => 1,
                'dimensions'    => 'video'
            ]);

            foreach ($videos_result->rows as $value){
                $video = new \stdClass();
                $video->video_id = $value[0];
                array_push($arr_videos, $video);
            }
        }

        if(!empty($arr_videos)){
            $video_info = Youtube::getVideoInfo($arr_videos[0]->video_id);
            $channel_info = Youtube::getChannelById($video_info->snippet->channelId);
            $total_videos = $channel_info->statistics->videoCount;
        }else{
            $total_videos = 0;
        }

        //$sort_videos = $arr_videos;
        //array_multisort(array_column($sort_videos, 'engangement'), SORT_DESC, $sort_videos);
        
        foreach ($old_analytics as $value) {
            $old->total_views += $value->views;
            $old->total_interactions += $value->likes + $value->shares + $value->comments;
            $old->total_followers += $value->subscribers_gained + $value->subscribers_lost;
        }

        foreach ($analytics as $value) {
            
            if(!in_array($value->date, $dates)){
                array_push($dates, $value->date);
                if($value->type == 'dailymotion'){
                    $data->total_views_dm += $value->views;
                    $data->total_likes_dm += $value->likes;
                    $data->total_followers_dm += $value->subscribers_gained;
                    $data->total_interactions_dm += $value->likes + $value->shares + $value->comments;
                    array_push($views_dm, $value->views);
                    array_push($subscribers_dm, $value->subscribers_gained);
                    array_push($estimate_minutes_watched_dm, 0);
                    array_push($average_view_duration_dm, 0);
                    array_push($all_interactions_dm, $value->likes + $value->shares + $value->comments);
                    array_push($shares_dm, 0);
                    array_push($likes_dm, $value->likes);
                    array_push($comments_dm, 0);
                    array_push($annotation_impressions_dm, 0);
                    array_push($annotation_click_through_rate_dm, 0);
                    array_push($annotation_close_rate_dm, 0);
                    array_push($click_impressions_dm, 0);
                    array_push($close_impressions_dm, 0);
                    array_push($annotation_click_dm, 0);
                    array_push($annotation_close_dm, 0);
                    array_push($views_yt, 0);
                    array_push($subscribers_yt, 0);
                    array_push($estimate_minutes_watched_yt, 0);
                    array_push($average_view_duration_yt, 0);
                    array_push($all_interactions_yt, 0);
                    array_push($shares_yt, 0);
                    array_push($likes_yt, 0);
                    array_push($comments_yt, 0);
                    array_push($annotation_impressions_yt, 0);
                    array_push($annotation_click_through_rate_yt, 0);
                    array_push($annotation_close_rate_yt, 0);
                    array_push($click_impressions_yt, 0);
                    array_push($close_impressions_yt, 0);
                    array_push($annotation_click_yt, 0);
                    array_push($annotation_close_yt, 0);
                }elseif($value->type == 'youtube'){
                    $data->total_views_yt += $value->views;
                    $data->total_likes_yt += $value->likes;
                    $data->total_shares_yt += $value->shares;
                    $data->total_comments_yt += $value->comments;
                    $data->total_interactions_yt += $value->likes + $value->shares + $value->comments;
                    $data->total_followers_yt += $value->subscribers_gained + $value->subscribers_lost;
                    $data->females_yt += $value->females;
                    $data->males_yt += $value->males;
                    array_push($views_yt, $value->views);
                    array_push($subscribers_yt, $value->subscribers_gained);
                    array_push($estimate_minutes_watched_yt, $value->estimate_minutes_watched);
                    array_push($average_view_duration_yt, $value->average_view_duration);
                    array_push($all_interactions_yt, $value->likes + $value->shares + $value->comments);
                    array_push($shares_yt, $value->shares);
                    array_push($likes_yt, $value->likes);
                    array_push($comments_yt, $value->comments);
                    array_push($annotation_impressions_yt, $value->annotation_impressions);
                    array_push($annotation_click_through_rate_yt, $value->annotation_click_through_rate);
                    array_push($annotation_close_rate_yt, $value->annotation_close_rate);
                    array_push($click_impressions_yt, $value->click_impressions);
                    array_push($close_impressions_yt, $value->close_impressions);
                    array_push($annotation_click_yt, $value->annotation_click);
                    array_push($annotation_close_yt, $value->annotation_close);
                    array_push($views_dm, 0);
                    array_push($subscribers_dm, 0);
                    array_push($estimate_minutes_watched_dm, 0);
                    array_push($average_view_duration_dm, 0);
                    array_push($all_interactions_dm, 0);
                    array_push($shares_dm, 0);
                    array_push($likes_dm, 0);
                    array_push($comments_dm, 0);
                    array_push($annotation_impressions_dm, 0);
                    array_push($annotation_click_through_rate_dm, 0);
                    array_push($annotation_close_rate_dm, 0);
                    array_push($click_impressions_dm, 0);
                    array_push($close_impressions_dm, 0);
                    array_push($annotation_click_dm, 0);
                    array_push($annotation_close_dm, 0);
                }
            }
        }

        //dd($data->total_views_dm);


        //$data->most_viewed_videos = $arr_videos;
        //$data->sort_engangement_videos = $sort_videos;
        $data->dates = $dates;
        $data->views_yt = $views_yt;
        $data->subscribers_yt = $subscribers_yt;
        $data->estimate_minutes_watched_yt = $estimate_minutes_watched_yt;
        $data->average_view_duration_yt = $average_view_duration_yt;
        $data->all_interactions_yt = $all_interactions_yt;
        $data->shares_yt = $shares_yt;
        $data->likes_yt = $likes_yt;
        $data->subscribers_yt = $subscribers_yt;
        $data->comments_yt = $comments_yt;
        $data->annotation_impressions_yt = $annotation_impressions_yt;
        $data->annotation_click_through_rate_yt = $annotation_click_through_rate_yt;
        $data->annotation_close_rate_yt = $annotation_close_rate_yt;
        $data->click_impressions_yt = $click_impressions_yt;
        $data->close_impressions_yt = $close_impressions_yt;
        $data->annotation_click_yt = $annotation_click_yt;
        $data->annotation_close_yt = $annotation_close_yt;
        $data->views_dm = $views_dm;
        $data->subscribers_dm = $subscribers_dm;
        $data->estimate_minutes_watched_dm = $estimate_minutes_watched_dm;
        $data->average_view_duration_dm = $average_view_duration_dm;
        $data->all_interactions_dm = $all_interactions_dm;
        $data->shares_dm = $shares_dm;
        $data->likes_dm = $likes_dm;
        $data->subscribers_dm = $subscribers_dm;
        $data->comments_dm = $comments_dm;
        $data->annotation_impressions_dm = $annotation_impressions_dm;
        $data->annotation_click_through_rate_dm = $annotation_click_through_rate_dm;
        $data->annotation_close_rate_dm = $annotation_close_rate_dm;
        $data->click_impressions_dm = $click_impressions_dm;
        $data->close_impressions_dm = $close_impressions_dm;
        $data->annotation_click_dm = $annotation_click_dm;
        $data->annotation_close_dm = $annotation_close_dm; 
        
        if($data->total_followers_yt+$data->total_followers_dm == 0) {
            $data->likes_subs = 0;
            $data->shares_subs = 0;
            $data->comments_subs = 0;
        }else{
            $data->likes_subs = round(($data->total_likes_yt+$data->total_likes_dm)/($data->total_followers_yt+$data->total_followers_dm), 2);
            $data->shares_subs = round($data->total_shares_yt/$data->total_followers_yt, 2);
            $data->comments_subs = round($data->total_comments_yt/$data->total_followers_yt, 2);
        }

        if($total_videos == 0) {
            $data->likes_videos = 0;
            $data->shares_videos = 0;
            $data->comments_videos = 0;
        }else{
            $data->likes_videos = round($data->total_likes_yt/$total_videos, 2);
            $data->shares_videos = round($data->total_shares_yt/$total_videos, 2);
            $data->comments_videos = round($data->total_comments_yt/$total_videos, 2);
        }
        
        if($old->total_views == 0){
            $data->percent_views = 0;
        }else{
            $data->percent_views = round((($data->total_views_yt + $data->total_views_dm - $old->total_views)*100)/$old->total_views, 2);
        }
        
        if($old->total_interactions == 0){
            $data->percent_interactions = 0;
        }else{
            $data->percent_interactions = round((($data->total_interactions_yt + $data->total_interactions_dm - $old->total_interactions)*100)/$old->total_interactions,2);
        }
        
        if($old->total_followers == 0){
            $data->percent_followers = 0;
        }else{
            $data->percent_followers = round((($data->total_followers_yt + $data->total_followers_dm - $old->total_followers)*100)/$old->total_followers, 2);
        }
        
        if($data->females_yt+$data->males_yt == 0){
            $data->percent_females = 0;
            $data->percent_males = 0;
        }else{
            $data->percent_females = round(($data->females_yt*100)/($data->females_yt + $data->males_yt),1);
            $data->percent_males = round(($data->males_yt*100)/($data->females_yt + $data->males_yt),1);
        }
        
        $data->total_views_info = number_format($data->total_views_yt+$data->total_views_dm, null, null, ".");
        $data->total_interactions_info = number_format($data->total_interactions_yt+$data->total_interactions_dm, null, null, ".");
        $data->total_followers_info = number_format($data->total_followers_yt+$data->total_followers_dm, null, null, ".");

        return response()->json([
            'result' => $data
        ]);

    }

}
