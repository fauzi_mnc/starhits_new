<?php

namespace App\Http\Controllers\Backend;

use App\Model\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Backend\Controller;
use App\DataTables\CategoryDataTable;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\StoreOrUpdateCategoryRequest;
use Flash;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(CategoryDataTable $categoryDataTable)
    {
        return $categoryDataTable->render('category.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $parent = Category::where('parent', NULL)->pluck('title', 'id')->toArray();
        return view('category.create', ['parent' => $parent]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreOrUpdateCategoryRequest $request)
    {
        $input = $request->except('_token');
        if ($input['is_active'] == 'on') {
            $input['is_active'] = '1';
        }

        Category::create($input);
        Flash::success('Category saved successfully.');

        return redirect(route('category.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::find($id);
        $parent = Category::where('parent', NULL)->pluck('title', 'id')->toArray();

        if (empty($category)) {
            Flash::error('Category not found');

            return redirect(route('category.index'));
        }

        return view('category.edit')->with(['category' => $category, 'parent' => $parent]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update($id, StoreOrUpdateCategoryRequest $request)
    {
        $category = Category::find($id);
        $category['parent'] = $request->parent;
        $category['title'] = $request->title;
        if ($request->is_active == 'on') {
            $category['is_active'] = '1';
        }else{
            $category['is_active'] = '0';
        }

        $category->save();
        Flash::success('Category update successfully.');

        return redirect(route('category.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        //
    }
}
