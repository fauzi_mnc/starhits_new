<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Backend\Controller;
use App\Repositories\RevenueRepository;
use App\Repositories\UserRepository;
use Flash;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;
use App\Model\RevenueApprover;

class RevenueApproverController extends Controller
{
    /**
     * The user repository instance.
     */
    protected $revenue;
    protected $users;

    /**
     * Create a new controller instance.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct(RevenueRepository $revenue, UserRepository $users)
    {
        $this->revenue = $revenue;
        $this->users = $users;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
        $approver = RevenueApprover::where('type', 1)->where('is_active', 1)->get();
        $checker = RevenueApprover::where('type', 2)->where('is_active', 1)->get();

        return view('revenue.approver.index', compact('approver', 'checker'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);
        $length = 0;
        $input = [
            'name'                  => $request->name,
            'email'                 => $request->email,
            'is_active'                => 1,
            'type'                  => $request->type
        ];

        try {

            $approver = RevenueApprover::create($input);

            if($request->type == 1){
            $message = 'Approver saved successfully.';
            }
            else{
                $message = 'Checker saved successfully.';
            }
            $status = 'success';
            $length = RevenueApprover::where('type', 1)->where('is_active', 1)->count();

        } catch (Exception $e) {
            $status = 'failed';
            $message = $e->errorMessage();
        }
        
        return response()->json([
            'status' => $status,
            'message' => $message,
            'data'    => $approver,
            'length'  => $length
        ]); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $approver = RevenueApprover::find($request->id);
        
        if (empty($approver)) {
            if($request->type == 1){
                $message = 'Approver saved successfully.';
            }
            else{
                $message = 'Checker saved successfully.';
            }

            return response()->json([
                    'message' => $message
            ]); 
        }

        $approver->name = $request->name;
        $approver->email = $request->email;

        try {

            $approver->save();

            if($request->type == 1){
            $message = 'Approver updated successfully.';
            }
            else{
                $message = 'Checker updated successfully.';
            }
            $status = 'success';

        } catch (Exception $e) {
            $status = 'failed';
            $message = $e->errorMessage();
        }

        return response()->json([
            'status' => $status,
            'message' => $message,
            'data'    => $approver
        ]);  
    }
}
