<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Backend\Controller;
use Illuminate\Http\Request;
use App\Model\SiteConfig;
use Illuminate\Support\Facades\DB;
use App\DataTables\WebConfigDataTable;
use App\Http\Requests\StoreOrUpdateConfigRequest;
use Flash;

class WebConfigController extends Controller
{
    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function index()
    {
        $config = SiteConfig::where('key', 'not like', '%'.'email'.'%')->get();
        return view('config.create')->with(['config'=> $config]);
    }

    public function create()
    {
        return view('config.create');
    }

    public function store(Request $request) {
        $input = $request->except('_token');
        // dd($input);
        foreach ($input as $key => $value) {
            $config = SiteConfig::where('key', $key)->first();
            $config['value'] = $value;
            if($config->key == 'website_logo_header'){
                $config['value'] = '/uploads/config/'.time().'.'.$value->getClientOriginalExtension();
                $value->move(public_path('uploads/config'), time().'.'.$value->getClientOriginalExtension());
            }
            if (empty($config->key == 'is_streaming')) {
                $is_streaming = SiteConfig::where('key', 'is_streaming')->first();
                $is_streaming['value'] = NULL;
                $is_streaming->save();   
            }
            $config->save();    
        }
        Flash::success('Config saved successfully.');

        return redirect(route('config.index'));
    }

    /**
     * undocumented function
     *
     * @return void
     * @author 
     **/
    function show($id)
    {
    	$config = SiteConfig::find($id);

        if (empty($config)) {
            Flash::error('Config not found');

            return redirect(route('config.index'));
        }

        return view('config.show')->with(['config'=> $config]);
    }

    function edit($id)
    {
        $config = SiteConfig::find($id);
        if (empty($config)) {
            Flash::error('Config not found');

            return redirect(route('config.index'));
        }

        return view('config.edit')->with(['config'=> $config]);
    }

    public function update($id, StoreOrUpdateConfigRequest $request) {
        $config = SiteConfig::find($id);
        $config['key'] = $request->key;
        $config['value'] = $request->value;
        
        $config->save();

        Flash::success('Config update successfully.');

        return redirect(route('config.index'));
    }
}
