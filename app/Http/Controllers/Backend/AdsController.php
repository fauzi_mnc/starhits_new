<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Backend\Controller;
use Illuminate\Http\Request;
use App\Model\Ads;
use Illuminate\Support\Facades\DB;
use App\DataTables\AdsDataTable;
use App\Http\Requests\StoreOrUpdateAdsRequest;
use App\Http\Requests\UpdateAdsRequest;
use Flash;
use Auth;
use Storage;

class AdsController extends Controller
{
    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function index(AdsDataTable $adsDataTable)
    {   
        return $adsDataTable->render('ads.index');
    }

    public function create()
    {
        return view('ads.create');
    }

    public function store(StoreOrUpdateAdsRequest $request) {
        $input = $request->except('_token');

        if ($request->has('end_date')) {
            $date = new \DateTime($request->end_date);
            $date->modify('+23 hour');
            $date->modify('+59 minute');
            $date->modify('+59 second');
            $ads['end_date'] = date_format($date, 'Y-m-d H:i:s');
        }

        if($request->has('image')){
            $path = 'uploads/ads/image/'.str_random(32).'.'.$request->image->getClientOriginalExtension();
            $image = Storage::disk('local_public')->put($path, file_get_contents($input['image']));
            $input['image'] = $path;
        }

        if($request->has('video')){
            $path = 'uploads/ads/video/'.str_random(32).'.'.$request->video->getClientOriginalExtension();
            $video = Storage::disk('local_public')->put($path, file_get_contents($input['video']));
            $input['video'] = $path;
        }

        Ads::create($input);
        Flash::success('Ads saved successfully.');

        return redirect(route('ads.index'));
    }

    /**
     * undocumented function
     *
     * @return void
     * @author 
     **/
    function show($id)
    {
    	$ads = Ads::find($id);

        if (empty($ads)) {
            Flash::error('ads not found');

            return redirect(route('ads.index'));
        }

        return view('ads.show')->with('ads', $ads);
    }

    function edit($id)
    {
        $ads = Ads::find($id);

        if (empty($ads)) {
            Flash::error('ads not found');

            return redirect(route('ads.index'));
        }

        return view('ads.edit')->with('ads', $ads);
    }

    public function update($id, UpdateAdsRequest $request) {
        $ads = Ads::find($id);
        $ads['title'] = $request->title;
        $ads['category'] = $request->category;
        $ads['position'] = $request->position;
        if ($request->category == '1') {
            $ads['script'] = $request->script;
        }else{
            $ads['type'] = $request->type;
            $ads['youtube_id'] = $request->youtube_id;
            $ads['target'] = $request->target;
            $ads['status'] = $request->status;
            $ads['start_date'] = $request->start_date;
            
            if ($request->has('end_date')) {
                $date = new \DateTime($request->end_date);
                $date->modify('+23 hour');
                $date->modify('+59 minute');
                $date->modify('+59 second');
                $ads['end_date'] = date_format($date, 'Y-m-d H:i:s');
            }

            if($request->has('image')){
                $path = 'uploads/ads/image/'.str_random(32).'.'.$request->image->getClientOriginalExtension();
                $image = Storage::disk('local_public')->put($path, file_get_contents($request->image));
                $ads['image'] = $path;
            }

            if($request->has('video')){
                $path = 'uploads/ads/video/'.str_random(32).'.'.$request->video->getClientOriginalExtension();
                $video = Storage::disk('local_public')->put($path, file_get_contents($request->video));
                $ads['video'] = $path;
            }
        }
        $ads->save();
        Flash::success('Ads update successfully.');

        return redirect(route('ads.index'));
    }

    public function destroy(Request $request,$id)
    {
        if ($request->action == 'act') {
            $ads = Ads::find($id);

            if (empty($ads)) {
                Flash::error('Ads not found');

                return redirect(route('ads.index'));
            }
            $ads['status'] = 1;

            $ads->save();

            Flash::success('Ads activated successfully.');
        }else{
            $ads = Ads::find($id);

            if (empty($ads)) {
                Flash::error('Ads not found');

                return redirect(route('ads.index'));
            }
            $ads['status'] = 0;
            $ads->save();

            Flash::success('Ads inactivated successfully.');
        }

        

        return redirect(route('ads.index'));
    }

    function channeltable(){
        // dd('test');
        return view('channel.table');
    }
}
