<?php

namespace App\Http\Controllers\Backend;

use Alaouy\Youtube\Facades\Youtube;
use App\Http\Controllers\Backend\Controller;
use App\Model\Channel;
use App\Model\Posts;
use App\Model\Series;
use App\Model\User;
use App\Model\Terms;
use App\Model\ContentsTerms;
use App\Model\ContentUsers;
use Flash;
use Auth;
use Log;
use Illuminate\Http\Request;
use App\Http\Requests\StoreOrUpdatePostRequest;
use App\DataTables\PostsDataTable;

class PostsController extends Controller
{
    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function index(PostsDataTable $postsDataTable)
    {
        return $postsDataTable->render('posts.index');
    }

    public function createManual()
    {
        $series = Series::where('is_active', 1)->get()->pluck('title', 'id')->toArray();
        
        $user = User::where('is_active', 1)
        ->get()
        ->pluck('name', 'id')->toArray();
        $tags = Terms::all()->pluck('name')->toArray();
        //dd($tags);
        $channel = Channel::where('is_active', 1)->get()->pluck('title', 'attr_1')->toArray();

        return view('posts.create_manual', ['series' => $series, 'user' => $user, 'channel' => $channel, 'terms' => $tags]);
    }

    public function createAutomatic()
    {
        $user = User::where('is_active', 1)
        ->with('accessrole')
        ->whereHas('accessrole', function($query){
            $query->where('role_id', '2');
        })
        ->get()
        ->pluck('name', 'id')
        ->toArray();
        
        $selected_user = array();
        $id_user = User::where('is_active', 1)->first()->id;

        if (Auth::user()->roles->first()->name == 'admin') {
            $series = Series::where('is_active', 1)->where('user_id', $id_user)->get()->pluck('title', 'id')->toArray();
        } elseif (Auth::user()->roles->first()->name == 'user') {
            $series = Series::where('is_active', 1)->where('user_id', Auth::id())->get()->pluck('title', 'id')->toArray();
        }else{
            $series = Series::where('is_active', 1)->where('user_id', Auth::id())->get()->pluck('title', 'id')->toArray();
        }

        $tags = Terms::all()->pluck('name')->toArray();

        return view('posts.create_automatic', ['series' => $series, 'user' => $user, 'terms' => $tags, 'selected_user' => $selected_user]);
    }

    public function store(StoreOrUpdatePostRequest $request) {
        if($request->postType == 'automatic'){
            $series = Series::find($request->parent_id);
            $channel = Channel::where('attr_1', $series->parent_id)->first();
            $input['title'] = $request->title;
            $input['subtitle'] = 'Subtitle '.$request->title;
            $input['slug'] = strtolower(trim(preg_replace('/[\s-]+/', '-', preg_replace('/[^A-Za-z0-9-]+/', '-', preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $request->title))))), '-'));
            $input['excerpt'] = 'Excerpt '.$request->title;
            $input['content'] = $request->content;
            if (Auth::user()->roles->first()->name == 'admin') {
                $input['created_by'] = Auth::user()->id;
                $input['user_id'] = $request->created_by;
            }elseif (Auth::user()->roles->first()->name == 'user') {
                $input['created_by'] = Auth::user()->id;
                $input['user_id'] = $request->created_by;
            }else{
                $input['created_by'] = Auth::user()->id;
                $input['user_id'] = Auth::user()->id;
            }
            $input['parent_id'] = $request->parent_id;
            $input['attr_1'] = $channel->attr_1;
            $input['attr_5'] = 'youtube';
            $input['attr_6'] = $request->attr_6;
            $input['type'] = 'video';
            $input['is_active'] = $request->is_active;
            $input['is_featured'] = 0;
            $input['post_type'] = $request->postType;
            if($request->image != NULL){
                $input['image'] = '/uploads/content/'.time().'.'.$request->image->getClientOriginalExtension();
                $request->image->move(public_path('uploads/content'), time().'.'.$request->image->getClientOriginalExtension());
            }
            //dd($request);exit();
            $id_posts = Posts::create($input)->id;

            // save users related
            if($request->user_id != NULL) {
                foreach($request->user_id as $value){
                    $input_content_users['content_id'] = $id_posts;
                    $input_content_users['user_id'] = $value;
                    ContentUsers::create($input_content_users);
                }
            }
            // end save users related

            // save terms
            $tags = explode(',', $request->tags);
            foreach($tags as $value){
                $input_terms['name'] = $value;
                $input_terms['slug'] = rtrim(preg_replace('/[^A-Za-z0-9\-]/', '-', str_replace('-', '', strtolower($value))),'-');
                $input_terms['type'] = 'tag';

                $terms = Terms::where('name', '=', $value)->first();
                if ($terms === null) {
                    $id_terms = Terms::create($input_terms)->id;
                    $input_content_terms['content_id'] = $id_posts;
                    $input_content_terms['term_id'] = $id_terms;
                    ContentsTerms::create($input_content_terms);
                } else {
                    $input_content_terms['content_id'] = $id_posts;
                    $input_content_terms['term_id'] = $terms->id;
                    ContentsTerms::create($input_content_terms);
                }
            }
            // end save terms
            Flash::success('Content saved successfully.');
        } else if($request->postType == 'manual'){
            $input['type'] = 'article';
            $input['title'] = $request->title;
            $input['subtitle'] = 'Subtitle '.$request->title;
            $input['slug'] = rtrim(preg_replace('/[^A-Za-z0-9\-]/', '-', str_replace('-', '', strtolower($request->title))),'-');
            $input['excerpt'] = 'Excerpt '.$request->title;
            $input['attr_1'] = $request->attr_1;
            $input['content'] = $request->content;
            $input['created_by'] = Auth::id();
            $input['user_id'] = Auth::id();
            $input['is_active'] = $request->is_active;
            $input['attr_5'] = 'youtube';
            $input['attr_6'] = $request->attr_6;
            $input['is_featured'] = 0;
            $input['post_type'] = $request->postType;
            if($request->image != NULL){
                $input['image'] = '/uploads/content/'.time().'.'.$request->image->getClientOriginalExtension();
                $request->image->move(public_path('uploads/content'), time().'.'.$request->image->getClientOriginalExtension());
            }
            //dd($input);
            $id_posts = Posts::create($input)->id;

            // save terms
            $tags = explode(',', $request->tags);
            foreach($tags as $value){
                $input_terms['name'] = $value;
                $input_terms['slug'] = rtrim(preg_replace('/[^A-Za-z0-9\-]/', '-', str_replace('-', '', strtolower($value))),'-');
                $input_terms['type'] = 'tag';

                $terms = Terms::where('name', '=', $value)->first();
                if ($terms === null) {
                    $id_terms = Terms::create($input_terms)->id;
                    $input_content_terms['content_id'] = $id_posts;
                    $input_content_terms['term_id'] = $id_terms;
                    ContentsTerms::create($input_content_terms);
                } else {
                    $input_content_terms['content_id'] = $id_posts;
                    $input_content_terms['term_id'] = $terms->id;
                    ContentsTerms::create($input_content_terms);
                }
            }
            // end save terms

            Flash::success('Post Manual saved successfully.');
        }
        
        if ($request->create == 1){
            if($request->postType == 'automatic'){
                if (Auth::user()->roles->first()->name == 'admin') {
                    return redirect(route('admin.posts.createAutomatic'));
                }elseif (Auth::user()->roles->first()->name == 'user') {
                    return redirect(route('user.posts.createAutomatic'));
                }else{
                    return redirect(route('creator.posts.createAutomatic'));
                }
            } else {
                if (Auth::user()->roles->first()->name == 'admin') {
                    return redirect(route('admin.posts.createManual'));
                }elseif (Auth::user()->roles->first()->name == 'user') {
                    return redirect(route('user.posts.createManual'));
                }else{
                    return redirect(route('creator.posts.createManual'));
                }
            }          
        } else {
            if (Auth::user()->roles->first()->name == 'admin') {
                return redirect(route('admin.posts.index'));
            }elseif (Auth::user()->roles->first()->name == 'user') {
                return redirect(route('user.posts.index'));
            }else{
                return redirect(route('creator.posts.index'));
            }
        }
    }

    public function editAutomatic($id)
    {

        $posts = Posts::find($id);
        $series = Series::where('is_active', 1)->where('user_id', $posts->user_id)->get()->pluck('title', 'id')->toArray();
        $user = User::where('is_active', 1)
                ->whereHas('roles', function($query){
                    $query->where('name', 'creator');
                })
                ->get()
                ->pluck('name', 'id')
                ->toArray();
        $tags = Terms::all()->pluck('name')->toArray();
        $selected_user = ContentUsers::where('content_id', $id)->get()->pluck('user_id')->toArray();
        //dd($selected_user);
        $content_terms = ContentsTerms::where('content_id', $id)->get();
        $terms = array();
        foreach ($content_terms as $value){
            $term = Terms::where('id', $value->term_id)->first();
            array_push($terms, $term->name);
        }
        $posts['tags'] = implode(',', $terms);
        //dd($posts['tags']);

        if (empty($posts)) {
            Flash::error('Posts not found');

            if (Auth::user()->roles->first()->name == 'admin') {
                return redirect(route('admin.posts.index'));
            }elseif (Auth::user()->roles->first()->name == 'user') {
                return redirect(route('user.posts.index'));
            }else{
                return redirect(route('creator.posts.index'));
            }
        }

        return view('posts.edit_automatic')->with(['posts'=> $posts, 'series'=> $series, 'user'=> $user, 'terms' => $tags, 'selected_user' => $selected_user]);
    }

    public function editManual($id)
    {
        $posts = Posts::find($id);
        $tags = Terms::all()->pluck('name')->toArray();

        $content_terms = ContentsTerms::where('content_id', $id)->get();
        $terms = array();
        foreach ($content_terms as $value){
            $term = Terms::where('id', $value->term_id)->first();
            array_push($terms, $term->name);
        }
        $posts['tags'] = implode(',', $terms);
        $channel = Channel::where('is_active', 1)->get()->pluck('title', 'attr_1')->toArray();
        if (empty($posts)) {
            Flash::error('Posts not found');

            if (Auth::user()->roles->first()->name == 'admin') {
                return redirect(route('admin.posts.index'));
            }elseif (Auth::user()->roles->first()->name == 'user') {
                return redirect(route('user.posts.index'));
            }else{
                return redirect(route('creator.posts.index'));
            }
        }

        return view('posts.edit_manual')->with(['posts'=> $posts, 'channel'=> $channel, 'terms' => $tags]);
    }

    public function update($id, StoreOrUpdatePostRequest $request) {
        if($request->postType == 'automatic'){
            $posts = Posts::find($id);
            $series = Series::find($request->parent_id);
            $channel = Channel::where('attr_1', $series->parent_id)->first();
            $posts['title'] = $request->title;
            $posts['subtitle'] = 'Subtitle '.$request->title;
            $posts['slug'] = strtolower(trim(preg_replace('/[\s-]+/', '-', preg_replace('/[^A-Za-z0-9-]+/', '-', preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $request->title))))), '-'));
            $posts['excerpt'] = 'Excerpt '.$request->title;
            $posts['content'] = $request->content;
            if (Auth::user()->roles->first()->name == 'admin') {
                $posts['created_by'] = Auth::user()->id;
                $posts['user_id'] = $request->created_by;
            }elseif (Auth::user()->roles->first()->name == 'user') {
                $posts['created_by'] = Auth::user()->id;
                $posts['user_id'] = $request->created_by;
            }else{
                $posts['created_by'] = Auth::user()->id;
                $posts['user_id'] = Auth::user()->id;
            }
            $posts['parent_id'] = $request->parent_id;
            $posts['attr_1'] = $channel->attr_1;
            $posts['attr_5'] = 'youtube';
            $posts['attr_6'] = $request->attr_6;
            $posts['type'] = 'video';
            $posts['is_active'] = $request->is_active;
            $posts['is_featured'] = 0;
            $posts['post_type'] = $request->postType;
            if($request->image != NULL){
                $posts['image'] = '/uploads/content/'.time().'.'.$request->image->getClientOriginalExtension();
                $request->image->move(public_path('uploads/content'), time().'.'.$request->image->getClientOriginalExtension());
            }

            $posts->save();

            // delete users first
            $content_users = ContentUsers::where('content_id', $id);
            $content_users->delete();

            // save users related
            if($request->user_id != NULL){
                foreach($request->user_id as $value){
                    $input_content_users['content_id'] = $id;
                    $input_content_users['user_id'] = $value;
                    ContentUsers::create($input_content_users);
                }
            }

            // save terms
            $tags = explode(',', $request->tags);

            ContentsTerms::where('content_id', $id)->delete();
            foreach($tags as $value){
                $input_terms['name'] = $value;
                $input_terms['slug'] = strtolower(trim(preg_replace('/[\s-]+/', '-', preg_replace('/[^A-Za-z0-9-]+/', '-', preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $value))))), '-'));
                $input_terms['type'] = 'tag';

                $terms = Terms::where('name', '=', $value)->first();
                if ($terms == null) {
                    $id_terms = Terms::create($input_terms)->id;
                    $input_content_terms['content_id'] = $id;
                    $input_content_terms['term_id'] = $id_terms;
                    ContentsTerms::create($input_content_terms);
                } else {
                    $input_content_terms['content_id'] = $id;
                    $input_content_terms['term_id'] = $terms->id;
                    ContentsTerms::create($input_content_terms);
                }

            }
            
            // end save terms

            Flash::success('Posts update successfully.');            
        }else {
            $posts = Posts::find($id);
            $posts['type'] = 'article';
            $posts['title'] = $request->title;
            $posts['subtitle'] = 'Subtitle '.$request->title;
            $posts['slug'] = strtolower(trim(preg_replace('/[\s-]+/', '-', preg_replace('/[^A-Za-z0-9-]+/', '-', preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $request->title))))), '-'));
            $posts['excerpt'] = 'Excerpt '.$request->title;
            $posts['content'] = $request->content;
            $posts['attr_1'] = $request->attr_1;
            $posts['created_by'] = Auth::id();
            $posts['user_id'] = Auth::id();
            $posts['is_active'] = $request->is_active;
            $posts['attr_5'] = 'youtube';
            $posts['attr_6'] = $request->attr_6;
            $posts['is_featured'] = 0;
            $posts['post_type'] = $request->postType;
            if($request->image != NULL){
                $posts['image'] = '/uploads/content/'.time().'.'.$request->image->getClientOriginalExtension();
                $request->image->move(public_path('uploads/content'), time().'.'.$request->image->getClientOriginalExtension());
            }

            //dd($input);
            //Log::debug($posts);exit();
            $posts->save();

            // Save terms
            $tags = explode(',', $request->tags);
            ContentsTerms::where('content_id', $id)->delete();
            foreach($tags as $value){
                $input_terms['name'] = $value;
                $input_terms['slug'] = strtolower(trim(preg_replace('/[\s-]+/', '-', preg_replace('/[^A-Za-z0-9-]+/', '-', preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $value))))), '-'));
                $input_terms['type'] = 'tag';

                $terms = Terms::where('name', '=', $value)->first();
                if ($terms === null) {
                    $id_terms = Terms::create($input_terms)->id;
                    $input_content_terms['content_id'] = $id;
                    $input_content_terms['term_id'] = $id_terms;
                    ContentsTerms::create($input_content_terms);
                } else {
                    $input_content_terms['content_id'] = $id;
                    $input_content_terms['term_id'] = $terms->id;
                    ContentsTerms::create($input_content_terms);
                }
            }

            Flash::success('Posts update successfully.'); 
        }

        if ($request->create == 1){
            if($request->postType == 'automatic'){
                if (Auth::user()->roles->first()->name == 'admin') {
                    return redirect(route('admin.posts.createAutomatic'));
                }elseif (Auth::user()->roles->first()->name == 'user') {
                    return redirect(route('user.posts.createAutomatic'));
                }else{
                    return redirect(route('creator.posts.createAutomatic'));
                }
            } else {
                if (Auth::user()->roles->first()->name == 'admin') {
                    return redirect(route('admin.posts.createManual'));
                }elseif (Auth::user()->roles->first()->name == 'user') {
                    return redirect(route('user.posts.createManual'));
                }else{
                    return redirect(route('creator.posts.createManual'));
                }
            }          
        } else {
            if (Auth::user()->roles->first()->name == 'admin') {
                return redirect(route('admin.posts.index'));
            }elseif (Auth::user()->roles->first()->name == 'user') {
                return redirect(route('user.posts.index'));
            }else{
                return redirect(route('creator.posts.index'));
            }
        }
    }

    public function updateFeatured(Request $request) 
    {   
        $posts = Posts::find($request->id);
        if($posts->is_featured == 0){
            $posts['is_featured'] = 1;
        }else{
            $posts['is_featured'] = 0;
        }

        $posts->save();
        return $posts;
    }

    public function getSeries(Request $request)
    {
        $series = Series::where('is_active', 1)->where('user_id', $request->id_user)->get()->pluck('title', 'id')->toArray();

        return $series;
    }

    public function checkYoutube(Request $request)
    {
        
        $video = Posts::where('attr_6', $request->id)->first();
        if(!empty($video)){
            return response()->json([
                'result' => Youtube::getVideoInfo($request->id),
                'message' => 'Video is duplicated.',
                'status' => 'failed'
            ]);
        }

        return response()->json([
            'result' => Youtube::getVideoInfo($request->id),
            'message' => 'Get data success',
            'status' => 'success'
        ]);
    }
    
    public function destroy(Request $request,$id)
    {
        if ($request->action == 'act') {
            $posts = posts::find($id);

            if (empty($posts)) {
                Flash::error('Posts not found');

                if (Auth::user()->roles->first()->name == 'admin') {
                    return redirect(route('admin.posts.index'));
                }elseif (Auth::user()->roles->first()->name == 'user') {
                    return redirect(route('user.posts.index'));
                }else{
                    return redirect(route('creator.posts.index'));
                }
            }
            $posts['is_active'] = 1;

            $posts->save();

            Flash::success('Posts activated successfully.');
        }elseif ($request->action == 'inact') {
            $posts = posts::find($id);

            if (empty($posts)) {
                Flash::error('Posts not found');

                if (Auth::user()->roles->first()->name == 'admin') {
                    return redirect(route('admin.posts.index'));
                }elseif (Auth::user()->roles->first()->name == 'user') {
                    return redirect(route('user.posts.index'));
                }else{
                    return redirect(route('creator.posts.index'));
                }
            }
            $posts['is_active'] = 0;

            $posts->save();

            Flash::success('Posts Inactive successfully.');
        }elseif ($request->action == 'del'){
            $posts = posts::find($id);

            if (empty($posts)) {
                Flash::error('Posts not found');

                if (Auth::user()->roles->first()->name == 'admin') {
                    return redirect(route('admin.posts.index'));
                }elseif (Auth::user()->roles->first()->name == 'user') {
                    return redirect(route('user.posts.index'));
                }else{
                    return redirect(route('creator.posts.index'));
                }
            }

            $posts->delete();

            Flash::success('Posts Delete successfully.');
        }
        

        if (Auth::user()->roles->first()->name == 'admin') {
            return redirect(route('admin.posts.index'));
        }elseif (Auth::user()->roles->first()->name == 'user') {
            return redirect(route('user.posts.index'));
        }else{
            return redirect(route('creator.posts.index'));
        }
    }
}
