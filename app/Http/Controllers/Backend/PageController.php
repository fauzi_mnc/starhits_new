<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Backend\Controller;
use Illuminate\Http\Request;
use App\Model\Page;
use Illuminate\Support\Facades\DB;
use App\DataTables\PageDataTable;
use App\Repositories\PageRepository;
use Flash;
use Storage;

class PageController extends Controller
{
    /**
     * The page repository instance.
     */
    protected $pages;

    /**
     * Create a new controller instance.
     *
     * @param  PageRepository  $users
     * @return void
     */
    public function __construct(PageRepository $pages)
    {
        $this->pages = $pages;
    }

    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function index(PageDataTable $pageDataTable)
    {   
        return $pageDataTable->render('page.index');
    }

    public function create()
    {
        $pageList = $this->pages->getList('As Parent', 'parent');

        return view('page.create', [
            'pageList' => $pageList
        ]);
    }

    public function store(Request $request) {
        $input = $request->except('_token');

        if($request->has('image')){
            if ($request->type == 'join') {
                $path = 'uploads/join/'.str_random(32).'.jpg';
            }else{
                $path = 'uploads/partnership/'.str_random(32).'.jpg';
            }
            $image = Storage::disk('local_public')->put($path, file_get_contents($input['image']));
            $input['image'] = $path;
        }
        
        $user = $this->pages->create($input);
        Flash::success('Page saved successfully.');

        return redirect(route('page.index'));
    }

    /**
     * undocumented function
     *
     * @return void
     * @author 
     **/
    function show($id)
    {
    	$page = $this->pages->find($id);

        if (empty($page)) {
            Flash::error('Page not found');

            return redirect(route('page.index'));
        }
        $pageList = $this->pages->getList('As Parent', 'parent');

        return view('page.show', [
            'pageList'  => $pageList,
            'page'      => $page
        ]);
    }

    function edit($id)
    {
        $page = $this->pages->find($id);

        if (empty($page)) {
            Flash::error('Page not found');

            return redirect(route('page.index'));
        }
        $pageList = $this->pages->getList('As Parent', 'parent');

        return view('page.edit', [
            'pageList'  => $pageList,
            'page'      => $page
        ]);
    }

    public function update($id, Request $request) {
        $page = $this->pages->find($id);
        
        if (empty($page)) {
            Flash::error('page not found');

            return redirect(route('page.index'));
        }

        $input = $request->except(['_method', '_token']);

        if($request->has('image')){
            if ($request->type == 'join') {
                $path = 'uploads/join/'.str_random(32).'.jpg';
            }else{
                $path = 'uploads/partnership/'.str_random(32).'.jpg';
            }
            $image = Storage::disk('local_public')->put($path, file_get_contents($input['image']));
            $input['image'] = $path;
        }
        
        $page = $this->pages->update($input, $id);

        Flash::success('Page update successfully.');

        return redirect(route('page.index'));
    }
}
