<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Backend\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Repositories\UserRepository;
use App\Repositories\RoleRepository;
use App\DataTables\CreatorDataTable;
use Flash;
use Youtube;
use App\Http\Requests\StoreOrUpdateCreatorRequest;
use App\Http\Requests\UpdateCreatorRequest;
use Storage;
use App\Model\User;
use App\Model\UserGroup;
use App\Model\Bank;
use App\Model\Socials;
use App\Model\CategoryInst;
use Auth;

class CreatorController extends Controller
{
    /**
     * The page repository instance.
     */
    protected $users;
    protected $roles;

    /**
     * Create a new controller instance.
     *
     * @param  PageRepository  $users
     * @return void
     */
    public function __construct(UserRepository $users, RoleRepository $roles)
    {
        $this->users = $users;
        $this->roles = $roles;
    }

    public function index()
    {
        if(auth()->user()->roles->first()->name == 'admin'){
            $creator = User::whereHas('accessrole', function ($query) {
                $query->where('role_id', '2');
            })->get();
        }else{
            $creator = User::whereHas('accessrole', function ($query) {
                $query->where('role_id', '2');
            })->whereIn('id', function($query)
            {
                $query->select('user_child')
                        ->from('users_group')
                        ->where('users_group.user_master', Auth::user()->id);
            })->get();
        }

        //return($creator);
        return view('creator.index', [
            'creator'  => $creator
        ]);
    }

    public function create()
    {
        return view('creator.create', [
        ]);
    }

    public function store(StoreOrUpdateCreatorRequest $request) {
        $input = $request->except('_token', 'username_instagram');
        
        if($request->has('cover')){
            $cover = $request->cover->store('uploads/creators/cover', 'local_public');
            $input['cover'] = $cover;
        }
        
        if($request->has('image')){
            $path = 'uploads/creators/image/'.str_random(32).'.jpg';
            $image = Storage::disk('local_public')->put($path, file_get_contents($input['image']));
            $input['image'] = $path;
        }
        $input['is_active'] = 1;
        $user = $this->users->create($input);

        Socials::updateOrCreate(
            ['user_id' => $user->id, 'name' => 'ig'],
            ['url'   => $request->input('socials')[0] ?: '']
        );
        Socials::updateOrCreate(
            ['user_id' => $user->id, 'name' => 'fb'],
            ['url'   => $request->input('socials')[1] ?: '']
        );
        Socials::updateOrCreate(
            ['user_id' => $user->id, 'name' => 'twitter'],
            ['url'   => $request->input('socials')[2] ?: '']
        );
        Socials::updateOrCreate(
            ['user_id' => $user->id, 'name' => 'gplus'],
            ['url'   => $request->input('socials')[3] ?: '']
        );

        $creatorId = $this->roles->findWhere([
            'name'=>'creator'
        ])->first()->id;
        $roles[] = $creatorId;    
        
        if($request->username_instagram){

            $user->detailInfluencer()->create(['username' => $request->username_instagram]);
            
            $influencerId = $this->roles->findWhere([
                'name'=>'influencer'
            ])->first()->id;
            $roles[] = $influencerId;
        }
        
        $user->roles()->sync($roles);

        Flash::success('Creator saved successfully.');

        return redirect(route('creator.index'));
    }

    public function edit($id)
    {
        $user = $this->users->find($id);
        $social = Socials::where('user_id', $id)->get();
        $ig = isset($social[0]) ? $social[0] : false;
        $fb = isset($social[1]) ? $social[1] : false;
        $tw = isset($social[2]) ? $social[2] : false;
        $gp = isset($social[3]) ? $social[3] : false;
        //dd($user->image);
        if (empty($user)) {
            Flash::error('Creator not found');

            return redirect(route('creator.index'));
        }
        
        return view('creator.edit', [
            'user'  => $user,
            'ig' => $ig,
            'fb' => $fb,
            'tw' => $tw,
            'gp' => $gp,
        ]);
    }

    public function update(UpdateCreatorRequest $request, $id)
    {
        $user = $this->users->find($id);

        if (empty($user)) {
            Flash::error('Creator not found');

            return redirect(route('creator.index'));
        }

        $input = $request->except(['_method', '_token']);
        
        if($request->file('cover')){
            $cover = $request->cover->store('uploads/creators/cover', 'local_public');
            $input['cover'] = $cover;
        }

        if($request->file('image')){
            $path = 'uploads/creators/image/'.str_random(32).'.jpg';
            $image = Storage::disk('local_public')->put($path, file_get_contents($input['image']));
            $input['image'] = $path;
        }

        Socials::updateOrCreate(
            ['user_id' => $id, 'name' => 'ig'],
            ['url'   => $request->input('socials')[0] ?: '']
        );
        Socials::updateOrCreate(
            ['user_id' => $id, 'name' => 'fb'],
            ['url'   => $request->input('socials')[1] ?: '']
        );
        Socials::updateOrCreate(
            ['user_id' => $id, 'name' => 'twitter'],
            ['url'   => $request->input('socials')[2] ?: '']
        );
        Socials::updateOrCreate(
            ['user_id' => $id, 'name' => 'gplus'],
            ['url'   => $request->input('socials')[3] ?: '']
        );
        
        $user = $this->users->update($input, $id);
        
        if($request->username_instagram){
            if(!$user->detailInfluencer){
                $user->detailInfluencer()->create(['username' => $request->username_instagram]);
            } else {
                $user->detailInfluencer()->update(['username' => $request->username_instagram]);
            }
        }

        Flash::success('Creator updated successfully.');

        return redirect(route('creator.index'));
    }

    public function checkYoutube(Request $request){
        $url = $request->input('url');
        $url = rtrim($url,"/");
        $uri_parts = explode('/', $url);
        $uri_tail = end($uri_parts);
        
        return response()->json([
            'result' => Youtube::getChannelById($uri_tail)
        ]);
    }

    public function setting($id, $context)
    {
        $user = $this->users->with(['detailInfluencer'])->find($id);
        //return $user;
        $countries = $this->users->getCountries();
        $cities = $this->users->getcities();
        $categories = CategoryInst::all()->pluck('title')->toArray();
        $banks = Bank::all()->pluck('name', 'id')->toArray();
        $social = Socials::where('user_id', $id)->get();
        $ig = isset($social[0]) ? $social[0] : false;
        $fb = isset($social[1]) ? $social[1] : false;
        $tw = isset($social[2]) ? $social[2] : false;
        $gp = isset($social[3]) ? $social[3] : false;

        if (empty($user)) {
            Flash::error('Creator not found');

            return redirect(route('creator.index'));
        }
        
        return view('creator.setting', [
            'ig'          => $ig,
            'fb'          => $fb,
            'tw'          => $tw,
            'gp'          => $gp,
            'user'        => $user,
            'countries'   => $countries,
            'cities'      => $cities,
            'banks'       => $banks,
            'context'     => $context,
            'categories'  => array_combine($categories,$categories)
        ]);
    }

    public function updateSetting(Request $request, $id, $context = null)
    {
        $user = $this->users->find($id);
        
        if (empty($user)) {
            Flash::error('Influencer not found');

            return redirect(route('creator.index'));
        }

        if($request->has('socials')){
            Socials::updateOrCreate(
                ['user_id' => $id, 'name' => 'ig'],
                ['url'   => $request->input('socials')[0] ?: '']
            );
            Socials::updateOrCreate(
                ['user_id' => $id, 'name' => 'fb'],
                ['url'   => $request->input('socials')[1] ?: '']
            );
            Socials::updateOrCreate(
                ['user_id' => $id, 'name' => 'twitter'],
                ['url'   => $request->input('socials')[2] ?: '']
            );
            Socials::updateOrCreate(
                ['user_id' => $id, 'name' => 'gplus'],
                ['url'   => $request->input('socials')[3] ?: '']
            );

            if($request->input('socials')[0]){
                if(!$user->hasRole('influencer')){
                    $influencerId = $this->roles->findWhere([
                        'name'=>'influencer'
                    ])->first()->id;
                    $user->roles()->attach($influencerId);
                }
                
                $user->detailInfluencer()->update(['username' => substr($request->input('socials')[0], strrpos($request->input('socials')[0], "/"))]);
            }
            Flash::success('Creator updated successfully.');

            return redirect(route('self.creator.edit', [auth()->id(), $context]));    
        }
        
        $userInput = $request->only([
            'name',
            'phone',
            'country',
            'password',
            'category',
            'email',
            'gender',
            'city',
            'is_active'
        ]);
        $influencerDetail = $request->only([
            'category',
            'instagram_photo_posting_rate',
            'instagram_video_posting_rate',
            'instagram_story_posting_rate',
            'youtube_posting_rate',
            'ig_highlight_rate',
            'package_rate',
            'bank',
            'bank_account_number',
            'bank_holder_name',
            'bank_location',
            'npwp',
            'username',
            'followers_ig',
            'profile_picture_ig'
        ]);
        
        if($request->has('category')){
            $influencerDetail['category'] = implode(',', $request->category);
        }
        
        if(!empty($userInput)){
            $userInput['is_active'] =  ($request->is_active == 'on') ? 1 : 0;
            $user = $this->users->update($userInput, $id);
        }
        
        if(!empty($influencerDetail)){
            $user->detailInfluencer()->update($influencerDetail);
        }

        Flash::success('Creator updated successfully.');

        if($context != null){
            return redirect(route('self.creator.edit', [auth()->id(), $context]));    
        }

        return redirect(route('creator.index'));
    }
}
