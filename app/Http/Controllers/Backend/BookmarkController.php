<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Backend\Controller;
use Flash;
use Storage;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Repositories\UserRepository;
use App\Model\Posts;

class BookmarkController extends Controller
{
    /**
     * The user repository instance.
     */
    protected $post;

    /**
     * Create a new controller instance.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct(Posts $post)
    {
        $this->post = $post;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bookmarks = auth()->user()->bookmarkPost()->paginate(5);
        
        return view('bookmark.index', ['bookmarks' => $bookmarks]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        auth()->user()->bookmarkPost()->detach($id);

        Flash::success('Bookmark deleted successfully.');

        return redirect(route('creator.bookmark.index'));
    }
}
