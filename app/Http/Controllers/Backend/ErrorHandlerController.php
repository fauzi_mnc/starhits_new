<?php

namespace App\Http\Controllers\Backend;
use Illuminate\Http\Request;
use App\Model\Channel;
use App\Model\Posts;
use App\Model\Series;
use App\Model\User;
use App\Model\Video;
use Flash;

class ErrorHandlerController extends Controller
{
	public function errorCode404()
    {
    	$user    = User::all()->count();
        $series  = Series::all()->count();
        $channel = Channel::all()->count();
        $videos   = Posts::all()->count();

    	Flash::error('The page does not exist');
        return view('dashboard.index', ['user' => $user, 'series' => $series, 'channel' => $channel, 'videos' => $videos]);
    }
}
