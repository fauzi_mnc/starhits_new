<?php

namespace App\Http\Controllers\Backend;

use App\Model\Menu;
use Illuminate\Http\Request;
use App\Http\Controllers\Backend\Controller;
use App\DataTables\MenuDataTable;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\StoreOrUpdateMenuRequest;
use Flash;
use App\Model\User;
use App\Model\UserGroup;
use App\Model\AccessRole;
use Auth;
use Session;
use Google_Client;
use Google_Service_Drive;
use Google_Service_Sheets_Spreadsheet;
use Google_Service_Drive_DriveFile;
use Google_Service_Sheets;
use Google_Service_Sheets_ValueRange;
use League\Csv\Reader;
use Hash;

use App\Http\Controllers\Backend\YoutubeController;



class GoogleSheetController extends Controller
{

 	protected $youtubeAnalitic;
  	public function __construct(YoutubeController $youtubeAnalitic)
  	{
    	$this->youtubeAnalitic = $youtubeAnalitic;
  	}


	public function index()
	{	
		$client = new Google_Client();
		putenv('GOOGLE_APPLICATION_CREDENTIALS=star-hits-indonesia-927afa072f56.json');
		$client->useApplicationDefaultCredentials();
		$client->addScope(Google_Service_Drive::DRIVE);
		$driveService = new Google_Service_Drive($client);

		$response = $driveService->files->listFiles();
		dd($driveService);
	}

	public function create_backup(){
		$client = new Google_Client();
		$jsonAuth = putenv('GOOGLE_APPLICATION_CREDENTIALS=star-hits-indonesia-927afa072f56.json');
		$client->useApplicationDefaultCredentials();
		$client->addScope(Google_Service_Drive::DRIVE);
		$client->setApplicationName("Excell Name");
		$client->setAccessType('offline');
		$client->setAuthConfig(json_decode($jsonAuth, true));

		$client->setScopes([
			'https://www.googleapis.com/auth/spreadsheets',
        	'https://www.googleapis.com/auth/drive',
        	'https://www.googleapis.com/auth/drive.file'
    	]);
		$service = new \Google_Service_Sheets($client);
		$drive = new \Google_Service_Drive($client);
		
		$ss = $service->spreadsheets->create(
			new \Google_Service_Sheets_Spreadsheet([
            'properties' => [
                'title' => 'Excell Isi',
            ],
            'sheets' => [
            	'properties'=>[
            		'title'	=>'Excell Isi 2'
            	]
            ]
        ]
		));

		$newPermission = new \Google_Service_Drive_Permission();
		$newPermission->setEmailAddress("starhits.official@gmail.com");
		$newPermission->setType('user');
		$newPermission->setRole('writer');
		$optParams = array('sendNotificationEmail' => false);
		$drive->permissions->create($ss->spreadsheetId,$newPermission,$optParams);

        
		dd($ss);

	}


	public function create12()
    {
        $client = new Google_Client();
		$jsonAuth = putenv('GOOGLE_APPLICATION_CREDENTIALS=star-hits-indonesia-927afa072f56.json');
		$client->useApplicationDefaultCredentials();
		$client->addScope(Google_Service_Drive::DRIVE);
		$driveService = new Google_Service_Drive($client);
		// List Files
		$response = $driveService->files->listFiles();

		// Set File ID and get the contents of your Google Sheet
		$fileID = '1OYyeuVWEjOIHC1fT51XASkKG4t1ExG5UkWnS-Z_N9o4';
		//dd($driveService->files);
		$spreadsheetId = $fileID;

		$client->setApplicationName('My PHP App');
		$client->setScopes([\Google_Service_Sheets::SPREADSHEETS]);
		$client->setAccessType('offline');
		$client->setAuthConfig(json_decode($jsonAuth, true));

		$sheets = new \Google_Service_Sheets($client);
		$data = [];
		$currentRow = 4;
		$date_added = date('Y');
		$valueInputOption = 'USER_ENTERED';
		$range = 'A2:Z';

		/*$values = array("this","is","my","awesome","test","$date_added");
		$values_encoded = json_encode($values);*/

		/*$data[] = new \Google_Service_Sheets_ValueRange(array(
			'range' => $range,
			'values' => $values
		));*/


		//$rows = $sheets->spreadsheets_values->get($spreadsheetId, $range, ['majorDimension' => 'ROWS']);

		// TODO: Assign values to desired properties of `requestBody`:
		/*$spreadsheet = new \Google_Service_Sheets_Spreadsheet([
            'properties' => [
                'title' => 'test',               
            ],
        ]);
        $spreadsheet = $sheets->spreadsheets->create($spreadsheet, [
            'name'	=>'Coba files',
        ]);
		$response = $sheets->spreadsheets->create($spreadsheet);
		dd($response);*/


		/*$spreadsheet = new \Google_Service_Sheets_Spreadsheet([
            'properties' => [
                'title' => 'test'
            ]
        ]);
        $spreadsheet = $sheets->spreadsheets->create($spreadsheet, [
            'fields' => 'spreadsheetId'
        ]);
        printf("Spreadsheet ID: %s\n", $spreadsheet->spreadsheetId);
        // [END sheets_create]
        return $spreadsheet->spreadsheetId;*/

		/*$body = new \Google_Service_Sheets_BatchUpdateValuesRequest(array(
			'valueInputOption' => $valueInputOption,
			'data' => $data
		));

		$result = $sheets->spreadsheets_values->batchUpdate($spreadsheetId, $body);
		dd($result);*/

		/*if (isset($rows['values'])) {
    		foreach ($rows['values'] as $row) {
        
		        if (empty($row[0])) {
		            break;
		        }

		        $data[] = [
		            'col-a' => $row[0],
		            'col-b' => $row[1],
		            'col-c' => $row[2],
		            'col-d' => $row[3],
		            'col-e' => $row[4],
		            'col-f' => $row[5],
		            'col-g' => $row[6],
		        ];

		        $updateRange = 'H'.$currentRow;
		        $updateBody = new \Google_Service_Sheets_ValueRange([
		            'range' => $updateRange,
		            'majorDimension' => 'ROWS',
		            'values' => ['values' => date('c')],
		        ]);
		        $sheets->spreadsheets_values->update(
		            $spreadsheetId,
		            $updateRange,
		            $updateBody,
		            ['valueInputOption' => 'USER_ENTERED']
		        );

		        $currentRow++;
		    }
		}*/


		//printf("Start token: %s\n", $response->startPageToken);


		/*$folderId = '1gWgEOMDo4nphsm6KqqyjOdAKq4IL7acJ';
		$fileMetadata = new Google_Service_Drive_DriveFile(array(
		    'name' => 'default_avatar_male.jpg',
		    'parents' => array($folderId)
		));

		dd($fileMetadata);
		$content = file_get_contents(public_path('img/default_avatar_male.jpg'));
		$file = $driveService->files->create($fileMetadata, array(
		    'data' => $content,
		    'mimeType' => 'image/jpeg',
		    'uploadType' => 'multipart',
		    'fields' => 'id'));
		printf("File ID: %s\n", $file->id);*/

		//dd("Folder ID: %s\n", $file->id);
		/*$response = $driveService->files->export($fileID, 'text/csv', array('alt' => 'media'));
		$content = $response->getBody()->getContents();
		// Create CSV from String

		$csv = Reader::createFromString($content, 'w');
		$csv->setHeaderOffset(0);
		$records = $csv->getRecords();
		// Create an Empty Array and Loop through the Records

		$newarray = array();
		foreach ($records as $value) {
		  $newarray[] = $value;
		}
		// Dump and Die
		*/

		//dd($newarray);
    }


    public function create(){
    	//$values = $this->youtubeAnalitic->analyticJson();
    	$date = date('Y-m');
    	$values = [];

		$values[0] = [
			$number[] = "No",
			$name_channel[] = "Name Channel",
			$subscribers1[] = "Subscriber Week 1",
			$subscribers2[] = "Subscriber Week 2",
			$subscribers3[] = "Subscriber Week 3",
			$subscribers4[] = "Subscriber Week 4",
		];

    	$client = new Google_Client();
		$jsonAuth = putenv('GOOGLE_APPLICATION_CREDENTIALS=star-hits-indonesia-927afa072f56.json');
		$client->useApplicationDefaultCredentials();
		$client->addScope(Google_Service_Drive::DRIVE);
		$client->setApplicationName("Youtube Subscriber Report ".date('m-Y'));
		$client->setAccessType('offline');
		$client->setAuthConfig(json_decode($jsonAuth, true));
		$client->setScopes([
			'https://www.googleapis.com/auth/spreadsheets',
        	'https://www.googleapis.com/auth/drive',
        	'https://www.googleapis.com/auth/drive.file'
    	]);
		$service = new \Google_Service_Sheets($client);
		$drive = new \Google_Service_Drive($client);
		
		$ss = $service->spreadsheets->create(
			new \Google_Service_Sheets_Spreadsheet([
            'properties' => [
                'title' => 'Youtube Subscriber Report '.date('m-Y'),
            ],
            'sheets' => [
            	'properties'=>[
            		'title'	=>'Youtube Subscriber Report '.date('m-Y'),
            	]
            ]
        ]));

		$newPermission = new \Google_Service_Drive_Permission();
		/*$newPermission->setEmailAddress("starhits.official@gmail.com");
		$newPermission->setType('user');
		$newPermission->setRole('writer');
		$optParams = array('sendNotificationEmail' => false);*/

		//$newPermission->setEmailAddress("starhits.official@gmail.com");
		$newPermission->setType('anyone');
		$newPermission->setRole('writer');
		$optParams = array('sendNotificationEmail' => false);

		$drive->permissions->create($ss->spreadsheetId,$newPermission,$optParams);

        
		$sheets = new \Google_Service_Sheets($client);
		$range = 'A3:Z';
		
		$requestBody = new Google_Service_Sheets_ValueRange(array('values' => $values));
		$params = ['valueInputOption' => 'RAW'];

		$response = $sheets->spreadsheets_values->update($ss->spreadsheetId, $range, $requestBody, $params);

		return $ss->spreadsheetId;
    }

    public function updateEntry($file){
    	$values = $this->youtubeAnalitic->analyticJson();

    	//return $values;
    	$client = new Google_Client();
		$jsonAuth = putenv('GOOGLE_APPLICATION_CREDENTIALS=star-hits-indonesia-927afa072f56.json');
		$client->useApplicationDefaultCredentials();
		$client->addScope(Google_Service_Drive::DRIVE);
		//$fileID = '1h1EiLaBtQcJzzAPI1Y_Mt00mCb7i2aFgT-ZxnTnSGMU';
		$spreadsheetId = $file;
		$client->setScopes([\Google_Service_Sheets::SPREADSHEETS]);
		$client->setAccessType('offline');
		$client->setAuthConfig(json_decode($jsonAuth, true));

		$sheets = new \Google_Service_Sheets($client);

		$range = 'A3:Z';
		
		$requestBody = new Google_Service_Sheets_ValueRange(array(
		    'values' => $values
		));

		$params = [
		    'valueInputOption' => 'RAW'
		];

		$response = $sheets->spreadsheets_values->update($spreadsheetId, $range, $requestBody, $params);
		return ($response);
    }

    public function readSpreadsheet(){
    	$client = new Google_Client();
		$jsonAuth = putenv('GOOGLE_APPLICATION_CREDENTIALS=star-hits-indonesia-927afa072f56.json');
		$client->useApplicationDefaultCredentials();
		$client->addScope(Google_Service_Drive::DRIVE);
		//$fileID = '15xxNIeXBpMcZFT9yqfwKlruQu5lopt_d';
		//https://drive.google.com/file/d/15xxNIeXBpMcZFT9yqfwKlruQu5lopt_d/view?usp=sharing
		//https://drive.google.com/file/d/1wk9maJyt3JHY2GiUSmCtYqOW_g0ufUuw/view?usp=sharing
		$fileID='1tipm2IuW8uxbLZeeCUyi33tz0wk8z_uw7IMaAFkXms8'; //
		$spreadsheetId = $fileID;
		$client->setScopes([\Google_Service_Sheets::SPREADSHEETS]);
		$client->setAccessType('offline');
		$client->setAuthConfig(json_decode($jsonAuth, true));
		$sheets = new \Google_Service_Sheets($client);
		$range = 'A4:Z';
		$rows = $sheets->spreadsheets_values->get($spreadsheetId, $range, ['majorDimension' => 'ROWS']);
		$data = [];
		//return json_encode($response->values);
		if (isset($rows['values'])) {
			//return count($rows['values']);

			foreach ($rows['values'] as $key => $row) {
				if($key !=0){
					if (empty($row[2])) {
						break;
					}
					$chanelID = explode('/', $row[10])[4];

					/*$emailUser = str_replace([' ', '.', '/', ','], ".", strtolower($row[3]))."@starhits.id";
					$users = User::where('name', 'like', '%'.$row[3].'%')->where('name_channel', 'like', '%'.$row[3].'%')->first();*/

					$emailUser = str_replace([' ', '.', '/', ','], ".", str_replace([' -','_',' &'], '', strtolower($row[3])))."@starhits.id";
					$users = User::where('provider_id', 'like', '%'.$chanelID.'%')->first();

					$cekDuplicateEmail = User::where('email', 'like', '%'.$emailUser.'%')->first();


					if(empty($users) && empty($cekDuplicateEmail)){ //Jika ada usser berdasarkan Providder
					//if(empty($users)){
			        	//Jika Ada Email di table User
						$in_user = new User();
						$in_user->unit 			= $row[1];
						$in_user->name 			= $row[3];
						$in_user->program		= $row[2];
						$in_user->name_master 	= $row[3];
						$in_user->name_channel 	= $row[3];                        
						$in_user->email 		= $emailUser;
						$in_user->slug 			= str_replace(" ", "-", str_replace([' -','_',' &'], '', strtolower($row[3])));
						$in_user->password 		= Hash::make('123456789012');
						$in_user->provider_id 	= $chanelID;
						$in_user->is_show 		= 0;
						$in_user->is_active 	= 1;
						$in_user->save();
						AccessRole::insert(['user_id' => $in_user->id, 'role_id' => '2' ]);
					}else{

						User::where([
							'provider_id'=> $chanelID
						])->update([
							'unit' 			=> $row[1],
							'name' 			=> $row[3],
							'program' 		=> $row[2],
							'name_master' 	=> $row[3],
							'name_channel' 	=> $row[3],
							'is_active' 	=> 1
						]);

					}
				}
			}
		}
    }


	public function spreadsheet() {
		$client = new Google_Client();
		putenv('GOOGLE_APPLICATION_CREDENTIALS=star-hits-indonesia-927afa072f56.json');
		$client->useApplicationDefaultCredentials();
		$client->addScope(Google_Service_Drive::DRIVE);
		$driveService = new Google_Service_Drive($client);
		// List Files
		$response = $driveService->files->listFiles();

		// Set File ID and get the contents of your Google Sheet
		$fileID = '1wk9maJyt3JHY2GiUSmCtYqOW_g0ufUuw';
		//$fileID = '1zDWLwrwv3Yzs0FAT-UoKkLhUYYy0XZMaBoFFYdbj1w8';
		$response = $driveService->files->export($fileID, 'text/csv', array('alt' => 'media'));
		$content = $response->getBody()->getContents();
		$csv = Reader::createFromString($content, 'r');
		$csv->setHeaderOffset(0);
		$records = $csv->getRecords();
		// Create an Empty Array and Loop through the Records

		$newarray = array();
		foreach ($records as $value) {
		  $newarray[] = $value;
		}
		// Dump and Die
		dd($newarray);

    }

}