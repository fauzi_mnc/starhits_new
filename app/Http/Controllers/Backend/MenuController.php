<?php

namespace App\Http\Controllers\Backend;

use App\Model\Menu;
use Illuminate\Http\Request;
use App\Http\Controllers\Backend\Controller;
use App\DataTables\MenuDataTable;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\StoreOrUpdateMenuRequest;
use Flash;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(MenuDataTable $menuDataTable)
    {
        return $menuDataTable->render('menu.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create_child()
    {
        $parent = Menu::where('parent_id', NULL)->pluck('title', 'id')->toArray();
        return view('menu.create', ['parent' => $parent]);
    }

    public function create_parent()
    {
        return view('menu.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreOrUpdateMenuRequest $request)
    {
        $input = $request->except('_token');
        if ($input['is_active'] == 'on') {
            $input['is_active'] = '1';
        }

        if($request->has('parent_id')){
            $parent = Menu::where('id', $request->parent_id)->first();
            $input['position'] = $parent->position;
        }

        Menu::create($input);
        Flash::success('Menu saved successfully.');

        return redirect(route('menu.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $menu = Menu::find($id);
        if (is_null($menu->parent_id)) {
            if (empty($menu)) {
                Flash::error('Menu not found');

                return redirect(route('menu.index'));
            }

            return view('menu.edit')->with(['menu' => $menu]);
        }else{
            $parent = Menu::where('parent_id', NULL)->pluck('title', 'id')->toArray();

            if (empty($menu)) {
                Flash::error('Menu not found');

                return redirect(route('menu.index'));
            }

            return view('menu.edit')->with(['menu' => $menu, 'parent' => $parent]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update($id, StoreOrUpdateMenuRequest $request)
    {
        $menu = Menu::find($id);
        if ($request->is_active == 'on') {
            $menu['is_active'] = '1';
        }else{
            $menu['is_active'] = '0';
        }

        $menu['title'] = $request->title;
        $menu['url'] = $request->url;

        if($request->has('position')){
            $menu['position'] = $request->position;
        }

        if($request->has('parent_id')){
            $parent = Menu::where('id', $request->parent_id)->first();
            $menu['position'] = $parent->position;
            $menu['parent_id'] = $request->position;
        }

        $menu->save();
        Flash::success('Menu update successfully.');

        return redirect(route('menu.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        //
    }
}