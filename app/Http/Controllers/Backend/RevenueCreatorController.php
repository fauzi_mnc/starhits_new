<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Backend\Controller;
use App\Repositories\RevenueRepository;
use App\Repositories\UserRepository;
use App\DataTables\RevenueCreatorDataTable;
use Flash;
use PDF;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;
use Auth;

use App\Model\User;
use App\Model\Revenue;
use App\Model\RevenueApprover;
use App\Model\RevenueCreator;
use App\Http\Requests\StoreRevenueRequest;
use App\Http\Requests\UpdateRevenueRequest;
use Excel;
use Carbon;
use DB;

class RevenueCreatorController extends Controller
{
    /**
     * The user repository instance.
     */
    protected $revenue;
    protected $users;

    public function __construct(RevenueRepository $revenue, UserRepository $users)
    {
        $this->revenue = $revenue;
        $this->users = $users;
    }

    public function index(RevenueCreatorDataTable $revenueCreator)
    {
        $months[''] = 'Month';
        $months['01'] = 'January';
        $months['02'] = 'February';
        $months['03'] = 'March';
        $months['04'] = 'April';
        $months['05'] = 'May';
        $months['06'] = 'June';
        $months['07'] = 'July';
        $months['08'] = 'August';
        $months['09'] = 'September';
        $months['10'] = 'October';
        $months['11'] = 'November';
        $months['12'] = 'December';
        
        return $revenueCreator->render('revenue.creator.index', ['months' => $months]);
    }

    public function index2(){
        $auth = Auth::user();
        $revenue = $this->revenue->with(['creator'])->find('55');
        
        return $revenue;

        if (empty($revenue)) {
            Flash::error('Revenue not found');
            return redirect(route('revenue.index'));
        }
        
        $creators = $this->users->getlistCreator($revenue->creator->pluck($auth->id)->toArray());
        
        
        return view('revenue.creator.index', [
            'revenue'  => $revenue,
            'creators' => $creators
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $creators = $this->users->getlistCreator();
        
        return view('revenue.create', [
            'creators' => $creators
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $revenueInput = [
            'month'                 => $request->month_b,
            'year'                  => $request->year_b,
            'status'                => $request->status,
            'total_gross_revenue'   => $request->total_gross_revenue
        ];
        
        $revenue = $this->revenue->create($revenueInput);
        
        foreach($request->revenue_creator as $revenue_creator){
            $data = explode(',', $revenue_creator);
            $revenueCreatorInput[] = [
                'revenue_id'        => $revenue->id,
                'creator_id'        => $data[0],
                'estimate_gross'    => $data[1],
                'cost_production'  => $data[2],
            ];
        }
        $revenue->creator()->attach($revenueCreatorInput);
        
        Flash::success('Revenue saved successfully.');

        return redirect(route('revenue.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = $this->users->find($id);

        if (empty($user)) {
            Flash::error('User not found');

            return redirect(route('revenue.index'));
        }
        
        $listRole = $this->roles->getList();

        return view('revenue.edit', [
            'user'  => $user,
            'roles' => $listRole
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, $id)
    {
        $user = $this->users->find($id);
        
        if (empty($user)) {
            Flash::error('user not found');

            return redirect(route('revenue.index'));
        }

        $input = $request->except(['_method', '_token']);
        
        if($request->has('image')){
            $result = Storage::disk('local_public')->delete($user->image);
            $image = $request->image->store('uploads/creators/image', 'local_public');
            $input['image'] = $image;
        }

        if($request->has('roles')){
            $user->roles()->sync($request->input('roles'));
        }
        
        $user = $this->users->update($input, $id);

        Flash::success('user updated successfully.');

        return redirect(route('revenue.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        if ($request->action == 'act') {
            $creator = User::find($id);

            if (empty($creator)) {
                Flash::error('Creator not found');

                return redirect(route('creator.index'));
            }
            $creator['is_active'] = 1;

            $creator->save();

            Flash::success('Creator activated successfully.');
        }else{
            $creator = User::find($id);

            if (empty($creator)) {
                Flash::error('Creator not found');

                return redirect(route('creator.index'));
            }
            $creator['is_active'] = 0;
            $channel = Channels::where('user_id', $creator->id)->get();
            foreach ($channel as $seri) {
                $ser = Channels::find($seri->id);
                $ser['is_active'] = 0;
                $ser->save();
            }
            $series = Serie::where('user_id', $creator->id)->get();
            foreach ($series as $seri) {
                $ser = Serie::find($seri->id);
                $ser['is_active'] = 0;
                $ser->save();
            }
            $video = Vids::where('user_id', $creator->id)->get();
            foreach ($video as $seri) {
                $ser = Vids::find($seri->id);
                $ser['is_active'] = 0;
                $ser->save();
            }

            $creator->save();

            Flash::success('Creator inactivated successfully.');
        }

        

        return redirect(route('creator.index'));
    }

    /**
     * View.
     *
     */
    public function view($id)
    {
        $revenue = $this->revenue->with(['creator'])->find($id);

        $months[''] = 'Month';
        $months['01'] = 'Januari';
        $months['02'] = 'Februari';
        $months['03'] = 'Maret';
        $months['04'] = 'April';
        $months['05'] = 'Mei';
        $months['06'] = 'Juni';
        $months['07'] = 'Juli';
        $months['08'] = 'Agustus';
        $months['09'] = 'September';
        $months['10'] = 'Oktober';
        $months['11'] = 'November';
        $months['12'] = 'Desember';
        
        if (empty($revenue)) {
            Flash::error('Revenue not found');

            return redirect(route('crator.revenue.index'));
        }
        
        $creators = $this->users->getlistCreator($revenue->creator->pluck('id')->toArray());


        return view('revenue.creator.view', [
            'revenue'  => $revenue,
            'creators' => $creators,
            'months' => $months
        ]);
    }

    public function pdf($id)
    {
        $revenue = $this->revenue->with(['creator'])->find($id);

        $months[''] = 'Month';
        $months['01'] = 'Januari';
        $months['02'] = 'Februari';
        $months['03'] = 'Maret';
        $months['04'] = 'April';
        $months['05'] = 'Mei';
        $months['06'] = 'Juni';
        $months['07'] = 'Juli';
        $months['08'] = 'Agustus';
        $months['09'] = 'September';
        $months['10'] = 'Oktober';
        $months['11'] = 'November';
        $months['12'] = 'Desember';

        $creators = $this->users->getlistCreator($revenue->creator->pluck('id')->toArray());

        return view('revenue.creator.pdf', [
            'revenue'  => $revenue,
            'creators' => $creators,
            'months' => $months
        ]);
    }
}
