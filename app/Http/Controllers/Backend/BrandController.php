<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Backend\Controller;
use App\Repositories\UserRepository;
use App\Repositories\RoleRepository;
use App\DataTables\BrandDataTable;
use App\Http\Requests\StoreOrUpdateBrandRequest;
use App\Http\Requests\UpdateBrandRequest;
use Storage;
use App\Model\User;
use Flash;
use DateTime;
use Auth;

class BrandController extends Controller
{
    /**
     * The page repository instance.
     */
    protected $users;
    protected $roles;

    /**
     * Create a new controller instance.
     *
     * @param  PageRepository  $users
     * @return void
     */
    public function __construct(UserRepository $users, RoleRepository $roles)
    {
        $this->users = $users;
        $this->roles = $roles;
    }
    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function index(BrandDataTable $brandDataTable)
    {
        return $brandDataTable->render('brand.index');
    }

    public function create()
    {
        return view('brand.create');
    }

    public function store(StoreOrUpdateBrandRequest $request) {
        $input = $request->except('_token');
        if ($input['is_active'] == 'on') {
            $input['is_active'] = '1';
        }

        $user = $this->users->create($input);

        $creatorId = $this->roles->findWhere([
            'name'=>'brand'
        ])->first()->id;

        $user->roles()->sync([$creatorId]);

        Flash::success('User brand saved successfully.');

        return redirect(route('brand.index'));
    }

    /**
     * undocumented function
     *
     * @return void
     * @author 
     **/
    function show($id)
    {
    	$user = $this->users->find($id);

        if (empty($user)) {
            Flash::error('Brand not found');

            return redirect(route('brand.index'));
        }

        return view('brand.show')->with(['user'=> $user]);
    }

    function edit($id)
    {
        $user = $this->users->find($id);

        if (empty($user)) {
            Flash::error('Creator not found');

            return redirect(route('brand.index'));
        }
        
        return view('brand.edit', [
            'user'  => $user
        ]);
    }

    public function updates($id, UpdateBrandRequest $request) {
        $user = $this->users->find($id);
        
        if (empty($user)) {
            Flash::error('Brand not found');

            return redirect(route('brand.index'));
        }

        $input = $request->except(['_method', '_token']);
        if ($input['is_active'] == 'on') {
            $input['is_active'] = '1';
        }

        $user = $this->users->update($input, $id);

        Flash::success('Brand updated successfully.');

        return redirect(route('brand.index'));
    }
}
