<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Backend\Controller;
use Illuminate\Http\Request;
use App\Model\Serie;
use App\Model\Channels;
use App\Model\Vids;
use Illuminate\Support\Facades\DB;
use App\DataTables\SeriesDataTable;
use App\Http\Requests\StoreOrUpdateSeriesRequest;
use App\Http\Requests\UpdateSeriesRequest;
use Flash;
use Auth;

class SeriesController extends Controller
{
    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function index(SeriesDataTable $seriesDataTable)
    {
        return $seriesDataTable->render('series.index');
    }

    public function create()
    {
        $users = DB::table('users')
            ->leftJoin('role_user', 'users.id', '=', 'role_user.user_id')
            ->where([
                    ['role_user.role_id', '=', 2],
                    ['users.is_active', '=', 1],
                ])->get()->pluck('name', 'id')->toArray();
        $channel = Channels::where('is_active', 1)->pluck('title', 'attr_1')->toArray();
        return view('series.create', ['channel' => $channel, 'users' => $users]);    
    }

    public function store(StoreOrUpdateSeriesRequest $request) {
        if (Auth::user()->roles->first()->name == 'admin') {
            $input['user_id'] = $request->user_id;
            $input['type'] = $request->type;
        }elseif (Auth::user()->roles->first()->name == 'user') {
            $input['user_id'] = $request->user_id;
            $input['type'] = $request->type;
        }else{
            $input['user_id'] = Auth::user()->id;
            $input['type'] = 'starhits';
        }
        $input['title'] = $request->title;
        $input['slug'] = rtrim(str_replace(' ', '-', strtolower($request->title)),'-');
        $input['content'] = $request->content;
        if ($request->image != NULL) {
            $input['image'] = '/uploads/series/'.time().'.'.$request->image->getClientOriginalExtension();
            $request->image->move(public_path('uploads/series'), time().'.'.$request->image->getClientOriginalExtension());
        }
        $input['parent_id'] = $request->parent_id;
        $input['excerpt'] = $request->excerpt;
        $input['is_active'] = 1;
        // var_dump($channel);exit();
        Serie::create($input);
        Flash::success('Series saved successfully.');
        if (Auth::user()->roles->first()->name == 'admin') {
            return redirect(route('admin.serie.index'));
        }elseif (Auth::user()->roles->first()->name == 'user') {
            return redirect(route('user.serie.index'));
        }else{
            return redirect(route('creator.serie.index'));
        }
    }

    /**
     * undocumented function
     *
     * @return void
     * @author 
     **/
    function show($id)
    {
        $series = Serie::find($id);
        $users = DB::table('users')
            ->leftJoin('role_user', 'users.id', '=', 'role_user.user_id')
            ->where([
                    ['role_user.role_id', '=', 2],
                    ['users.is_active', '=', 1],
                ])->get()->pluck('name', 'id')->toArray();
        $channel = Channels::where('is_active', 1)->pluck('title', 'attr_1')->toArray();

        if (empty($series)) {
            Flash::error('Series not found');
            if (Auth::user()->roles->first()->name == 'admin') {
                return redirect(route('admin.serie.index'));
            }elseif (Auth::user()->roles->first()->name == 'user') {
                return redirect(route('user.serie.index'));
            }else{
                return redirect(route('creator.serie.index'));
            }
        }

        return view('series.show', ['channel' => $channel, 'users' => $users, 'series' => $series]);
    }

    function edit($id)
    {
        $series = Serie::find($id);
        $users = DB::table('users')
            ->leftJoin('role_user', 'users.id', '=', 'role_user.user_id')
            ->where([
                    ['role_user.role_id', '=', 2],
                    ['users.is_active', '=', 1],
                ])->get()->pluck('name', 'id')->toArray();
        $channel = Channels::where('is_active', 1)->pluck('title', 'attr_1')->toArray();
        if (empty($series)) {
            Flash::error('Series not found');
            if (Auth::user()->roles->first()->name == 'admin') {
                return redirect(route('admin.serie.index'));
            }elseif (Auth::user()->roles->first()->name == 'user') {
                return redirect(route('user.serie.index'));
            }else{
                return redirect(route('creator.serie.index'));
            }
        }

        return view('series.edit')->with(['series'=> $series, 'channel'=> $channel, 'users' => $users]);
    }

    public function update($id, UpdateSeriesRequest $request) {
        $series = Serie::find($id);
        if (Auth::user()->roles->first()->name == 'admin') {
            $input['user_id'] = $request->user_id;
            $input['type'] = $request->type;
        }elseif (Auth::user()->roles->first()->name == 'user') {
            $input['user_id'] = $request->user_id;
            $input['type'] = $request->type;
        }else{
            $input['user_id'] = Auth::user()->id;
            $input['type'] = 'starhits';
        }
        //dd($request->type);
        $series['title'] = $request->title;
        $series['slug'] = rtrim(str_replace(' ', '-', strtolower($request->title)),'-');
        $series['content'] = $request->content;
        if ($request->image != NULL) {
            $series['image'] = '/uploads/series/'.time().'.'.$request->image->getClientOriginalExtension();
            $request->image->move(public_path('uploads/series'), time().'.'.$request->image->getClientOriginalExtension());
        }
        $series['type'] = $input['type'];
        $series['parent_id'] = $request->parent_id;
        $series['excerpt'] = $request->excerpt;
        $series->save();

        Flash::success('Series update successfully.');

        if (Auth::user()->roles->first()->name == 'admin') {
            return redirect(route('admin.serie.index'));
        }elseif (Auth::user()->roles->first()->name == 'user') {
            return redirect(route('user.serie.index'));
        }else{
            return redirect(route('creator.serie.index'));
        }
    }

    public function updateFeatured(Request $request) 
    {   
        $posts = Serie::find($request->id);
        if($posts->is_featured == 0){
            $posts['is_featured'] = 1;
        }else{
            $posts['is_featured'] = 0;
        }

        $posts->save();
        return $posts;
    }

    public function destroy(Request $request,$id)
    {
        if ($request->action == 'act') {
            $series = Serie::find($id);

            if (empty($series)) {
                Flash::error('Series not found');

                if (Auth::user()->roles->first()->name == 'admin') {
                    return redirect(route('admin.serie.index'));
                }elseif (Auth::user()->roles->first()->name == 'user') {
                    return redirect(route('user.serie.index'));
                }else{
                    return redirect(route('creator.serie.index'));
                }
            }
            $series['is_active'] = 1;

            $series->save();

            Flash::success('Series activated successfully.');
        }elseif ($request->action == 'inact') {
            $series = Serie::find($id);

            if (empty($series)) {
                Flash::error('Series not found');

                if (Auth::user()->roles->first()->name == 'admin') {
                    return redirect(route('admin.serie.index'));
                }elseif (Auth::user()->roles->first()->name == 'user') {
                    return redirect(route('user.serie.index'));
                }else{
                    return redirect(route('creator.serie.index'));
                }
            }
            $series['is_active'] = 0;
            $video = Vids::where('parent_id', $series->id)->get();
            foreach ($video as $vid) {
                $vide = Vids::find($vid->id);
                $vide['is_active'] = 0;
                $vide->save();
            }

            $series->save();

            Flash::success('Series inactivated successfully.');
        }elseif ($request->action == 'del'){
            $series = Serie::find($id);

            if (empty($series)) {
                Flash::error('Series not found');

                if (Auth::user()->roles->first()->name == 'admin') {
                    return redirect(route('admin.serie.index'));
                }elseif (Auth::user()->roles->first()->name == 'user') {
                    return redirect(route('user.serie.index'));
                }else{
                    return redirect(route('creator.serie.index'));
                }
            }

            $series->delete();

            Flash::success('Series Delete successfully.');
        }
        

        if (Auth::user()->roles->first()->name == 'admin') {
            return redirect(route('admin.serie.index'));
        }elseif (Auth::user()->roles->first()->name == 'user') {
            return redirect(route('user.serie.index'));
        }else{
            return redirect(route('creator.serie.index'));
        }
    }
}
