<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Backend\Controller;
use App\Repositories\MasterSingerRepository;
use App\Repositories\UserRepository;
use App\DataTables\DspDataTable;
use App\DataTables\MasterSingerDataTabel;
use Flash;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;
use Storage;
use Log;
use App\Model\User;
use App\Model\RoleUser;
use App\Model\AccessRole;
use App\Model\MasterLagu;
use App\Model\RoleLagu;
use App\Model\Bank;
//use App\Http\Requests\StoreOrUpdateMasterSingerRequest;
use Excel;
use Carbon;
use Auth;
use DB;
use PDF;

class MasterSingerController extends Controller
{
    protected $mastersinger;
    protected $users;


    public function __construct(MasterSingerRepository $mastersinger, UserRepository $users)
    {
        $this->mastersinger = $mastersinger;
        $this->users = $users;
    }
    public function index(){
        $singer = User::join('access_role', 'users.id', '=', 'access_role.user_id')
                        ->where(['access_role.role_id'=>'9'])
                        ->orderBy('users.name_master', 'ASC')
                        ->groupBy('users.name_master')
                        ->get();
        return view('masterlagu.singer.index', [
            'singer'  => $singer
        ]);
    }
    public function create(){
        $singers = User::join('role_user', 'users.id', '=', 'role_user.user_id')
                        ->where(['role_user.role_id'=>'9'])
                        ->select('users.name')
                        ->orderBy('users.name', 'ASC')
                        ->groupBy('users.name')
                        ->get();
        $banks = Bank::where('active','Y')->orderBy('sort','desc')->orderBy('name','asc')->pluck('name', 'id')->toArray();
        return view('masterlagu.singer.create',compact('singers','banks'));
    }

    public function store(Request $request){
        $singer             =   $request->name_singer;
        $name_singer        =   str_replace([' ', '.', '/', ','], ".", strtolower($singer));
        $typemail           =   $request->typemail;
        if(empty($request->email_singer)){
            $email_singer   =   $name_singer.'@starhits.id';
        }else{
            $email_singer   =   strtolower($request->email_singer);
        }
        if(empty($request->password_singer)){
            $password_singer            =   123456789012;
        }else{
            $password_singer            =   $request->password_singer;
        }        
        $role_singer            =   9;
        $bank_id                =   $request->bank_id;
        $bank_account_number    =   $request->bank_account_number;
        $bank_holder_name       =   $request->bank_holder_name;
        $bank_location          =   $request->bank_location;
        $npwp                   =   $request->npwp;

        $users_penyanyi = User::whereIn('users.name', array($singer))->whereIn('users.email', array($email_singer))->first();
        //dd($users_penyanyi);
        if ($singer != '') {
            if (empty($users_penyanyi)) {
                $in_user_penyanyi                       = new User();
                $in_user_penyanyi->name                 = $singer;
                $in_user_penyanyi->name_master          = $singer;
                $in_user_penyanyi->email                = $email_singer;
                $in_user_penyanyi->bank_id              = $bank_id;
                $in_user_penyanyi->bank_account_number  = $bank_account_number;
                $in_user_penyanyi->bank_holder_name     = $bank_holder_name;
                $in_user_penyanyi->bank_location        = $bank_location;
                $in_user_penyanyi->npwp                 = $npwp;
                $in_user_penyanyi->slug                 = str_replace([' ', '.', '/', ','], '', strtolower($singer));
                $in_user_penyanyi->password             = bcrypt($password_singer);
                $in_user_penyanyi->save();

                $role_users_penyanyi = AccessRole::where([['user_id', '=', $in_user_penyanyi->id],['role_id', '=', 9]])->first();
                if (empty($role_users_penyanyi)) {
                    $rolePenyanyi = new AccessRole();
                    $rolePenyanyi->user_id = $in_user_penyanyi->id;
                    $rolePenyanyi->role_id = 9;
                    $rolePenyanyi->save();
                }            
            } else {
                Flash::error('Data Master Singer Failed Save. Email Already use.');
                if (Auth::user()->hasRole('admin')) {
                    return redirect(route('mastersinger.create'));
                }elseif (Auth::user()->hasRole('anr')) {
                    return redirect(route('anr.mastersinger.create'));
                }else{
                    return redirect(route('legal.mastersinger.create'));
                }
            }
        }else{
            Flash::error('Data Master Singer Failed Save.');
            if (Auth::user()->hasRole('admin')) {
                return redirect(route('mastersinger.index'));
            }elseif (Auth::user()->hasRole('anr')) {
                return redirect(route('anr.mastersinger.index'));
            }else{
                return redirect(route('legal.mastersinger.index'));
            }
        }

        Flash::success('Master Singer saved successfully.');
        if (Auth::user()->hasRole('admin')) {
            return redirect(route('mastersinger.index'));
        }elseif (Auth::user()->hasRole('anr')) {
            return redirect(route('anr.mastersinger.index'));
        }else{
            return redirect(route('legal.mastersinger.index'));
        }
    }

    public function edit(Request $request, $id_penyanyi){
        $singers = User::find($id_penyanyi);
        if (empty($singers)) {
            Flash::error('Singer not found');
            if (Auth::user()->hasRole('admin')) {
                return redirect(route('mastersinger.index'));
            }elseif (Auth::user()->hasRole('anr')) {
                return redirect(route('anr.mastersinger.index'));
            }else{
                return redirect(route('legal.mastersinger.index'));
            }
        }
        $banks = Bank::all()->where('active','Y')->pluck('name', 'id')->toArray();
        return view('masterlagu.singer.edit', [ 'singers'   => $singers, 'banks'   => $banks ]);
    }

    public function update(Request $request, $id){
        //dd($id);
        $user = User::find($id);
        //dd($user->name);
        
        if (empty($user)) {
            Flash::error('user not found');

            return redirect(route('user.index'));
        }

        $input = $request->except(['_method', '_token']);
        if(empty($request->password_singer)){
            
            $user = User::whereId($id)->update([
                'name'                 =>   $request->name_singer,
                'name_master'          =>   $request->name_singer,
                'email'                =>   $request->email_singer,
                'bank_id'              =>   $request->bank_id,
                'bank_account_number'  =>   $request->bank_account_number,
                'bank_holder_name'     =>   $request->bank_holder_name,
                'bank_location'        =>   $request->bank_location,
                'npwp'                 =>   $request->npwp
            ]);

        }else{

            $user = User::whereId($id)->update([
                'name'                 =>   $request->name_singer,
                'name_master'          =>   $request->name_singer,
                'email'                =>   $request->email_singer,
                'password'             =>   $request->password_singer,
                'bank_id'              =>   $request->bank_id,
                'bank_account_number'  =>   $request->bank_account_number,
                'bank_holder_name'     =>   $request->bank_holder_name,
                'bank_location'        =>   $request->bank_location,
                'npwp'                 =>   $request->npwp
            ]);

        }

        Flash::success('Singer Updated successfully.');
        //return redirect(route('mastersinger.index'));
        if (Auth::user()->hasRole('admin')) {
            return redirect(route('mastersinger.index'));
        }elseif (Auth::user()->hasRole('anr')) {
            return redirect(route('anr.mastersinger.index'));
        }else{
            return redirect(route('legal.mastersinger.index'));
        }

    }

    public function destroy($id){
        $user = User::find($id);
        $deleteuser = User::where('id', $id)->delete();

        if($deleteuser){
            Flash::success('Data Master Singer Successfully Delete.');
            if (Auth::user()->hasRole('admin')) {
                return redirect(route('mastersinger.index'));
            }elseif(Auth::user()->hasRole('legal')){
                return redirect(route('legal.mastersinger.index'));
            }elseif(Auth::user()->hasRole('anr')){
                return redirect(route('anr.mastersinger.index'));
            }
        }else{
            Flash::error('Data Master Singer Failed Delete.');
            if (Auth::user()->hasRole('admin')) {
                return redirect(route('mastersinger.index'));
            }elseif(Auth::user()->hasRole('legal')){
                return redirect(route('legal.mastersinger.index'));
            }elseif(Auth::user()->hasRole('anr')){
                return redirect(route('anr.mastersinger.index'));
            }
        }
    }
}
