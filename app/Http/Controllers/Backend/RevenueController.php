<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Backend\Controller;
use App\Repositories\RevenueRepository;
use App\Repositories\UserRepository;
use App\DataTables\RevenueDataTable;
use Flash;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;
use App\Events\RevenueWaitingToApproved;
use App\Enum\RevenueStatus;
use Storage;
use Log;
use App\Model\User;
use App\Model\Revenue;
use App\Model\RevenueApprover;
use App\Model\RevenueCreator;
use App\Http\Requests\StoreRevenueRequest;
use App\Http\Requests\UpdateRevenueRequest;
use Excel;
use Carbon;
use Auth;
use DB;
use PDF;

class RevenueController extends Controller
{

    protected $revenue;
    protected $users;


    public function __construct(RevenueRepository $revenue, UserRepository $users)
    {
        $this->revenue = $revenue;
        $this->users = $users;
    }

    public function index(RevenueDataTable $revenueDatatable)
    {
        $months[''] = 'Month';
        $months['01'] = 'January';
        $months['02'] = 'February';
        $months['03'] = 'March';
        $months['04'] = 'April';
        $months['05'] = 'May';
        $months['06'] = 'June';
        $months['07'] = 'July';
        $months['08'] = 'August';
        $months['09'] = 'September';
        $months['10'] = 'October';
        $months['11'] = 'November';
        $months['12'] = 'December';
        return $revenueDatatable->render('revenue.index', ['months' => $months]);
    }

    public function create()
    {
        $creators = $this->users->getlistCreator();
        return view('revenue.create', ['creators' => $creators]);
    }

    public function store(StoreRevenueRequest $request)
    {
        if ($request->type == 1) {
            foreach ($request->revenue_creator as $revenue_creator) {
                $data = explode(',', $revenue_creator);
                $user = User::where('id', $data[0])->first();
                if (empty($user)) {
                    Flash::error('Channel '.$data[1].' has not been registered.');
                    $creators = $this->users->getlistCreator();
                    return view('revenue.create', [
                        'creators' => $creators
                    ]);
                }
            }
        }

        $revenueInput = [
            'month'                 => $request->month_b,
            'year'                  => $request->year_b,
            'status'                => $request->status,
            //'total_gross_revenue'   => $request->total_gross_revenue
        ];
        $CekRevenue = Revenue::where(['month'=>$request->month_b, 'year'=>$request->year_b])->first();
        if(!empty($CekRevenue)){
            Flash::error('Data has been registered.');
            if (Auth::user()->hasRole('admin')) {
                return redirect(route('revenue.index'));
            }elseif (Auth::user()->hasRole('finance')) {
                return redirect(route('finance.revenue.index'));
            }
        }else{
            $revenue = $this->revenue->create($revenueInput);
            if ($request->type == 1) {
                foreach ($request->revenue_creator as $revenue_creator) {
                    $data = explode(',', $revenue_creator);
                    $user = User::where('id', $data[0])->first();
                    if (empty($user)) {
                        Flash::error('Channel '.$data[1].' has not been registered.');
                        $creators = $this->users->getlistCreator();
                        return view('revenue.create', [
                            'creators' => $creators
                        ]);
                    }
                    $revenueCreatorInput[] = [
                        'revenue_id'        => $revenue->id,
                        'creator_id'        => $user->id,
                        'revenue'           => $data[2],
                        'cost_production'   => $data[3],
                        'nett_revenue'      => $data[4],
                        'share_revenue'     => $data[5],
                    ];
                }
                RevenueCreator::insert($revenueCreatorInput);
                //$revenue->creator()->create($revenueCreatorInput);
            }else{
                foreach ($request->revenue_creator as $revenue_creator) {
                    $data = explode(',', $revenue_creator);
                    $revenueCreatorInput[] = [
                        'revenue_id'        => $revenue->id,
                        'creator_id'        => $data[0],
                        'revenue'           => $data[2],
                        'cost_production'   => $data[3],
                        'nett_revenue'      => $data[4],
                        'share_revenue'     => $data[5],
                    ];
                }
                RevenueCreator::insert($revenueCreatorInput);
                //$revenue->creator()->create($revenueCreatorInput);
            }
            
            /*if ($request->status == RevenueStatus::ITG()) {
                event(new RevenueWaitingToApproved($revenue));
            }*/
            
            Flash::success('Revenue saved successfully.');

            if (Auth::user()->hasRole('admin')) {
                return redirect(route('revenue.index'));
            }elseif (Auth::user()->hasRole('finance')) {
                return redirect(route('finance.revenue.index'));
            }
        }
    }

    /* store edited by aditya*/
    public function storeEdited(StoreRevenueRequest $request)
    {
        $creator_id = $request->creator_id;
        $revenue_id = $request->revenue_id;
        $estimate_gross = $request->estimate_gross;
        $cost_production = $request->cost_production;
        $revenue_creator = $request->revenue_creator;

        $revenueInput = [
            'month'                 => $request->month_b,
            'year'                  => $request->year_b,
            'status'                => $request->status,
            'total_gross_revenue'   => $request->total_gross_revenue
        ];
        $revenue = $this->revenue->create($revenueInput);

        for($i=0;$i<count($revenue_creator);$i++){
            $data = explode(',', $revenue_creator[$i]);
            $user = User::where('id', $data[0])->first();

            DB::table('revenue_creator')
                ->insert([
                    'revenue_id'        => $revenue->id,
                    'creator_id'        => $user->id,
                    'estimate_gross'    => $data[2],
                    'cost_production'  => $data[2],

                ]);
        }

        if(Auth::user()->hasRole('admin')) {
            return redirect(route('revenue.index'));
        }elseif (Auth::user()->hasRole('finance')) {
            return redirect(route('finance.revenue.index'));
        }

    }

    public function edit(Request $request, $id)
    {
        $revenue = $this->revenue->with(['creator'])->find($id);
        if (empty($revenue)) {
            Flash::error('Revenue not found');
            
            if(Auth::user()->hasRole('admin')) {
                return redirect(route('revenue.index'));
            }elseif (Auth::user()->hasRole('finance')) {
                return redirect(route('finance.revenue.index'));
            }
        }
        
        $creators = $this->users->getlistCreator($revenue->creator->pluck('id')->toArray());

        return view('revenue.edit', [
            'revenue'  => $revenue,
            'creators' => $creators,
            'type'     => $request->edit_status
        ]);
    }

   /*status view For edit status payment*/
    //public function statusEdit(Request $request, $id){
    public function statusEdit(Request $request){
        $revenue = RevenueCreator::where([
                            'revenue_id'=> $request->id_revenue_,
                            'creator_id'=>$request->id_creator_
                        ])
                        ->update([
                            'date_payment'  => $request->date_payment,
                            'status_paid'   => $request->status_paid,
                        ]);
        if($revenue){
            Flash::success('Revenue updated successfully.');
            if (Auth::user()->hasRole('admin')) {
                return redirect('/admin/revenue/'.$request->id_revenue_.'/edit');
            }elseif (Auth::user()->hasRole('finance')) {
                return redirect('/finance/revenue/'.$request->id_revenue_.'/edit');
            }
        }else{
            Flash::error('Revenue updated failed.');
            if (Auth::user()->hasRole('admin')) {
                return redirect('/admin/revenue/'.$request->id_revenue_.'/edit');
            }elseif (Auth::user()->hasRole('finance')) {
                return redirect('/finance/revenue/'.$request->id_revenue_.'/edit');
            }
        }
    }


    /*-- Form untuk ubah revisi revenue --*/
    public function edit_revisi(Request $request, $id){
        $revenue = $this->revenue->with(['creator'])->find($id);
        if (empty($revenue)) {
            Flash::error('Revenue not found');
            if (Auth::user()->hasRole('admin')) {
                return redirect(route('revenue.index'));
            }elseif (Auth::user()->hasRole('finance')) {
                return redirect(route('revenue.index'));
            }
        }
        
        $creators = $this->users->getlistCreator($revenue->creator->pluck('id')->toArray());
        return view('revenue.revisi', [
            'revenue'  => $revenue,
            'creators' => $creators,
            'type'     => $request->edit_status
        ]);
    }

    public function update_revisi(Request $request){

        $revenue = RevenueCreator::where([
                            'revenue_id'=> $request->id_revenue_,
                            'creator_id'=>$request->id_creator_
                        ])
                        ->update([
                            'revenue'           => $request->revenue_txt ,
                            'cost_production'   => $request->cost_txt ,
                            'nett_revenue'      => $request->net_rev_txt ,
                            'share_revenue'     => $request->share_rev_txt ,
                        ]);
        if($revenue){
            Flash::success('Revenue updated successfully.');
            if (Auth::user()->hasRole('admin')) {
                return redirect('/admin/revenue/'.$request->id_revenue_.'/edit/revisi');
            }elseif (Auth::user()->hasRole('finance')) {
                return redirect('/finance/revenue/'.$request->id_revenue_.'/edit/revisi');
            }
        }else{
            Flash::error('Revenue updated failed.');
            if (Auth::user()->hasRole('admin')) {
                return redirect('/admin/revenue/'.$request->id_revenue_.'/edit/revisi');
            }elseif (Auth::user()->hasRole('finance')) {
                return redirect('/finance/revenue/'.$request->id_revenue_.'/edit/revisi');
            }
        }
    }



    public function StatusChecked($id){ 
        $updated = Revenue::where(['id'=>$id])->update(['status' => 3]);
        if($updated){
            Flash::success('Revenue updated successfully.');
            if (Auth::user()->hasRole('admin')) {
                return redirect(route('revenue.index'));
            }elseif (Auth::user()->hasRole('finance')) {
                return redirect(route('finance.revenue.index'));
            }
        }else{
            Flash::error('Revenue updated failed.');
            if (Auth::user()->hasRole('admin')) {
                return redirect(route('revenue.index'));
            }elseif (Auth::user()->hasRole('finance')) {
                return redirect(route('finance.revenue.index'));
            }
        }
    }

    public function StatusApproved($id){ 
        $updated = Revenue::where(['id'=>$id])->update(['status' => 4]);
        if($updated){
            Flash::success('Revenue succes to Approved.');
            return redirect(route('revenue.approval.index'));
        }else{
            Flash::error('Revenue failed to Check.');
            return redirect(route('revenue.approval.index'));
        }
    }
    public function StatusRevision($id){ 
        $updated = Revenue::where(['id'=>$id])->update(['status' => 5]);
        if($updated){
            Flash::success('Revenue succes to Revision.');
            if (Auth::user()->hasRole('admin')) {
                return redirect(route('revenue.approval.index'));
            }elseif (Auth::user()->hasRole('finance')) {
                return redirect(route('finance.revenue.index'));
            }
        }else{
            Flash::error('Revenue failed to Check.');
            if (Auth::user()->hasRole('admin')) {
                return redirect(route('revenue.approval.index'));
            }elseif (Auth::user()->hasRole('finance')) {
                return redirect(route('finance.revenue.index'));
            }
        }
    }

    public function StatusRejected($id){ 
        $updated = Revenue::where(['id'=>$id])->update(['status' => 6]);
        if ($updated) {
            Flash::success('Revenue succes to Rejected.');
            return redirect(route('revenue.approval.index'));
        } else {
            Flash::error('Revenue failed to Check.');
            return redirect(route('revenue.approval.index'));
        }
    }



    public function update(UpdateRevenueRequest $request, $id)
    {   
        $revenue = $this->revenue->find($id);
        
        if (empty($revenue)) {
            Flash::error('Revenue not found');
            return redirect(route('revenue.index'));
        }

        $revenueInput['status'] = $request->status;
        if($request->has('total_gross_revenue_val')){
            $revenueInput['total_gross_revenue'] = $request->total_gross_revenue_val;
        }
        if($request->has('reason')){
            $revenueInput['reason'] = $request->reason;
        }
        
        if($request->reupload == 'true'){
            foreach($request->revenue_creator as $revenue_creator){
                $data = explode(',', $revenue_creator);
                $user = User::where('name', $data[0])->first();
                if(empty($user)){
                    Flash::error('Channel '.$data[0].' has not been registered.');
                    $creators = $this->users->getlistCreator();
        
                    return view('revenue.edit', [
                        'revenue'  => $revenue,
                        'creators' => $creators,
                        'type'     => 'csv'
                    ]);
                }
                $revenueCreatorInput[] = [
                    'revenue_id'        => $revenue->id,
                    'creator_id'        => $user->id,
                    'estimate_gross'    => $data[1],
                    'cost_production'  => $data[2],
                ];
            }
            $revenue->creator()->detach();
            $revenue->creator()->attach($revenueCreatorInput);

        }else{
            if($request->has('revenue_creator')){
                foreach($request->revenue_creator as $revenue_creator){
                    $data = explode(',', $revenue_creator);
                    $revenueCreatorInput[] = [
                        'revenue_id'        => $revenue->id,
                        'creator_id'        => $data[0],
                        'estimate_gross'    => $data[1],
                        'cost_production'  => $data[2],
                    ];
                }

                $revenue->creator()->detach();
                $revenue->creator()->attach($revenueCreatorInput);
            }
        }
        
        if($request->has('evidence')){
            $result = Storage::disk('local_public')->delete($revenue->evidence);
            $image = $request->evidence->store('uploads/revenue/evidence', 'local_public');
            $revenueInput['evidence'] = $image;
        }
        
        $revenue = $this->revenue->update($revenueInput, $id);

        Flash::success('Revenue updated successfully.');

        if($request->status == RevenueStatus::WTA()){
            event(new RevenueWaitingToApproved($revenue));
        }
        
        if($request->has('approval')){
            if(Auth::user()->hasRole('finance')){
                return redirect(route('finance.revenue.approval.index'));
            }else{
                return redirect(route('revenue.approval.index'));
            }
            
        }

        if(Auth::user()->hasRole('finance')){
            return redirect(route('finance.revenue.index'));
        }else{
            return redirect(route('revenue.index'));
        }
    }


    public function download($id){
        $revenue = $this->revenue->find($id);
        return response()->download(public_path($revenue->pdf));
    }

    public function downloadTry(){
        $creators = User::join('access_role', 'users.id', '=', 'access_role.user_id')
                    ->whereNotNull('users.percentage')
                    ->where(['access_role.role_id'=>'2','users.is_active'=>1])
                    ->select('access_role.user_id','access_role.role_id', 'users.name','users.percentage')
                    ->get();
        return view('revenue.template_csv',compact('creators'));
    }

    
    /**
     * Read csv file.
     *
     */

    public function uploadCsv(){
        return view('revenue.upload_csv');
    }


    /*readcsv EDITED*/
    public function readCsv(Request $request){
        $data = new \stdClass();
        $array_revenue = array();
        if($request->file != NULL){
            $request->file->storeAs('uploads/csv', $request->file->getClientOriginalName(), 'local_public');
            $file_path = public_path().'/uploads/csv/'.$request->file->getClientOriginalName();
            $file = fopen($file_path,"r");
            while(! feof($file)){
                $row1 = fgetcsv($file);
                $record = new \stdClass();
                $record->user_id = $row1[0];
                if($row1[1]){
                    $record->creator = $row1[1];
                }else{
                    $record->creator = "";
                }
                if(is_int(intval($row1[2]))){
                    $record->estimate_gross = $row1[2];
                }else{
                    $record->estimate_gross = 0;
                }
                array_push($array_revenue, $record);
            }
            fclose($file);
        }
        $data = $array_revenue;
        return response()->json([
            'data' => $data
        ]);
    }

    /* function readCsv Origin*/
    public function readExcel(Request $request){
        $data = new \stdClass();
        $array_revenue = array();
        if($request->file != NULL){
            // Moves file to folder on server
            $request->file->storeAs('uploads/revenue', $request->file->getClientOriginalName(), 'local_public');
            $file_path = public_path().'/uploads/revenue/'.$request->file->getClientOriginalName();
            
            if($request->file->getClientOriginalExtension() == 'xlsx'){
                $reader = ReaderFactory::create(Type::XLSX); // for CSV files

                $reader->open($file_path);

                foreach ($reader->getSheetIterator() as $sheet) {
                    foreach ($sheet->getRowIterator() as $key => $row) {
                        if($key>1){
                            if(empty($row[0])){
                                break;
                            }
                            $record = new \stdClass();
                            $record->user_id = preg_replace("/[^0-9]/","",$row[0]);
                            $record->creator = $row[1];

                            if(is_int(intval($row[3]))){
                                $record->revenue = preg_replace("/[^0-9]/","",$row[3]);
                            }else{
                                $record->revenue = 0;
                            }

                            if(is_int(intval($row[4]))){
                                $record->cost = preg_replace("/[^0-9]/","",$row[4]);
                            }else{
                                $record->cost = "0";
                            }

                            if(is_int(intval($row[5]))){
                                $record->net_revenue = preg_replace("/[^0-9]/","",$row[5]);
                            }else{
                                $record->net_revenue = 0;
                            }
                            if(is_int(intval($row[7]))){
                                $record->share_revenue = preg_replace("/[^0-9]/","",$row[7]);
                            }else{
                                $record->share_revenue = 0;
                            }
                            array_push($array_revenue, $record);                            
                        }
                    }
                }
            }else {
                $reader = ReaderFactory::create(Type::CSV); // for CSV files
                $reader->open($file_path);
                $file_path = public_path().'/uploads/revenue/'.$request->file->getClientOriginalName();
                $file = fopen($file_path,"r");
                $numb = 0;
                while(! feof($file)){
                    
                    $row = fgetcsv($file);
                    $cekRow = preg_replace("/[^0-9]/","",$row[0]);
                    $record = new \stdClass();
                    $numb++;
                    if($numb==1){  continue; }

                        $record->user_id = preg_replace("/[^0-9]/","",$row[0]);
                        $record->creator = $row[1];

                        if(is_int(intval($row[3]))){
                            $record->revenue = preg_replace("/[^0-9]/","",$row[3]);
                        }else{
                            $record->revenue = 0;
                        }

                        if(is_int(intval($row[4]))){
                            $record->cost = preg_replace("/[^0-9]/","",$row[4]);
                        }else{
                            $record->cost = "0";
                        }

                        if(is_int(intval($row[5]))){
                            $record->net_revenue = preg_replace("/[^0-9]/","",$row[5]);
                        }else{
                            $record->net_revenue = 0;
                        }
                        if(is_int(intval($row[7]))){
                            $record->share_revenue = preg_replace("/[^0-9]/","",$row[7]);
                        }else{
                            $record->share_revenue = 0;
                        }
                    if($record->creator==""){break;}

                    array_push($array_revenue, $record);
                }
                fclose($file);
            }
            $reader->close();
        }
        
        $data = $array_revenue;
        return response()->json([
            'data' => $data
        ]); 
    }

    public function view($id)
    {
        $revenue = $this->revenue->with(['creator'])->find($id);
        
        if (empty($revenue)) {
            Flash::error('Revenue not found');
            if (Auth::user()->hasRole('admin')) {
                return redirect(route('revenue.index'));
            }elseif(Auth::user()->hasRole('finance')){
                return redirect(route('finance.revenue.index'));
            }
        }
        
        $creators = $this->users->getlistCreator($revenue->creator->pluck('id')->toArray());

        return view('revenue.view', [
            'revenue'  => $revenue,
            'creators' => $creators
        ]);
    }

    public function excel($id){
        $revenues = Revenue::find($id);
        $month = Carbon::createFromDate($revenues->year, $revenues->month, null)->format('F');
        $excel = Excel::create('Perhitungan-Revenue-'.$month.'-'.$revenues->year.'', function($excel) use ($id) {

            $excel->sheet('Sheet1', function($sheet) use ($id) {
                $revenue = Revenue::find($id);
                $creators = $revenue->creator;
                $approver = RevenueApprover::where('type', 1)->where('is_active', 1)->first();
                $checker = RevenueApprover::where('type', 2)->where('is_active', 1)->get();
                
                $total_gross_revenue = $revenue->total_gross_revenue;
                $sum_estimate_gross = $creators->sum(function ($creator) {
                    return $creator->pivot->estimate_gross;
                });
        
                $creators->map(function ($creator) use($sum_estimate_gross, $total_gross_revenue) {
                    
                    $creator->percentage_revenue = ($creator->pivot->estimate_gross / $sum_estimate_gross);
                    $creator->gross_revenue = $creator->percentage_revenue * $total_gross_revenue;
                    $creator->share_revenue = $creator->gross_revenue - $creator->pivot->cost_production;
                    $creator->nett_revenue = $creator->share_revenue * ($creator->percentage / 100);
                    
                    return $creator;
                });

                $month = Carbon::createFromDate($revenue->year, $revenue->month, null)->format('F');
                $sheet->loadView('revenue.revenue_calculation_excel', [
                    'revenue' => $revenue,
                    'creators' => $creators,
                    'approver' => $approver,
                    'checker' => $checker,
                    'month' => $month
                ]);
            });
        });
        return $excel->export('xlsx');
    }

    public function template(){
        $pathToFile = public_path('/uploads/template/template_upload_revenue_excel.xlsx');
        return response()->download($pathToFile);
    }


    /*Download reporting*/
    public function download_report($id){
        $revenue = $this->revenue->with(['creator'])->find($id);
        
        if (empty($revenue)) {
            Flash::error('Revenue not found');
            if (Auth::user()->hasRole('admin')) {
                return redirect(route('revenue.index'));
            }elseif(Auth::user()->hasRole('finance')){
                return redirect(route('finance.revenue.index'));
            }
        }
        $approver = RevenueApprover::where('type', 1)->where('is_active', 1)->first();
        $checker = RevenueApprover::where('type', 2)->where('is_active', 1)->get();
        $creators = $this->users->getlistCreator($revenue->creator->pluck('id')->toArray());
        
        return view('revenue.report_excell', [
            'revenue'  => $revenue,
            'creators' => $creators,
            'approver' => $approver,
            'checker'  => $checker
        ]);
    }


    public function download_report_pdf($id){
        $revenue = $this->revenue->with(['creator'])->find($id);
        
        if (empty($revenue)) {
            Flash::error('Revenue not found');
            if (Auth::user()->hasRole('admin')) {
                return redirect(route('revenue.index'));
            }elseif(Auth::user()->hasRole('finance')){
                return redirect(route('finance.revenue.index'));
            }
        }
        $approver = RevenueApprover::where('type', 1)->where('is_active', 1)->first();
        $checker = RevenueApprover::where('type', 2)->where('is_active', 1)->get();
        $creators = $this->users->getlistCreator($revenue->creator->pluck('id')->toArray());
        
        return view('revenue.report_pdf', [
            'revenue'  => $revenue,
            'creators' => $creators,
            'approver' => $approver,
            'checker'  => $checker
        ]);
    }



    public function indexFinance(RevenueDataTable $revenueDatatable)
    {
        $months[''] = 'Month';
        $months['01'] = 'January';
        $months['02'] = 'February';
        $months['03'] = 'March';
        $months['04'] = 'April';
        $months['05'] = 'May';
        $months['06'] = 'June';
        $months['07'] = 'July';
        $months['08'] = 'August';
        $months['09'] = 'September';
        $months['10'] = 'October';
        $months['11'] = 'November';
        $months['12'] = 'December';
        
        return $revenueDatatable->render('revenue.finance.index', ['months' => $months]);
    }
    //view form edit
    public function viewFinanceInput(){
        $creators = $this->users->getlistCreator();
        return view('revenue.finance.create', ['creators' => $creators]);
    }

    //get data from data Revenue, for edited
    public function financeInput(Request $request){
        $creators = Revenue::where(['month'=>$request->month_a, 'year'=>$request->year_a])->first();
        if(!empty($creators->id)){
            $revenue_creator = DB::table('revenue_creator')->where(['revenue_id'=>$creators->id])->get();
        }else{
            Flash::error('No data available in table.');
            return redirect('finance/revenue/create_finance');
        }

        return view('revenue.finance.create', [
            'creators' => $creators,
            'revenue_creator' => $revenue_creator,
            //'revenue'=>$creators
        ]);
    }

    public function financeUpdate(Request $request){
        $creator_id = $request->creator_id;
        $revenue_id = $request->revenue_id;
        $revenue_status = $request->revenue_status;
        $estimate_gross = $request->estimate_gross;
        $cost_production = $request->cost_production;
        $total_gross =$request->total_gross_revenue_val;
        $revenue_creator =$request->revenue_creator;
        /*foreach ($request->cost_production as $cost_production) {
            $cost_production = explode(',', $cost_production);
            dd($cost_production);
        }*/
        //dd($request);
         $revenue = $this->revenue->find($revenue_id);

        if ($revenue_status  == '2'){

            foreach ($request->revenue_creator as $revenue_creator) {
                $data = explode(',', $revenue_creator);  

                DB::table('revenue_creator')
                ->where([
                    'revenue_id'=> $revenue_id,
                    'creator_id'=>$data[0]
                    ])
                ->update([
                    'cost_production' => preg_replace("/[., ]/","",$data[2]) ]);

                $revenueCreatorInput[] = [
                    'revenue_id'        => $revenue_id,
                    'creator_id'        => $data[0],
                    'estimate_gross'    => $data[1],
                    'cost_production'  => $data[2],
                ];
            }

            Revenue::where('id', $revenue_id)
                ->update([
                'status'  => 1
            ]);

                return view('revenue.revenue_calculation',compact('revenueCreatorInput'));

            /*$revenue->creator()->detach();
            $revenue->creator()->attach($revenueCreatorInput);*/

        }elseif ($revenue_status  == '3'){
            foreach ($request->revenue_creator as $revenue_creator) {
               
                $data = explode(',', $revenue_creator);
                DB::table('revenue_creator')
                    ->where([
                        'revenue_id'=> $revenue_id,
                        'creator_id'=>$data[0]
                        ])
                    ->update([
                        'estimate_gross' => $data[1]]);
            }
            //Jika Data ada. update data, agar data tidak double
            Revenue::where('id', $revenue_id)
                ->update([
                'status'                => 2,
                'total_gross_revenue'   => preg_replace("/[., ]/","",$total_gross)
            ]); 
        }  
    
        Flash::success('Data successfully updated.');
        return redirect('finance/revenue/');
    }

    public function financeInputTotalGross(Request $request){
        $revenue_id = $request->revenue_id;
        $estimate_gross = $request->estimate_gross;

        Revenue::where('id', $request->revenue_id)
                ->update([
                    'status'                => 2,
                    'total_gross_revenue'   => preg_replace("/[., ]/","",$request->total_gross_revenue_val)
                ]);   
    
        Flash::success('Input Total Gross Successfully.');
        return redirect('finance/revenue/create_finance');

    }


    public function financeInputCostProduction(Request $request){
        $revenue_id = $request->revenue_id;
        $estimate_gross = $request->estimate_gross;

        Revenue::where('id', $request->revenue_id)
                ->update([
                    'status'                => 2,
                    'total_gross_revenue'   => preg_replace("/[., ]/","",$request->total_gross_revenue_val)
                ]);   
    
        Flash::success('Input Total Gross Successfully.');
        return redirect('finance/revenue/create_finance');

    }
    public function destroyRevenue($id){
        $creator = RevenueCreator::where(['revenue_id'=>$id])->delete();
        $revenue = Revenue::where('id', $id)->delete();

        if($revenue){
            Flash::success('Data successfully Delete.');
            if (Auth::user()->hasRole('admin')) {
                return redirect(route('revenue.index'));
            }elseif(Auth::user()->hasRole('finance')){
                return redirect(route('finance.revenue.index'));
            }
        }else{
            Flash::error('Data Failed Delete.');
            if (Auth::user()->hasRole('admin')) {
                return redirect(route('revenue.index'));
            }elseif(Auth::user()->hasRole('finance')){
                return redirect(route('finance.revenue.index'));
            }
        }
    }
    
}
