<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Backend\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Repositories\UserRepository;
use App\Model\Bank;
use App\Model\Socials;
use App\Model\CategoryInst;
use App\Repositories\RoleRepository;
use App\DataTables\InfluencerDataTable;
use Flash;
use App\Http\Requests\StoreInfluencerRequest;
use App\Http\Requests\UpdateInfluencerRequest;
use Storage;

class InfluencerController extends Controller
{
    /**
     * The page repository instance.
     */
    protected $users;
    protected $roles;

    /**
     * Create a new controller instance.
     *
     * @param  PageRepository  $users
     * @return void
     */
    public function __construct(UserRepository $users, RoleRepository $roles)
    {
        $this->users = $users;
        $this->roles = $roles;
    }

    public function index(InfluencerDataTable $influencerDatatable)
    {
        return $influencerDatatable->render('influencer.index');
    }

    public function create()
    {
        $countries = $this->users->getCountries();
        $cities = $this->users->getcities();
        $banks = Bank::all()->pluck('name', 'id')->toArray();
        $categories = CategoryInst::all()->pluck('title')->toArray();

        return view('influencer.create', [
            'countries'   => $countries,
            'cities'      => $cities,
            'banks'       => $banks,
            'categories'  => array_combine($categories,$categories)
        ]);
    }

    public function store(StoreInfluencerRequest $request) {
        $user = [
            'name'      => $request->name,
            'phone'     => $request->phone,
            'country'   => $request->country,
            'password'  => $request->password,
            'category'  => $request->category,
            'email'     => $request->email,
            'gender'    => $request->gender,
            'city'      => $request->city,
            'is_active' => ($request->is_active == 'on') ? 1 : 0
        ];
        $influencerDetail =[
            'profile_picture_ig' => $request->profile_picture_ig
        ];
        $influencerDetail = $request->only([
            'category',
            'instagram_photo_posting_rate',
            'instagram_video_posting_rate',
            'instagram_story_posting_rate',
            'youtube_posting_rate',
            'ig_highlight_rate',
            'package_rate',
            'bank_id',
            'bank_account_number',
            'bank_holder_name',
            'bank_location',
            'npwp',
            'username',
            'followers_ig',
            'profile_picture_ig'
        ]);
        $influencerDetail['category'] = implode(',', $request->category);

        $user = $this->users->create($user);

        $user->detailInfluencer()->create($influencerDetail);

        $influencerId = $this->roles->findWhere([
            'name'=>'influencer'
        ])->first()->id;

        $user->roles()->sync([$influencerId]);
        
        Flash::success('Influencer saved successfully.');

        return redirect(route('influencer.index'));
    }

    public function edit($id)
    {
        $user = $this->users->with(['detailInfluencer'])->find($id);
        
        $countries = $this->users->getCountries();
        $cities = $this->users->getcities();
        $categories = CategoryInst::all()->pluck('title')->toArray();
        $banks = Bank::all()->pluck('name', 'id')->toArray();

        if (empty($user)) {
            Flash::error('Influencer not found');

            return redirect(route('influencer.index'));
        }
        
        return view('influencer.edit', [
            'user'        => $user,
            'countries'   => $countries,
            'cities'      => $cities,
            'banks'       => $banks,
            'categories'  => array_combine($categories,$categories)
        ]);
    }

    public function update(UpdateInfluencerRequest $request, $id, $context = null)
    {
        $user = $this->users->find($id);

        if (empty($user)) {
            Flash::error('Influencer not found');

            return redirect(route('influencer.index'));
        }
        if($context == 'social_media'){
            
        }

        //dd($request);
        if($request->has('username')){
            Socials::updateOrCreate(
                ['user_id' => $id, 'name' => 'ig'],
                ['url'   => $request->input('username') ? 'https://instagram.com/'.$request->input('username') : '']
            );

            $user->detailInfluencer()->updateOrCreate(
                ['user_id' => $id, 'username' => $request->username, 'followers_ig' => $request->followers_ig, 'profile_picture_ig' => $request->profile_picture_ig]
            );
            
            if($context == 'social_media'){
                Flash::success('Influencer updated successfully...');
                return redirect(route('self.influencer.edit', [auth()->id(), $context]));  
            }
        }
        
        $userInput = $request->only([
            'name',
            'phone',
            'country',
            'password',
            'category',
            'email',
            'gender',
            'city',
            'is_active'
        ]);

        $influencerDetail = $request->only([
            'category',
            'instagram_photo_posting_rate',
            'instagram_video_posting_rate',
            'instagram_story_posting_rate',
            'youtube_posting_rate',
            'ig_highlight_rate',
            'package_rate',
            'bank',
            'bank_account_number',
            'bank_holder_name',
            'bank_location',
            'npwp',
            'username',
            'followers_ig',
            'profile_picture_ig'
        ]);
        
        if($request->has('category')){
            $influencerDetail['category'] = implode(',', $request->category);
        }
        
        if(!empty($userInput)){
            $userInput['is_active'] =  ($request->is_active == 'on') ? 1 : 0;
            $user = $this->users->update($userInput, $id);
        }
        
        if(!empty($influencerDetail)){
            $user->detailInfluencer()->update($influencerDetail);
        }
        Flash::success('Influencer updated successfully.');
        if($context != null){
            return redirect(route('self.influencer.edit', [auth()->id(), $context]));    
        }
        return redirect(route('influencer.index'));
    }

    public function checkInstagram(Request $request){
        $cache = new \Instagram\Storage\CacheManager(storage_path('framework/cache'));
        $api   = new \Instagram\Api($cache);
        $api->setUserName($request->username);

        try{
            $result = $api->getFeed();
        } catch(\Exception $e){
            $result = false;
        }
        
        return response()->json([
            'result' => $result
        ]);
    }

    public function setting($id, $context)
    {
        $user = $this->users->with(['detailInfluencer'])->find($id);
        
        $countries = $this->users->getCountries();
        $cities = $this->users->getcities();
        $categories = CategoryInst::all()->pluck('title')->toArray();
        $banks = Bank::all()->pluck('name', 'id')->toArray();
        $social = Socials::where('user_id', $id)->get();
        $ig = isset($social[0]) ? $social[0] : false;
        // $fb = isset($social[1]) ? $social[1] : false;
        // $tw = isset($social[2]) ? $social[2] : false;
        // $gp = isset($social[3]) ? $social[3] : false;

        if (empty($user)) {
            Flash::error('Influencer not found');

            return redirect(route('influencer.index'));
        }
        
        return view('influencer.setting', [
            'ig'          => $ig,
            // 'fb'          => $fb,
            // 'tw'          => $tw,
            // 'gp'          => $gp,
            'user'        => $user,
            'countries'   => $countries,
            'cities'      => $cities,
            'banks'       => $banks,
            'context'     => $context,
            'categories'  => array_combine($categories,$categories)
        ]);
    }
      public function destroy(Request $request,$id) {
        $user = $this->users->find($id);

             if (empty($user)) {
            Flash::error('Influencer not found');

            return redirect(route('influencer.index'));
            }

        $user->delete();

        Flash::success('Influencer Delete successfully.');

        return redirect(route('influencer.index'));
    }
}
