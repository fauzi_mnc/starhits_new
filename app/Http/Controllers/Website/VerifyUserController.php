<?php

namespace App\Http\Controllers\Website;

use Illuminate\Http\Request;
use App\Http\Controllers\Website\Controller;
use Illuminate\Support\Facades\DB;
use App\Repositories\UserRepository;
use App\Model\Bank;
use App\Model\Socials;
use App\Model\CategoryInst;
use App\Model\User;
use App\Model\VerifyUser;
use App\Mail\VerifyMail;
use Flash;
use Storage;
use Auth;
use Mail;

class VerifyUserController extends Controller
{
    public function verifyUser($token)
    {
        $verifyUser = VerifyUser::where('token', $token)->first();
        if(isset($verifyUser) ){
            $user = $verifyUser->user;
            if(!$user->verified) {
                $verifyUser->user->verified = 1;
                $verifyUser->user->save();
                $status = "Your e-mail is verified. You can now login.";
            }else{
                $status = "Your e-mail is already verified. You can now login.";
            }
        }else{
            return redirect('/')->with('warning', "Sorry your email cannot be identified.");
        }

        return redirect('/')->with('status', $status);
    }
}
