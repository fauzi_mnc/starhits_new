<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Website\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Repositories\UserRepository;
use App\Model\SiteConfig;
use App\Model\Bank;
use App\Model\Socials;
use App\Model\CategoryInst;
use App\Model\User;
use App\Model\VerifyUser;
use App\Mail\VerifyMemberMail;
use App\Repositories\RoleRepository;
use Flash;
use App\Http\Requests\StoreUserFrontRequest;
use Storage;
use Mail;

class RegisterController extends Controller
{
    /**
     * The page repository instance.
     */
    protected $users;
    protected $roles;

    /**
     * Create a new controller instance.
     *
     * @param  PageRepository  $users
     * @return void
     */
    public function __construct(UserRepository $users, RoleRepository $roles)
    {
        $this->users = $users;
        $this->roles = $roles;
    }

    public function create()
    {
        $countries = $this->users->getCountries();
        $cities = $this->users->getcities();
        $banks = Bank::all()->pluck('name', 'id')->toArray();
        $categories = CategoryInst::all();

        return view('frontend.partials._modals-register', [
            'countries'   => $countries,
            'cities'      => $cities,
            'banks'       => $banks,
            'categories'  => $categories
        ]);
    }

    public function store(StoreUserFrontRequest $request) {
        $input = $request->all();
        $user = $this->users->create($input);
        $verifyUser = VerifyUser::create([
            'user_id' => $user->id,
            'token' => str_random(40)
        ]);
        Mail::to($user->email)->send(new VerifyMemberMail($user));
        Flash::success('successfully register. Please check your email to activation your account.');
        return redirect()->back();
    }
}