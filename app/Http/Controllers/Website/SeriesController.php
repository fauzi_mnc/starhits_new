<?php

namespace App\Http\Controllers\Website;

use Alert;
use App\Http\Controllers\Website\Controller;
use App\Model\Content;
use App\Model\Series;
use App\Model\User;
use App\Model\City;
use App\Model\Country;
use App\Model\CategoryInst;
use App\Repositories\UserRepository;
use App\Model\Bank;
use Auth;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use Khill\Duration\Duration;

class SeriesController extends Controller
{
    protected $users;

    /**
     * Create a new controller instance.
     *
     * @param  PageRepository  $users
     * @return void
     */
    public function __construct(UserRepository $users)
    {
        $this->users = $users;
    }

    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function index()
    {
        return view('frontend.series.index');
    }

    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function show($slug)
    {
        $series = Series::with(['videos', 'channels', 'users'])->where('slug', '=', $slug)->first();

        $recomendedSeries = Series::with(['videos', 'channels', 'users'])->where(function ($query) {
            $query->where('is_active', '=', '1')
                ->where('is_featured', '=', '1');
        })->inRandomOrder()->limit(4)->get();

        $defaultvideo = $series->videos->take(1)->sortByDesc('created_at');
        $video = isset($defaultvideo[0]) ? $defaultvideo[0] : json_decode(json_encode(["title" => "", "image" => "", "id" => ""]));
        // dd($defaultvideo);
        $userVideo = User::where('id', '=', $series->user_id)->first();
        $moreCreator = $userVideo->videos()->where('id', '!=', $video->id)->orderBy('created_at', 'desc')->get();

        return view('frontend.series.show', compact('series', 'recomendedSeries', 'moreCreator', 'video', 'defaultvideo'));
    }

    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function shows($slug)
    {
        $duration = new Duration;

        $series = Series::with(['videos', 'channels', 'users'])->where('slug', '=', $slug)->first();

        $defaultvideo = $series->videos->take(1)->sortByDesc('created_at');

        $video = isset($defaultvideo[0]) ? $defaultvideo[0] : json_decode(json_encode(["title" => "", "image" => "", "id" => ""]));

        $video->link = '';
        if (isset($video->attr_5)) {
            $link = '<iframe allowfullscreen="" class="embed-responsive-item" frameborder="0" src="https://www.metube.id/embed/' . $video->attr_6 . '/">';
            if ($video->attr_5 == 'youtube') {
                $link = '
                <iframe src="https://www.youtube.com/embed/' . $video->attr_6 . '?modestbranding=1&showinfo=1&fs=1&autoplay=1&rel=0"></iframe>';
            }
            $video->link = $link;
        }

        $userVideos = User::where('id', '=', $series->user_id)->first();

        $moreVideos = $userVideos->videos()->where('id', '!=', $video->id)->orderBy('created_at', 'desc')->get();

        $recomendedSeries = Series::with(['videos', 'channels', 'users'])->where(function ($query) {
            $query->where('is_active', '=', '1')
                ->where('is_featured', '=', '1');
        })->inRandomOrder()->get();

        $relatedVideos = DB::table('contents as c')
            ->select('c.slug', 'c.title', 'c.attr_6', 'c.attr_7', 'ct.content_id', 't.name as termname')
            ->join('contents_terms as ct', 'ct.content_id', '=', 'c.id')
            ->join('terms as t', 't.id', '=', 'ct.term_id')
            ->whereRaw('t.id IN (SELECT term_id FROM contents_terms WHERE content_id = ' . $video->id . ')')
            ->where('ct.content_id', '!=', $video->id)
            ->where('c.is_active', '=', '1')
            ->get();

        // dd($series->toArray());

        $bookmarkClass = '';
        if (isset(Auth::user()->id)) {

            $bookmark = DB::table('bookmark')
                ->select('*')
                ->where('content_id', '=', $video->id)
                ->where('user_id', '=', Auth::user()->id)
                ->get();

            $bookmarkClass = $bookmark->isNotEmpty() ? '' : 'active';
        }

        $metaTag = Content::where('slug', '=', $slug)->first();
        $countries = $this->users->getCountries();
        $cities = $this->users->getcities();
        $categories = CategoryInst::all();
        $banks = Bank::all()->pluck('name', 'id')->toArray();

        return view('frontend.series.show', 
            ['countries'    => $countries,
            'cities'    => $cities,
            'categories'    => $categories,
            'banks' => $banks],
            compact('duration', 'series', 'moreVideos', 'video', 'recomendedSeries', 'relatedVideos', 'slug', 'bookmarkClass', 'metaTag'));
    }


    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function filter()
    {
        $models = array();
        $type   = '';
        $sortby = '';
        if (isset($_POST['search'])) {

            $type   = $_POST['type'];
            $sortby = $_POST['sortby'];

            switch ($sortby) {
                case '1':
                case 1:
                    $sort = 'viewed'; // vewed
                    break;
                case '2':
                case 2:
                    $sort = 'attr_2'; // liked
                    break;
                case '3':
                case 3:
                    $sort = 'attr_4'; // comment
                    break;
                case '4':
                case 4:
                    $sort = 'created_at'; // date publish
                    break;

                default:
                    # code...
                    Alert::info('Sort Cannot Be Empty!');
                    return redirect()->back();
                    break;
            }

            if (isset($sort)) {

                // series videos
                $series = Series::with(['videos', 'channels', 'users'])->where('title', 'like', '%' . $type . '%')->first();
                if (!empty($series)) {
                    $videos = $series->videos;
                    if (sizeof($videos) > 0) {
                        foreach ($videos as $key => $value) {
                            $models[] = array(
                                'id'         => $value->id,
                                'title'      => $value->title,
                                'image'      => $value->image,
                                'created_at' => $value->created_at,
                                'slug'       => $value->slug,
                            );
                        }
                    }

                }

                if (sizeof($models) <= 0) {
                    Alert::info('There is no data to display!');
                    return redirect()->back();
                }

                $models = Collection::make($models)->take(20);
            }
        }

        $countries = $this->users->getCountries();
        $cities = $this->users->getcities();
        $categories = CategoryInst::all();
        $banks = Bank::all()->pluck('name', 'id')->toArray();

        return view('frontend.series.filter', 
            ['countries'    => $countries,
            'cities'    => $cities,
            'categories'    => $categories,
            'banks' => $banks],
            compact('models', 'type', 'sort'));
    }

    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function videoSeries($slug, $videoslug)
    {
        $duration = new Duration;

        $series = Series::with(['videos', 'channels', 'users'])->where('slug', '=', $slug)->first();

        $video = $series->videos->where('slug', '=', $videoslug)->first();

        // dd($video->toArray());

        $video->link = '';
        if (isset($video->attr_5)) {
            $link = '<iframe allowfullscreen="" class="embed-responsive-item" frameborder="0" src="https://www.metube.id/embed/' . $video->attr_6 . '/">';
            if ($video->attr_5 == 'youtube') {
                $link = '
                <iframe src="https://www.youtube.com/embed/' . $video->attr_6 . '?modestbranding=1&showinfo=1&fs=1&autoplay=1&rel=0"></iframe>';
            }
            $video->link = $link;
        }

        $userVideos = User::where('id', '=', $series->user_id)->first();

        $moreVideos = $userVideos->videos()->where('id', '!=', $video->id)->orderBy('created_at', 'desc')->get();

        $recomendedSeries = Series::with(['videos', 'channels', 'users'])->where(function ($query) {
            $query->where('is_active', '=', '1')
                ->where('is_featured', '=', '1');
        })->inRandomOrder()->get();

        $relatedVideos = DB::table('contents as c')
            ->select('c.slug', 'c.title', 'c.attr_6', 'c.attr_7', 'ct.content_id', 't.name as termname')
            ->join('contents_terms as ct', 'ct.content_id', '=', 'c.id')
            ->join('terms as t', 't.id', '=', 'ct.term_id')
            ->whereRaw('t.id IN (SELECT term_id FROM contents_terms WHERE content_id = ' . $video->id . ')')
            ->where('ct.content_id', '!=', $video->id)
            ->where('c.is_active', '=', '1')
            ->get();

        $metaTag = Content::where('slug', '=', $slug)->first();
        $countries = $this->users->getCountries();
        $cities = $this->users->getcities();
        $categories = CategoryInst::all();
        $banks = Bank::all()->pluck('name', 'id')->toArray();

        return view('frontend.series.show-video', 
            ['countries'    => $countries,
            'cities'    => $cities,
            'categories'    => $categories,
            'banks' => $banks],
            compact('duration', 'series', 'moreVideos', 'video', 'recomendedSeries', 'relatedVideos', 'slug', 'metaTag'));
    }
}
