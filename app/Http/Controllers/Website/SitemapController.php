<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SitemapController extends Controller
{
    //
    public function Sitemap()
    {
        $post = Home::orderBy('created_at', 'DESC')->first();
        $kategori = Category::orderBy('created_at', 'DESC')->first();

        return response()->view('partials._sitemap', [
            'post' => $post,
            'kategori' => $kategori
        ])->header('Content-Type', 'text/xml');
    }
}
