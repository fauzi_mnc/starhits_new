<?php

namespace App\Http\Controllers\Website;

use Alert;
use App\Http\Controllers\Website\Controller;
use App\Model\Contact;
use App\Model\SiteConfig;
use App\Model\User;
use App\Model\City;
use App\Model\Country;
use App\Model\CategoryInst;
use App\Model\Bank;
use App\Repositories\UserRepository;
use App\Notifications\MailContact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use Illuminate\Notifications\Notifiable;
use Validator;

class ContactController extends Controller
{
    protected $users;

    /**
     * Create a new controller instance.
     *
     * @param  PageRepository  $users
     * @return void
     */
    public function __construct(UserRepository $users)
    {
        $this->users = $users;
    }
    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function index()
    {
        $contacts = SiteConfig::all();
        $countries = $this->users->getCountries();
        $cities = $this->users->getcities();
        $categories = CategoryInst::all();
        $banks = Bank::all()->pluck('name', 'id')->toArray();
        
        $models = array();
        if(sizeof($contacts)>0){
            foreach ($contacts as $key => $value) {
                $models[$value->key] = $value->value;
            }
        }
        
        return view('frontend.contact', 
            ['countries'    => $countries,
            'cities'    => $cities,
            'categories'    => $categories,
            'banks' => $banks],
            compact('models'));
    }

    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function store(Request $request, Contact $admin)
    {
        
        if(empty($request['g-recaptcha-response'])) {
            Alert::info('Captcha is required !!');
            return redirect()->back();
        }
        
        $validator = Validator::make($request->all(), [
            // Do not allow any shady characters
            'name' => 'required|max:255|regex:/^[A-Za-z. -]+$/',
            'email' => 'required|max:255|email',
            'message' => 'required|max:255|regex:/^[A-Za-z. -]+$/',
        ]);
     
        if ($validator->fails()) {
           return redirect('/contact')
           ->withInput()
           ->withErrors($validator, 'contact');
        }

        $contact = Contact::create([

            'name'    => $request->name . ' ' . $request->last_name,
            'email'   => $request->email,
            'message' => $request->message,
        ]);


        $users = User::find(68);
        // $users = SiteConfig::where('key', '=', 'website_email')->get();
        // dd($users);
        
        Notification::send($users, new MailContact($contact));

        Alert::info('Email was sent!');

        // redirect the user back
        return redirect()->back();
    }
}
