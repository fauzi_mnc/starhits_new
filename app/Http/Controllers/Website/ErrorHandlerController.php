<?php

namespace App\Http\Controllers\Website;

use App\Model\Series;
use App\Model\User;
use App\Model\Video;
use App\Model\City;
use App\Model\Bank;
use App\Model\Country;
use App\Model\CategoryInst;
use App\Repositories\UserRepository;
use Khill\Duration\Duration;

class ErrorHandlerController extends Controller
{
    protected $users;

    /**
     * Create a new controller instance.
     *
     * @param  PageRepository  $users
     * @return void
     */
    public function __construct(UserRepository $users)
    {
        $this->users = $users;
    }
    public function errorCode404()
    {
        $duration = new Duration;
        $popularCreators = User::with('videosPopular')->inRandomOrder()->take(6)->get();
        $latestVideoStarHits = Video::with('series')->orderBy('created_at', 'desc')->get();
        $recomendedSeries = Series::with(['videos', 'channels', 'users'])->where('is_featured', '=', '1')->inRandomOrder()->take(4)->get();
        $recomendedVideos = Video::with('users')->where('is_featured', '=', '1')->inRandomOrder()->take(8)->get();
        $countries = $this->users->getCountries();
        $cities = $this->users->getcities();
        $categories = CategoryInst::all();
        $banks = Bank::all()->pluck('name', 'id')->toArray();

        return view('frontend.partials._errors',
            ['countries'    => $countries,
            'cities'    => $cities,
            'categories'    => $categories,
            'banks' => $banks],
            compact('duration', 'latestVideoStarHits', 'popularCreators', 'recomendedSeries', 'recomendedVideos'));
    }
}
