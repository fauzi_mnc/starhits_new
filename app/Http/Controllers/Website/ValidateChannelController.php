<?php

namespace App\Http\Controllers\Website;

use Alaouy\Youtube\Facades\Youtube;
use Alert;
use App\Http\Controllers\Website\Controller;
use App\Model\User;
use Auth;
use Carbon\Carbon;
use Hash;
use Illuminate\Http\Request;

class ValidateChannelController extends Controller
{
    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function index($provider_id)
    {
        $youtube = new Youtube;

        $authUsers = auth()->user()->roles() ? auth()->user()->roles()->first()->name : 'norole';

        //dd($authUsers);

        $authCreators = User::where('provider_id', $provider_id)->first();

        $minSubcriber = number_format(1000);

        if (isset(Youtube::getChannelById($provider_id)->statistics->hiddenSubscriberCount)) {
            $statusSubcriber = Youtube::getChannelById($provider_id)->statistics->hiddenSubscriberCount;
        } else {
            $statusSubcriber = 'false';
        }

        dd($statusSubcriber);

        $totalSubscriber = isset(Youtube::getChannelById($provider_id)->statistics->subscriberCount) ? number_format(Youtube::getChannelById($provider_id)->statistics->subscriberCount) : 0;

        $channelView = isset(Youtube::getChannelById($provider_id)->statistics->viewCount) ? number_format(Youtube::getChannelById($provider_id)->statistics->viewCount) : 0;

        $totalVideo = isset(Youtube::getChannelById($provider_id)->statistics->videoCount) ? number_format(Youtube::getChannelById($provider_id)->statistics->videoCount) : 0;

        return view('frontend.partials._form-authenticate', compact('authUsers', 'authCreators', 'statusSubcriber', 'minSubcriber', 'totalSubscriber', 'channelView', 'totalVideo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $provider_id)
    {
        $this->validate($request, [
            'name'     => 'required|min:5|max:35|regex:/^[A-Za-z. -]+$/',
            'gender'   => 'required',
            'email'    => 'required|email|max:50|unique:users',
            'password' => 'required|min:6',
            'cover'    => 'required',
        ], [
            'name.required'     => ' The first name field is required.',
            'gender.required'   => ' The gender name field is required.',
            'email.required'    => ' The email name field is required.',
            'password.required' => ' The password name field is required.',
        ]);

        $input = $request->only('email', 'password');
        if (!empty($input['password'])) {
            $input['password'] = Hash::make($input['password']); //update the password
        } else {
            $input = array_except($input, array('password')); //remove password from the input array
        }

        $image = $request->cover->store('uploads/creators/cover', 'local_public');

        // $imageName = time() . '.' . request()->cover->getClientOriginalExtension();
        // dd($imageName);
        // request()->cover->move(public_path('uploads/creators/cover'), $imageName);

        $user              = User::find(Auth::user()->id);
        $user->subscribers = $request->input('subscribers');
        $user->attr_1      = $request->input('totalvideo');
        $user->attr_2      = $request->input('channelview');
        $user->name        = $request->input('name');
        $user->dob         = Carbon::parse($request->input('dob'));
        $user->gender      = $request->input('gender');
        $user->cover       = $image;
        $user->is_active   = 1;

        $user->update($input); //update the user info
        $user->roles()->sync([]);
        $user->roles()->attach(2);

        // dd($user->toArray());
        //store status message
        Alert::info('Congratulations, now you become our creator!');

        return redirect()->route('home')->withErrors($request->all());
    }
}
