<?php

namespace App\Http\Controllers\Modules;

use App\Model\Series;
use App\Model\Video;
use App\Http\Controllers\Website\Controller;
use App\Model\Channel;

class APIWebsiteController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function seriesAPI()
    {
        $seriesAPI = Series::has('videos')->orderBy('created_at', 'desc')->where('is_active', '=', '1')->paginate(8);
        
        return response()->json([
            'items' => $seriesAPI->items(),
            'next_page_url' => $seriesAPI->nextPageUrl()
        ])->header('Access-Control-Allow-Origin', '*');
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function channelVideos()
    {
        $channelVideos = Video::with('users')->orderBy('created_at', 'desc')->where('is_active', '=', '1')->paginate(4);

        return response()->json([
            'items' => $channelVideos->items(),
            'next_page_url' => $channelVideos->nextPageUrl()
        ])->header('Access-Control-Allow-Origin', '*');
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDataVideoChannel($slug)
    {
        $channels = Channel::with(['videos'])->where('slug', '=', $slug)->first();

        $getLatestMusic = Video::with(['channelVideo', 'users'])->where('is_active', '=', '1')->whereHas('channelVideo', function ($query) use ($slug) {
            $query->where('slug', $slug);
        })->orderBy('created_at', 'desc')->paginate(4);

        return response()->json([
            'items' => $getLatestMusic->items(),
            'next_page_url' => $getLatestMusic->nextPageUrl()
        ])->header('Access-Control-Allow-Origin', '*');
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDataSeriesChannel($slug)
    {
        $channels = Channel::with(['videos'])->where('slug', '=', $slug)->first();

        $getLatestSeries = $channels->series()->orderBy('created_at', 'DESC')->paginate(4);

        return response()->json([
            'items' => $getLatestSeries->items(),
            'next_page_url' => $getLatestSeries->nextPageUrl()
        ])->header('Access-Control-Allow-Origin', '*');
    }
}
