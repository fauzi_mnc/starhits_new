<?php

Route::get('sitemap.xml', function () {
    $sitemap = app("sitemap");      
    $articles = App\Article::all() ; //fetch all articles to make url 
    $sitemap->setCache('laravel.sitemap',60);
    $date = date('c'); 
    $priority = 1.0;
    foreach($articles as $key=>$article){
        $url = route('detail',[$article->slug]) ;
        $sitemap->add($url,$article->updated_at,$priority) ;
        }
    return $sitemap->render('xml'); 
});