<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Model\User;
use Auth;

class UpdateBrandRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = User::find(request()->type);
        return [
            'name'      => 'required|max:30|regex:/^[a-zA-Z0-9\s]*$/',
            'email'     => 'required|email|unique:users,email,'.$user->id,
            'brand_name'=> 'required|max:30',
            'phone'     => ['max:30', 'regex:/\+?([ -]?\d+)+|\(\d+\)([ -]\d+)/', 'nullable']
        ];
    }
}
