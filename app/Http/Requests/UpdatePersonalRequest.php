<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Model\User;
use Auth;

class UpdatePersonalRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // dd(request()->password);
        $user = User::find(request()->type);
        if (Auth::user()->roles->first()->name == 'brand') {
            if (!empty(request()->password)) {
                return [
                    'name'      => 'required|max:30|regex:/^[a-zA-Z0-9\s]*$/',
                    'email'     => 'required|email|unique:users,email,'.$user->id,
                    'brand_name'=> 'required|max:30',
                    'phone'     => ['max:30', 'regex:/\+?([ -]?\d+)+|\(\d+\)([ -]\d+)/', 'nullable'],
                    'password'  => "min:12|required|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/",
                ];
            }else{
                return [
                    'name'      => 'required|max:30|regex:/^[a-zA-Z0-9\s]*$/',
                    'email'     => 'required|email|unique:users,email,'.$user->id,
                    'brand_name'=> 'required|max:30',
                    'phone'     => ['max:30', 'regex:/\+?([ -]?\d+)+|\(\d+\)([ -]\d+)/', 'nullable']
                ];
            }
        }else{
            return [
                'name'      => 'required|max:50|regex:/^[a-zA-Z\s]*$/',
                'image'     => 'image|mimes:jpg,png,jpeg|max:1000',
                'cover'     => 'image|mimes:jpg,png,jpeg|max:1000',
                'email'     => 'required|email|unique:users,email,'.$user->id,
                'dob'       => 'date',
                'phone'     => ['max:30', 'regex:/\+?([ -]?\d+)+|\(\d+\)([ -]\d+)/', 'nullable'],
                'country'   => 'max:40|regex:/^[a-zA-Z\s]*$/|nullable',
                'city'      => 'max:40|nullable',
                'ktp'       => 'max:30|regex:/^[0-9\s]*$/|nullable',
            ];
        }
    }

    public function messages()
    {
        return [
            'password.regex' => 'Your password should contain at least 1 Uppercase, 1 Lowercase, 1 Numeric and 1 special character'
        ];
    }
}
