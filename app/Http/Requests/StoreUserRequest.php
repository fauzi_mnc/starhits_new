<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'      => 'required|max:30',
            'email'     => 'required|email|unique:users,email',
            //'password'  => "min:8|required|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/",
            //'password'  => "min:8|required|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])/",
            //'image'     => 'required|image|mimes:jpg,png,jpeg|max:1000',
            'roles'     => 'required',
            'is_active' => 'required',
            'percentage'=> 'numeric'
        ];
    }
}
