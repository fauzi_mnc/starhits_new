<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreOrUpdateCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                'title'     => 'required|max:100|regex:/^[a-zA-Z0-9\s]*$/',
                'parent'      => 'required',
                'is_active'      => 'required',
            ];
    }

    public function messages()
    {
        return [
            'title.regex' => 'Sub Category format is invalid.',
            'title.required' => 'Sub Category field is required.',
            'parent.required' => 'Category field is required.',
            'title.max' => 'Sub Category field may not be greater than 60 characters.'
        ];
    }
}
