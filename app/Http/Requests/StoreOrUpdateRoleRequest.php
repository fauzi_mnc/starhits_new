<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreOrUpdateRoleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'display_name'      => 'required|max:50|regex:/^[a-zA-Z0-9\s]*$/|unique:roles,name',
            'description'       => 'required|max:191|regex:/^[a-zA-Z0-9\s]*$/'
        ];
    }
}
