<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Model\User;

class UpdateInfluencerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {        
        if(request()->has('context')){
            $user = User::find(request()->type);
            $rules = [];
            if(request()->context == 'payment'){
                $rules  = [
                    'bank_account_number'   => 'integer',
                    'bank_holder_name'      => 'regex:/^[a-zA-Z0-9\s]*$/',
                    'bank_location'         => 'regex:/^[a-zA-Z0-9\s]*$/',
                    'npwp'                  => 'regex:/^[a-zA-Z0-9\s]*$/',
                ];
            }
        } else {
            $user = User::find(request()->type);
            $rules = [
                'email'                 => 'required|unique:users,email,' .$user->id,
                'bank_account_number'   => 'integer',
                'bank_holder_name'      => 'regex:/^[a-zA-Z0-9\s]*$/',
                'bank_location'         => 'regex:/^[a-zA-Z0-9\s]*$/',
                'npwp'                  => 'regex:/^[a-zA-Z0-9\s]*$/',
            ];
        }

        return $rules;
    }
}
