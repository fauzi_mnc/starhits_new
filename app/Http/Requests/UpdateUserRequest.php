<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Model\User;
use Auth;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = User::find($this->user);
        return [
            'name'      => 'required|max:30',
            'email'     => 'required|max:50|email|unique:users,email,'.$user->id,
            'image'     => 'image|mimes:jpg,png,jpeg|max:1000',
            'roles'     => 'required',
            'is_active' => 'required',

        ];
    }
}
