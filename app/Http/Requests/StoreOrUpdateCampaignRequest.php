<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class StoreOrUpdateCampaignRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {   
        if (Auth::user()->roles->first()->name == 'admin') {
            return [
                'brand_id'      => 'required',
                'type'          => 'required',
                'category_id'   => 'required',
                'start_date'    => 'required|date',
                'end_date'      => 'required|date',
                'socio_economic_status'     => 'required',
                'title'         => 'required|max:125|regex:/^[a-zA-Z0-9\s]*$/',
                'brief_campaign'      => 'required|string',
                'budget'        => 'required|numeric',
                'tags'          => 'required',
                'post_to'       => 'required|array',
                'category'      => 'required|array',
            ];
        }else{
            return [
                'type'          => 'required',
                'category_id'   => 'required',
                'start_date'    => 'required|date',
                'end_date'      => 'required|date',
                'socio_economic_status'     => 'required',
                'title'         => 'required|max:125|regex:/^[a-zA-Z0-9\s]*$/',
                'brief_campaign'      => 'required|string',
                'budget'        => 'required|numeric',
                'tags'          => 'required',
                'post_to'       => 'required|array',
                'category'      => 'required|array',
            ];
        }
    }

    public function messages()
    {
        return [
            'brand_id.required'   => 'Brand field is required.',
            'category_id.required'   => 'Sub Category field is required.',
            'tags.required'   => 'Hastag field is required.',
        ];
    }
}
