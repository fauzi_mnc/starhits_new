<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class UpdateSeriesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (Auth::user()->roles->first()->name == 'admin') {
            return [
                'user_id'   => 'integer',
                'parent_id' => 'required|integer',
                'type'      => 'required|regex:/^[a-zA-Z0-9\s]*$/',
                'title'     => 'required|max:125|regex:/^[a-zA-Z0-9\s\#\&\/]*$/',
                'excerpt'   => 'required|string',
                'content'   => 'required|string',
                'image'     => 'image|mimes:jpg,png,jpeg|max:1000'
            ];
        }else{
            return [
                'user_id'   => 'integer',
                'parent_id' => 'required|integer',
                'title'     => 'required|max:125|regex:/^[a-zA-Z0-9\s\#\&\/]*$/',
                'excerpt'   => 'required|string',
                'content'   => 'required|string',
                'image'     => 'image|mimes:jpg,png,jpeg|max:1000'
            ];
        }
    }
}
