<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Model\User;

class UpdateCreatorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = User::find(request()->id);
        
        $rules = [
            'name'      => 'required|max:50|regex:/^[a-zA-Z\s]*$/',
            'provider_id'     => 'required|unique:users,provider_id,'.$user->id,
            // 'password'  => "required|min:12|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/",
            'percentage'=> 'numeric',
            'socials.*'   => ['max:100', 'nullable', 'regex:/(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9]\.[^\s]{2,})/']
        ];

        if(request()->has('password')){
            $rules['password'] = 'required|min:8';
        }
        
        if(request()->username_instagram){
            $rules['username_instagram'] = $user->detailInfluencer ? 'unique:detail_influencer,username,'.$user->detailInfluencer->id : 'unique:detail_influencer,username';
        }
        
        if(request()->hasFile('image')){
            $rules['image'] = 'image|mimes:jpg,png,jpeg|max:1000';
        }

        if(request()->hasFile('cover')){
            $rules['cover'] = 'image|mimes:jpg,png,jpeg|max:1000';
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'password.regex' => 'Your password should contain at-least 1 Uppercase, 1 Lowercase, 1 Numeric and 1 special character',
            'provider_id.required' => 'Youtube channel is required.',
            'provider_id.unique' => 'Youtube channel has already been taken.'
        ];
    }
}
