<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreInfluencerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'                 => 'required|email|unique:users,email',
            'username'              => 'unique:detail_influencer,username',
            // 'bank_account_number'   => 'integer',
            // 'bank_holder_name'      => 'regex:/^[a-zA-Z0-9\s]*$/',
            // 'bank_location'         => 'regex:/^[a-zA-Z0-9\s]*$/',
            // 'npwp'                  => 'regex:/^[a-zA-Z0-9\s]*$/',
        ];
    }
}
