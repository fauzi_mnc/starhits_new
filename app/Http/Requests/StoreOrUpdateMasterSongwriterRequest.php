<?php

namespace App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;

class StoreOrUpdateMasterSongwriterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        if($request->typemail ==1){
            $songwriter             =   $request->name_songwriter;
            $pencipta           =   explode(',', $songwriter);
            $name_songwriter        =   str_replace(" ", ".", strtolower($pencipta[0]));
            $email_songwriter   =   $name_songwriter.'@starhits.id';
            if(empty($request->password_songwriter)){
                $password_songwriter            =   123456789012;
            }else{
                $password_songwriter            =   $request->password_songwriter;
            }
            return [
                'name_songwriter'      => 'required|max:30',
                'password_songwriter'  => 'min:8|regex:/^(?=.*?[0-9]).{6,}$/'
            ];
        }else{
            $email_songwriter   =   strtolower($request->email_songwriter);
            if(empty($request->password_songwriter)){
                $password_songwriter            =   123456789012;
            }else{
                $password_songwriter            =   $request->password_songwriter;
            }
            return [
                'name_songwriter'      => 'required|max:30',
                'email_songwriter'     => 'required|email|unique:users,email',
                'password_songwriter'  => 'min:8|regex:/^(?=.*?[0-9]).{6,}$/'
            ];
        }
    }

    public function messages()
    {
        return [
            'name_songwriter.max:30'    => 'Maksimal 30 Character',
            'email_songwriter.email'   => 'Format Email Salah',
            'email_songwriter.unique'   => 'Email Sudah terdaftar',
            'password_songwriter.regex' => 'Harus ada Kombinasi Angka.'
        ];
    }
}
