<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Session\Store;
use Auth;
use App\Model\User;

class SessionTimeout {

    protected $session;
    protected $timeout = 18000;

    public function __construct(Store $session){
        $this->session = $session;
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // $isLoggedIn = $request->path() != '/logout';
        // if(! session('lastActivityTime'))
        //     $this->session->put('lastActivityTime', time());
        // elseif(time() - $this->session->get('lastActivityTime') > $this->timeout){
        //     $this->session->forget('lastActivityTime');
        //     $user = User::where('id', Auth::id())->first();
        //     $user->is_login = 0;
        //     $user->save();
        //     $request->session()->invalidate();

        //     return redirect('/login');
        // }
        // $isLoggedIn ? $this->session->put('lastActivityTime', time()) : $this->session->forget('lastActivityTime');
        return $next($request);
    }

}