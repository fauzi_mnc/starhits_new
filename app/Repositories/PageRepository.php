<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Model\Page;

/**
 * Class RoleRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class PageRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Page::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    

    public function create(array $input){

        $input['slug'] = rtrim(str_replace(' ', '-', strtolower($input['title'])),'-');
        
        return $this->model->create($input);
    }

    public function update(array $input, $id){
        $input['slug'] = rtrim(str_replace(' ', '-', strtolower($input['title'])),'-');
        
        return parent::update($input, $id);
    }

    public function getList($prependTitle = 'Select', $prependValue = ''){
        return $this->all()->pluck('title', 'id')->prepend($prependTitle, $prependValue)->toArray();
    }
}
