<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use DB;
use App\Extend\DailymotionCli;

/**
 * Class UserRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class MultiUploadRepository
{

    public function getClient(){
        $client = new \Google_Client();
        $client->setClientId(env('GOOGLE_CLIENT_ID'));
        $client->setClientSecret(env('GOOGLE_CLIENT_SECRET'));
        $client->setScopes('https://www.googleapis.com/auth/youtube');
        $client->setRedirectUri(env('GOOGLE_REDIRECT'));
        $client->setAccessType('offline');
        $client->setPrompt('consent select_account');

        $token = DB::table('user_token')->where('user_id', auth()->user()->id)->where('type', 'youtube')->first();
        $token = collect($token)->except(['id', 'user_id', 'type'])->toArray();
        $client->setAccessToken($token);

        return $client;
    }

    public function dailymotionPost($content){
        $api = new DailymotionCli();
        $api->setGrantType(\Dailymotion::GRANT_TYPE_AUTHORIZATION, env('DAILYMOTION_CLIENT_ID'), env('DAILYMOTION_CLIENT_SECRET'), ['manage_videos', 'write', 'delete']);

        $url = $api->uploadFile(public_path($content['video']));
        $result = $api->post('/videos',
            array(
                'url'           => $url,
                'title'         => $content['title'],
                'tags'          => $content['tags'],
                'published'     => true,
                'description'   => $content['desc']
            )
        );
    }

    public function twitterPost($content){
        \Codebird\Codebird::setConsumerKey(env('TWITTER_CLIENT_ID'), env('TWITTER_CLIENT_SECRET')); // static, see README

        $twitter  = \Codebird\Codebird::getInstance();
        $token = DB::table('user_token')->where('user_id', auth()->user()->id)->where('type', 'twitter')->first();
        $twitter->setToken($token->access_token, $token->token_secret);
        $twitter->setTimeout(120000);
        $twitter->setConnectionTimeout(10000);
        $files       = public_path($content['video']);
        $file = fopen($files, 'rb');
        $size = fstat($file)['size'];

        // INIT the upload

        $media = $twitter->media_upload([
            'command' => 'INIT',
            'media_type' => 'video/mp4',
            'media_category' => 'tweet_video',
            'total_bytes' => $size,
        ]);

        $mediaId = $media->media_id_string;
        $segmentId = 0;
        while (!feof($file)) {
            $chunk = fread($file, 5 * 1024 * 1024);

            $media = $twitter->media_upload([
                'command' => 'APPEND',
                'media_id' => $mediaId,
                'segment_index' => $segmentId,
                'media' => $chunk,
            ]);

            $segmentId++;
        }

        fclose($file);
        $media = $twitter->media_upload([
            'command' => 'FINALIZE',
            'media_id' => $mediaId,
        ]);

        if(isset($media->processing_info)) {
            $info = $media->processing_info;
            if($info->state != 'succeeded') {
                $attempts = 0;
                $checkAfterSecs = $info->check_after_secs;
                $success = false;
                do {
                    $attempts++;
                    sleep($checkAfterSecs);

                    $media = $twitter->media_upload([
                        'command' => 'STATUS',
                        'media_id' => $mediaId,
                    ]);

                    $procInfo = $media->processing_info;

                    if($procInfo->state == 'succeeded' || $procInfo->state == 'failed') {
                        break;
                    }

                    $checkAfterSecs = $procInfo->check_after_secs;
                } while($attempts <= 10);
            }
        }

        $params = [
            'status'    => $content['title']." ".$content['tags'],
            'media_ids' => $mediaId,
        ];

        $tweet = $twitter->statuses_update($params);

    }

    public function youtubePost($content){
        $client = $this->getClient();
        $youtube = new \Google_Service_YouTube($client);
        // Create a snippet with title, description, tags and category ID
        // Create an asset resource and set its snippet metadata and type.
        // This example sets the video's title, description, keyword tags, and
        // video category.
        $snippet = new \Google_Service_YouTube_VideoSnippet();
        $snippet->setTitle($content['title']);
        $snippet->setDescription($content['desc']);
        $snippet->setTags(explode(',', $content['tags']));
    
        // Numeric video category. See
        // https://developers.google.com/youtube/v3/docs/videoCategories/list
        $snippet->setCategoryId("22");
    
        // Set the video's status to "public". Valid statuses are "public",
        // "private" and "unlisted".
        $status = new \Google_Service_YouTube_VideoStatus();
        $status->privacyStatus = "public";
    
        // Associate the snippet and status objects with a new video resource.
        $video = new \Google_Service_YouTube_Video();
        $video->setSnippet($snippet);
        $video->setStatus($status);
    
        // Specify the size of each chunk of data, in bytes. Set a higher value for
        // reliable connection as fewer chunks lead to faster uploads. Set a lower
        // value for better recovery on less reliable connections.
        $chunkSizeBytes = 1 * 1024 * 1024;
    
        // Setting the defer flag to true tells the client to return a request which can be called
        // with ->execute(); instead of making the API call immediately.
        $client->setDefer(true);
    
        // Create a request for the API's videos.insert method to create and upload the video.
        $insertRequest = $youtube->videos->insert("status,snippet", $video);
        $videoPath = public_path($content['video']);
        // Create a MediaFileUpload object for resumable uploads.
        $media = new \Google_Http_MediaFileUpload(
            $client,
            $insertRequest,
            'video/*',
            null,
            true,
            $chunkSizeBytes
        );
        $media->setFileSize(filesize($videoPath));
    
    
        // Read the media file and upload it chunk by chunk.
        $status = false;
        $handle = fopen($videoPath, "rb");
        while (!$status && !feof($handle)) {
            $chunk = fread($handle, $chunkSizeBytes);
            $status = $media->nextChunk($chunk);
        }
    
        fclose($handle);
    
        // If you want to make other calls after the file upload, set setDefer back to false
        $client->setDefer(false);
        
        // update youtube video id to database
        // $db->update($videoData['id'],$status['id']);
        
        // delete video file from local server
        // @unlink("videos/".$videoData['file_name']);
        
        // uploaded video data

        if(isset($content['thumbnail'])){
            // REPLACE this value with the video ID of the video being updated.
            $videoId = $status['id'];
            // REPLACE this value with the path to the image file you are uploading.
            $imagePath = public_path($content['thumbnail']);
            // Specify the size of each chunk of data, in bytes. Set a higher value for
            // reliable connection as fewer chunks lead to faster uploads. Set a lower
            // value for better recovery on less reliable connections.
            $chunkSizeBytes = 1 * 1024 * 1024;
            // Setting the defer flag to true tells the client to return a request which can be called
            // with ->execute(); instead of making the API call immediately.
            $client->setDefer(true);
            // Create a request for the API's thumbnails.set method to upload the image and associate
            // it with the appropriate video.
            $setRequest = $youtube->thumbnails->set($videoId);
            // Create a MediaFileUpload object for resumable uploads.
            $media = new \Google_Http_MediaFileUpload(
                $client,
                $setRequest,
                mime_content_type($content['thumbnail']),
                null,
                true,
                $chunkSizeBytes
            );
            $media->setFileSize(filesize($imagePath));
            // Read the media file and upload it chunk by chunk.
            $status = false;
            $handle = fopen($imagePath, "rb");
            while (!$status && !feof($handle)) {
                $chunk = fread($handle, $chunkSizeBytes);
                $status = $media->nextChunk($chunk);
            }
            fclose($handle);
            // If you want to make other calls after the file upload, set setDefer back to false
            $client->setDefer(false);
        }
    }
}
