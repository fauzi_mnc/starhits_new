<div class="col-header-content">
    <div class="row">
        <div class="d-none d-sm-block col-sm-6 col-header-logo">
            <a href="index.html">
                <amp-img src="<?php echo e(url($SiteConfig['configs']['website_logo_header'])); ?>" width="151" height="52"></amp-img>
            </a>
        </div>
        <div class="col-xs-12 col-sm-6">
            <div class="row">
                <div class="col-sm-12 d-none d-sm-block text-right">
                    <a href="#" class="header-menu-1">PORTOFOLIO</a>
                    <a href="#" class="header-menu-1">CREATE PROJECT</a>
                </div>
            </div>
            <div class="header-menu">
                <header class="d-flex d-sm-none">
                    <div role="button" tabindex="0" on="tap:sidebar-left.toggle">
                        <span class="nav-menu-bar">☰</span>
                    </div>
                    <amp-img src="<?php echo e(url($SiteConfig['configs']['website_logo_header'])); ?>" class="nav-menu-logo mx-auto" width="102" height="35">
                    </amp-img>
                </header>
                <div id="target-element-left">
                </div>
            </div>
        </div>
    </div>
</div>