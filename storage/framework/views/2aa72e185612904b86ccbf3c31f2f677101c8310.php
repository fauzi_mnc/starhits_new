<?php $__env->startSection('data'); ?>
<div class="row bg-orange pt-5">
    <div class="col-xl-10 mx-auto">
        <h5 class="text-center" style="margin-bottom: 1.5em;">LATEST MUSIC VIDEO</h5>

        <div class="latest-entertainment-video" style="position: relative">
        <amp-list id="amp-list-tab1-1" class="list" reset-on-refresh layout="fixed-height" height="156" src="//starhits.id/api/channel-latest-video/music" binding="refresh" load-more="manual" load-more-bookmark="next_page_url">
                <template type="amp-mustache">
                    <amp-img src="https://i3.ytimg.com/vi/{{attr_6}}/mqdefault.jpg" width="256" height="156" layout="responsive"></amp-img>
                    <div class="preview-popup">
                        <a class="preview-popup-link" href="<?php echo e(route('channels')); ?>/{{slug}}">watch video</a>
                    </div>
                    <div class="caption">
                        <amp-fit-text layout="fixed-height" height="50" max-font-size="20">
                            {{subtitle}}</amp-fit-text>
                        <div class="title">By {{users.name}}</div>
                    </div>
                </template>
                <div fallback>FALLBACK</div>
                <div placeholder>PLACEHOLDER</div>
                <amp-list-load-more load-more-failed>ERROR</amp-list-load-more>
                <amp-list-load-more load-more-end>END</amp-list-load-more>
                <amp-list-load-more load-more-button>
                    <div class="amp-load-more">
                        <button class="load-more-link"><label>LOAD MORE</label></button>
                    </div>
                </amp-list-load-more>                                    
            </amp-list>
        </div>                            
    </div>
</div>

<div class="row pt-5">
    <div class="col-xl-10 mx-auto">
        <h5 class="text-center" style="margin-bottom: 1.5em;">LATEST MUSIC SERIES</h5>

        <div class="latest-entertainment-video" style="position: relative">
            <amp-list id="amp-list-tab1" class="list" reset-on-refresh layout="fixed-height"
                height="564" src="//starhits.id/api/channel-latest-series/music" binding="refresh" load-more="manual" load-more-bookmark="next_page_url">
                <template type="amp-mustache">
                    <amp-img src="{{image}}" width="258" height="408" layout="responsive"></amp-img>
                    <div class="preview-popup">
                        <div class="preview-popup-description">
                            <small>{{totalVideo}} Videos</small>
                            <p class="subtitle">
                                <strong>
                                    {{title}}
                                </strong>
                            </p>
                            <amp-fit-text layout="fixed-height" height="150" max-font-size="12"
                                style="font-weight: 300">{{excerpt}}</amp-fit-text>
                        </div>
                        <a class="preview-popup-link" href="<?php echo e(route('series')); ?>/{{slug}}">watch video</a>
                    </div>
                    <div class="caption">
                        <h6>{{title}}</h6>
                        <p><small>{{totalVideo}} Videos</small></p>
                    </div>
                </template>
                <div fallback>FALLBACK</div>
                <div placeholder>PLACEHOLDER</div>
                <amp-list-load-more load-more-failed>ERROR</amp-list-load-more>
                <amp-list-load-more load-more-end>END</amp-list-load-more>
                <amp-list-load-more load-more-button>
                        <div class="amp-load-more">
                            <button class="load-more-link"><label>LOAD MORE</label></button>
                        </div>
                </amp-list-load-more>  
            </amp-list>
        </div>

        
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.channels.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>