<amp-sidebar id="sidebar-left" class="nav-sidebar" layout="nodisplay" side="left">
    <h6>NAVIGATION</h6>
    <span tabindex="0" role="button" on="tap:sidebar-left.close" class="nav-sidebar-close">✕</span>
    <nav toolbar="(min-width: 784px)" toolbar-target="target-element-left">
        <ul class="w-100 nav-justified">
            <li class="nav-item"><a href="<?php echo e(url('')); ?>" class="header-menu-2">HOME</a></li>
            <li class="nav-item"><a href="<?php echo e(url('about-us')); ?>" class="header-menu-2">ABOUT US</a></li>
            <li class="nav-item"><a href="<?php echo e(url('creators')); ?>" class="header-menu-2">CREATOR</a></li>
            <li class="nav-item"><a href="<?php echo e(url('series')); ?>" class="header-menu-2">SERIES</a></li>
            <li class="nav-item"><a href="<?php echo e(url('channels')); ?>" class="header-menu-2">CHANNEL</a></li>
            <li class="nav-item group-menu d-none d-sm-block text-nowrap" style="white-space: nowrap;padding:.5em 1em">
                <span><a href="<?php echo e(url('join-us')); ?>">JOIN US</a></span>
                <span class="delimiter">|</span>
                <span><a href="<?php echo e(url('login')); ?>">LOGIN</a></span>
            </li>
        </ul>
    </nav>
    <ul>
        <li><a href="portofolio.html" class="header-menu-2">PORTOFOLIO</a></li>
        <li><a href="#" class="header-menu-2">CREATE PROJECT</a></li>
        <li><a href="join-us.html" class="header-menu-2">JOIN US</a></li>
        <li><a href="login.html" class="header-menu-2">LOGIN</a></li>
    </ul>
</amp-sidebar>