<div class="col-xs-12 col-sm-12 col-footer">
    <div class="row">
        <div class="col-xl-5 col-sm-5 col-footer-left">
            <amp-img src="<?php echo e(asset('frontend/assets/images/logo-light.png')); ?>" width="125" height="43"></amp-img>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
        </div>
        <div class="col-xl-2 col-sm-2 text-center col-footer-center">
            <amp-img src="<?php echo e(asset('frontend/assets/icon/icon-yt-light.png')); ?>" width="73" height="43"></amp-img>
        </div>
        <div class="col-xl-5 col-sm-5 col-footer-right text-center">
            <div class="row">
                <div class="col-xs-12 col-sm-12">
                    <span>Subcribe our letter :</span>
                        <div class="input-group input-group-sm mb-3">
                        <input type="email" class="form-control">
                        <div class="input-group-append">
                            <button class="btn btn-outline-secondary" type="button">Send</button>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12">
                    <span class="icon-sosmed"><i class="fab fa-youtube"></i></span>
                    <span class="icon-sosmed"><i class="fab fa-facebook-f"></i></span>
                    <span class="icon-sosmed"><i class="fab fa-twitter"></i></span>
                    <span class="icon-sosmed"><i class="fab fa-instagram"></i></span>
                </div>
            </div>
        </div>
        <div class="col-xl-12 col-sm-12 col-footer-delimiter">
            <p class="delimeter">&nbsp</p>
        </div>

        <div class="col-xl-6 col-sm-6 col-footer-menu">
            <a href="#">COPYRIGHT &COPY; 2019 STARHITS</a>
            <a href="#">CONTACT</a>
            <a href="#">PRIVACY POLICY</a>
            <a href="#">TERMS OF SERVICE</a>
            <a href="#">FAQ</a>
        </div>

        <div class="col-xl-6 col-sm-6 col-footer-right-2">
            <div class="row">
                <div class="col-10">
                    <span>SOUTHEAST ASIA'S LARGEST AND MOST INTEGRATED MEDIA GROUP</span>
                </div>
                <div class="col-2 text-right ">
                    <amp-img src="<?php echo e(asset('frontend/assets/icon/icon-mnc-media.png')); ?>" width="80" height="41"></amp-img>
                </div>
            </div>
        </div>
    </div>
</div>