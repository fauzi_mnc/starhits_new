<!doctype html>
<html amp lang="en">

<head>
    <title>
        <?php if($__env->yieldContent('title')): ?> <?php echo $__env->yieldContent('title'); ?> - <?php endif; ?> <?php if(isset($SiteConfig['configs']['website_title'])): ?> <?php echo e($SiteConfig['configs']['website_title']); ?> <?php endif; ?>
    </title>
    <!-- Required meta tags -->
    <meta name="viewport" content="width=device-width, minimum-scale=1,initial-scale=1, shrink-to-fit=no">
    <meta charset="utf-8">

    <?php echo $__env->yieldContent('css'); ?>
    
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,900,900i&display=swap" rel="stylesheet">

    <link rel="canonical" href=".">
</head>

<body>
    <?php echo $__env->make('frontend.includes._menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <div class="container-fluid">
        <div class="row">

            <div class="col-xs-12 col-sm-12 col-header">
                <!-- BEGIN HEADER -->
                <?php echo $__env->make('frontend.includes._header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

                <!-- BEGIN CONTENT -->
                <?php echo $__env->yieldContent('content'); ?>
            </div>

            <!-- BEGIN FOOTER -->
            <?php echo $__env->make('frontend.includes._footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        </div>
    </div>

    <div class="scrolltop-wrap">
        <a href="#" role="button" aria-label="Scroll to top">
            &#x276E;
        </a>
    </div>

    <script async src="https://cdn.ampproject.org/v0.js"></script>
    <link rel="preload" as="script" href="https://cdn.ampproject.org/v0.js">

    <link rel="preconnect dns-prefetch" href="https://fonts.gstatic.com/" crossorigin>
     <!-- Import other AMP Extensions here -->
    <script async custom-element="amp-carousel" src="https://cdn.ampproject.org/v0/amp-carousel-0.1.js"></script>
    <script async custom-element="amp-selector" src="https://cdn.ampproject.org/v0/amp-selector-0.1.js"></script>
    <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>
    <script async custom-element="amp-fit-text" src="https://cdn.ampproject.org/v0/amp-fit-text-0.1.js"></script>
    <script custom-element="amp-accordion" src="https://cdn.ampproject.org/v0/amp-accordion-0.1.js" async=""></script>
    <script async custom-element="amp-list" src="https://cdn.ampproject.org/v0/amp-list-0.1.js"></script>
    <script async custom-template="amp-mustache" src="https://cdn.ampproject.org/v0/amp-mustache-0.2.js"></script>
    <script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>
    <script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>
    <script async custom-element="amp-video" src="https://cdn.ampproject.org/v0/amp-video-0.1.js"></script>
    <script async custom-element="amp-youtube" src="https://cdn.ampproject.org/v0/amp-youtube-0.1.js"></script>
    <?php echo $__env->yieldContent('script'); ?>
</body>
</html>