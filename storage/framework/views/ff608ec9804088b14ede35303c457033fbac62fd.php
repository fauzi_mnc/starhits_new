<?php $__env->startSection('css'); ?>
    <?php echo $__env->make('frontend.includes.extensions.amp-channel', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<!-- BEGIN MAIN CONTENT -->
<div class="col-xs-12 col-sm-12 col-content">
    <div style="width: 34vw;height:62vh;position: absolute;top:360px;">
        <amp-img src="<?php echo e(asset('pubs/images/bg-circle.png')); ?>" width="473" height="474" layout="responsive"
            style="transform: rotate(45deg)">
        </amp-img>
    </div>

    <div style="width: 34vw;height:62vh;position: absolute;top:132px;right:0">
        <amp-img src="<?php echo e(asset('pubs/images/bg-circle.png')); ?>" width="473" height="474" layout="responsive"
            style="transform: rotate(45deg)">
        </amp-img>
    </div>

    <div style="width: 34vw;height:62vh;position: absolute;bottom:50px;transform: rotate(-90deg)">
        <amp-img src="<?php echo e(asset('pubs/images/bg-circle.png')); ?>" width="473" height="474" layout="responsive"
            style="transform: rotate(45deg)">
        </amp-img>
    </div>

    <div class="profile">
        <div class="profile__container">
            <div class="profile__picture">
            <amp-img src="<?php echo e(asset('pubs/images/Layer-987.png')); ?>" width="363" height="567" layout="responsive">
                </amp-img>
            </div>
            <div class="profile__picture">
                <amp-img src="<?php echo e(asset('pubs/images/Layer-986.png')); ?>" width="363" height="567" layout="responsive">
                </amp-img>
            </div>
            <div class="profile__picture">
                <amp-img src="<?php echo e(asset('pubs/images/Layer-988.png')); ?>" width="363" height="567" layout="responsive">
                </amp-img>
            </div>
        </div>
        
        <div class="profile__container">
            <div class="page-title">
                <h1 class="text-outline font-style-i font-weight-900 font-size-xl">CHANNEL</h1>
                <h1 class="font-style-i font-weight-900 font-size-xl">CHANNEL</h1>
            </div>
        </div>
    </div>

    <div>
        <nav class="nav">
            <ul id="main">
                <?php $__currentLoopData = $SiteConfig['_channel']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $channels): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li><a href="<?php echo e(route('channels')); ?>/<?php echo e($channels->slug); ?>"><?php echo e($channels->title); ?></a></li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
        </nav>
    </div>

    <?php echo $__env->yieldContent('data'); ?>

</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.frontend', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>