<?php
$name = "adidasfootball";
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "https://www.instagram.com/$name/");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
$result = curl_exec($ch);
$http = curl_getinfo($ch, CURLINFO_HTTP_CODE);
curl_close($ch);
if($http=="200") {
  $doc = new \DOMDocument();
  libxml_use_internal_errors(true);
  $doc->loadHTML($result);
  $xpath = new \DOMXPath($doc);
  $js = $xpath->query('//body/script[@type="text/javascript"]')->item(0)->nodeValue;
  $start = strpos($js, '{');
  $end = strrpos($js, ';');
  $json = substr($js, $start, $end - $start);
  $data = json_decode($json, true);
  //ProfilePage
  $user_id = $data["entry_data"]["ProfilePage"][0]["graphql"]["user"]["id"];
  $username = $data["entry_data"]["ProfilePage"][0]["graphql"]["user"]["username"];
  $user_followers = $data["entry_data"]["ProfilePage"][0]["graphql"]["user"]["edge_followed_by"]["count"];
  $user_pic = $data["entry_data"]["ProfilePage"][0]["graphql"]["user"]["profile_pic_url"];
  $user_pic_hd = $data["entry_data"]["ProfilePage"][0]["graphql"]["user"]["profile_pic_url_hd"];
  $biography =$data["entry_data"]["ProfilePage"][0]["graphql"]["user"]["biography"];
  $external_url =$data["entry_data"]["ProfilePage"][0]["graphql"]["user"]["external_url"];
  $business_category =$data["entry_data"]["ProfilePage"][0]["graphql"]["user"]["business_category_name"];



  $countLike = 0;
  $countComment = 0;
  $countView = 0;
  $totalPost = 3;
  for($i=0; $i<=$totalPost; $i++){
      $liked_by = $data["entry_data"]["ProfilePage"][0]["graphql"]["user"]["edge_owner_to_timeline_media"]["edges"][$i]["node"]["edge_liked_by"]["count"];
      $viewed = $data["entry_data"]["ProfilePage"][0]["graphql"]["user"]["edge_owner_to_timeline_media"]["edges"][$i]["node"]["edge_media_preview_like"]["count"];
      $media_to_comment = $data["entry_data"]["ProfilePage"][0]["graphql"]["user"]["edge_owner_to_timeline_media"]["edges"][$i]["node"]["edge_media_to_comment"]["count"];
      $countLike= $countLike+$liked_by;
      $countView= $countView+$viewed;
      $countComment = $countComment + $media_to_comment;
  }

  
  $avgLike = (float)$countLike/$totalPost;
  $avgComment = (float)$countComment/$totalPost;
  $engagement = (($avgLike+$avgComment)/$user_followers)*100;
}
print_r($json);
// echo $countLike."<br>";
// echo $countView."<br>";
// echo 'id : '.$user_id."<br>";
// echo 'username : '.$username."<br>";
// echo 'followers : '.$user_followers."<br>";
// echo $user_pic."<br>";
// echo $user_pic_hd."<br>";
?>