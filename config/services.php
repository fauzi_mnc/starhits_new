<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'youtube' => [
        'client_id' => env('GOOGLE_CLIENT_ID'),
        'client_secret' => env('GOOGLE_CLIENT_SECRET'),
        'redirect' => env('GOOGLE_REDIRECT'),
    ], 

    'google' => [
        'client_id' => env('GOOGLE_CLIENT_ID'),
        'client_secret' => env('GOOGLE_CLIENT_SECRET'),
        'redirect' => env('GOOGLE_REDIRECT'),
    ],

    'facebook' => [
        'client_id' => '249172948960376',
        'client_secret' => 'b350b44e60ab545e3f7ed3cfbb2df144',
        'redirect' => 'https://127.0.0.1:8000/admin/platforms/facebook/callback',
    ],

    'twitter' => [
        'client_id' => env('TWITTER_CLIENT_ID', 'd4D66kyYhORuYNa74DDFM9nh4'),
        'client_secret' => env('TWITTER_CLIENT_SECRET', 'TkT5vrw5c6YYmF0k7jA3zUaHOWWTExAXVwDufJjHev7ur7JYir'),
        'redirect' => env('TWITTER_REDIRECT', 'http://localhost:8000/creator/platforms/twitter/callback'),
    ],

    'dailymotion' => [
        'client_id' => env('DAILYMOTION_CLIENT_ID', '528eb89aec77eb757fe1'),
        'client_secret' => env('DAILYMOTION_CLIENT_SECRET', 'db7b511f8acad2c6214ec37297ad747f986b9d38'),
        'redirect' => env('DAILYMOTION_REDIRECT', 'http://localhost:8000/creator/platforms/dailymotion/callback'),
    ],
];
